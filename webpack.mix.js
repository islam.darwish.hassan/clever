const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
.js('resources/js/custom.js','public/js')
.js('./resources/js/charts.js', 'public/js/charts.js')
.js('./resources/js/aos.js','public/js/aos.js')
.js('./resources/js/website.js','public/js/website2.js')
.sass('resources/sass/app.scss', 'public/css')
.sass('resources/sass/website/website.scss','public/css/website6.css');
