<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Admin
Route::group(['namespace' => 'App\Http\Controllers'], function () {
    // Route::get('import','ImportController@import_products');
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
        // Auth
        Route::group(['namespace' => 'Auth'], function () {
            Route::get('login-admin', 'LoginController@showLoginForm')->name('admin_login_form');
            Route::post('login-admin', 'ConfirmPasswordController@login')->name('admin_login');
            Route::post('logout-admin', 'LoginController@logout')->name('admin_logout');
        });
        Route::group(['middleware' => 'auth'], function () {
            // Dashboard
            Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');
            // All
            Route::resource('users', 'UserController');
            Route::post('users/create/fetch', 'UserController@fetch_category_data')->name('category.fetch');

            // Clients
            Route::resource('clients', 'ClientController');
            //Cities
            Route::resource('cities', 'CityController');
            // // Stores
            // Route::resource('stores', 'StoreController');
            // Route::resource('stores/{store}/tags', 'StoreTagController');
            // Route::put('stores/{store}/cover', 'StoreController@update_cover')->name('stores.update_cover');
            // Route::resource('stores/{store}/offers', 'StoreOfferController');
            // products
            Route::resource('products', 'ProductsController');
            Route::resource('products/{product}/specs', 'SpecsController');
            Route::get('products', 'ProductsController@all_index')->name('all_products.index');
            Route::put('products/{product}/to_review', 'ProductsController@to_review')->name('products.to_review');
            Route::put('products/{product}/to_approve', 'ProductsController@to_approve')->name('products.to_approve');
            Route::put('products/{product}/to_reject', 'ProductsController@to_reject')->name('products.to_reject');
            Route::put('products/{product}/to_offline', 'ProductsController@to_offline')->name('products.to_offline');

            // //offers
            // Route::resource('products/{product}/offers', 'ProductOffersController');
            ////
            Route::put('products/{product}/update_image', 'ProductsController@update_image')->name('products.update_image');
            Route::post('products/{product}/add_image', 'ProductsController@add_image')->name('products.add_image');
            Route::delete('products/{product}/product_image/{product_image}', 'ProductsController@remove_image')->name('products.remove_image');
            //Orders
            Route::resource('orders', 'OrderController');
            Route::put('orders/{order}/confirm', 'OrderController@confirm')->name('orders.confirm');
            Route::put('orders/{order}/shipped', 'OrderController@shipped')->name('orders.shipped');
            Route::get('orders/{order}/print', 'PrintController@prnpriview')->name('orders.print');

            Route::put('orders/{order}/in_shipping', 'OrderController@in_shipping')->name('orders.in_shipping');

            //shipments
            Route::get('shipments', 'ShipmentsController@index')->name('shipments.index');
            Route::put('shipments/{shipment}', 'ShipmentsController@confirm')->name('shipments.confirm');
            //reviews
            Route::resource('stores/{store}/reviews', 'ProductsController');
            //Banners
            Route::resource('banners', 'BannerController');
            // Categories
            Route::resource('categories', 'CategoryController');
            Route::resource('categories/{category}/subs', 'SubCategoryController');
            // Notifications
            Route::resource('notifications', 'NotificationController', ['only' => ['index', 'create', 'store']]);
            //Tags Presets
            Route::resource('tags_presets', 'TagsPresetsController');
            Route::resource('tags_presets/{tags_preset}/tags_params', 'TagsPresetsParametersController');
        });
    });


    //Website
    Route::group(['namespace' => 'Website'], function () {
        // Auth
        Route::group(['namespace' => 'Auth', 'middleware' => 'guest'], function () {
            Route::get('login', 'LoginController@showLoginForm')->name('login');
            Route::get('signup', 'LoginController@showRegisterForm')->name('signup');
            Route::post('login', 'LoginController@login')->name('confirm_login');
            Route::post('signup', 'LoginController@signup')->name('confirm_signup');

        });
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');


        //home page
        Route::get('/', function () {
            return redirect()->route('home');
        });
        Route::get('/home', 'HomeController@index')->name('home');
        //products
        Route::get('/products', 'Products\ProductsController@index')->name('products');
        Route::get('/products/{product}', 'Products\ProductsController@show')->name('product');
        //cart
        Route::get('/cart-test', function () {
            return view('website.cart');
        });
        Route::group(['namespace' => 'Cart'], function () {
            Route::get('cart', 'CartController@index')->name('cart.index');
            Route::post('cart/{product}/store', 'CartController@store')->name('cart.store');
            Route::delete('cart/empty', 'CartController@empty')->name('cart.empty');
            Route::delete('cart/{product}/remove', 'CartController@remove')->name('cart.remove');
        });

        Route::group(['middleware' => 'auth'], function () {
            Route::group(['namespace' => 'Profile'], function () {
                //addresses
                Route::get('MyAddresses/index', 'AddressesController@index')->name('web.myaddresses.index');
                Route::get('MyAddresses/create', 'AddressesController@create')->name('web.myaddresses.create');
                Route::get('MyAddresses/{address}/edit', 'AddressesController@edit')->name('web.myaddresses.edit');
                Route::put('MyAddresses/{address}/update', 'AddressesController@update')->name('web.myaddresses.update');
                Route::delete('MyAddresses/{address}/destroy', 'AddressesController@destroy')->name('web.myaddresses.destroy');
                Route::post('MyAddresses/store', 'AddressesController@store')->name('web.myaddresses.store');

                //wislists
                Route::get('MyWishlists', 'WishlistsController@index')->name('web.mywishlists.index');
                Route::get('MyWishlists/create', 'WishlistsController@create')->name('web.mywishlists.create');
                Route::post('MyWishlists/add', 'WishlistsController@add')->name('web.mywishlists.add');
                Route::put('MyWishlists/update', 'WishlistsController@update')->name('web.mywishlists.update');
                Route::delete('MyWishlists/{wishlist_product}/remove', 'WishlistsController@remove')->name('web.mywishlists.remove');
                Route::delete('MyWishlists/{wishlist}/destroy', 'WishlistsController@destroy')->name('web.mywishlists.destroy');
                Route::post('MyWishlists/store', 'WishlistsController@store')->name('web.mywishlists.store');
            });

            //orders
            Route::group(['namespace' => 'Orders'], function () {
                Route::get('MyOrders', 'OrdersController@index')->name('web.myorders.index');
                Route::get('MyOrders/{order}/order', 'OrdersController@show')->name('web.myorders.show');
                Route::post('checkout', 'OrdersController@get_checkout')->name('web.checkout');
                Route::post('checkout/place_order/{order}', 'OrdersController@place_order')->name('web.orders.place_order');
            });
        });
    });
});
