// Imports
import $ from "jquery";
import './bootstrap';
// Stylesheets
import '../sass/website/website.scss';


$(document).mouseup(function (e) {
	var container = $("#myDrop");

	// If the target of the click isn't the container
	if (!container.is(e.target) && container.has(e.target).length === 0) {
		container = closeDrop();
		closeDrop2();
	}
});

function closeDrop() {
	document.getElementById("myDrop").style.height = "0";
	document.getElementById("myDrop").style.opacity = "0";
}
function closeDrop2() {
	document.getElementById("myDrop2").style.height = "0";
	document.getElementById("myDrop2").style.opacity = "0";
}
jQuery(document).ready(function () {

	/*
			Carousel
	*/
	$('#carousel-example').on('slide.bs.carousel', function (e) {

		/*
				CC 2.0 License Iatek LLC 2018
				Attribution required
		*/
		var $e = $(e.relatedTarget);
		var idx = $e.index();
		var itemsPerSlide = 5;
		var totalItems = $('.cr-it-1').length;

		if (idx >= totalItems - (itemsPerSlide - 1)) {
			var it = itemsPerSlide - (totalItems - idx);
			for (var i = 0; i < it; i++) {
				// append slides to end
				if (e.direction == "left") {
					$('.cr-it-1').eq(i).appendTo('.cr-in-1');
				}
				else {
					$('.cr-it-1').eq(0).appendTo('.cr-in-1');
				}
			}
		}
	});

	$('#carousel-example1').on('slide.bs.carousel', function (e) {

		/*
				CC 2.0 License Iatek LLC 2018
				Attribution required
		*/
		var $e = $(e.relatedTarget);
		var idx = $e.index();
		var itemsPerSlide = 5;
		var totalItems = $('.cr-it-2').length;

		if (idx >= totalItems - (itemsPerSlide - 1)) {
			var it = itemsPerSlide - (totalItems - idx);
			for (var i = 0; i < it; i++) {
				// append slides to end
				if (e.direction == "left") {
					$('.cr-it-2').eq(i).appendTo('.cr-in-2');
				}
				else {
					$('.cr-it-2').eq(0).appendTo('.cr-in-2');
				}
			}
		}
	});

	$('#carousel-example2').on('slide.bs.carousel', function (e) {

		/*
				CC 2.0 License Iatek LLC 2018
				Attribution required
		*/
		var $e = $(e.relatedTarget);
		var idx = $e.index();
		var itemsPerSlide = 5;
		var totalItems = $('.cr-it-3').length;

		if (idx >= totalItems - (itemsPerSlide - 1)) {
			var it = itemsPerSlide - (totalItems - idx);
			for (var i = 0; i < it; i++) {
				// append slides to end
				if (e.direction == "left") {
					$('.cr-it-3').eq(i).appendTo('.cr-in-3');
				}
				else {
					$('.cr-it-3').eq(0).appendTo('.cr-in-3');
				}
			}
		}
	});
});

// const togglePassword = document.querySelector('#togglePassword');
// const password = document.querySelector('#password');

// togglePassword.addEventListener('click', function (e) {
// 	// toggle the type attribute
// 	const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
// 	password.setAttribute('type', type);
// 	// toggle the eye slash icon
// 	this.classList.toggle('fa-eye-slash');
// });
