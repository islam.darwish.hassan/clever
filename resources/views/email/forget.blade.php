<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    We received a request to reset your Clever password.
     <br>
     <br>
    <p>Enter the following  reset code :</p>
    <H3 >{{ $verification_code }} </H3>
    <br/>
</div>

</body>
</html>
