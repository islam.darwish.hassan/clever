@extends('layouts.resource.edit')


@section('form')
<form method="POST" action="{{ route($name. '.update', $city->id) }}" enctype="multipart/form-data">

                   @csrf


        @method('PUT')

        <div class="d-flex bd-highlight">
            <div class="form1 p-2 flex-fill bd-highlight">
                <div class="row">
                    <div class="col form-group">
                        <label>English الاسم <b style="color:red"> * </b></label>
                        <input type="text" name="en_name" class="form-control" placeholder="En الاسم" required
                            value="{{ $city->en_name ?$city->en_name :old('en_name') }}">
                    </div>
                    <div class="col form-group">
                        <label>Arabic الاسم <b style="color:red"> * </b></label>
                        <input type="text" name="ar_name" class="form-control" placeholder="Ar الاسم" required
                            value="{{ $city->ar_name ?$city->ar_name :old('ar_name') }}">
                    </div>
                    <div class="col form-group">
                        <label>Delivery Fees <b style="color:red"> * </b></label>
                        <input type="number" name="fees" class="form-control" placeholder="Fees" required
                            value="{{ $city->delivery_fees ?$city->delivery_fees :old('fees') }}">
                    </div>

                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-block btn-success bg-brandgreen ">تحديث</button>

    </form>
@endsection
