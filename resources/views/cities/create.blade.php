@extends('layouts.resource.create')
@section('title', 'اضافة مدينة جديدة')


@section('form')

    <form class="p-2 flex-grow-1" method="post" action="{{ route($name. '.store') }}" enctype="multipart/form-data">
                   @csrf

        <div class="d-flex bd-highlight">
            <div class="form1 p-2 flex-fill bd-highlight">
                <div class="row">
                    <div class="col form-group">
                        <label>English الاسم <b style="color:red"> * </b></label>
                        <input type="text" name="en_name" class="form-control" placeholder="En الاسم" required
                            value="{{ old('en_name') }}">
                    </div>
                    <div class="col form-group">
                        <label>Arabic الاسم <b style="color:red"> * </b></label>
                        <input type="text" name="ar_name" class="form-control" placeholder="Ar الاسم" required
                            value="{{ old('ar_name') }}">
                    </div>
                    <div class="col form-group">
                        <label>Delivery Fees <b style="color:red"> * </b></label>
                        <input type="number" name="fees" class="form-control" placeholder="Fees" required
                            value="{{ old('fees') }}">
                    </div>

                </div>
            </div>
        </div>
        <button type="submit" class="btn1 btn btn-block btn-success bg-brandgreen ">اضافة مدينة جديدة</button>
    </form>
@endsection
