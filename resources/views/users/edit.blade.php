@extends('layouts.resource.edit')

@section('form')
<form method="POST" action="{{ route($name. '.update', $user->id) }}">

        @csrf

        @method('PUT')

        <div class="d-flex bd-highlight">
            <div class="p-2 flex-fill bd-highlight">
                <div class="row">
                    <div class="col form-group">
                        <label>الصلاحية</label>
                        <select class="custom-select" name="role">
                            <option selected>-- Choose --</option>
                            <option value="1">المشرف</option>
                            <option value="2">المستخدم</option>
                            <option value="3">Business</option>
                            <option value="4">Casheer</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
            
        <button type="submit" class="btn btn-block btn-success bg-brandgreen ">تحديث</button>

    </form>
@endsection