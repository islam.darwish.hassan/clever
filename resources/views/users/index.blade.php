@extends('layouts.resource.index')
@section('title', 'ادارة المستخدمين')
@section('create-btn')
<a href="{{ route($name . '.create') }}" class="  p-2 "><i class="fas fa-plus"></i></a>
@endsection
@section('search-filter')

    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="role">
                    <option value="none" @if(request()->query('role') == 'none') selected @endif>- صنف بالصلاحية -</option>
                    <option value="1" @if(request()->query('role') ==  App\Models\User::ADMIN) selected @endif>مشرف</option>
                    <option value="2" @if(request()->query('role') ==  App\Models\User::CLIENT) selected @endif>مستخدم</option>
                </select>
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block btn-primary mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>
    </form>

@endsection

@section('table-header')

    <th scope="col" style="width: 10%" >@sortablelink('id', '#')</th>
    <th scope="col"  >@sortablelink('email','البريد الالكترونى')</th>
    <th scope="col" >الاسم</th>
    <th scope="col"  >@sortablelink('role','الصلاحية')</th>
    <th scope="col">@sortablelink('created_at','وقت الاضافة')</th>

    <th  scope="col" style="width: 10%">العمليات</th>
@endsection



@section('table-body')

    @foreach ($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->email }}</td>
            <td>
                @if($user->role!= App\Models\User::STORE )
                    @if(null !==(App\Models\Client::where('user_id',$user->id)->pluck('name')->first()))
                    {{ App\Models\Client::where('user_id',$user->id)->pluck('name')->first() }}
                    @else
                    Not Available
                    @endif
                @elseif($user->role == App\Models\User::STORE)
                    @if(null !==(App\Models\المحل::where('user_id',$user->id)->pluck('name')->first()))
                    {{ App\Models\المحل::where('user_id',$user->id)->pluck('name')->first() }}
                    @else
                    Not Available
                    @endif
                @endif

            </td>

            <td>
                @if($user->role ==  App\Models\User::ADMIN)
                    المشرف
                @elseif($user->role ==  App\Models\User::CLIENT)
                    User
                @elseif($user->role ==  App\Models\User::STORE)
                    Businness
                @elseif($user->role == '4')
                    Cashier
                @endif
            </td>
            <td>
                @php
                Carbon\Carbon::setlocale("ar");   
                echo Carbon\Carbon::parse($user->created_at)->diffForHumans()
                @endphp
            </td>
            <td class="d-flex justify-content-center">
                @if($user->role== App\Models\User::ADMIN)
                <a href="{{ route($name.'.show', $user->id) }}"
                    class="btn   "><i class="fas fa-eye"></i></a>
                @elseif($user->role ==  App\Models\User::CLIENT)
                <a href="{{ route('clients'.'.show', $user->client->id) }}"
                    class="btn   "><i class="fas fa-eye"></i></a>
                @elseif($user->role ==  App\Models\User::STORE && isset( $user->store))
                <a href="{{ route('stores'.'.show', $user->store->id) }}"
                    class="btn   "><i class="fas fa-eye"></i></a>
                @elseif($user->role == '4')
                <a href="{{ route('casheers'.'.show' ,  [$user->casheer->store->id ,$user->casheer->id]) }}"
                        class="btn   "><i class="fas fa-eye"></i></a>
                @endif
                @if($user->role== App\Models\User::ADMIN)
                <a href="{{ route($name.'.edit', $user->id) }}"
                    class="btn  "><i class="fas fa-edit"></i></a>
                    @elseif($user->role ==  App\Models\User::CLIENT)
                <a href="{{ route($name.'.edit', $user->client->id) }}"
                    class="btn  "><i class="fas fa-edit"></i></a>
                    @elseif($user->role ==  App\Models\User::STORE && isset( $user->store))
                    <a href="{{ route('stores'.'.edit', $user->store->id) }}"
                    class="btn  "><i class="fas fa-edit"></i></a>
                    @elseif($user->role == '4')
                <a href="{{ route('casheers'.'.edit' ,  [$user->casheer->store->id ,$user->casheer->id]) }}"
                    class="btn  "><i class="fas fa-edit"></i></a>
                @endif
                <form method="POST" action="{{ route($name.'.destroy', $user->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit"
                     onclick="return confirm('Are you sure you want to delete this user?')"
                     data-toggle="modal" data-target="#exampleModal"
                     class="btn    bg-transparent"><i class="fas fa-trash-alt "></i></button>
                </form>
            </td>
        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
@endsection

@endsection
