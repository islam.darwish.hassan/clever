@extends('layouts.resource.edit')
@section('title', 'Show ' . ucfirst(\Illuminate\Support\Str::singular($name)))


@section('form')
    
<form class="form" action="{{ route($name . '.show', $user->id) }}" method="get">

    @csrf

    @method('put')

    <div class="row">
        
        <div class=" col form-group">
            <label>البريد الإلكترونى</label>
            <input disabled type="email" name="email-view" class="form-control" placeholder="البريد الإلكترونى Address"
                value="{{  $user->email }}">
        </div>
        <div class="col form-group">
            <label>الصلاحية</label>
             <select  disabled class="form-control m-input" name="role" id="role" >
                <option value="">Select role</option>
                <option value="1" {{  $user->role=="1" ? 'selected' : '' }}>المشرف</option>
                <option value="2" {{  $user->role=="2" ? 'selected' : '' }}>المحل</option>
                <option value="3" {{  $user->role=="3" ? 'selected' : '' }}>المستخدم</option>
                <option value="4" {{  $user->role=="4" ? 'selected' : '' }}>Casheer</option>
            </select>
        </div>
    </div>

</form>


@endsection