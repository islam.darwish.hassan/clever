@extends('layouts.resource.create')
@section('title', 'اضافة مستخدم جديد للموقع')

@section('form')

<form class="p-2 " method="post" action="{{ route($name. '.store') }}" enctype="multipart/form-data">
    @csrf
    <div class="d-flex flex-fill">
            <div class="row flex-fill">
                <div class="col form-group">
                    <label class="form-label">الصلاحية</label>
                    <select class="sel custom-select " name="role" id="role">
                        <option selected>-- Choose --</option>
                        <option value="1">المشرف</option>
                        <option value="2" >المستخدم</option>
                    </select>
                </div>
                <div class="col form-group">
                    <label class="form-label">البريد الالكترونى</label>
                    <input type="email" name="email" class="form-control" placeholder="E-mail" required
                    value="{{request()->query('email')}}" >
                </div>
                <div class=" col form-group">
                    <label class="form-label">كلمة المرور</label>
                    <input type="password" name="password" class="form-control" placeholder="Password" required
                        value="{{request()->query('password')}}" becrypt>
                </div>

        </div>
    </div>
        <!-- BEGIN : Additional  Section for User -->
        <div >
            <div class="form2">
                <div class=" mb-3">
                    <h3 class="section-header ">Additional information: </h3>
                    <p class="section-subheader">
                        This is required additional information for creating new user
                        <code>User</code>
                    </p>
                    </div>
                            <div class="m-section__content">
                        <div class="row">
                            <div class="form-group col">
                                <label class="form-label">الاسم الاول :  </label>
                                <input type="first_name" name="first_name" class="form-control m-input"
                                id="first_name" aria-describedby="nameHelp" placeholder="ادخل الاسم الاول" value="{{ old('first_name') }}">
                            </div>

                            <div class="form-group col">
                                <label class="form-label">الاسم الاخير : </label>
                                <input type="last_name" name="last_name" class="form-control m-input"
                                 id="last_name" aria-describedby="nameHelp" placeholder="ادخل الاسم الاخير" value="{{ old('last_name') }}">
                            </div>
                            <div class="form-group col ">
                                <label class="form-label">النوع : </label>
                                <div>
                                    <select class="form-control m-input " name="gender" ">
                                                            <option value="">اختيار النوع </option>
                                                            <option value="m" @if(request()->query('gender') == 'm') selected @endif>ذكر</option>
                                        <option value="f" @if(request()->query('gender') == 'f') selected @endif>أنثى</option>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group col">
                                <label class="form-label">تليفون : </label>
                                <input type="phone" name="phone" class="form-control m-input"
                                 id="phone" aria-describedby="nameHelp" placeholder=" ادخال رقم التليفون" value="{{ old('phone') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col ">
                                <label class="form-label">الصورة : </label>
                                <div>
                                <div class="custom-file">
                                    <label class="custom-file-label" id="custom-file-label2" for="validatedCustomFile">اختر الملف ...</label>
                                    <input type="file" name="avatar" id="custom-file-input-image2" class="custom-file-input">
                                </div>

                                </div>
                            </div>
                            <div class="col form-group">
                                <label class="form-label">الحالة</label>
                                <select name="status" class="custom-select">
                                    <option value="0">اختر ...</option>
                                    <option value="1">موثوق</option>
                                    <option value="2">غير موثوق</option>
                                    <option value="3">موقوف</option>
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
                    <!-- END : of Additional Section for User -->
    <button type="submit" class=" btn btn-primary  mb-5 ">Submit</button>
        </div>
    </div>

</form>



@endsection
@section('extra-scripts')
<script>
    $('#custom-file-input-image').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
    $('#custom-file-input-image2').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image2").files[0].name;
        //replace the "Choose a file" label
        console.log(fileName)
        $('#custom-file-label2').html(fileName);

    })


</script>
<script>
    $(".form1").hide();
    $(".sel").on('change', function (e) {
        if ($(this).val() == "1") {
            $(".form1").show();
            window.location="create?type=1"
        } else {

            $(".form1").hide();
        }
    });
    // client form
    $(".form2").hide();
    $(".sel").on('change', function (e) {
        if ($(this).val() == "2") {
            $(".form2").show();
            window.location="create?type=2"
        } else {

            $(".form2").hide();
        }
    });
    $(".form3").hide();
    $(".sel").on('change', function (e) {
        if ($(this).val() == "3") {
            $(".form3").show();
            window.location="create?type=3"
        } else {
            $(".form3").hide();
        }
    });
    $(".form4").hide();
    $(".sel").on('change', function (e) {
        if ($(this).val() == "4") {
            $(".form4").show();
            window.location="create?type=4"
        } else {

            $(".form4").hide();
        }
    });
</script>
<script>
    $(document).ready(function () {
        //to get params from url
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        var type = $.urlParam('type');
        if (type) {
            console.log(type);
            $(".sel").val(type);
            $(".form" + type).show();
        } else {
            console.log('no type')
        }
    });

</script>
<script>
    $('.sub').prop('disabled', false);
    $('.cat').change(function () {
       // $('.sub').prop('disabled', false);
        if ($(this).val() != '') {
            var select = $(this).attr("id");
            var value = $(this).val();
            var dependent = $(this).data('dependent');
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('category.fetch') }}",
                method: "POST",
                data: {
                    select: select,
                    value: value,
                    dependent: dependent,
                    _token: _token
                },
                success: function (result) {
                    $('#' + dependent).html(result);
                }
            })
        }
    });

    $('#category').change(function () {
        $('#sub_category').val('');
    });
</script>

@endsection
