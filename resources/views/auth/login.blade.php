<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
    <title>Clever Login Page</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div class="container-fluid h-100  bg-dark text-white ">
		<div class="col-md-12">
				@if (session('message'))
					<div class="alert alert-success mt-2" role="alert">
						<b>{{ session('message') }}</b>
					</div>
				@endif
				@if (session('error'))
					<div class="alert alert-danger mt-2" role="alert" dir="rtl">
						<b>{{ session('error') }}</b>
					</div>
				@endif
				@if ($errors->any())
					<div class="alert alert-danger mt-2">
						<p><b>Please fix these errors.</b></p>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
		</div>
		<div class="d-flex justify-content-center  ">
			<div class="card mt-5">
				<div class="card-body">
                    <form method="POST" action="{{ route('admin_login') }}">
                        @csrf
						<div class="input-group mb-3">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-user"></i></span>
							</div>
                            <input id="email" type="email" name="email" class="form-control @error('email') is-invalid @enderror"value="{{ old('email') }}" required autocomplete="email" placeholder="البريد الالكترونى" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>
						<div class="input-group mb-2">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-key"></i></span>
							</div>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="كلمة المرور">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>
						<div class="d-flex justify-content-center my-3 ">
				 	        <button type="submit" name="button" class="btn btn-primary">تسجيل الدخول </button>
				        </div>
					</form>
                </div>
                <div>
				</div>

			</div>
        </div>
	</div>
</body>
</html>
