@extends('layouts.resource.index')
@section('title', "ادارة الشحنات")

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="row d-flex justify-content-between">
            <div class="p-2 col-lg-6 col-md-12 ">
             @include('layouts.includes.forms.form_text',['field' => ['name' => 'code', 'placeholder' => 'Search By كود..']])
            </div>
            <div class="p-2 col-lg-2 col-md-4">
                <div class="col form-group">
                    <select name="status" class="custom-select">
                        <option value="0"  @if(request()->query('status') == '0') selected @endif>الحالة...</option>
                        <option value="1"  @if(request()->query('status') == '1') selected @endif>مطلوب</option>
                        <option value="2"  @if(request()->query('status') == '2') selected @endif>تحت الفحص</option>
                        <option value="3" @if(request()->query('status') == '3') selected @endif>فى الشحن</option>
                        <option value="3" @if(request()->query('status') == '4') selected @endif>تم شحنها</option>

                    </select>
                </div>

             </div>
             <div class="p-2 col-lg-2 col-md-4">
             <div class="col form-group">
                <select name="delivey_city" class="custom-select">
                    <option value="0"  @if(request()->query('delivey_city') == '0') selected @endif>مدينة التوصيل...</option>
                    @foreach(App\Models\City::all() as $city)
                   <option value="{{$city->id}}"  @if(request()->query('delivey_city') ==$city->id) selected @endif>{{$city->en_name}}</option>
                    @endforeach
                </select>
            </div>
             </div>
            <div class="p-2 col-lg-2 col-md-2  ">
                <button type="submit" class="btn btn-block  mb-2 ">تصفية</button>
            <div class="float-right  ">
            <a href="{{ route($name. '.index') }}" class="small-header-bold "><b class="text-muted small"> تفريغ التصفيات  X</b></a>
            </div>
           </div>

        </div>
    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-body')

@section('table-header')

        <th scope="col">@sortablelink('id', '#')</th>
        <th scope="col">@sortablelink('hash_code', 'كود')</th>
        <th scope="col">@sortablelink('ordered_products_count', 'Products')</th>
        <th scope="col">مدينة التوصيل</th>
        <th scope="col">@sortablelink('status', 'الحالة')</th>
        <th scope="col">@sortablelink('created_at', 'وقت الاضافة') </th>
        <th scope="col">العمليات </th>
@endsection
@section('table-body')
@foreach($shipments as $shipment)
<!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->


<td>{{ $shipment->id }}</td>
<td>{{ $shipment->hash_code }}</td>
<td>{{ $shipment->ordered_products->count() }}</td>
<td>{{ $shipment->order->address->city->en_name }}</td>
<td>
    @if ($shipment->status == 1)
    <span class="badge bg-info text-white">مطلوب</span>
    @elseif ($shipment->status == 2)
    <span class="badge badge-primary text-white">تحت الفحص</span>
    @elseif ($shipment->status == 3)
    <span class="badge badge-warning text-black">In فى الشحن</span>
    @elseif ($shipment->status == 4)
    <span class="badge badge-success text-white">تم شحنها</span>
    @elseif ($shipment->status == 5)
    <span class="badge bg-dark text-white text-black">درافت</span>
    @else    <span class="badge bg-dark text-white text-black">غير محدد</span>
    @endif
</td>
<td>{{Carbon\Carbon::parse($shipment->created_at)->diffForHumans()}}</td>

<!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->

<td class="d-flex justify-content-center"><a href="{{ route('orders.show',[$shipment->order->id]) }}"
    class="btn clr-black  "><i class="fas fa-eye"></i></a>
</tr>
@endforeach
@endsection
    {{ $data->appends(request()->query())->links() }}
@section('table-footer')
<p class="table-footer">النتائج :  {{$data->total()}}</p>

@endsection
@endsection
