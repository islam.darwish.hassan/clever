
@extends('layouts.website')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('images/bg_6.jpg');">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9  text-center">
          <h1 class="mb-0 bread">My Profile</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Add New Address</a></span> </p>
        </div>
      </div>
    </div>

  @if (session('message'))
  <div class="alert alert-success mt-2 " role="alert">
      <b>{{ session('message') }}</b>
  </div>
@endif
@if (session('error'))
  <div class="alert alert-danger mt-2" role="alert" >
      <b>{{ session('error') }}</b>
  </div>
@endif
  </div>


      <section class=" ">
      <div class="container">
        <form class="form-group " method="POST" action={{route('web.mywishlists.store')}}>
           @csrf

            <div class="container ">
            <div class="form-group  ">
                <label for="AddressTextArea">Wishlist الاسم : <b style="color:red"> * </b></label>
                <input type="text" name="name" class="form-control m-input"
                id="name" aria-describedby="nameHelp" placeholder="Enter Wishlist الاسم" value="{{ old('name') }}">
          </div>
          <div class=" form-group col">
            <button type="submit" class="btn btn-primary mb-2 d-flex float-right">Add New Wishlist </button>
        </div>
            </div>




      </form>

      </div>

  </section>
@endsection
