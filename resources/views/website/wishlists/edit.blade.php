
@extends('layouts.website')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('images/bg_6.jpg');">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9  text-center">
          <h1 class="mb-0 bread">My Profile</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">تعديل My Address</a></span> </p>
        </div>
      </div>
      @if (session('message'))
      <div class="alert alert-success mt-2 " role="alert">
          <b>{{ session('message') }}</b>
      </div>
    @endif
    @if (session('error'))
      <div class="alert alert-danger mt-2" role="alert" >
          <b>{{ session('error') }}</b>
      </div>
    @endif

    </div>
  </div>

      <section class=" ">
      <div class="container">
        <form class="form-group " method="POST" action={{route('web.myaddresses.update',$address->id)}}>
           @csrf
            {{ method_field('PUT') }}
            <div class="row">
                <div class="form-group col ">
                    <label class="form-label">مدينة التوصيل : <b style="color:red"> * </b> </label>
                    <div>
                        <select class="form-control m-input " name="city_id">
                       <option value="">Select a City </option>
                        @foreach(App\Models\City::all() as $city)
                        <option value="{{$city->id}}" @if($address->city->id) selected @endif>{{$city->en_name}}</option>
                        @endforeach
                        </select>

                    </div>
                </div>
            <div class="form-group col-md-8">
                <label for="AddressTextArea">Delivery Address : <b style="color:red"> * </b></label>
                <textarea class="form-control" id="AddressTextArea" name="address" rows="2">{{$address->address}}</textarea>
              </div>

            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label class="form-label">Apartment No : </label>
                    <input type="text" name="apartment" class="form-control m-input"
                    id="apartment" aria-describedby="nameHelp" placeholder="Enter apartment number" value="{{ $address->apartment }}">
                </div>
                    <div class="form-group col-md-3">
                        <label class="form-label">Building No : </label>
                        <input type="number" name="building" class="form-control m-input"
                        id="building" aria-describedby="nameHelp" placeholder="Enter building number" value="{{$address->building }}">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="form-label">Floor : </label>
                        <input type="number" name="floor" class="form-control m-input"
                        id="floor" aria-describedby="nameHelp" placeholder="Enter floor number" value="{{$address->floor }}">
                    </div>
                    <div class="form-group col-md-3">
                        <label class="form-label">Landline : </label>
                        <input type="phone" name="landline" class="form-control m-input"
                         id="landline" aria-describedby="nameHelp" placeholder="Enter Address landline" value="{{$address->landline }}">
                    </div>

            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label class="form-label">Street : </label>
                    <input type="text" name="street" class="form-control m-input"
                    id="street" aria-describedby="nameHelp" placeholder="Enter Street الاسم" value="{{$address->street}}">
                </div>
                <div class="form-group col-md-6">
                    <label for="AddressTextArea">Landmarks:</label>
                <textarea class="form-control" id="AddressTextArea" name="landmark" rows="1">{{$address->landmark}}</textarea>
                </div>
            </div>


        <div class="form-group ">
            <label for="AddressTextArea">فى الشحن Notes</label>
            <textarea class="form-control" id="AddressTextArea" name="shipping_note" rows="3">{{$address->shipping_note}}</textarea>
          </div>

              <button type="submit" class="btn btn-primary mb-2 d-flex float-right">تحديث Address </button>

      </form>
      </div>

  </section>
@endsection
