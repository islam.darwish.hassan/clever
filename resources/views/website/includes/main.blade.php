<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clever</title>
    <link rel="icon" type="image/png" href="{{ asset('images/logo.png') }}">
    <link rel="stylesheet" href="{{ asset('css/website.css') }}">
    <link rel="stylesheet" href="https://use.typekit.net/oov2wcw.css">
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/website2.js') }}"></script>
</head>

<body>

    <div class="clever-navbar">
        <nav class="navbar">
            {{-- @yield('section_a') --}}
            <div class="section_a">
                {{-- <a id="theDrop2" onclick="openDrop2()" class="under">kids</a> --}}
                <a id="theDrop" onclick="openDrop()" class="under">men</a>
              </div>


            <div class="brand"><a href="{{ route('home') }}"><img
                        src="{{ asset('images/logo.png') }}" /></a></div>
            {{-- @yield('section_b') --}}
            <div class="section_b ">
                @guest
                <a href="{{ route('login') }}">login</a>
                @endguest
                <a id="theSearch" onclick="openSearch()"><i class="fas fa-search pr-2"></i>search</a>
                <a href="{{ route('cart.index') }}"><i class="fas fa-shopping-cart pr-2"></i>cart</a>
                @auth
                  <a class="btn btn-primary dropdown-toggle "  id="dropdown-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      @if(Auth::user()->role==2)
                      <span id="text-btn">{{App\Models\Client::where('user_id',Auth::user()->id)->first()->name}}</span>
                      @else
                      <span id="text-btn">{{Auth::user()->email}}</span>
                    @endif
                  </a>
                  <div class="dropdown-menu bg-dark text-white dropdown-menu-right" aria-labelledby="dropdown-menu">
                    <li><a class="dropdown-item">  My Addresses</a><li>
                      <li><a class="dropdown-item">  My Wishlists</a><li>
                      <li>
                          <a class="dropdown-item"
                          onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                           {{ __('Logout') }}
                       </a>
                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                        </form>
                    </div>
                @endauth

              </div>

        </nav>

        {{-- @yield('drop') --}}
        <div id="myDrop" class="drop">
            <div class="inner">
              <h3>new in</h3>

              <div class="column">
                <h3>collection</h3>
                <div class="new">
                  <a href="{{ route('products') }}">Comfy</a>
                  <span class="badge">new</span>
                </div>
                <a href="#">Coats</a>
                <a href="#">Jackets</a>
                <a href="#">Leather Jackets</a>
                <a href="#">Blazers</a>
                <a href="#">Sweaters</a>
                <a href="#">Sweatshirts</a>
                <a href="#">Cardigans</a>
                <a href="#">Trousers</a>
                <a href="#">Jeans</a>
                <a href="#">Shirts</a>
                <a href="#">Polos</a>
                <a href="#">T-shirts</a>
                <a href="#">Denim</a>
                <a href="#">100% Cashmere</a>
                <a href="#">Suits</a>
                <a href="#">Formal shirts</a>
                <a href="#">Accessories</a>
                <a href="#">Perfumes</a>
                <a href="#">Homewear</a>
              </div>

              <div class="column">
                <h3>shoes</h3>
                <a href="#">View all</a>
                <a href="#">Sneakers</a>
                <a href="#">Loafers</a>
                <a href="#">Smart shoes</a>

                <div class="column-in">
                  <h3>join life</h3>
                  <a href="#">Collection</a>
                  <a href="#">Commitments</a>
                </div>
              </div>

              <div class="column">
                <h3>editorial</h3>
                <a href="#">At the break of dawn</a>
                <a href="#">Interlude</a>
                <a href="#">Re-shape the Present</a>

                <div class="column-in">
                  <h3>paper</h3>
                  <a href="#">Limited edition. Special Issue</a>
                  <a href="#">View all</a>
                </div>
              </div>
            </div>
          </div>
          <div id="myDrop2" class="drop">
            <div class="inner">
              <h3>new in2</h3>

              <div class="column">
                <h3>collection</h3>
                <div class="new">
                  <a href="{{ route('products') }}">Comfy</a>
                  <span class="badge">new</span>
                </div>
                <a href="#">Coats</a>
                <a href="#">Jackets</a>
                <a href="#">Leather Jackets</a>
                <a href="#">Blazers</a>
                <a href="#">Sweaters</a>
                <a href="#">Sweatshirts</a>
                <a href="#">Cardigans</a>
                <a href="#">Trousers</a>
                <a href="#">Jeans</a>
                <a href="#">Shirts</a>
                <a href="#">Polos</a>
                <a href="#">T-shirts</a>
                <a href="#">Denim</a>
                <a href="#">100% Cashmere</a>
                <a href="#">Suits</a>
                <a href="#">Formal shirts</a>
                <a href="#">Accessories</a>
                <a href="#">Perfumes</a>
                <a href="#">Homewear</a>
              </div>

              <div class="column">
                <h3>shoes</h3>
                <a href="#">View all</a>
                <a href="#">Sneakers</a>
                <a href="#">Loafers</a>
                <a href="#">Smart shoes</a>

                <div class="column-in">
                  <h3>join life</h3>
                  <a href="#">Collection</a>
                  <a href="#">Commitments</a>
                </div>
              </div>

              <div class="column">
                <h3>editorial</h3>
                <a href="#">At the break of dawn</a>
                <a href="#">Interlude</a>
                <a href="#">Re-shape the Present</a>

                <div class="column-in">
                  <h3>paper</h3>
                  <a href="#">Limited edition. Special Issue</a>
                  <a href="#">View all</a>
                </div>
              </div>
            </div>
          </div>

          <script>
            function openDrop() {
              closeDrop();
              closeDrop2();
              document.getElementById("myDrop").style.height = "60%";
              document.getElementById("myDrop").style.opacity = "1";
            }

            function closeDrop() {
              document.getElementById("myDrop").style.height = "0";
              document.getElementById("myDrop").style.opacity = "0";
            }
            function openDrop2() {
              closeDrop();
              closeDrop2();

              document.getElementById("myDrop2").style.height = "60%";
              document.getElementById("myDrop2").style.opacity = "1";
            }

            function closeDrop2() {
              document.getElementById("myDrop2").style.height = "0";
              document.getElementById("myDrop2").style.opacity = "0";
            }
          </script>

        {{--@yield('search')--}}
        <div id="mySearch" class="search">
            <a href="javascript:void(0)" class="closebtn" onclick="closeSearch()">&times;</a>
            <div class="search-bar">
              <i class="fas fa-search"></i>
              <input placeholder="What you are searching for ...">
            </div>

            <!-- Top content -->
            <div class="top-content">
              <div id="carousel-example" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner row w-100 mx-auto cr-in-1" role="listbox">
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-1 active">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img1">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-1">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img2">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-1">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img3">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-1">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img4">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-1">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img5">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-1">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img6">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>


            <!-- Top searches -->
            <div class="top-content top-search">
              <h3>top searches</h3>
              <div id="carousel-example1" class="carousel slide" data-ride="carousel1">
                <div class="carousel-inner row w-100 mx-auto cr-in-2" role="listbox1">
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-2 active">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img1">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-2">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img2">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-2">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img3">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-2">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img4">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-2">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img5">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                  <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-2">
                    <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img6">
                    <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
                    <label class="price mx-auto d-block" for="">400 EGP</label>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carousel-example1" role="button1" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-example1" role="button1" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>

          </div>

          <script>

            function openSearch() {
              document.getElementById("theSearch").onclick = function() { closeSearch(); }
              document.getElementById("mySearch").style.height = "100%";
              document.getElementById("mySearch").style.opacity = "1";
            }

            function closeSearch() {
              document.getElementById("theSearch").onclick = function() { openSearch(); }
              document.getElementById("mySearch").style.height = "0";
              document.getElementById("mySearch").style.opacity = "0";
            }

          </script>    </div>

    @yield('filter')
    @yield('content')

</body>
<script src="{{ asset('js/website2.js') }}"></script>

</html>
