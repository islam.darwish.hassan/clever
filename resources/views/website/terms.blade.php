<!DOCTYPE html>
<html lang="en">

<head>
    <title>Terms & Conditions</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- MainStyle -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- CustomStyle -->
    <link rel="stylesheet" href="{{ asset('css/main2.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free-5.11.2-web/css/all.min.css') }}">
    <!-- BootstrapScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</head>

    
<body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light ">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
            aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand  " href="{{ route('home') }}"><img src="{{ asset('assets/imgs/logo.png') }}" width="auto" height="30px"></a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
                <li class="nav-item ">
                    <a class="nav-link small active" href="{{ route('terms') }}"><b>Terms & Conditions</b> <span
                            class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link small" href="{{ route('policy') }}"><b>Privacy Policy </b><span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="policy">
        <h1 class="text-center" style="text-decoration: underline; padding-bottom: 10px;">Terms & conditions</h1>
        <div style="padding-bottom: 5px;">
            Thank you for using our services which are provided through the application. By using our services you agree
            upon these Terms of Use so, please give it a good read.
        </div>
        <div class=WordSection1>

<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;border-collapse:collapse;border:none'>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#D0CECE;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>Introduction</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#D0CECE;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-EG style='font-family:"brandfont";color:black'>&#1578;&#1605;&#1607;&#1610;&#1583;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>Welcome
  to Clever </span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1605;&#1585;&#1581;&#1576;&#1575;
  &#1576;&#1603;&#1605; &#1601;&#1610; &#1602;&#1610;&#1605;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>These
  are the terms and conditions that govern the use of Clever and all its related
  sites and services. By using Clever Website, you agree to these terms and
  conditions. If you do not agree with these terms and conditions, please do
  not access or use the site.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1610;&#1585;&#1583;
  &#1601;&#1610;&#1605;&#1575; &#1610;&#1604;&#1610;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1578;&#1610; &#1578;&#1587;&#1585;&#1610;
  &#1576;&#1588;&#1571;&#1606; &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1605;&#1608;&#1602;&#1593; &quot;</span><span lang=AR-EG style='font-family:
  "Times New Roman",serif'>&#1602;&#1610;&#1605;</span><span lang=AR-SA
  style='font-family:"brandfont"'>&quot;
  &#1608;&#1603;&#1584;&#1575; &#1603;&#1604; &#1605;&#1575;
  &#1610;&#1585;&#1578;&#1576;&#1591; &#1576;&#1607;
  &#1605;&#1608;&#1575;&#1602;&#1593;
  &#1608;&#1582;&#1583;&#1605;&#1575;&#1578;&#1548;
  &#1593;&#1604;&#1605;&#1575; &#1576;&#1571;&#1606; &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610; &quot;&#1602;&#1610;&#1605;&quot;
  &#1573;&#1584; &#1610;&#1593;&#1578;&#1576;&#1585;
  &#1576;&#1605;&#1579;&#1575;&#1576;&#1577;
  &#1605;&#1608;&#1575;&#1601;&#1602;&#1577; &#1605;&#1606;
  &#1580;&#1575;&#1606;&#1576;&#1603;&#1605; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591; &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;.
  &#1608;&#1573;&#1584;&#1575; &#1604;&#1605;
  &#1578;&#1608;&#1601;&#1602;&#1608;&#1575; &#1593;&#1604;&#1609;
  &#1607;&#1584;&#1607; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;&#1548;
  &#1601;&#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1593;&#1583;&#1605; &#1575;&#1604;&#1583;&#1582;&#1608;&#1604;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1571;&#1608; &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1607;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>The
  following Terms and Conditions shall apply to all Users in relation to Clever
  provided services through Clever website ''the Website''. These Terms and Conditions
  come into force once you accept a service through the Website or through any
  other form of communications with Clever.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1578;&#1587;&#1585;&#1610;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1578;&#1575;&#1604;&#1610;
  &#1576;&#1610;&#1575;&#1606;&#1607;&#1575; &#1593;&#1604;&#1609;
  &#1580;&#1605;&#1610;&#1593; &#1575;&#1604;&#1605;&#1615;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;&#1606;&#1548;
  &#1608;&#1584;&#1604;&#1603; &#1601;&#1610;&#1605;&#1575;
  &#1610;&#1578;&#1593;&#1604;&#1602; &#1576;&#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1610; &#1578;&#1602;&#1583;&#1605;&#1607;&#1575; &#1602;&#1610;&#1605;
  &#1605;&#1606; &#1582;&#1604;&#1575;&#1604;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610; &quot;&#1602;&#1610;&#1605;&quot;.
  &#1608;&#1578;&#1587;&#1585;&#1610;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;
  &#1608;&#1578;&#1606;&#1601;&#1584; &#1576;&#1605;&#1580;&#1585;&#1583;
  &#1602;&#1576;&#1608;&#1604;&#1603;&#1605; &#1604;&#1571;&#1610;&#1577;
  &#1582;&#1583;&#1605;&#1577; &#1578;&#1580;&#1585;&#1610;
  &#1593;&#1576;&#1585; &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1571;&#1608; &#1605;&#1606; &#1582;&#1604;&#1575;&#1604;
  &#1571;&#1610;&#1577; &#1608;&#1587;&#1610;&#1604;&#1577;
  &#1578;&#1608;&#1575;&#1589;&#1604; &#1571;&#1582;&#1585;&#1609;
  &#1605;&#1593; &#1602;&#1610;&#1605;.</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>Please
  note that you should review and agree with our Privacy Policy [PP's LINK] as
  it is an integral part of these terms and conditions.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1606;&#1585;&#1580;&#1608;
  &#1575;&#1604;&#1573;&#1581;&#1575;&#1591;&#1577;
  &#1576;&#1571;&#1606;&#1607; &#1610;&#1606;&#1576;&#1594;&#1610; &#1605;&#1615;&#1585;&#1575;&#1580;&#1593;&#1577;
  &#1587;&#1610;&#1575;&#1587;&#1577;
  &#1575;&#1604;&#1582;&#1589;&#1608;&#1589;&#1610;&#1577;
  &#1575;&#1604;&#1587;&#1575;&#1585;&#1610;&#1577;
  &#1604;&#1583;&#1610;&#1606;&#1575; &#1593;&#1604;&#1609; (&#1585;&#1575;&#1576;&#1591;
  &#1587;&#1610;&#1575;&#1587;&#1577;
  &#1575;&#1604;&#1582;&#1589;&#1608;&#1589;&#1610;&#1577;)
  &#1608;&#1575;&#1604;&#1605;&#1615;&#1608;&#1575;&#1601;&#1602;&#1577;
  &#1593;&#1604;&#1610;&#1607;&#1575;
  &#1576;&#1575;&#1593;&#1578;&#1576;&#1575;&#1585;&#1607;&#1575;
  &#1580;&#1586;&#1569;&#1611;&#1575; &#1604;&#1575;
  &#1610;&#1578;&#1580;&#1586;&#1571; &#1605;&#1606; &#1607;&#1584;&#1607;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;.</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:90%'><span style=' 
;line-height:90%;
  font-family:"brandfont"'>The User agrees and accepts that the use
  of the Website and the Services provided by Clever is at the sole liability of
  the User, and further acknowledges that Clever disclaims all representations and
  warranties of any kind, whether expressed or implied.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:105%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1610;&#1615;&#1600;&#1608;&#1575;&#1601;&#1602;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605; &#1576;&#1604;
  &#1608;&#1610;&#1602;&#1576;&#1604; &#1576;&#1571;&#1606;
  &#1610;&#1603;&#1608;&#1606;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1608;&#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1610; &#1610;&#1602;&#1583;&#1605;&#1607;&#1575; &#1602;&#1610;&#1605;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1575;
  &#1610;&#1587;&#1585;&#1610; &#1593;&#1604;&#1609;
  &#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1578;&#1607;
  &#1575;&#1604;&#1582;&#1575;&#1589;&#1577;&#1548; &#1603;&#1605;&#1575;
  &#1610;&#1615;&#1600;&#1602;&#1585; &#1571;&#1610;&#1590;&#1575;
  &#1576;&#1571;&#1606; &#1602;&#1610;&#1605; &#1573;&#1584; &#1578;&#1576;&#1585;&#1571;
  &#1576;&#1604; &#1608;&#1578;&#1593;&#1601;&#1610; &#1605;&#1606;
  &#1571;&#1610;&#1577;
  &#1573;&#1602;&#1585;&#1575;&#1585;&#1575;&#1578;&#1613; &#1571;&#1608;
  &#1590;&#1605;&#1575;&#1606;&#1575;&#1578;&#1613; &#1605;&#1606;
  &#1571;&#1610; &#1606;&#1608;&#1593;&#1548; &#1587;&#1608;&#1575;&#1569;&#1611;
  &#1589;&#1583;&#1585;&#1578; &#1589;&#1585;&#1575;&#1581;&#1577;
  &#1571;&#1608; &#1590;&#1605;&#1606;&#1575;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Definitions</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1578;&#1593;&#1585;&#1610;&#1601;&#1575;&#1578;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify; 
'><span style='font-family:"brandfont"'>The
  following definitions apply to the Terms and Conditions set out below. These
  Terms and Conditions, along with the Privacy Policy, hereby set out the whole
  agreement and understanding between Clever and the ''User'' whether an
  individual or entity that accesses our Website.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1578;&#1587;&#1585;&#1610;
  &#1575;&#1604;&#1578;&#1593;&#1585;&#1610;&#1601;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1575;&#1604;&#1610;&#1577; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1593;&#1585;&#1608;&#1590;&#1577;
  &#1571;&#1583;&#1606;&#1575;&#1607;&#1548; &#1593;&#1604;&#1605;&#1575;
  &#1576;&#1571;&#1606; &#1607;&#1584;&#1607;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591; &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1607;&#1610; &#1608;&#1587;&#1610;&#1575;&#1587;&#1577;
  &#1575;&#1604;&#1582;&#1589;&#1608;&#1589;&#1610;&#1577; &#1573;&#1584;
  &#1578;&#1593;&#1578;&#1576;&#1585;
  &#1608;&#1576;&#1605;&#1608;&#1580;&#1576; &#1607;&#1584;&#1607;
  &#1575;&#1604;&#1605;&#1615;&#1581;&#1585;&#1585; &#1603;&#1604;
  &#1605;&#1575; &#1578;&#1605;
  &#1575;&#1604;&#1575;&#1578;&#1601;&#1575;&#1602;
  &#1593;&#1604;&#1610;&#1607;
  &#1608;&#1575;&#1604;&#1578;&#1601;&#1575;&#1607;&#1605;
  &#1576;&#1588;&#1571;&#1606;&#1607; &#1601;&#1610;&#1605;&#1575;
  &#1576;&#1610;&#1606; &#1602;&#1610;&#1605; &#1608;
  &quot;&#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&quot;&#1548;
  &#1587;&#1608;&#1575;&#1569; &#1603;&#1575;&#1606;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605; &#1601;&#1610;
  &#1589;&#1608;&#1585;&#1577; &#1588;&#1582;&#1589; &#1571;&#1608;
  &#1603;&#1610;&#1575;&#1606; &#1610;&#1602;&#1608;&#1605;
  &#1576;&#1575;&#1604;&#1583;&#1582;&#1608;&#1604; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;.</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>''We'',
  ''Us'', ''Our'', the ''Company'', ''Clever'', ''Site'' shall refer to Clever, its
  employees, and authorized agents.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1610;&#1615;&#1600;&#1588;&#1610;&#1585;
  &#1604;&#1601;&#1592; &quot;&#1606;&#1581;&#1606;&quot;&#1548; &#1608;
  &quot;&#1606;&#1575;&quot;&#1548;
  &#1608;&quot;&#1604;&#1583;&#1610;&#1606;&#1575;&quot;&#1548;
  &#1608;&quot;&#1575;&#1604;&#1588;&#1585;&#1603;&#1577;&quot;&#1548; &#1608; &quot;&#1602;&#1610;&#1605;&quot;&#1548;
  &#1608;&quot;&#1575;&#1604;&#1605;&#1608;&#1602;&#1593;&quot;
  &#1573;&#1604;&#1609; &#1602;&#1610;&#1605;
  &#1608;&#1593;&#1575;&#1605;&#1604;&#1610;&#1607;
  &#1608;&#1575;&#1604;&#1608;&#1603;&#1604;&#1575;&#1569;
  &#1575;&#1604;&#1605;&#1593;&#1578;&#1605;&#1583;&#1610;&#1606;
  &#1604;&#1583;&#1610;&#1607;.</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>''the
  User'', ''Users'', ''You'', ''Your'', shall refer to the Users of the Service.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1603;&#1605;&#1575;
  &#1610;&#1615;&#1600;&#1588;&#1610;&#1585; &#1604;&#1601;&#1592;
  &quot;&#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&quot;&#1548;
  &#1608;&quot;&#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;&#1606;&quot;&#1548;
  &#1608;&quot;&#1571;&#1606;&#1578;&#1605;&quot;&#1548; &#1608;
  &quot;&#1604;&#1583;&#1610;&#1603;&#1605;&quot;&#1548; &#1573;&#1604;&#1609;
  &#1605;&#1615;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;.</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif'>Services</span></b><span style=' 
;
  font-family:"brandfont"'>: means Clever's Services as described by
  these Terms and Conditions.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style='font-family:
  "Times New Roman",serif'>Service providers</span></b><b><span lang=EN-GB
  style='font-family:"brandfont"'> Products</span></b><span
  lang=EN-GB style='font-family:"brandfont"'>:
  means the products listed by the </span><span style='
  font-family:"brandfont"'>Service provider</span><span
  style='font-family:"brandfont"'> </span><span
  lang=EN-GB style='font-family:"brandfont"'>for 'use
  'by the Users through the Website</span><span style=' 
;
  font-family:"brandfont"'>.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span lang=EN-GB style='
  font-family:"brandfont"'>Service provider Offer</span></b><b><span
  style='font-family:"brandfont"'>s</span></b><span
  style='font-family:"brandfont"'>:</span><span
  lang=EN-GB style='font-family:"brandfont"'>
  means the offers and/or discounts which may be provided by the service
  provider.</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1575;<b>&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;</b>:
  &#1608;&#1610;&#1602;&#1589;&#1583; &#1576;&#1607;&#1575;
  &#1582;&#1583;&#1605;&#1575;&#1578; &#1602;&#1610;&#1605;
  &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;
  &#1576;&#1610;&#1575;&#1606;&#1607;&#1575; &#1608;&#1601;&#1602;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;. </span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-EG style='line-height:90%;font-family:"brandfont";
  color:black'>&#1605;&#1606;&#1578;&#1580;&#1575;&#1578; &#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;:</span></b><span
  lang=AR-EG style='line-height:90%;font-family:"brandfont";
  color:black'> </span><span lang=AR-SA style='line-height:
  90%;font-family:"brandfont";color:black'>&#1610;&#1602;&#1589;&#1583;
  &#1576;&#1607;&#1575;
  &#1575;&#1604;&#1605;&#1606;&#1578;&#1580;&#1575;&#1578;
  &#1575;&#1604;&#1605;&#1583;&#1585;&#1580;&#1577; &#1605;&#1606;
  &#1602;&#1576;&#1604; &#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1604;&#1610;&#1578;&#1605; &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1607;&#1575;
  &#1605;&#1606; &#1602;&#1576;&#1604; &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;&#1606;
  &#1605;&#1606; &#1582;&#1604;&#1575;&#1604;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1573;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;.</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='line-height:90%;font-family:"brandfont";
  color:black'>&#1593;&#1585;&#1608;&#1590; &#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;: </span></b><span
  lang=AR-SA style='line-height:90%;font-family:"brandfont";
  color:black'>&#1610;&#1602;&#1589;&#1583; &#1576;&#1607;&#1575;
  &#1575;&#1604;&#1593;&#1585;&#1608;&#1590; &#1608; / &#1571;&#1608;
  &#1575;&#1604;&#1582;&#1589;&#1608;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1610; &#1602;&#1583;
  &#1610;&#1602;&#1583;&#1605;&#1607;&#1575; </span><span lang=AR-EG
  style='line-height:90%;font-family:"brandfont";
  color:black'>&#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif'>Website</span></b><span style=' 
;
  font-family:"brandfont"'>: means Clever platforms whereby its
  Services shall be used.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont"'>&#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;</span></b><span
  lang=AR-SA style='font-family:"brandfont"'>:
  &#1608;&#1610;&#1602;&#1589;&#1583; &#1576;&#1607;
  &#1605;&#1606;&#1589;&#1575;&#1578; &#1602;&#1610;&#1605;</span><span
  lang=AR-EG style='font-family:"brandfont"'>
  &#1575;&#1604;&#1578;&#1610; &#1610;&#1578;&#1605; &#1605;&#1606;
  &#1582;&#1604;&#1575;&#1604;&#1607;&#1575;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1582;&#1583;&#1605;&#1575;&#1578;&#1607;.</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:97%'><b><span style=' 
;line-height:97%;
  font-family:"brandfont"'>Force Majeure</span></b><span
  style=' 
;line-height:97%;font-family:"brandfont"'>:
  mean an act of God, including but not limited to fire, floods, storms,
  hurricanes, typhoons, volcanic activities, or earthquakes, as well as acts,
  orders or requests of a sovereign or a government and /or of anybody
  purporting to be or to act for such authority, wars, whether declared or not,
  war-like actions, revolutions, riots , civil insurrections, or civil
  commotions , as well as strikes, lock outs, and/or similar actions in labor
  disputes, and any other comparable cause beyond the control of a party unless
  its performance has been expressly guaranteed by expressly waving the
  exemption of a Force Majeure event.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont"'>&#1575;&#1604;&#1602;&#1608;&#1577;
  &#1575;&#1604;&#1602;&#1575;&#1607;&#1585;&#1577;</span></b><span lang=AR-SA
  style='font-family:"brandfont"'>: &#1608;&#1610;&#1615;&#1600;&#1602;&#1589;&#1583;
  &#1576;&#1607;&#1575; &#1571;&#1610; &#1581;&#1583;&#1579;
  &#1602;&#1583;&#1585;&#1610;&#1548; &#1608;&#1605;&#1606;
  &#1584;&#1604;&#1603; &#1593;&#1604;&#1609; &#1608;&#1580;&#1607;
  &#1575;&#1604;&#1593;&#1605;&#1608;&#1605; &#1604;&#1575;
  &#1575;&#1604;&#1581;&#1589;&#1585;
  &#1575;&#1604;&#1581;&#1585;&#1610;&#1602;
  &#1608;&#1575;&#1604;&#1601;&#1610;&#1590;&#1575;&#1606;&#1575;&#1578;
  &#1608;&#1575;&#1604;&#1593;&#1608;&#1575;&#1589;&#1601;
  &#1608;&#1575;&#1604;&#1575;&#1593;&#1575;&#1589;&#1610;&#1585;
  &#1608;&#1575;&#1604;&#1586;&#1608;&#1575;&#1576;&#1593;
  &#1608;&#1575;&#1604;&#1606;&#1588;&#1575;&#1591;&#1575;&#1578;
  &#1575;&#1604;&#1576;&#1585;&#1603;&#1575;&#1606;&#1610;&#1577; &#1608;&#1575;&#1604;&#1586;&#1604;&#1575;&#1586;&#1604;&#1548;
  &#1608;&#1603;&#1584;&#1575; &#1571;&#1610;&#1577;
  &#1578;&#1589;&#1585;&#1601;&#1575;&#1578; &#1571;&#1608;
  &#1571;&#1608;&#1575;&#1605;&#1585; &#1571;&#1608;
  &#1591;&#1604;&#1576;&#1575;&#1578; &#1578;&#1589;&#1583;&#1585;
  &#1605;&#1606; &#1580;&#1607;&#1577;
  &#1587;&#1610;&#1575;&#1583;&#1610;&#1577; &#1571;&#1608;
  &#1581;&#1603;&#1608;&#1605;&#1577; &#1608;/&#1571;&#1608;
  &#1571;&#1610;&#1577; &#1607;&#1610;&#1574;&#1577; &#1610;&#1615;&#1600;&#1601;&#1578;&#1585;&#1590;
  &#1571;&#1606;&#1607;&#1575; &#1578;&#1593;&#1605;&#1604;
  &#1604;&#1589;&#1575;&#1604;&#1581; &#1578;&#1604;&#1603;
  &#1575;&#1604;&#1580;&#1607;&#1577;&#1548; &#1608;&#1603;&#1584;&#1575;
  &#1575;&#1604;&#1581;&#1585;&#1576;&#1548; &#1587;&#1608;&#1575;&#1569;
  &#1575;&#1604;&#1605;&#1593;&#1604;&#1606;&#1577; &#1571;&#1608;
  &#1594;&#1610;&#1585;
  &#1575;&#1604;&#1605;&#1593;&#1604;&#1606;&#1577;&#1548;
  &#1608;&#1575;&#1604;&#1578;&#1589;&#1585;&#1601;&#1575;&#1578; &#1575;&#1604;&#1605;&#1579;&#1610;&#1604;&#1577;
  &#1604;&#1604;&#1581;&#1585;&#1576;&#1548;
  &#1608;&#1575;&#1604;&#1579;&#1608;&#1585;&#1575;&#1578;&#1548;
  &#1608;&#1581;&#1575;&#1604;&#1575;&#1578;
  &#1575;&#1604;&#1588;&#1594;&#1576;&#1548;
  &#1608;&#1575;&#1604;&#1593;&#1589;&#1610;&#1575;&#1606;
  &#1575;&#1604;&#1605;&#1583;&#1606;&#1610;&#1548;
  &#1608;&#1575;&#1604;&#1575;&#1590;&#1591;&#1585;&#1575;&#1576;
  &#1575;&#1604;&#1605;&#1583;&#1606;&#1610;&#1548;
  &#1608;&#1603;&#1584;&#1575; &#1593;&#1605;&#1604;&#1610;&#1575;&#1578;
  &#1575;&#1604;&#1575;&#1590;&#1585;&#1575;&#1576;&#1548;
  &#1608;&#1575;&#1604;&#1578;&#1608;&#1602;&#1601; &#1593;&#1606;
  &#1575;&#1604;&#1593;&#1605;&#1604;&#1548; &#1608;/&#1571;&#1608;
  &#1605;&#1575; &#1588;&#1575;&#1576;&#1577; &#1584;&#1604;&#1603;
  &#1605;&#1606; &#1581;&#1575;&#1604;&#1575;&#1578; &#1571;&#1608;
  &#1578;&#1589;&#1585;&#1601;&#1575;&#1578;
  &#1578;&#1606;&#1583;&#1585;&#1580; &#1590;&#1605;&#1606;
  &#1575;&#1604;&#1606;&#1586;&#1575;&#1593;&#1575;&#1578;
  &#1575;&#1604;&#1593;&#1605;&#1575;&#1604;&#1610;&#1577;&#1548;
  &#1608;&#1571;&#1610; &#1587;&#1576;&#1576; &#1570;&#1582;&#1585;
  &#1605;&#1579;&#1610;&#1604; &#1608;&#1610;&#1582;&#1585;&#1580;
  &#1593;&#1606; &#1606;&#1591;&#1575;&#1602; &#1578;&#1581;&#1603;&#1605;
  &#1571;&#1608; &#1587;&#1610;&#1591;&#1585;&#1577; &#1571;&#1610;
  &#1591;&#1585;&#1601;&#1548; &#1607;&#1584;&#1575;
  &#1605;&#1575;&#1604;&#1605; &#1610;&#1603;&#1606;
  &#1578;&#1606;&#1601;&#1610;&#1584;&#1607; &#1607;&#1608;
  &#1578;&#1606;&#1601;&#1610;&#1584; &#1605;&#1581;&#1604;
  &#1590;&#1605;&#1575;&#1606; &#1610;&#1587;&#1585;&#1610;
  &#1576;&#1605;&#1608;&#1580;&#1576; &#1578;&#1606;&#1575;&#1586;&#1604;
  &#1589;&#1585;&#1610;&#1581; &#1593;&#1606; &#1575;&#1604;&#1575;&#1593;&#1601;&#1575;&#1569;
  &#1601;&#1610; &#1581;&#1575;&#1604;&#1577; &#1608;&#1602;&#1608;&#1593; &#1581;&#1583;&#1579;
  &#1575;&#1604;&#1602;&#1608;&#1577;
  &#1575;&#1604;&#1602;&#1575;&#1607;&#1585;&#1577;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif'>Applicable Laws</span></b><span style='font-size:
  10.0pt;font-family:"brandfont"'>: means the laws of the Arab
  Republic of Egypt.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont"'>&#1575;&#1604;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606;
  &#1608;&#1575;&#1580;&#1576;&#1577;
  &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;</span></b><span lang=AR-SA
  style='font-family:"brandfont"'>:
  &#1608;&#1610;&#1602;&#1589;&#1583; &#1576;&#1607;&#1575;
  &#1604;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606; &#1575;&#1604;&#1587;&#1575;&#1585;&#1610;&#1577;
  &#1604;&#1583;&#1609; &#1580;&#1605;&#1607;&#1608;&#1585;&#1610;&#1577;
  &#1605;&#1589;&#1585; &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Eligibility</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1575;&#1604;&#1589;&#1604;&#1575;&#1581;&#1610;&#1577;
  </span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>In
  ensuring that Users are able to form legally binding contracts, eligibility
  to use the site is not granted to persons under the age of 18 years, or the
  age of legal majority in your jurisdiction, whichever is greater.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1581;&#1578;&#1609;
  &#1606;&#1590;&#1605;&#1606; &#1575;&#1606;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;&#1606;
  &#1604;&#1583;&#1610;&#1607;&#1605;
  &#1575;&#1604;&#1602;&#1583;&#1585;&#1577; &#1593;&#1604;&#1609; &#1575;&#1576;&#1585;&#1575;&#1605;
  &#1593;&#1602;&#1608;&#1583;
  &#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1577; &#1605;&#1615;&#1600;&#1604;&#1586;&#1605;&#1577;&#1548;
  &#1601;&#1604;&#1575; &#1610;&#1578;&#1605; &#1605;&#1606;&#1581;
  &#1589;&#1604;&#1575;&#1581;&#1610;&#1577;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593; &#1604;&#1604;&#1571;&#1588;&#1582;&#1575;&#1589;
  &#1605;&#1605;&#1606; &#1578;&#1602;&#1604; &#1571;&#1593;&#1605;&#1575;&#1585;&#1607;&#1605;
  &#1593;&#1606; &#1575;&#1604;&#1579;&#1605;&#1575;&#1606;&#1610;&#1577;
  &#1593;&#1588;&#1585; &#1593;&#1575;&#1605;&#1575; &#1571;&#1608;
  &#1593;&#1606; &#1575;&#1604;&#1587;&#1606;
  &#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1577; &#1575;&#1604;&#1587;&#1575;&#1585;&#1610;&#1577;
  &#1601;&#1610; &#1605;&#1606;&#1591;&#1602;&#1578;&#1603;&#1605;&#1548;
  &#1571;&#1610;&#1607;&#1605;&#1575; &#1571;&#1593;&#1604;&#1609;.</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>If
  you are registering as a business entity, you represent that you have the
  authority to bind that entity to Terms and Conditions and that you and the
  business entity will comply with all applicable laws and terms governing the
  use of the Website.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1573;&#1584;&#1575;
  &#1602;&#1605;&#1578;&#1605;
  &#1576;&#1575;&#1604;&#1578;&#1587;&#1580;&#1610;&#1604;
  &#1576;&#1589;&#1601;&#1578;&#1603;&#1605; &#1580;&#1607;&#1577;
  &#1593;&#1605;&#1604;&#1548; &#1601;&#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1573;&#1602;&#1585;&#1575;&#1585; &#1576;&#1571;&#1606;
  &#1604;&#1583;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1589;&#1604;&#1575;&#1581;&#1610;&#1577;
  &#1575;&#1604;&#1604;&#1575;&#1586;&#1605;&#1577; &#1604;&#1573;&#1604;&#1586;&#1575;&#1605;
  &#1575;&#1604;&#1603;&#1610;&#1575;&#1606;
  &#1575;&#1604;&#1582;&#1575;&#1589; &#1576;&#1603;&#1605;
  &#1576;&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605; &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;&#1548;
  &#1608;&#1571;&#1606;&#1603;&#1605; &#1608;&#1603;&#1584;&#1575;
  &#1603;&#1610;&#1575;&#1606; &#1575;&#1604;&#1593;&#1605;&#1604;
  &#1587;&#1578;&#1604;&#1578;&#1586;&#1605;&#1608;&#1606; &#1576;&#1603;&#1604;
  &#1605;&#1575; &#1607;&#1608; &#1608;&#1575;&#1580;&#1576;
  &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602; &#1605;&#1606;
  &#1602;&#1608;&#1575;&#1606;&#1610;&#1606;
  &#1608;&#1575;&#1588;&#1578;&#1585;&#1575;&#1591;&#1575;&#1578;&#1613;
  &#1578;&#1587;&#1585;&#1610; &#1593;&#1604;&#1609; -
  &#1608;&#1578;&#1582;&#1590;&#1593; &#1604;&#1607;&#1575;-
  &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Accounts and Registration</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&#1575;&#1578;
  &#1608;&#1575;&#1604;&#1578;&#1587;&#1580;&#1610;&#1604;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;  ;

'><span style=' 
;
  font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:95%'><span style=' 
;line-height:95%;
  font-family:"brandfont"'>Account registration requires you to
  submit to Clever 'certain personal information, including but not limited to
  your full name, address, email address, mobile phone number, and age. You
  agree to maintain true and accurate, complete, and up to date information in
  your Account. You are responsible for all activity that occurs under your
  Account, and as such, you agree to maintain the security of your Account
  username and password at all times, unless otherwise permitted by Clever 'in
  writing.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1578;&#1587;&#1578;&#1604;&#1586;&#1605;
  &#1605;&#1606;&#1603;&#1605; &#1593;&#1605;&#1604;&#1610;&#1577; &#1578;&#1587;&#1580;&#1610;&#1604;
  &#1575;&#1604;&#1581;&#1587;&#1575;&#1576; &#1571;&#1606;
  &#1578;&#1578;&#1602;&#1583;&#1605;&#1608;&#1575; &#1573;&#1604;&#1609; &#1602;&#1610;&#1605;
  &#1576;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1613;
  &#1588;&#1582;&#1589;&#1610;&#1577; &#1605;&#1615;&#1593;&#1610;&#1606;&#1577;&#1548;
  &#1608;&#1605;&#1606;&#1607;&#1575; &#1593;&#1604;&#1609;
  &#1608;&#1580;&#1607; &#1575;&#1604;&#1593;&#1605;&#1608;&#1605;
  &#1604;&#1575; &#1575;&#1604;&#1581;&#1589;&#1585;
  &#1575;&#1604;&#1575;&#1587;&#1605; &#1576;&#1575;&#1604;&#1603;&#1575;&#1605;&#1604;
  &#1608;&#1575;&#1604;&#1593;&#1606;&#1608;&#1575;&#1606;
  &#1608;&#1575;&#1604;&#1576;&#1585;&#1610;&#1583;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1608;&#1585;&#1602;&#1605; &#1575;&#1604;&#1607;&#1575;&#1578;&#1601;
  &#1608;&#1575;&#1604;&#1593;&#1605;&#1585;.
  &#1608;&#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
  &#1593;&#1604;&#1609; &#1571;&#1606; &#1610;&#1578;&#1605;
  &#1581;&#1601;&#1592;
  &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1613;
  &#1589;&#1581;&#1610;&#1581;&#1577;
  &#1608;&#1583;&#1602;&#1610;&#1602;&#1577;
  &#1608;&#1603;&#1575;&#1605;&#1604;&#1577; &#1576;&#1604; &#1608;&#1605;&#1615;&#1581;&#1583;&#1579;&#1577;
  &#1576;&#1588;&#1571;&#1606;&#1603;&#1605; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1581;&#1587;&#1575;&#1576;. &#1603;&#1605;&#1575;
  &#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1571;&#1606; &#1578;&#1578;&#1581;&#1605;&#1604;&#1608;&#1575;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1593;&#1606; &#1603;&#1604; &#1606;&#1588;&#1575;&#1591;&#1613;
  &#1610;&#1580;&#1585;&#1610; &#1601;&#1610; &#1573;&#1591;&#1575;&#1585;
  &#1581;&#1587;&#1575;&#1576;&#1603;&#1605;&#1548; &#1603;&#1605;&#1575;
  &#1578;&#1604;&#1578;&#1586;&#1605;&#1608;&#1606;
  &#1576;&#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1581;&#1601;&#1575;&#1592;
  &#1608;&#1576;&#1575;&#1587;&#1578;&#1605;&#1585;&#1575;&#1585;
  &#1593;&#1604;&#1609; &#1587;&#1585;&#1610;&#1575;&#1606;
  &#1573;&#1580;&#1585;&#1575;&#1569;&#1575;&#1578;&#1613;
  &#1575;&#1605;&#1606;&#1610;&#1577; &#1593;&#1604;&#1609; </span><span
  lang=AR-EG style='font-family:"brandfont"'>&#1575;&#1587;&#1605;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  &#1608;&#1603;&#1604;&#1605;&#1577;
  &#1575;&#1604;&#1605;&#1585;&#1608;&#1585;
  &#1575;&#1604;&#1582;&#1575;&#1589;&#1577; &#1576;</span><span lang=AR-SA
  style='font-family:"brandfont"'>&#1581;&#1587;&#1575;&#1576;&#1603;&#1605;&#1548;
  &#1607;&#1584;&#1575; &#1605;&#1575; &#1604;&#1605;
  &#1578;&#1589;&#1585;&#1581; &#1602;&#1610;&#1605; '&#1576;&#1594;&#1610;&#1585;
  &#1584;&#1604;&#1603; &#1603;&#1578;&#1575;&#1576;&#1577;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>User
  accounts are not transferable. You agree to reimburse Clever for any improper,
  unauthorized or illegal use of your account by you or by any person obtaining
  access to the Website, services or otherwise by using your designated
  username and password, whether or not you authorized such access.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;</span><span
  lang=AR-EG style='font-family:"brandfont"'>&#1581;&#1610;&#1579;
  &#1571;&#1606;&#1607; </span><span lang=AR-SA style='font-family:"brandfont"'>&#1604;&#1575;
  &#1610;&#1580;&#1608;&#1586; &#1606;&#1602;&#1604; &#1571;&#1608;
  &#1581;&#1608;&#1575;&#1604;&#1577; &#1581;&#1587;&#1575;&#1576;&#1575;&#1578;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&#1548; &#1601;&#1604;&#1607;&#1584;&#1575;
  &#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
  &#1593;&#1604;&#1609; &#1571;&#1606; &#1578;&#1585;&#1583;&#1608;&#1575;
  &#1573;&#1604;&#1609; &#1602;&#1610;&#1605; '&#1602;&#1610;&#1605;&#1577;
  &#1571;&#1610;&#1577; &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1604;&#1581;&#1587;&#1575;&#1576;&#1603;&#1605;&#1548;
  &#1608;&#1604;&#1605; &#1578;&#1603;&#1606; &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1589;&#1581;&#1610;&#1581;&#1577; &#1571;&#1608;
  &#1605;&#1615;&#1589;&#1585;&#1581; &#1576;&#1607;&#1575; &#1571;&#1608;
  &#1578;&#1587;&#1585;&#1610; &#1608;&#1601;&#1602;
  &#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;&#1548; &#1608;&#1581;&#1583;&#1579;
  &#1584;&#1604;&#1603; &#1605;&#1606;
  &#1580;&#1575;&#1606;&#1576;&#1603;&#1605; &#1571;&#1608; &#1605;&#1606;
  &#1580;&#1575;&#1606;&#1576; &#1571;&#1610; &#1588;&#1582;&#1589;
  &#1610;&#1605;&#1603;&#1606;&#1607; &#1575;&#1604;&#1583;&#1582;&#1608;&#1604;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1571;&#1608; &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1571;&#1608; &#1582;&#1604;&#1575;&#1601;&#1607; &#1605;&#1606;
  &#1582;&#1604;&#1575;&#1604; &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1575;&#1587;&#1605;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605; &#1571;&#1608;
  &#1603;&#1604;&#1605;&#1577; &#1575;&#1604;&#1605;&#1585;&#1608;&#1585;
  &#1575;&#1604;&#1605;&#1581;&#1583;&#1583;&#1577; &#1605;&#1606;
  &#1580;&#1575;&#1606;&#1576;&#1603;&#1605;&#1548; &#1576;&#1604; &#1608;&#1587;&#1608;&#1575;&#1569;
  &#1587;&#1605;&#1581;&#1578;&#1605; &#1576;&#1584;&#1604;&#1603;
  &#1575;&#1604;&#1583;&#1582;&#1608;&#1604; &#1571;&#1605; &#1604;&#1575;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>If
  Clever 'suspects, at its sole discretion, that any of the information you
  provided is untrue, inaccurate, incomplete, or not current, without prejudice
  to any other rights and remedies of Clever under these Terms and Conditions or
  under the Applicable Laws, We have the right to suspend, or limit your access
  to the Website and its Services.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1573;&#1584;&#1575;
  &#1606;&#1605;&#1575; &#1604;&#1583;&#1609; &#1602;&#1610;&#1605; '(&#1608;&#1581;&#1587;&#1576;
  &#1605;&#1591;&#1604;&#1602;
  &#1575;&#1582;&#1578;&#1610;&#1575;&#1585;&#1607;&#1575;)
  &#1579;&#1605;&#1577; &#1588;&#1603;&#1608;&#1603; &#1576;&#1571;&#1606;
  &#1571;&#1610;&#1577; &#1605;&#1593;&#1604;&#1608;&#1605;&#1577;
  &#1605;&#1606;
  &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1610; &#1578;&#1602;&#1583;&#1605;&#1578;&#1605;
  &#1576;&#1607;&#1575; &#1607;&#1610;
  &#1605;&#1593;&#1604;&#1608;&#1605;&#1577; &#1594;&#1610;&#1585;
  &#1589;&#1581;&#1610;&#1581;&#1577; &#1571;&#1608; &#1594;&#1610;&#1585;
  &#1583;&#1602;&#1610;&#1602;&#1577; &#1571;&#1608; &#1594;&#1610;&#1585;
  &#1603;&#1575;&#1605;&#1604;&#1577; &#1571;&#1608; &#1594;&#1610;&#1585;
  &#1605;&#1581;&#1583;&#1579;&#1577;&#1548;
  &#1601;&#1583;&#1608;&#1606;&#1605;&#1575;
  &#1573;&#1582;&#1604;&#1575;&#1604; &#1576;&#1571;&#1610;&#1577;
  &#1581;&#1602;&#1608;&#1602; &#1571;&#1582;&#1585;&#1609;
  &#1608;&#1578;&#1593;&#1608;&#1610;&#1590;&#1575;&#1578;&#1613; &#1605;&#1603;&#1601;&#1608;&#1604;&#1577;
  &#1604;&#1602;&#1610;&#1605; '&#1608;&#1601;&#1602;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577; &#1571;&#1608;
  &#1576;&#1605;&#1602;&#1578;&#1590;&#1609; &#1575;&#1604;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606;
  &#1608;&#1575;&#1580;&#1576;&#1577;
  &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1548;
  &#1601;&#1610;&#1603;&#1608;&#1606; &#1604;&#1583;&#1610;&#1606;&#1575;
  &#1575;&#1604;&#1581;&#1602; &#1601;&#1610;
  &#1573;&#1610;&#1602;&#1575;&#1601; &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1583;&#1582;&#1608;&#1604;&#1603;&#1605; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593; &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1571;&#1608; &#1582;&#1583;&#1605;&#1575;&#1578;&#1607;&#1548;
  &#1571;&#1608; &#1578;&#1581;&#1583;&#1610;&#1583; &#1607;&#1584;&#1575;
  &#1575;&#1604;&#1583;&#1582;&#1608;&#1604; &#1571;&#1608;
  &#1602;&#1589;&#1585;&#1607;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:95%'><span style=' 
;line-height:95%;
  font-family:"brandfont"'>Clever 'may (in its sole discretion and at
  any time), make any inquiries it considers necessary (whether directly or
  through a third party), and request that you provide a response with further
  information or documentation, including without limitation to verify your
  identity and/or ownership of your financial instruments. Without limiting the
  foregoing, if you are a business entity or registered on behalf of a business
  entity such information or documentation may include your trade license,
  other incorporation documents and/or documentation showing any person's
  authority to act on your behalf. You agree to provide any information and/or
  documentation to Clever upon such requests. You acknowledge and agree that if
  you do not, Clever without liability may limit, suspend or withdraw your access
  to the Website. We also reserve the right to cancel unconfirmed / unverified
  accounts or accounts that have been inactive for a long time.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:97%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1610;&#1580;&#1608;&#1586;
  &#1604;&#1602;&#1610;&#1605; &#1575;&#1604;&#1602;&#1610;&#1575;&#1605;
  (&#1608;&#1581;&#1587;&#1576; &#1605;&#1591;&#1604;&#1602;
  &#1575;&#1582;&#1578;&#1610;&#1575;&#1585;&#1607;&#1575; &#1576;&#1604;
  &#1608;&#1601;&#1610; &#1571;&#1610; &#1581;&#1610;&#1606;) &#1576;&#1573;&#1576;&#1583;&#1575;&#1569;
  &#1571;&#1610;&#1577;
  &#1575;&#1587;&#1578;&#1601;&#1587;&#1575;&#1585;&#1575;&#1578;
  &#1578;&#1585;&#1575;&#1607;&#1575;
  &#1590;&#1585;&#1608;&#1585;&#1610;&#1577; (&#1587;&#1608;&#1575;&#1569;
  &#1578;&#1605; &#1584;&#1604;&#1603; &#1576;&#1589;&#1608;&#1585;&#1577;
  &#1605;&#1576;&#1575;&#1588;&#1585;&#1577; &#1571;&#1608; &#1605;&#1606;
  &#1582;&#1604;&#1575;&#1604; &#1591;&#1585;&#1601; &#1605;&#1606;
  &#1575;&#1604;&#1594;&#1610;&#1585;)&#1548; &#1608;&#1603;&#1584;&#1575;
  &#1605;&#1591;&#1575;&#1604;&#1576;&#1578;&#1603;&#1605;
  &#1576;&#1578;&#1602;&#1583;&#1610;&#1605; &#1575;&#1604;&#1585;&#1583;
  &#1608;&#1576;&#1605;&#1586;&#1610;&#1583; &#1605;&#1606;
  &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;</span><span
  lang=AR-EG style='font-family:"brandfont"'> &#1571;&#1608;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;&#1548;
  &#1608;&#1605;&#1606;&#1607;&#1575; &#1593;&#1604;&#1609;
  &#1608;&#1580;&#1607; &#1575;&#1604;&#1593;&#1605;&#1608;&#1605;
  &#1604;&#1575; &#1575;&#1604;&#1581;&#1589;&#1585;
  &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1613; &#1608;&#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;
  &#1604;&#1604;&#1578;&#1581;&#1602;&#1602; &#1605;&#1606;
  &#1607;&#1608;&#1610;&#1578;&#1603;&#1605; &#1608;/&#1571;&#1608;
  &#1605;&#1604;&#1603;&#1610;&#1578;&#1603;&#1605;
  &#1604;&#1587;&#1606;&#1583;&#1575;&#1578;&#1603;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1604;&#1610;&#1577;. &#1608;&#1583;&#1608;&#1606;&#1605;&#1575;
  &#1578;&#1602;&#1610;&#1610;&#1583; &#1604;&#1605;&#1575;
  &#1587;&#1604;&#1601; &#1576;&#1610;&#1575;&#1606;&#1607;&#1548;
  &#1601;&#1573;&#1584;&#1575; &#1603;&#1606;&#1578;&#1605;
  &#1576;&#1605;&#1579;&#1575;&#1576;&#1577; &#1580;&#1607;&#1577;
  &#1593;&#1605;&#1604; &#1571;&#1608; &#1605;&#1615;&#1587;&#1580;&#1604;&#1610;&#1606;
  &#1576;&#1575;&#1604;&#1606;&#1610;&#1575;&#1576;&#1577; &#1593;&#1606;
  &#1580;&#1607;&#1577; &#1593;&#1605;&#1604;&#1548;
  &#1601;&#1610;&#1580;&#1608;&#1586; &#1571;&#1606;
  &#1578;&#1588;&#1605;&#1604; &#1607;&#1584;&#1607;
  &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1571;&#1608;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1585;&#1582;&#1610;&#1589;
  &#1575;&#1604;&#1578;&#1580;&#1575;&#1585;&#1610; &#1608;&#1594;&#1610;&#1585;
  &#1584;&#1604;&#1603; &#1605;&#1606;
  &#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;&#1613;
  &#1585;&#1587;&#1605;&#1610;&#1577; &#1582;&#1575;&#1589;&#1577;
  &#1576;&#1575;&#1604;&#1588;&#1585;&#1603;&#1577; &#1608;/&#1571;&#1608;
  &#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;&#1613;
  &#1578;&#1587;&#1578;&#1593;&#1585;&#1590; &#1571;&#1610;&#1577;
  &#1589;&#1604;&#1575;&#1581;&#1610;&#1577;
  &#1605;&#1603;&#1601;&#1608;&#1604;&#1577; &#1604;&#1571;&#1610;
  &#1588;&#1582;&#1589; &#1604;&#1604;&#1593;&#1605;&#1604;
  &#1606;&#1610;&#1575;&#1576;&#1577; &#1593;&#1606;&#1603;&#1605;.
  &#1608;&#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577; &#1593;&#1604;&#1609;
  &#1578;&#1608;&#1601;&#1610;&#1585; &#1571;&#1610;&#1577;
  &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1613;
  &#1608;/&#1571;&#1608; &#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;
  &#1573;&#1604;&#1609; &#1602;&#1610;&#1605; '&#1601;&#1608;&#1585;
  &#1591;&#1604;&#1576;&#1607;&#1575; &#1573;&#1610;&#1575;&#1607;&#1575;.
  &#1603;&#1605;&#1575; &#1610;&#1603;&#1608;&#1606;
  &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1573;&#1602;&#1585;&#1575;&#1585;
  &#1608;&#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
  &#1593;&#1604;&#1609; &#1571;&#1606;&#1607; &#1573;&#1584;&#1575;
  &#1578;&#1593;&#1584;&#1585; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1602;&#1610;&#1575;&#1605;
  &#1576;&#1584;&#1604;&#1603;&#1548; &#1601;&#1610;&#1580;&#1608;&#1586; &#1604;&#1602;&#1610;&#1605;
  &#1575;&#1604;&#1602;&#1610;&#1575;&#1605;
  &#1608;&#1583;&#1608;&#1606;&#1605;&#1575; &#1571;&#1610;&#1577;
  &#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1593;&#1604;&#1610;&#1607; &#1576;&#1578;&#1602;&#1610;&#1610;&#1583;
  &#1571;&#1608; &#1573;&#1610;&#1602;&#1575;&#1601; &#1571;&#1608;
  &#1587;&#1581;&#1576; &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1583;&#1582;&#1608;&#1604;&#1603;&#1605; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;.
  &#1603;&#1605;&#1575; &#1606;&#1581;&#1578;&#1601;&#1592;
  &#1571;&#1610;&#1590;&#1575; &#1576;&#1575;&#1604;&#1581;&#1602; &#1601;&#1610;
  &#1575;&#1604;&#1594;&#1575;&#1569; &#1571;&#1610;&#1577;
  &#1581;&#1587;&#1575;&#1576;&#1575;&#1578;&#1613; &#1604;&#1605;
  &#1610;&#1585;&#1583; &#1578;&#1571;&#1603;&#1610;&#1583;
  &#1576;&#1588;&#1571;&#1606;&#1607;&#1575; &#1571;&#1608; &#1604;&#1605; &#1610;&#1578;&#1605;
  &#1575;&#1604;&#1578;&#1581;&#1602;&#1602; &#1605;&#1606;&#1607;&#1575;
  &#1571;&#1608; &#1571;&#1610;&#1577;
  &#1581;&#1587;&#1575;&#1576;&#1575;&#1578;&#1613; &#1604;&#1605;
  &#1578;&#1603;&#1606; &#1605;&#1601;&#1593;&#1604;&#1577; &#1604;&#1605;&#1583;&#1577;
  &#1591;&#1608;&#1610;&#1604;&#1577;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>By
  completing your registration, you acknowledge having read, understood and
  agreed to be bound by these Terms and Conditions, along with the Privacy
  Policy, as they may be amended from time to time, which are hereby
  incorporated and made an integral part hereof.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1593;&#1606;&#1583;
  &#1573;&#1578;&#1605;&#1575;&#1605; &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1575;&#1604;&#1578;&#1587;&#1580;&#1610;&#1604; &#1605;&#1606;
  &#1580;&#1575;&#1606;&#1576;&#1603;&#1605;&#1548;
  &#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1573;&#1602;&#1585;&#1575;&#1585;
  &#1576;&#1571;&#1606;&#1603;&#1605;
  &#1575;&#1591;&#1604;&#1593;&#1578;&#1605; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591; &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577; &#1576;&#1604;
  &#1608;&#1578;&#1601;&#1607;&#1605;&#1578;&#1605;&#1608;&#1607;&#1575;
  &#1608;&#1608;&#1575;&#1601;&#1602;&#1578;&#1605; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1575;&#1604;&#1578;&#1586;&#1575;&#1605;
  &#1576;&#1607;&#1575;&#1548; &#1608;&#1603;&#1584;&#1575;
  &#1587;&#1610;&#1575;&#1587;&#1577;
  &#1575;&#1604;&#1582;&#1589;&#1608;&#1589;&#1610;&#1577; &#1576;&#1604;
  &#1608;&#1603;&#1604; &#1605;&#1575; &#1610;&#1591;&#1585;&#1571;
  &#1593;&#1604;&#1610;&#1607;&#1575; &#1605;&#1606;
  &#1578;&#1593;&#1583;&#1610;&#1604; &#1605;&#1606; &#1581;&#1610;&#1606;
  &#1604;&#1570;&#1582;&#1585;&#1548; &#1581;&#1610;&#1579; &#1578;&#1593;&#1578;&#1576;&#1585;
  &#1575;&#1604;&#1587;&#1610;&#1575;&#1587;&#1577;
  &#1608;&#1578;&#1593;&#1583;&#1610;&#1604;&#1575;&#1578;&#1607;&#1575; - &#1608;&#1576;&#1605;&#1608;&#1580;&#1576;
  &#1607;&#1584;&#1575; &#1575;&#1604;&#1605;&#1581;&#1585;&#1585;- &#1576;&#1605;&#1579;&#1575;&#1576;&#1577;
  &#1580;&#1586;&#1569; &#1604;&#1575; &#1610;&#1578;&#1580;&#1586;&#1571;
  &#1576;&#1604; &#1608;&#1578;&#1606;&#1583;&#1585;&#1580;
  &#1590;&#1605;&#1606; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Services</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span lang=EN-GB style='
  font-family:"brandfont"'>We are driven by a passion for creating
  a service provider-focused platform that is unrivalled in this part of the
  world. Everything we do is guided by the principle of providing what is
  easier and more convenient to our Customers.</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:92%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='line-height:92%;font-family:"brandfont"'>&#1606;&#1581;&#1606;
  &#1605;&#1583;&#1601;&#1608;&#1593;&#1608;&#1606;
  &#1576;&#1588;&#1594;&#1601; &#1604;&#1573;&#1606;&#1588;&#1575;&#1569;
  &#1605;&#1606;&#1589;&#1577; </span><span lang=AR-EG style='
  line-height:92%;font-family:"brandfont"'>&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;&#1607;
  </span><span lang=AR-SA style='line-height:92%;font-family:
  "Times New Roman",serif'>&#1578;&#1585;&#1603;&#1586; &#1593;&#1604;&#1609; &#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1575;&#1604;&#1584;&#1610;
  &#1604;&#1575; &#1605;&#1579;&#1610;&#1604; &#1604;&#1607;&#1605;
  &#1601;&#1610; &#1575;&#1604;&#1593;&#1575;&#1604;&#1605;.
  &#1610;&#1587;&#1578;&#1585;&#1588;&#1583; &#1603;&#1604; &#1605;&#1575;
  &#1606;&#1602;&#1608;&#1605; &#1576;&#1607;
  &#1576;&#1605;&#1576;&#1583;&#1571; &#1578;&#1608;&#1601;&#1610;&#1585; &#1605;&#1575;
  &#1607;&#1608; &#1571;&#1587;&#1607;&#1604;
  &#1608;&#1571;&#1603;&#1579;&#1585; &#1585;&#1575;&#1581;&#1577;
  &#1604;&#1593;&#1605;&#1604;&#1575;&#1574;&#1606;&#1575;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style='font-family:"brandfont"'>Clever</span><span
  lang=EN-GB style='font-family:"brandfont"'>'s
  Website offers the </span><span style='font-family:"brandfont"'>service
  providers</span><span lang=EN-GB style='font-family:"brandfont"'>
  products and services where Users can benefit from these products and service
  'with the best prices and offers using our Website. In addition, where the </span><span
  style='font-family:"brandfont"'>service
  providers</span><span lang=EN-GB style='font-family:"brandfont"'>
  can list their products with a high-quality photos and descriptions.</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='line-height:90%;font-family:"brandfont"'>&#1610;&#1602;&#1608;&#1605;
  &#1605;&#1608;&#1602;&#1593; <b>&#1602;&#1610;&#1605;</b> '&#1576;&#1593;&#1585;&#1590;
  &#1605;&#1606;&#1578;&#1580;&#1575;&#1578;
  &#1608;&#1582;&#1583;&#1605;&#1575;&#1578; '&#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1607;</span><span dir=LTR></span><span
  lang=AR-SA dir=LTR style='line-height:90%;font-family:"brandfont"'><span
  dir=LTR></span> </span><span lang=AR-SA style='line-height:
  90%;font-family:"brandfont"'>&#1581;&#1610;&#1579; &#1610;&#1605;&#1603;&#1606;
  &#1604;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;&#1606; &#1575;&#1604;&#1575;&#1587;&#1578;&#1601;&#1575;&#1583;&#1607;
  &#1605;&#1606; </span><span lang=AR-EG style='line-height:
  90%;font-family:"brandfont"'>&#1578;&#1604;&#1603; &#1575;&#1604;</span><span
  lang=AR-SA style='line-height:90%;font-family:"brandfont"'>&#1605;&#1606;&#1578;&#1580;&#1575;&#1578;
  &#1608;&#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; '&#1576;&#1571;&#1601;&#1590;&#1604;
  &#1575;&#1604;&#1571;&#1587;&#1593;&#1575;&#1585;
  &#1608;&#1575;&#1604;&#1593;&#1585;&#1608;&#1590;
  &#1576;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1605;&#1608;&#1602;&#1593;&#1606;&#1575; &#1575;&#1604;&#1573;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;.
  &#1571;&#1610;&#1590;&#1575;&#1548; &#1581;&#1610;&#1579;
  &#1610;&#1605;&#1603;&#1606; &#1604;&#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1607; &#1587;&#1585;&#1583;
  &#1605;&#1606;&#1578;&#1580;&#1575;&#1578;&#1607;&#1605; &#1605;&#1593;
  &#1589;&#1608;&#1585; &#1608;&#1571;&#1608;&#1589;&#1575;&#1601;
  &#1593;&#1575;&#1604;&#1610;&#1577;
  &#1575;&#1604;&#1580;&#1608;&#1583;&#1577;.</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1603;&#1605;&#1575;
  &#1610;&#1587;&#1578;&#1602;&#1576;&#1604; &#1602;&#1610;&#1605;
  &#1605;&#1585;&#1575;&#1580;&#1593;&#1575;&#1578; &#1605;&#1606;
  &#1602;&#1576;&#1604;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;&#1606;
  &#1593;&#1604;&#1610; &#1581;&#1587;&#1575;&#1576;&#1575;&#1578;
  &#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1587;&#1608;&#1575;&#1569;'
  &#1576;&#1575;&#1604;&#1587;&#1604;&#1576; &#1575;&#1608;
  &#1575;&#1604;&#1575;&#1610;&#1580;&#1575;&#1576;
  &#1576;&#1607;&#1583;&#1601; &#1578;&#1591;&#1608;&#1585;&#1607;&#1575;
  &#1608; &#1575;&#1604;&#1608;&#1589;&#1608;&#1604;
  &#1604;&#1571;&#1581;&#1587;&#1606; &#1589;&#1608;&#1585;&#1577; </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Electronic Communications</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1593;&#1605;&#1604;&#1610;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1608;&#1575;&#1589;&#1604;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:90%'><span style=' 
;line-height:90%;
  font-family:"brandfont"'>By creating an Account, you agree that
  you are communicating with us electronically. Therefore, you consent to
  receive periodic communications from us. Clever 'will communicate with you via
  e-mail or may send you information via text messages (SMS), as well as by
  posting notices on the Website as part of the normal business operation of
  your use of the Services. You acknowledge that opting out of any of the said
  means of communication may affect your use of the Services.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:95%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1593;&#1606;&#1583;
  &#1575;&#1606;&#1588;&#1575;&#1569; &#1581;&#1587;&#1575;&#1576;&#1548;
  &#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577; &#1593;&#1604;&#1609;
  &#1571;&#1606;&#1603;&#1605;
  &#1587;&#1578;&#1578;&#1608;&#1575;&#1589;&#1604;&#1608;&#1606;
  &#1605;&#1593;&#1606;&#1575;
  &#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;&#1575;.
  &#1608;&#1576;&#1575;&#1604;&#1578;&#1575;&#1604;&#1610;&#1548;
  &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
  &#1593;&#1604;&#1609; &#1575;&#1587;&#1578;&#1604;&#1575;&#1605;
  &#1585;&#1587;&#1575;&#1574;&#1604; &#1583;&#1608;&#1585;&#1610;&#1577;
  &#1605;&#1606; &#1580;&#1575;&#1606;&#1576;&#1606;&#1575;.
  &#1608;&#1587;&#1608;&#1601; &#1610;&#1578;&#1608;&#1575;&#1589;&#1604;
  &#1605;&#1593;&#1603;&#1605; &#1602;&#1610;&#1605; &#1593;&#1576;&#1585;
  &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1571;&#1608; &#1602;&#1583; &#1610;&#1585;&#1587;&#1604;
  &#1604;&#1603;&#1605; &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1613;
  &#1593;&#1576;&#1585; &#1585;&#1587;&#1575;&#1574;&#1604; &#1606;&#1589;&#1610;&#1577;
  &#1602;&#1589;&#1610;&#1585;&#1577;&#1548; &#1608;&#1603;&#1584;&#1575;
  &#1575;&#1604;&#1602;&#1610;&#1575;&#1605; &#1576;&#1606;&#1588;&#1585;
  &#1573;&#1582;&#1591;&#1575;&#1585;&#1575;&#1578;&#1613;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1603;&#1580;&#1586;&#1569; &#1605;&#1606;
  &#1573;&#1583;&#1575;&#1585;&#1577;
  &#1575;&#1604;&#1575;&#1593;&#1605;&#1575;&#1604; &#1575;&#1604;&#1605;&#1593;&#1578;&#1575;&#1583;&#1577;
  &#1608;&#1575;&#1604;&#1578;&#1610; &#1578;&#1585;&#1578;&#1576;&#1591;
  &#1576;&#1593;&#1605;&#1604;&#1610;&#1577;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1603;&#1605;
  &#1604;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;.
  &#1608;&#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1573;&#1602;&#1585;&#1575;&#1585; &#1576;&#1571;&#1606;
  &#1575;&#1582;&#1578;&#1610;&#1575;&#1585;&#1603;&#1605;
  &#1593;&#1583;&#1605; &#1578;&#1591;&#1576;&#1610;&#1602;
  &#1608;&#1587;&#1575;&#1574;&#1604;
  &#1575;&#1604;&#1578;&#1608;&#1575;&#1589;&#1604;
  &#1575;&#1604;&#1605;&#1584;&#1603;&#1608;&#1585;&#1577; &#1607;&#1608;
  &#1575;&#1605;&#1585; &#1605;&#1606; &#1588;&#1571;&#1606;&#1607; &#1573;&#1581;&#1583;&#1575;&#1579;
  &#1579;&#1605;&#1577; &#1578;&#1571;&#1579;&#1610;&#1585;
  &#1593;&#1604;&#1609; &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1603;&#1605;
  &#1604;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>You
  agree that all agreements, notices, disclosures and other communications that
  we provide to you electronically satisfy any legal requirement that such
  communications be in writing.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1603;&#1605;&#1575;
  &#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
  &#1593;&#1604;&#1609; &#1571;&#1606; &#1578;&#1603;&#1608;&#1606; &#1580;&#1605;&#1610;&#1593;
  &#1575;&#1604;&#1575;&#1578;&#1601;&#1575;&#1602;&#1575;&#1578;
  &#1608;&#1575;&#1604;&#1575;&#1582;&#1591;&#1575;&#1585;&#1575;&#1578;
  &#1608;&#1575;&#1604;&#1573;&#1601;&#1589;&#1575;&#1581;&#1575;&#1578;
  &#1608;&#1594;&#1610;&#1585; &#1584;&#1604;&#1603; &#1605;&#1606;
  &#1605;&#1585;&#1575;&#1587;&#1604;&#1575;&#1578;&#1613; &#1605;&#1605;&#1575;
  &#1606;&#1602;&#1608;&#1605;
  &#1576;&#1578;&#1602;&#1583;&#1610;&#1605;&#1607;
  &#1575;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;&#1575; &#1607;&#1610;
  &#1605;&#1585;&#1575;&#1587;&#1604;&#1575;&#1578;&#1613;
  &#1578;&#1587;&#1578;&#1608;&#1601;&#1610; &#1571;&#1610;
  &#1575;&#1588;&#1578;&#1585;&#1575;&#1591;
  &#1602;&#1575;&#1606;&#1608;&#1606;&#1610;
  &#1610;&#1587;&#1578;&#1604;&#1586;&#1605; &#1578;&#1602;&#1583;&#1610;&#1605;
  &#1578;&#1604;&#1603;
  &#1575;&#1604;&#1605;&#1585;&#1575;&#1587;&#1604;&#1575;&#1578;
  &#1593;&#1604;&#1609; &#1606;&#1581;&#1608;
  &#1603;&#1578;&#1575;&#1576;&#1610;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>Clever
  will request your agreement during the registration process to send you
  promotional emails or notifications related to the Website and its services.
  If, at any time, you decide that you do not wish to receive promotional
  emails, you can opt out of receiving such promotional emails by clicking on
  the link at the bottom of any promotional email.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1603;&#1605;&#1575;
  &#1587;&#1610;&#1591;&#1604;&#1576; &#1605;&#1606;&#1603;&#1605; &#1602;&#1610;&#1605;
  '&#1582;&#1604;&#1575;&#1604; &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1575;&#1604;&#1578;&#1587;&#1580;&#1610;&#1604;
  &#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
  &#1593;&#1604;&#1609; &#1575;&#1606; &#1606;&#1585;&#1587;&#1604;
  &#1604;&#1603;&#1605; &#1585;&#1587;&#1575;&#1574;&#1604;
  &#1576;&#1585;&#1610;&#1583;
  &#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610; &#1571;&#1608;
  &#1573;&#1582;&#1591;&#1575;&#1585;&#1575;&#1578;&#1613;
  &#1583;&#1593;&#1575;&#1574;&#1610;&#1577; &#1578;&#1585;&#1578;&#1576;&#1591;
  &#1576;&#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1608;&#1605;&#1575; &#1576;&#1607; &#1605;&#1606;
  &#1582;&#1583;&#1605;&#1575;&#1578;. &#1608;&#1573;&#1584;&#1575;
  &#1581;&#1583;&#1579; &#1601;&#1610; &#1571;&#1610; &#1581;&#1610;&#1606;
  &#1571;&#1606; &#1602;&#1585;&#1585;&#1578;&#1605; &#1571;&#1608;
  &#1575;&#1576;&#1583;&#1610;&#1578;&#1605; &#1593;&#1583;&#1605;
  &#1575;&#1604;&#1585;&#1594;&#1576;&#1577; &#1601;&#1610;
  &#1575;&#1587;&#1578;&#1604;&#1575;&#1605;
  &#1585;&#1587;&#1575;&#1574;&#1604;
  &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1575;&#1604;&#1583;&#1593;&#1575;&#1574;&#1610;&#1577;&#1548;
  &#1601;&#1610;&#1605;&#1603;&#1606;&#1603;&#1605;
  &#1575;&#1582;&#1578;&#1610;&#1575;&#1585; &#1593;&#1583;&#1605;
  &#1575;&#1587;&#1578;&#1604;&#1575;&#1605;&#1607;&#1575; &#1605;&#1606;
  &#1582;&#1604;&#1575;&#1604; &#1575;&#1604;&#1590;&#1594;&#1591;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1585;&#1575;&#1576;&#1591;
  &#1575;&#1604;&#1603;&#1575;&#1574;&#1606; &#1601;&#1610;
  &#1575;&#1587;&#1601;&#1604; &#1571;&#1610;&#1577;
  &#1585;&#1587;&#1575;&#1604;&#1577; &#1576;&#1585;&#1610;&#1583;
  &#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1583;&#1593;&#1575;&#1574;&#1610;&#1577;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Copyrights and Trademarks</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1581;&#1602;&#1608;&#1602;
  &#1575;&#1604;&#1578;&#1571;&#1604;&#1610;&#1601;
  &#1608;&#1575;&#1604;&#1593;&#1604;&#1575;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1580;&#1575;&#1585;&#1610;&#1577;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:109%'><span style=' 
;line-height:109%;
  font-family:"brandfont"'>All content included on the Website,
  including but not limited to text, graphics, Clever's logos, button icons,
  images, audio clips, digital downloads, data compilations and software, is
  the property and copyright work of , Clever or its licensors and is protected by
  copyright, trademarks, patents or other intellectual property rights and
  laws. The compilation of the content on the Website is the exclusive property
  and copyright of , Clever and is protected by copyright, trademarks, patents or
  other intellectual property rights and laws.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-EG style='font-family:"brandfont"'>&#1576;&#1575;&#1604;&#1606;&#1587;&#1576;&#1577;
  &#1604;&#1580;&#1605;&#1610;&#1593;
  &#1575;&#1604;&#1605;&#1581;&#1578;&#1608;&#1609;
  &#1575;&#1604;&#1608;&#1575;&#1585;&#1583; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593; &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;&#1548;
  &#1608;&#1605;&#1606; &#1584;&#1604;&#1603; &#1593;&#1604;&#1609;
  &#1608;&#1580;&#1607; &#1575;&#1604;&#1593;&#1605;&#1608;&#1605;
  &#1604;&#1575; &#1575;&#1604;&#1581;&#1589;&#1585; &#1575;&#1604;&#1606;&#1589;&#1548;
  &#1608;&#1589;&#1608;&#1585;
  &#1575;&#1604;&#1580;&#1585;&#1575;&#1601;&#1610;&#1603;&#1548;
  &#1608;&#1588;&#1593;&#1575;&#1585;&#1575;&#1578; &#1602;&#1610;&#1605; &#1548;
  &#1608;&#1575;&#1604;&#1575;&#1586;&#1585;&#1575;&#1585;&#1548;
  &#1608;&#1575;&#1604;&#1589;&#1608;&#1585;&#1548;
  &#1608;&#1575;&#1604;&#1605;&#1602;&#1575;&#1591;&#1593;
  &#1575;&#1604;&#1589;&#1608;&#1578;&#1610;&#1577;&#1548;
  &#1608;&#1593;&#1605;&#1604;&#1610;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1581;&#1605;&#1610;&#1604;
  &#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1548;
  &#1608;&#1578;&#1580;&#1605;&#1610;&#1593;
  &#1575;&#1604;&#1576;&#1610;&#1575;&#1606;&#1575;&#1578;&#1548;
  &#1608;&#1575;&#1604;&#1576;&#1585;&#1575;&#1605;&#1580; &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;&#1577;&#1548;
  &#1601;&#1610;&#1593;&#1578;&#1576;&#1585; &#1584;&#1604;&#1603;
  &#1575;&#1604;&#1605;&#1581;&#1578;&#1608;&#1609;
  &#1576;&#1605;&#1579;&#1575;&#1576;&#1577; &#1605;&#1604;&#1603;&#1610;&#1577;
  &#1576;&#1604; &#1608;&#1593;&#1605;&#1604; &#1578;&#1571;&#1604;&#1610;&#1601;&#1610;
  &#1582;&#1575;&#1589;<b> &#1576;&#1602;&#1610;&#1605;</b> '&#1571;&#1608;
  &#1576;&#1605;&#1585;&#1582;&#1589;&#1610;&#1607;. &#1603;&#1605;&#1575;
  &#1610;&#1582;&#1590;&#1593; &#1607;&#1584;&#1575;
  &#1575;&#1604;&#1605;&#1581;&#1578;&#1608;&#1609;
  &#1604;&#1604;&#1581;&#1605;&#1575;&#1610;&#1577;
  &#1576;&#1605;&#1608;&#1580;&#1576; &#1581;&#1602;
  &#1575;&#1604;&#1605;&#1572;&#1604;&#1601;
  &#1608;&#1581;&#1602;&#1608;&#1602;
  &#1575;&#1604;&#1593;&#1604;&#1575;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1580;&#1575;&#1585;&#1610;&#1577;
  &#1608;&#1576;&#1585;&#1575;&#1569;&#1575;&#1578;
  &#1575;&#1604;&#1575;&#1582;&#1578;&#1585;&#1575;&#1593;
  &#1608;&#1594;&#1610;&#1585; &#1584;&#1604;&#1603; &#1605;&#1606;
  &#1581;&#1602;&#1608;&#1602; &#1605;&#1604;&#1603;&#1610;&#1577; &#1601;&#1603;&#1585;&#1610;&#1577;
  &#1608;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606;. &#1603;&#1605;&#1575;
  &#1610;&#1603;&#1608;&#1606;
  &#1575;&#1604;&#1605;&#1581;&#1578;&#1608;&#1609;
  &#1575;&#1604;&#1605;&#1580;&#1605;&#1593; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1576;&#1605;&#1579;&#1575;&#1576;&#1577;
  &#1605;&#1604;&#1603;&#1610;&#1577; &#1581;&#1589;&#1585;&#1610;&#1577; '<b>&#1604;&#1602;&#1610;&#1605;
  </b>&#1609;&#1576;&#1604; &#1608;&#1581;&#1602;
  &#1578;&#1571;&#1604;&#1610;&#1601; &#1604;&#1607;&#1575;&#1548;
  &#1576;&#1604; &#1608;&#1610;&#1582;&#1590;&#1593;
  &#1604;&#1604;&#1581;&#1605;&#1575;&#1610;&#1577;
  &#1576;&#1605;&#1608;&#1580;&#1576; &#1581;&#1602; &#1575;&#1604;&#1605;&#1572;&#1604;&#1601;
  &#1608;&#1581;&#1602;&#1608;&#1602;
  &#1575;&#1604;&#1593;&#1604;&#1575;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1580;&#1575;&#1585;&#1610;&#1577;
  &#1608;&#1576;&#1585;&#1575;&#1569;&#1575;&#1578; &#1575;&#1604;&#1575;&#1582;&#1578;&#1585;&#1575;&#1593;
  &#1608;&#1594;&#1610;&#1585; &#1584;&#1604;&#1603; &#1605;&#1606;
  &#1581;&#1602;&#1608;&#1602; &#1605;&#1604;&#1603;&#1610;&#1577;
  &#1601;&#1603;&#1585;&#1610;&#1577;
  &#1608;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>Clever
  and related logos are the trademarks of Clever. Clever's trademarks may not be used
  in connection with any product or service that is not Clever's nor in any manner
  that disparages or discredits , Clever</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1578;&#1593;&#1578;&#1576;&#1585;
  &#1593;&#1604;&#1575;&#1605;&#1577;<b> &#1602;&#1610;&#1605;</b> '&#1608;&#1605;&#1575;
  &#1610;&#1585;&#1578;&#1576;&#1591; &#1576;&#1607;&#1575; &#1605;&#1606; </span><span
  lang=AR-EG style='font-family:"brandfont"'>&#1585;&#1605;&#1608;&#1586;
  </span><span lang=AR-SA style='font-family:"brandfont"'>&#1607;&#1610;
  &#1593;&#1604;&#1575;&#1605;&#1575;&#1578;&#1613;
  &#1578;&#1580;&#1575;&#1585;&#1610;&#1577; &#1582;&#1575;&#1589;&#1577; <b>&#1576;&#1602;&#1610;&#1605;&#1548;</b>
  &#1593;&#1604;&#1605;&#1575; &#1576;&#1571;&#1606;&#1607; &#1604;&#1575;
  &#1610;&#1580;&#1608;&#1586; &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1575;&#1604;&#1593;&#1604;&#1575;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1580;&#1575;&#1585;&#1610;&#1577; <b>&#1604;&#1602;&#1610;&#1605;</b>
  '&#1601;&#1610;&#1605;&#1575; &#1610;&#1578;&#1593;&#1604;&#1602;
  &#1576;&#1571;&#1610; &#1605;&#1606;&#1578;&#1580; &#1571;&#1608;
  &#1571;&#1610;&#1577; &#1582;&#1583;&#1605;&#1577;
  &#1604;&#1610;&#1587;&#1578; &#1605;&#1593;&#1585;&#1608;&#1590;&#1577;
  &#1593;&#1604;&#1609; <b>&#1602;&#1610;&#1605;</b> &#1548; &#1571;&#1608;
  &#1571;&#1610; &#1605;&#1606;&#1578;&#1580; &#1571;&#1608;
  &#1582;&#1583;&#1605;&#1577; &#1605;&#1606;
  &#1588;&#1571;&#1606;&#1607;&#1575;
  &#1575;&#1604;&#1578;&#1602;&#1604;&#1610;&#1604; &#1605;&#1606;
  &#1588;&#1571;&#1606; <b>&#1602;&#1610;&#1605; </b>'&#1571;&#1608; &#1575;&#1604;&#1573;&#1587;&#1575;&#1569;&#1577;
  &#1604;&#1587;&#1605;&#1593;&#1578;&#1607;&#1575;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Use of the Website</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610; </span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>Subject
  to the User`s compliance with these Terms and Conditions, <b>Clever</b> grants
  the User a limited, non-exclusive, non-sub licensable, revocable,
  non-transferrable license to:</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1606;&#1592;&#1610;&#1585;
  &#1575;&#1604;&#1578;&#1586;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  &#1576;&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;&#1548;
  &#1578;&#1605;&#1606;&#1581; <b>&#1602;&#1610;&#1605;</b> &#1573;&#1604;&#1609;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  &#1578;&#1585;&#1582;&#1610;&#1589;&#1611;&#1575;
  &#1605;&#1581;&#1583;&#1608;&#1583;&#1575; &#1608;&#1594;&#1610;&#1585;
  &#1581;&#1589;&#1585;&#1610; &#1608;&#1604;&#1575;
  &#1610;&#1580;&#1608;&#1586; &#1605;&#1606;&#1581;&#1607; &#1605;&#1606;
  &#1575;&#1604;&#1576;&#1575;&#1591;&#1606; &#1608;&#1594;&#1610;&#1585;
  &#1602;&#1575;&#1576;&#1604;
  &#1604;&#1604;&#1573;&#1604;&#1594;&#1575;&#1569; &#1571;&#1608;
  &#1575;&#1604;&#1573;&#1576;&#1591;&#1575;&#1604; &#1576;&#1604;
  &#1608;&#1594;&#1610;&#1585; &#1602;&#1575;&#1576;&#1604;
  &#1604;&#1604;&#1581;&#1608;&#1575;&#1604;&#1577; &#1571;&#1608;
  &#1575;&#1604;&#1606;&#1602;&#1604; &#1601;&#1610;&#1605;&#1575;
  &#1610;&#1578;&#1593;&#1604;&#1602; &#1576;&#1605;&#1575;
  &#1610;&#1604;&#1610;: '</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>-
  access and use the Services solely in connection with the provision of the
  Services;</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-family:"brandfont"'><span
  dir=RTL></span>- &#1575;&#1604;&#1583;&#1582;&#1608;&#1604;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1608;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1607;&#1575;
  &#1601;&#1610;&#1605;&#1575; &#1610;&#1578;&#1593;&#1604;&#1602; &#1576;&#1593;&#1605;&#1604;&#1610;&#1577;
  &#1578;&#1602;&#1583;&#1610;&#1605;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1608;&#1581;&#1587;&#1576;.</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>-
  access and use any content, information and related materials that may be
  made available through the Services; and</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-family:"brandfont"'><span
  dir=RTL></span>- &#1608;&#1575;&#1604;&#1583;&#1582;&#1608;&#1604;
  &#1593;&#1604;&#1609; &#1571;&#1610; &#1605;&#1581;&#1578;&#1608;&#1609;
  &#1608;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1607;
  &#1608;&#1603;&#1584;&#1575; &#1571;&#1610;&#1577; &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1608;&#1605;&#1575; &#1610;&#1585;&#1578;&#1576;&#1591;
  &#1576;&#1607;&#1575; &#1605;&#1606; &#1605;&#1608;&#1575;&#1583;
  &#1602;&#1583; &#1578;&#1578;&#1608;&#1601;&#1585; &#1605;&#1606;
  &#1582;&#1604;&#1575;&#1604;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;.' '</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>Any
  rights not expressly granted herein are reserved by Clever 'and may be revoked
  at any time without notice to the customer.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1593;&#1604;&#1605;&#1575;
  &#1576;&#1571;&#1606; &#1571;&#1610; &#1581;&#1602; &#1604;&#1605;
  &#1610;&#1578;&#1605; &#1605;&#1606;&#1581;&#1607; &#1593;&#1604;&#1609;
  &#1606;&#1581;&#1608; &#1589;&#1585;&#1610;&#1581; &#1601;&#1610;
  &#1607;&#1584;&#1607; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1607;&#1608; &#1581;&#1602; &#1605;&#1581;&#1601;&#1608;&#1592;
  &#1604;&#1602;&#1610;&#1605; &#1548; &#1576;&#1604;
  &#1608;&#1610;&#1580;&#1608;&#1586;
  &#1575;&#1604;&#1594;&#1575;&#1574;&#1607; &#1571;&#1608;
  &#1575;&#1576;&#1591;&#1575;&#1604;&#1607; &#1601;&#1610; &#1571;&#1610;
  &#1581;&#1610;&#1606; &#1576;&#1604; &#1608;&#1583;&#1608;&#1606;&#1605;&#1575;
  &#1571;&#1610; &#1573;&#1582;&#1591;&#1575;&#1585;&#1613;
  &#1576;&#1584;&#1604;&#1603; &#1573;&#1604;&#1609;
  &#1575;&#1604;&#1593;&#1605;&#1610;&#1604;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>The
  User shall not use any trademark or any intellectual property rights
  belonging to Clever from any portion of the Services, and shall not reproduce,
  modify, prepare, publicly, perform, transmit, stream, broadcast, or otherwise
  exploit the Services except as expressly permitted by Clever 'in writing, and shall
  not decompile, reverse engineer or disassemble the Services, and shall not
  link to mirror or frame any portion of the Services or launch any programs or
  scripts for the purpose of scraping, indexing, surveying or otherwise data
  mining any portion of the Services, or unduly burdening, or hindering the
  operation and/or functionality of any aspect of the Services or attempt to
  gain unauthorized access to or impair any aspect of the Services or its
  related systems or networks.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1610;&#1581;&#1592;&#1585;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1593;&#1605;&#1610;&#1604;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605; &#1571;&#1610;&#1577;
  &#1593;&#1604;&#1575;&#1605;&#1577;
  &#1578;&#1580;&#1575;&#1585;&#1610;&#1577; &#1571;&#1608; &#1571;&#1610;
  &#1581;&#1602;&#1608;&#1602; &#1605;&#1604;&#1603;&#1610;&#1577;
  &#1601;&#1603;&#1585;&#1610;&#1577; &#1578;&#1582;&#1589; <b>&#1602;&#1610;&#1605;</b>
  &#1548; &#1608;&#1584;&#1604;&#1603; &#1605;&#1606; &#1571;&#1610;
  &#1580;&#1586;&#1569; &#1605;&#1606;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;&#1548;
  &#1603;&#1605;&#1575; &#1610;&#1581;&#1592;&#1585;
  &#1593;&#1604;&#1610;&#1607; &#1573;&#1593;&#1575;&#1583;&#1577;
  &#1573;&#1606;&#1578;&#1575;&#1580;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1571;&#1608;
  &#1578;&#1593;&#1583;&#1610;&#1604;&#1607;&#1575; &#1571;&#1608;
  &#1573;&#1580;&#1585;&#1575;&#1569; &#1571;&#1610;&#1577;
  &#1593;&#1605;&#1604;&#1610;&#1577; &#1575;&#1593;&#1583;&#1575;&#1583;
  &#1571;&#1608; &#1606;&#1588;&#1585; &#1571;&#1608;
  &#1578;&#1606;&#1601;&#1610;&#1584; &#1571;&#1608;
  &#1575;&#1585;&#1587;&#1575;&#1604; &#1571;&#1608; &#1593;&#1585;&#1590;
  &#1571;&#1608; &#1576;&#1579; &#1571;&#1608; &#1594;&#1610;&#1585;
  &#1584;&#1604;&#1603; &#1605;&#1606; &#1608;&#1587;&#1575;&#1574;&#1604;
  &#1575;&#1587;&#1578;&#1594;&#1604;&#1575;&#1604;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;&#1548;
  &#1607;&#1584;&#1575; &#1605;&#1575; &#1604;&#1605;
  &#1610;&#1587;&#1605;&#1581; <b>&#1602;&#1610;&#1605;</b> '&#1576;&#1584;&#1604;&#1603;
  &#1571;&#1608; &#1610;&#1571;&#1584;&#1606;
  &#1589;&#1585;&#1575;&#1581;&#1577;
  &#1608;&#1576;&#1589;&#1608;&#1585;&#1577;
  &#1603;&#1578;&#1575;&#1576;&#1610;&#1577;&#1548; &#1603;&#1605;&#1575;
  &#1604;&#1575; &#1610;&#1580;&#1608;&#1586; &#1573;&#1580;&#1585;&#1575;&#1569;
  &#1575;&#1593;&#1605;&#1575;&#1604; &#1578;&#1601;&#1603;&#1610;&#1603; &#1604;&#1606;&#1592;&#1575;&#1605;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1571;&#1608;
  &#1571;&#1610;&#1577; &#1607;&#1606;&#1583;&#1587;&#1610;&#1577;
  &#1593;&#1603;&#1587;&#1610;&#1577; &#1571;&#1608;
  &#1573;&#1593;&#1575;&#1583;&#1577; &#1578;&#1588;&#1603;&#1610;&#1604;
  &#1604;&#1607;&#1575;&#1548; &#1571;&#1608;
  &#1575;&#1604;&#1602;&#1610;&#1575;&#1605; &#1576;&#1585;&#1576;&#1591;
  &#1571;&#1610; &#1580;&#1586;&#1569; &#1605;&#1606;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1576;&#1571;&#1610;
  &#1606;&#1592;&#1575;&#1605; &#1593;&#1575;&#1603;&#1587; &#1571;&#1608;
  &#1578;&#1571;&#1591;&#1610;&#1585;&#1607; &#1571;&#1608;
  &#1608;&#1590;&#1593; &#1571;&#1610;&#1577;
  &#1576;&#1585;&#1575;&#1605;&#1580; &#1571;&#1608; &#1606;&#1589;&#1608;&#1589;
  &#1604;&#1594;&#1585;&#1590; &#1573;&#1586;&#1575;&#1604;&#1577;
  &#1571;&#1610; &#1580;&#1586;&#1569; &#1605;&#1606;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1571;&#1608; &#1601;&#1607;&#1585;&#1587;&#1578;&#1607;&#1575;
  &#1571;&#1608; &#1605;&#1587;&#1581;&#1607;&#1575; &#1571;&#1608;
  &#1573;&#1580;&#1585;&#1575;&#1569; &#1594;&#1610;&#1585;
  &#1584;&#1604;&#1603; &#1605;&#1606;
  &#1593;&#1605;&#1604;&#1610;&#1575;&#1578; &#1575;&#1587;&#1578;&#1582;&#1585;&#1575;&#1580;
  &#1604;&#1604;&#1576;&#1610;&#1575;&#1606;&#1575;&#1578;
  &#1605;&#1606;&#1607;&#1548; &#1571;&#1608;
  &#1575;&#1604;&#1602;&#1610;&#1575;&#1605;
  &#1576;&#1578;&#1581;&#1605;&#1610;&#1604; &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1575;&#1604;&#1578;&#1588;&#1594;&#1610;&#1604; &#1608;/ &#1571;&#1608;
  &#1575;&#1604;&#1593;&#1605;&#1604;
  &#1575;&#1604;&#1608;&#1592;&#1610;&#1601;&#1610; &#1604;&#1571;&#1610;
  &#1605;&#1606;&#1581;&#1609; &#1605;&#1606; &#1605;&#1606;&#1575;&#1581;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1576;&#1571;&#1581;&#1605;&#1575;&#1604; &#1604;&#1575;
  &#1605;&#1576;&#1585;&#1585; &#1604;&#1607;&#1575; &#1571;&#1608;
  &#1575;&#1604;&#1602;&#1610;&#1575;&#1605; &#1576;&#1573;&#1593;&#1575;&#1602;&#1578;&#1607;&#1575;
  &#1571;&#1608; &#1605;&#1581;&#1575;&#1608;&#1604;&#1577;
  &#1575;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609;
  &#1573;&#1605;&#1603;&#1575;&#1606;&#1610;&#1577; &#1575;&#1604;&#1583;&#1582;&#1608;&#1604;
  &#1593;&#1604;&#1610;&#1607;&#1575; &#1583;&#1608;&#1606;&#1605;&#1575;
  &#1578;&#1589;&#1585;&#1610;&#1581; &#1576;&#1584;&#1604;&#1603;
  &#1571;&#1608; &#1575;&#1604;&#1593;&#1605;&#1604; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1573;&#1590;&#1585;&#1575;&#1585;
  &#1576;&#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1571;&#1608;
  &#1605;&#1575; &#1610;&#1585;&#1578;&#1576;&#1591; &#1576;&#1607;&#1575;
  &#1605;&#1606; &#1571;&#1606;&#1592;&#1605;&#1577; &#1571;&#1608;
  &#1588;&#1576;&#1603;&#1575;&#1578;. <span style='display:none'>&#1571;&#1610;&#1577;
  </span></span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>The
  User may not post in anywhere else on the site (as determined at our sole
  discretion): solicitation, advertising, foul language, profanities,
  obscenities, culturally offensive material, religiously offensive material,
  critical political content, material that may threaten the public interest or
  national security, defamatory or libelous harassment, or other content that
  may be offensive or indecent.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1604;&#1575;
  &#1610;&#1580;&#1608;&#1586;
  &#1604;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605; &#1575;&#1604;&#1602;&#1610;&#1575;&#1605;
  &#1601;&#1610; &#1571;&#1610; &#1605;&#1608;&#1590;&#1593;
  &#1570;&#1582;&#1585; &#1576;&#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  (&#1608;&#1584;&#1604;&#1603; &#1581;&#1587;&#1576;&#1605;&#1575;
  &#1610;&#1578;&#1602;&#1585;&#1585; &#1608;&#1581;&#1587;&#1576;
  &#1605;&#1591;&#1604;&#1602;
  &#1575;&#1582;&#1578;&#1610;&#1575;&#1585;&#1606;&#1575;)
  &#1576;&#1571;&#1610;&#1577; &#1593;&#1605;&#1604;&#1610;&#1577;
  &#1606;&#1588;&#1585; &#1604;&#1604;&#1570;&#1578;&#1610;:
  &#1571;&#1610;&#1577; &#1583;&#1593;&#1575;&#1610;&#1577;
  &#1604;&#1594;&#1585;&#1590;
  &#1575;&#1604;&#1575;&#1587;&#1578;&#1602;&#1591;&#1575;&#1576;
  &#1571;&#1608; &#1571;&#1610;&#1577; &#1573;&#1593;&#1604;&#1575;&#1606;&#1575;&#1578;
  &#1571;&#1608; &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1604;&#1594;&#1577; &#1605;&#1576;&#1578;&#1584;&#1604;&#1577;
  &#1571;&#1608; &#1605;&#1608;&#1575;&#1583; &#1578;&#1588;&#1608;&#1576;&#1607;&#1575;
  &#1575;&#1604;&#1576;&#1584;&#1575;&#1569;&#1577; &#1571;&#1608;
  &#1578;&#1581;&#1578;&#1608;&#1610; &#1593;&#1604;&#1609;
  &#1587;&#1576;&#1575;&#1576; &#1571;&#1608;
  &#1578;&#1580;&#1575;&#1608;&#1586;&#1575;&#1578; &#1605;&#1606;
  &#1575;&#1604;&#1606;&#1575;&#1581;&#1610;&#1577;
  &#1575;&#1604;&#1579;&#1602;&#1575;&#1601;&#1610;&#1577; &#1571;&#1608;
  &#1575;&#1604;&#1583;&#1610;&#1606;&#1610;&#1577; &#1571;&#1608;
  &#1571;&#1610; &#1605;&#1581;&#1578;&#1608;&#1609; &#1587;&#1610;&#1575;&#1587;&#1610;
  &#1606;&#1602;&#1583;&#1610; &#1571;&#1608; &#1605;&#1608;&#1575;&#1583;
  &#1605;&#1606; &#1588;&#1571;&#1606;&#1607;&#1575;
  &#1573;&#1581;&#1583;&#1575;&#1579; &#1578;&#1607;&#1583;&#1610;&#1583;
  &#1604;&#1604;&#1589;&#1575;&#1604;&#1581;
  &#1575;&#1604;&#1593;&#1575;&#1605; &#1571;&#1608;
  &#1575;&#1604;&#1575;&#1605;&#1606;
  &#1575;&#1604;&#1602;&#1608;&#1605;&#1610; &#1571;&#1608;
  &#1575;&#1604;&#1578;&#1587;&#1576;&#1576; &#1601;&#1610;
  &#1573;&#1587;&#1575;&#1569;&#1577;
  &#1575;&#1604;&#1587;&#1605;&#1593;&#1577; &#1571;&#1608;
  &#1575;&#1604;&#1602;&#1584;&#1601; &#1571;&#1608;
  &#1575;&#1604;&#1578;&#1588;&#1607;&#1610;&#1585; &#1571;&#1608;
  &#1571;&#1610; &#1605;&#1581;&#1578;&#1608;&#1609; &#1570;&#1582;&#1585;
  &#1602;&#1583; &#1578;&#1588;&#1608;&#1576;&#1607;
  &#1575;&#1604;&#1573;&#1607;&#1575;&#1606;&#1577; &#1571;&#1608;
  &#1575;&#1604;&#1576;&#1584;&#1575;&#1569;&#1577; &#1571;&#1608;
  &#1593;&#1583;&#1605; &#1575;&#1604;&#1604;&#1610;&#1575;&#1602;&#1577;. '&#1608;
  &#1610;&#1578;&#1581;&#1605;&#1604; &#1603;&#1604;
  &#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  &#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577; &#1605;&#1575;
  &#1610;&#1602;&#1608;&#1605;
  &#1576;&#1603;&#1578;&#1575;&#1576;&#1578;&#1607; </span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-EG style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Third Party Services</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1594;&#1610;&#1585;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:95%'><span style=' 
;line-height:95%;
  font-family:"brandfont"'>The Services may be made available or
  accessed through Third-Party Service Providers and content (including
  advertising) that Clever does not control. You acknowledge that different terms
  of use and privacy policies may apply to you for use of such third-party
  services and content. Clever does not endorse such third-party services and
  content and in no event shall Clever be responsible or liable for any of the
  services of these Third-Party Service Providers.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1602;&#1583;
  &#1610;&#1578;&#1605; &#1578;&#1608;&#1601;&#1610;&#1585;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1571;&#1608;
  &#1575;&#1604;&#1583;&#1582;&#1608;&#1604;
  &#1593;&#1604;&#1610;&#1607;&#1575; &#1605;&#1606;
  &#1582;&#1604;&#1575;&#1604; &#1605;&#1602;&#1583;&#1605;&#1610;
  &#1582;&#1583;&#1605;&#1577; &#1605;&#1606;
  &#1575;&#1604;&#1594;&#1610;&#1585; &#1608;&#1603;&#1584;&#1575;
  &#1593;&#1576;&#1585; &#1605;&#1581;&#1578;&#1608;&#1609;
  (&#1608;&#1605;&#1606; &#1584;&#1604;&#1603; &#1571;&#1610;
  &#1605;&#1581;&#1578;&#1608;&#1609;
  &#1573;&#1593;&#1604;&#1575;&#1606;&#1610;) &#1604;&#1575; &#1610;&#1582;&#1590;&#1593;
  &#1604;&#1606;&#1591;&#1575;&#1602; &#1587;&#1610;&#1591;&#1585;&#1577;
  &#1571;&#1608; &#1578;&#1581;&#1603;&#1605; &#1602;&#1610;&#1605;.
  &#1608;&#1604;&#1607;&#1584;&#1575; &#1610;&#1603;&#1608;&#1606;
  &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1573;&#1602;&#1585;&#1575;&#1585; &#1576;&#1571;&#1606;
  &#1607;&#1606;&#1575;&#1603; &#1588;&#1585;&#1608;&#1591;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605; &#1605;&#1582;&#1578;&#1604;&#1601;&#1577;
  &#1576;&#1604; &#1608;&#1587;&#1610;&#1575;&#1587;&#1575;&#1578;
  &#1582;&#1589;&#1608;&#1589;&#1610;&#1577; &#1571;&#1582;&#1585;&#1609;
  &#1602;&#1583; &#1578;&#1587;&#1585;&#1610;
  &#1593;&#1604;&#1610;&#1603;&#1605; &#1581;&#1610;&#1606;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1582;&#1583;&#1605;&#1575;&#1578; &#1584;&#1604;&#1603;
  &#1575;&#1604;&#1594;&#1610;&#1585; &#1608;&#1584;&#1604;&#1603;
  &#1575;&#1604;&#1605;&#1581;&#1578;&#1608;&#1609;. &#1573;&#1604;&#1575;
  &#1571;&#1606; &#1602;&#1610;&#1605; &#1604;&#1575;
  &#1578;&#1605;&#1606;&#1581; &#1571;&#1610;&#1605;&#1575;
  &#1578;&#1589;&#1583;&#1610;&#1602; &#1593;&#1604;&#1609;
  &#1582;&#1583;&#1605;&#1575;&#1578; &#1575;&#1604;&#1594;&#1610;&#1585;
  &#1571;&#1608; &#1605;&#1575; &#1610;&#1582;&#1589;&#1607;&#1605;
  &#1605;&#1606; &#1605;&#1581;&#1578;&#1608;&#1609;&#1548; &#1576;&#1604;
  &#1608;&#1604;&#1575; &#1578;&#1578;&#1581;&#1605;&#1604;
  &#1602;&#1610;&#1605; '&#1571;&#1610;&#1577;
  &#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577; &#1571;&#1608;
  &#1575;&#1604;&#1578;&#1586;&#1575;&#1605; &#1605;&#1606; &#1571;&#1610;
  &#1606;&#1608;&#1593; &#1593;&#1606; &#1571;&#1610;&#1577;
  &#1582;&#1583;&#1605;&#1577; &#1605;&#1606; &#1578;&#1604;&#1603;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1582;&#1575;&#1589;&#1577;
  &#1576;&#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1605;&#1606; &#1575;&#1604;&#1594;&#1610;&#1585;.
  </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Privacy and Confidentiality</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1575;&#1604;&#1587;&#1585;&#1610;&#1577;
  &#1608;&#1575;&#1604;&#1582;&#1589;&#1608;&#1589;&#1610;&#1577;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>User(s)
  shall not disclose any information received under the contract of service
  with Clever to any third party. Access to any information which pertains to
  business of Clever shall be kept confidential to the extent it might adversely
  impact Clever`s business. User(s) shall be liable to indemnify Clever against any
  loss of business or reputation due to the act of the User(s).</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1604;&#1575;
  &#1610;&#1580;&#1608;&#1586;
  &#1604;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  (&#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;&#1606;)
  &#1575;&#1604;&#1573;&#1601;&#1589;&#1575;&#1581;
  &#1604;&#1604;&#1594;&#1610;&#1585; &#1593;&#1606; &#1571;&#1610;&#1577;
  &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1613;
  &#1578;&#1585;&#1583; &#1575;&#1604;&#1610;&#1607;
  &#1576;&#1605;&#1608;&#1580;&#1576; &#1593;&#1602;&#1583;
  &#1582;&#1583;&#1605;&#1577; &#1605;&#1615;&#1600;&#1576;&#1585;&#1605;
  &#1605;&#1593; &#1602;&#1610;&#1605; &#1548; &#1576;&#1604; &#1608;&#1610;&#1606;&#1576;&#1594;&#1610;
  &#1571;&#1606; &#1610;&#1578;&#1605;
  &#1575;&#1604;&#1583;&#1582;&#1608;&#1604; &#1593;&#1604;&#1609;
  &#1571;&#1610;&#1577;
  &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1613;
  &#1578;&#1585;&#1578;&#1576;&#1591;
  &#1576;&#1571;&#1593;&#1605;&#1575;&#1604; &#1602;&#1610;&#1605; '&#1593;&#1604;&#1609;
  &#1606;&#1581;&#1608; &#1578;&#1588;&#1605;&#1604;&#1607;
  &#1575;&#1604;&#1587;&#1585;&#1610;&#1577;&#1548;
  &#1608;&#1584;&#1604;&#1603; &#1601;&#1610;
  &#1575;&#1604;&#1581;&#1583;&#1608;&#1583; &#1575;&#1604;&#1578;&#1610;
  &#1602;&#1583; &#1578;&#1572;&#1579;&#1585; &#1587;&#1604;&#1576;&#1575;
  &#1593;&#1604;&#1609; &#1571;&#1593;&#1605;&#1575;&#1604; &#1602;&#1610;&#1605;.
  &#1608;&#1610;&#1578;&#1581;&#1605;&#1604;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  (&#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;&#1606;)
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1593;&#1606; &#1578;&#1593;&#1608;&#1610;&#1590; &#1602;&#1610;&#1605; '&#1593;&#1606;
  &#1571;&#1610;&#1577; &#1582;&#1587;&#1575;&#1585;&#1577; &#1601;&#1610;
  &#1575;&#1604;&#1593;&#1605;&#1604; &#1571;&#1608; &#1601;&#1610;
  &#1575;&#1604;&#1587;&#1605;&#1593;&#1577;&#1548; &#1573;&#1606;
  &#1581;&#1583;&#1579; &#1584;&#1604;&#1603; &#1580;&#1585;&#1575;&#1569;
  &#1571;&#1610; &#1578;&#1589;&#1585;&#1601; &#1610;&#1602;&#1593;
  &#1605;&#1606; &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  (&#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;&#1606;). </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Disclaimer</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1575;&#1604;&#1575;&#1576;&#1585;&#1575;&#1569;
  &#1605;&#1606;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577; </span></b></p>
  </td>
 </tr>
 <tr style='height:7.45pt'>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt;height:7.45pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt;height:7.45pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:95%'><span style='line-height:95%;
  font-family:"brandfont"'>THE SERVICES ARE PROVIDED ''AS IS'' AND ''AS
  AVAILABLE.'' Clever 'DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES, EXPRESS,
  IMPLIED OR STATUTORY, NOT EXPRESSLY SET OUT IN THESE TERMS, INCLUDING THE
  IMPLIED WARRANTIES OF SUPPLIER ABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NON-INFRINGEMENT, IN ADDITION, Clever MAKES NO REPRESENTATION, WARRANTY, OR
  GUARANTEE REGARDING THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY OR
  AVAILABILITY OF THE SERVICES OR ANY SERVICES REQUESTED THROUGH THE USE OF THE
  SERVICES, OR THAT THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE. Clever DOES
  NOT GUARANTEE THE QUALITY, SUITABILITY OR ABILITY OF THE SARVICES . YOU AGREE
  THAT THE ENTIRE RISK ARISING FROM THE USE OF SERVICES REMAINS SOLELY WITH
  YOU, TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:kashida;text-kashida:20%;line-height:97%;
  direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-family:"brandfont"'>&#1610;&#1578;&#1605;
  &#1578;&#1602;&#1583;&#1610;&#1605;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &quot;&#1603;&#1605;&#1575;
  &#1607;&#1610;&quot; &#1605;&#1593;&#1585;&#1608;&#1590;&#1577; &#1608;&quot;&#1581;&#1587;&#1576;&#1605;&#1575;
  &#1607;&#1608; &#1605;&#1578;&#1575;&#1581;&quot;
  &#1605;&#1606;&#1607;&#1575;. &#1608;&#1578;&#1593;&#1601;&#1609; &#1602;&#1610;&#1605;
  '&#1576;&#1604; &#1608;&#1578;&#1576;&#1585;&#1571; &#1605;&#1606;
  &#1603;&#1575;&#1601;&#1577;
  &#1575;&#1604;&#1575;&#1602;&#1585;&#1575;&#1585;&#1575;&#1578;
  &#1608;&#1575;&#1604;&#1590;&#1605;&#1575;&#1606;&#1575;&#1578;
  &#1575;&#1604;&#1589;&#1585;&#1610;&#1581;&#1577;
  &#1605;&#1606;&#1607;&#1575; &#1571;&#1608; &#1575;&#1604;&#1590;&#1605;&#1606;&#1610;&#1577;
  &#1571;&#1608;
  &#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1577;
  &#1575;&#1604;&#1578;&#1610; &#1604;&#1605; &#1610;&#1585;&#1583;
  &#1606;&#1589;&#1607;&#1575; &#1589;&#1585;&#1575;&#1581;&#1577;
  &#1601;&#1610; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;&#1548;
  &#1608;&#1605;&#1606; &#1584;&#1604;&#1603; &#1575;&#1604;&#1590;&#1605;&#1575;&#1606;&#1575;&#1578;&#1613;
  &#1575;&#1604;&#1605;&#1593;&#1585;&#1608;&#1590;&#1577;
  &#1590;&#1605;&#1606;&#1575; &#1576;&#1588;&#1571;&#1606;
  &#1602;&#1583;&#1585;&#1577; &#1575;&#1604;&#1605;&#1608;&#1585;&#1583;
  &#1608;&#1605;&#1604;&#1575;&#1569;&#1605;&#1577;
  &#1582;&#1583;&#1605;&#1575;&#1578;&#1607; &#1604;&#1594;&#1585;&#1590;
  &#1605;&#1593;&#1610;&#1606; &#1608;&#1593;&#1583;&#1605;
  &#1575;&#1582;&#1604;&#1575;&#1604;&#1607;. &#1603;&#1605;&#1575;
  &#1604;&#1575; &#1610;&#1578;&#1602;&#1583;&#1605; &#1602;&#1610;&#1605; '&#1576;&#1571;&#1610;
  &#1573;&#1602;&#1585;&#1575;&#1585; &#1571;&#1608;
  &#1590;&#1605;&#1575;&#1606; &#1576;&#1588;&#1571;&#1606;
  &#1605;&#1589;&#1583;&#1575;&#1602;&#1610;&#1577;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1571;&#1608;
  &#1583;&#1602;&#1577;
  &#1605;&#1608;&#1575;&#1593;&#1610;&#1583;&#1607;&#1575; &#1571;&#1608;
  &#1580;&#1608;&#1583;&#1578;&#1607;&#1575; &#1571;&#1608;
  &#1575;&#1587;&#1578;&#1583;&#1575;&#1605;&#1578;&#1607;&#1575;
  &#1571;&#1608; &#1578;&#1608;&#1575;&#1601;&#1585;&#1607;&#1575;
  &#1607;&#1610; &#1571;&#1608; &#1571;&#1610;&#1577;
  &#1582;&#1583;&#1605;&#1575;&#1578; &#1578;&#1591;&#1604;&#1576;
  &#1605;&#1606; &#1582;&#1604;&#1575;&#1604;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1582;&#1583;&#1605;&#1575;&#1578; &#1602;&#1610;&#1605; &#1548;
  &#1603;&#1605;&#1575; &#1604;&#1575; &#1578;&#1590;&#1605;&#1606; &#1602;&#1610;&#1605;
  '&#1593;&#1583;&#1605; &#1578;&#1593;&#1585;&#1590;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1604;&#1604;&#1578;&#1593;&#1591;&#1610;&#1604; &#1571;&#1608;
  &#1582;&#1604;&#1608;&#1607;&#1575; &#1605;&#1606;
  &#1575;&#1604;&#1582;&#1591;&#1571;. &#1608;&#1604;&#1575;
  &#1578;&#1605;&#1606;&#1581; &#1602;&#1610;&#1605;' &#1571;&#1610;&#1605;&#1575;
  &#1590;&#1605;&#1575;&#1606; &#1576;&#1588;&#1571;&#1606;
  &#1580;&#1608;&#1583;&#1577;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; '&#1608;&#1605;&#1604;&#1575;&#1569;&#1605;&#1578;&#1607;&#1605;
  &#1608;&#1602;&#1583;&#1585;&#1575;&#1578;&#1607;&#1605;.
  &#1608;&#1604;&#1607;&#1584;&#1575; &#1610;&#1603;&#1608;&#1606;
  &#1593;&#1604;&#1610;&#1603;&#1605; &#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
  &#1593;&#1604;&#1609; &#1578;&#1581;&#1605;&#1604;&#1603;&#1605;
  &#1581;&#1589;&#1585;&#1575; &#1603;&#1575;&#1605;&#1604;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1575;&#1604;&#1606;&#1575;&#1588;&#1574;&#1577; &#1593;&#1606;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1603;&#1605;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;&#1548;
  &#1608;&#1584;&#1604;&#1603; &#1573;&#1604;&#1609;
  &#1571;&#1602;&#1589;&#1609; &#1581;&#1583; &#1610;&#1587;&#1605;&#1581;
  &#1576;&#1607; &#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;
  &#1608;&#1575;&#1580;&#1576;
  &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style='font-family:"brandfont"'>THE
  INFORMATION CONTAINED IN THE WEBSITE IS FOR GENERAL INFORMATION PURPOSES
  ONLY. THE INFORMATION PROVIDED BY Clever WHILE WE ENDEAVOR TO KEEP THE
  INFORMATION UP TO DATE AND CORRECT, WE MAKE NO REPRESENTATIONS OR WARRANTIES
  OF ANY KIND, EXPRESS OR IMPLIED, ABOUT THE COMPLETENESS, ACCURACY,
  RELIABILITY, SUITABILITY OR AVAILABILITY WITH RESPECT TO THE WEBSITE OR THE
  INFORMATION, SERVICES, OR RELATED GRAPHICS CONTAINED ON THE WEBSITE FOR ANY
  PURPOSE. ANY RELIANCE YOU PLACE ON SUCH INFORMATION IS THEREFORE STRICTLY AT
  YOUR OWN RISK.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:kashida;text-kashida:10%;line-height:107%;
  direction:rtl;unicode-bidi:embed'><span lang=AR-EG style='font-family:"brandfont"'>&#1608;&#1578;&#1593;&#1578;&#1576;&#1585;
  &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1601;&#1610;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1607;&#1610; &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1605;&#1593;&#1585;&#1608;&#1590;&#1577;
  &#1604;&#1571;&#1594;&#1585;&#1575;&#1590; &#1593;&#1575;&#1605;&#1577;
  &#1601;&#1602;&#1591;. &#1571;&#1605;&#1575;
  &#1576;&#1575;&#1604;&#1606;&#1587;&#1576;&#1577;
  &#1604;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1578;&#1610; &#1578;&#1585;&#1583; &#1605;&#1606;
  &#1580;&#1575;&#1606;&#1576; &#1602;&#1610;&#1605; &#1548;
  &#1601;&#1606;&#1581;&#1606; &#1606;&#1576;&#1584;&#1604;
  &#1576;&#1588;&#1571;&#1606;&#1607;&#1575;
  &#1580;&#1607;&#1608;&#1583;&#1606;&#1575;
  &#1604;&#1604;&#1593;&#1605;&#1604; &#1593;&#1604;&#1609; &#1571;&#1606;
  &#1578;&#1592;&#1604; &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1613;
  &#1605;&#1581;&#1583;&#1579;&#1577;
  &#1608;&#1589;&#1581;&#1610;&#1581;&#1577;&#1548; &#1573;&#1604;&#1575;
  &#1571;&#1606;&#1606;&#1575; &#1604;&#1575;
  &#1606;&#1578;&#1602;&#1583;&#1605; &#1576;&#1571;&#1610;&#1577;
  &#1573;&#1602;&#1585;&#1575;&#1585;&#1575;&#1578;&#1613; &#1571;&#1608;
  &#1590;&#1605;&#1575;&#1606;&#1575;&#1578; &#1605;&#1606; &#1571;&#1610;
  &#1606;&#1608;&#1593;&#1548; &#1587;&#1608;&#1575;&#1569;
  &#1593;&#1604;&#1609; &#1606;&#1581;&#1608; &#1588;&#1601;&#1607;&#1610;
  &#1571;&#1608; &#1590;&#1605;&#1606;&#1610;&#1548;
  &#1608;&#1584;&#1604;&#1603; &#1605;&#1606; &#1581;&#1610;&#1579;
  &#1575;&#1603;&#1578;&#1605;&#1575;&#1604; &#1571;&#1608;
  &#1583;&#1602;&#1577; &#1571;&#1608;
  &#1605;&#1589;&#1583;&#1575;&#1602;&#1610;&#1577; &#1571;&#1608;
  &#1605;&#1604;&#1575;&#1569;&#1605;&#1577; &#1571;&#1608;
  &#1578;&#1608;&#1575;&#1601;&#1585;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1571;&#1608; &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1571;&#1608; &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1571;&#1608; &#1605;&#1575; &#1610;&#1585;&#1578;&#1576;&#1591;
  &#1576;&#1607;&#1575; &#1605;&#1606; &#1585;&#1587;&#1608;&#1605;
  &#1580;&#1585;&#1575;&#1601;&#1610;&#1603;
  &#1608;&#1575;&#1585;&#1583;&#1577; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
  &#1576;&#1575;&#1604;&#1606;&#1587;&#1576;&#1577; &#1604;&#1571;&#1610; &#1594;&#1585;&#1590;.
  &#1608;&#1576;&#1575;&#1604;&#1578;&#1575;&#1604;&#1610;
  &#1610;&#1603;&#1608;&#1606;
  &#1575;&#1593;&#1578;&#1605;&#1575;&#1583;&#1603;&#1605;
  &#1593;&#1604;&#1609; &#1578;&#1604;&#1603;
  &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1607;&#1608; &#1571;&#1605;&#1585; &#1610;&#1602;&#1593;
  &#1593;&#1604;&#1609;
  &#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1578;&#1603;&#1605;
  &#1575;&#1604;&#1582;&#1575;&#1589;&#1577;
  &#1578;&#1605;&#1575;&#1605;&#1575;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style='font-family:"brandfont"'>ALL
  ACCOUNTS REGISTERED WITH Clever 'ARE CONTINUOUSLY TRACKED FOR SECURITY PURPOSES
  AND PERFORMANCE REASONS. IT IS EXPRESSLY MADE CLEAR TO YOU HEREBY THAT Clever 'DOES
  NOT OWN ANY ACCOUNT OF ANY KIND.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1578;&#1593;&#1578;&#1576;&#1585;
  &#1580;&#1605;&#1610;&#1593;
  &#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&#1575;&#1578;
  &#1575;&#1604;&#1605;&#1587;&#1580;&#1604;&#1577; &#1593;&#1604;&#1609; &#1602;&#1610;&#1605;
  '&#1607;&#1610; &#1581;&#1587;&#1575;&#1576;&#1575;&#1578;
  &#1605;&#1581;&#1604; &#1605;&#1578;&#1575;&#1576;&#1593;&#1577;
  &#1605;&#1587;&#1578;&#1605;&#1585;&#1577;&#1548;
  &#1608;&#1584;&#1604;&#1603; &#1604;&#1571;&#1594;&#1585;&#1575;&#1590;
  &#1571;&#1605;&#1606;&#1610;&#1577; &#1608;&#1604;&#1571;&#1587;&#1576;&#1575;&#1576;&#1613;
  &#1578;&#1578;&#1593;&#1604;&#1602;
  &#1576;&#1575;&#1604;&#1571;&#1583;&#1575;&#1569;. &#1603;&#1605;&#1575;
  &#1571;&#1606;&#1606;&#1575; &#1608;&#1576;&#1605;&#1608;&#1580;&#1576;
  &#1607;&#1584;&#1575; &#1575;&#1604;&#1605;&#1581;&#1585;&#1585;
  &#1573;&#1584; &#1606;&#1608;&#1590;&#1581; &#1604;&#1603;&#1605;
  &#1589;&#1585;&#1575;&#1581;&#1577; &#1571;&#1606; &#1602;&#1610;&#1605; '&#1604;&#1575;
  &#1578;&#1605;&#1604;&#1603; &#1571;&#1610; &#1581;&#1587;&#1575;&#1576;
  &#1605;&#1606; &#1571;&#1610; &#1606;&#1608;&#1593;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Limitations of Liability</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1581;&#1583;&#1608;&#1583;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577; </span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>Clever
  shall not be liable to you for indirect, incidental, special, exemplary,
  punitive, or consequential damages, lost data, or damage to your bank
  account, even if Clever has been advised of the possibility of such damages. Clever
  shall not be liable for any damages, liability or losses incurred by you
  arising out of your use of or reliance on the Services or your inability to
  access or use the Services or any transaction or relationship between you and
  any The users, even if Clever has been advised of the possibility of such damages.
  Clever shall not be liable for delay or failure in performance resulting from
  causes beyond Clever`s reasonable control. These limitations do not purport to
  limit liability that cannot be excluded under the applicable laws.</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-EG style='font-family:"brandfont"'>&#1604;&#1575;
  &#1578;&#1578;&#1581;&#1605;&#1604; &#1602;&#1610;&#1605;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1578;&#1580;&#1575;&#1607;&#1603;&#1605; &#1593;&#1606; &#1571;&#1610;&#1577;
  &#1571;&#1590;&#1585;&#1575;&#1585;&#1613; &#1594;&#1610;&#1585;
  &#1605;&#1576;&#1575;&#1588;&#1585;&#1577; &#1571;&#1608;
  &#1571;&#1590;&#1585;&#1575;&#1585; &#1593;&#1575;&#1585;&#1590;&#1577;
  &#1571;&#1608; &#1582;&#1575;&#1589;&#1577; &#1571;&#1608;
  &#1580;&#1586;&#1575;&#1574;&#1610;&#1577; &#1571;&#1608;
  &#1593;&#1602;&#1575;&#1576;&#1610;&#1577; &#1571;&#1608;
  &#1578;&#1576;&#1593;&#1610;&#1577;&#1548; &#1608;&#1605;&#1606;
  &#1584;&#1604;&#1603; &#1601;&#1602;&#1583; &#1575;&#1604;&#1576;&#1610;&#1575;&#1606;&#1575;&#1578;
  &#1571;&#1608; &#1575;&#1610; &#1575;&#1590;&#1585;&#1575;&#1585;
  &#1576;&#1581;&#1587;&#1575;&#1576;&#1603;&#1605;
  &#1575;&#1604;&#1588;&#1582;&#1589;&#1610; &#1601;&#1610;
  &#1575;&#1604;&#1576;&#1606;&#1603;&#1548; &#1608;&#1584;&#1604;&#1603;
  &#1581;&#1578;&#1609; &#1604;&#1608; &#1603;&#1575;&#1606;&#1578;
  &#1602;&#1610;&#1605; &#1602;&#1583; &#1575;&#1581;&#1610;&#1591;&#1578;
  &#1593;&#1604;&#1605;&#1611;&#1575;
  &#1576;&#1575;&#1581;&#1578;&#1605;&#1575;&#1604; &#1608;&#1602;&#1608;&#1593;
  &#1607;&#1584;&#1607; &#1575;&#1604;&#1575;&#1590;&#1585;&#1575;&#1585;.
  &#1608;&#1604;&#1575; &#1578;&#1578;&#1581;&#1605;&#1604;
  &#1602;&#1610;&#1605;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577; &#1593;&#1606;
  &#1571;&#1610;&#1577; &#1571;&#1590;&#1585;&#1575;&#1585; &#1571;&#1608;
  &#1571;&#1610;&#1577; &#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1571;&#1608; &#1571;&#1610;&#1577; &#1582;&#1587;&#1575;&#1574;&#1585;
  &#1578;&#1578;&#1581;&#1605;&#1604;&#1608;&#1606;&#1607;&#1575;
  &#1608;&#1578;&#1606;&#1588;&#1571; &#1605;&#1606;
  &#1608;&#1575;&#1602;&#1593;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1603;&#1605;
  &#1604;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1571;&#1608;
  &#1575;&#1593;&#1578;&#1605;&#1575;&#1583;&#1603;&#1605;
  &#1593;&#1604;&#1610;&#1607;&#1575; &#1571;&#1608;
  &#1580;&#1585;&#1575;&#1569; &#1593;&#1583;&#1605;
  &#1578;&#1605;&#1603;&#1606;&#1603;&#1605; &#1605;&#1606; &#1575;&#1604;&#1583;&#1582;&#1608;&#1604;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1571;&#1608;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1607;&#1575;
  &#1571;&#1608; &#1605;&#1606; &#1608;&#1575;&#1602;&#1593; &#1571;&#1610;&#1577;
  &#1605;&#1593;&#1575;&#1605;&#1604;&#1577; &#1580;&#1585;&#1578;
  &#1571;&#1608; &#1593;&#1604;&#1575;&#1602;&#1577;
  &#1606;&#1588;&#1571;&#1578; &#1576;&#1610;&#1606;&#1603;&#1605;
  &#1608;&#1576;&#1610;&#1606; &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  &#1548; &#1608;&#1584;&#1604;&#1603; &#1581;&#1578;&#1609; &#1604;&#1608;
  &#1578;&#1605; &#1573;&#1581;&#1575;&#1591;&#1577; &#1602;&#1610;&#1605;'
  &#1576;&#1573;&#1605;&#1603;&#1575;&#1606;&#1610;&#1577; &#1608;&#1602;&#1608;&#1593;
  &#1578;&#1604;&#1603; &#1575;&#1604;&#1575;&#1590;&#1585;&#1575;&#1585;.
  &#1603;&#1605;&#1575; &#1604;&#1575; &#1578;&#1578;&#1581;&#1605;&#1604;
  &#1602;&#1610;&#1605;' &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1593;&#1606; &#1571;&#1610;&#1577; &#1581;&#1575;&#1604;&#1577;
  &#1578;&#1571;&#1582;&#1610;&#1585; &#1571;&#1608;
  &#1573;&#1582;&#1601;&#1575;&#1602; &#1601;&#1610;
  &#1575;&#1604;&#1578;&#1606;&#1601;&#1610;&#1584;&#1548; &#1608;&#1603;&#1575;&#1606;
  &#1584;&#1604;&#1603; &#1610;&#1585;&#1580;&#1593;
  &#1604;&#1571;&#1587;&#1576;&#1575;&#1576;
  &#1582;&#1575;&#1585;&#1580;&#1577; &#1593;&#1606;
  &#1606;&#1591;&#1575;&#1602; &#1575;&#1604;&#1578;&#1581;&#1603;&#1605;
  &#1575;&#1604;&#1605;&#1593;&#1602;&#1608;&#1604;
  &#1604;&#1602;&#1610;&#1605; .' &#1608;&#1604;&#1575;
  &#1610;&#1580;&#1608;&#1586; &#1575;&#1593;&#1578;&#1576;&#1575;&#1585;
  &#1581;&#1583;&#1608;&#1583;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577; &#1575;&#1604;&#1605;&#1584;&#1603;&#1608;&#1585;&#1577;
  &#1581;&#1583;&#1608;&#1583;&#1575; &#1578;&#1602;&#1610;&#1583;
  &#1571;&#1608; &#1578;&#1581;&#1583; &#1605;&#1606; &#1571;&#1610;&#1577;
  &#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577; &#1604;&#1575;
  &#1578;&#1587;&#1578;&#1579;&#1606;&#1610;&#1607;&#1575;
  &#1575;&#1604;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606;
  &#1608;&#1575;&#1580;&#1576;&#1577;
  &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:90%'><span style=' 
;line-height:90%;
  font-family:"brandfont"'>, Clever shall not be held liable in any
  case or for any matter related to the Supplier and/or the Supplier Products
  and/ or the brand of such products and its authenticity. However, , Clever will
  use it best endeavors to ensure the authenticity of such products.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:105%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1604;&#1575;
  &#1578;&#1578;&#1581;&#1605;&#1604; &#1602;&#1610;&#1605;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577; &#1576;&#1571;&#1610;
  &#1581;&#1575;&#1604; &#1605;&#1606;
  &#1575;&#1604;&#1571;&#1581;&#1608;&#1575;&#1604; &#1571;&#1608;
  &#1593;&#1606; &#1571;&#1610; &#1608;&#1590;&#1593; &#1605;&#1606;
  &#1575;&#1604;&#1571;&#1608;&#1590;&#1575;&#1593;
  &#1575;&#1604;&#1605;&#1585;&#1578;&#1576;&#1591;&#1577;
  &#1576;&#1575;&#1604;&#1605;&#1608;&#1585;&#1583; &#1608;/&#1571;&#1608;
  &#1605;&#1606;&#1578;&#1580;&#1575;&#1578;
  &#1575;&#1604;&#1605;&#1608;&#1585;&#1583; &#1608;/&#1571;&#1608;
  &#1591;&#1585;&#1575;&#1586;
  &#1575;&#1604;&#1605;&#1606;&#1578;&#1580;&#1575;&#1578; &#1575;&#1608;
  &#1593;&#1604;&#1575;&#1605;&#1578;&#1607;&#1575; &#1571;&#1608;
  &#1605;&#1589;&#1583;&#1575;&#1602;&#1610;&#1578;&#1607;&#1575;&#1548;
  &#1573;&#1604;&#1575; &#1571;&#1606; &#1602;&#1610;&#1605;
  &#1587;&#1608;&#1601; &#1578;&#1576;&#1584;&#1604;
  &#1602;&#1589;&#1575;&#1585;&#1609;
  &#1580;&#1607;&#1608;&#1583;&#1607;&#1575;
  &#1604;&#1590;&#1605;&#1575;&#1606;
  &#1605;&#1589;&#1583;&#1575;&#1602;&#1610;&#1577; &#1578;&#1604;&#1603;
  &#1575;&#1604;&#1605;&#1606;&#1578;&#1580;&#1575;&#1578;
  &#1608;&#1571;&#1589;&#1604;&#1610;&#1578;&#1607;&#1575;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:95%'><span style=' 
;line-height:95%;
  font-family:"brandfont"'>Users and the Suppliers agree that Clever
  shall not participate in disputes between both parties in any case. Whereas,
  by using the said Services the User acknowledge the potential risks or
  mistakes of the Third-Party Service Providers. Accordingly, the use of the
  Services is at the User's risk and judgment, and Clever shall have no liability
  arising from or in any way related to the User's transactions or relationship
  with the Suppliers or the Third-Party Service Providers.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:95%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1610;&#1608;&#1575;&#1601;&#1602;
  &#1603;&#1604; &#1605;&#1606;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;&#1610;&#1606; &#1608;&#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1605;&#1606;
  &#1575;&#1604;&#1594;&#1610;&#1585; &#1593;&#1604;&#1609;
  &#1571;&#1604;&#1575; &#1578;&#1588;&#1575;&#1585;&#1603; &#1602;&#1610;&#1605;
  '&#1601;&#1610; &#1571;&#1610;&#1577;
  &#1606;&#1586;&#1575;&#1593;&#1575;&#1578;&#1613; &#1576;&#1610;&#1606;
  &#1603;&#1604;&#1575; &#1575;&#1604;&#1591;&#1585;&#1601;&#1610;&#1606;
  &#1576;&#1571;&#1610; &#1581;&#1575;&#1604; &#1605;&#1606; &#1575;&#1604;&#1571;&#1581;&#1608;&#1575;&#1604;.
  &#1608;&#1581;&#1610;&#1579; &#1571;&#1606;&#1607;
  &#1576;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1605;&#1584;&#1603;&#1608;&#1585;&#1577;
  &#1610;&#1603;&#1608;&#1606;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605; &#1602;&#1583;
  &#1571;&#1602;&#1585; &#1576;&#1575;&#1581;&#1578;&#1605;&#1575;&#1604;
  &#1608;&#1602;&#1608;&#1593; &#1605;&#1582;&#1575;&#1591;&#1585; &#1605;&#1606;
  &#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1605;&#1606;
  &#1575;&#1604;&#1594;&#1610;&#1585; '&#1571;&#1608;
  &#1571;&#1582;&#1591;&#1575;&#1569; &#1605;&#1606;&#1607;&#1605; &#1548;
  &#1576;&#1575;&#1604;&#1578;&#1575;&#1604;&#1610;
  &#1610;&#1603;&#1608;&#1606;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1571;&#1608;
  &#1571;&#1605;&#1585; &#1610;&#1602;&#1593; &#1593;&#1604;&#1609;
  &#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  &#1608;&#1608;&#1601;&#1602; &#1602;&#1585;&#1575;&#1585;&#1607;&#1548;
  &#1608;&#1604;&#1575; &#1578;&#1578;&#1581;&#1605;&#1604; &#1602;&#1610;&#1605;
  &#1571;&#1610;&#1605;&#1575;
  &#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1578;&#1606;&#1588;&#1571; &#1571;&#1608;
  &#1578;&#1585;&#1578;&#1576;&#1591; &#1593;&#1604;&#1609; &#1571;&#1610;
  &#1606;&#1581;&#1608;&#1613; &#1571;&#1610;&#1575; &#1603;&#1575;&#1606;
  &#1576;&#1605;&#1593;&#1575;&#1605;&#1604;&#1575;&#1578;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605; &#1571;&#1608;
  &#1593;&#1604;&#1575;&#1602;&#1575;&#1578;&#1607; &#1605;&#1593;
  &#1575;&#1604;&#1605;&#1608;&#1585;&#1583;&#1610;&#1606; &#1571;&#1608;
  &#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1605;&#1606;
  &#1575;&#1604;&#1594;&#1610;&#1585;.' </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>Clever
  shall be entitled to disclose any User information it may possess to
  affiliated corporate entities within its group of companies, or if required
  by law, or if requested or directed to do so by any official government body.
  Clever will do so at its sole discretion or as We may determine to be suitable
  or in Our best interest to do so.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1610;&#1603;&#1608;&#1606;
  &#1605;&#1606; &#1581;&#1602; &#1602;&#1610;&#1605;
  &#1575;&#1604;&#1573;&#1601;&#1589;&#1575;&#1581; &#1604;&#1571;&#1610;
  &#1605;&#1616;&#1605;&#1617;&#1614;&#1575; &#1601;&#1610;
  &#1581;&#1610;&#1575;&#1586;&#1578;&#1607;
  &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578; &#1578;&#1582;&#1589;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  &#1608;&#1584;&#1604;&#1603; &#1573;&#1604;&#1609;
  &#1575;&#1604;&#1603;&#1610;&#1575;&#1606;&#1575;&#1578;
  &#1575;&#1604;&#1605;&#1572;&#1587;&#1587;&#1610;&#1577;
  &#1575;&#1604;&#1578;&#1575;&#1576;&#1593;&#1577; &#1604;&#1602;&#1610;&#1605;
  &#1608;&#1575;&#1604;&#1603;&#1575;&#1574;&#1606;&#1577;
  &#1590;&#1605;&#1606; &#1605;&#1580;&#1605;&#1608;&#1593;&#1577;
  &#1588;&#1585;&#1603;&#1575;&#1578;&#1607; &#1575;&#1608; &#1571;&#1606;
  &#1575;&#1587;&#1578;&#1604;&#1586;&#1605; &#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;
  &#1584;&#1604;&#1603; &#1571;&#1608; &#1573;&#1606;
  &#1591;&#1600;&#1615;&#1600;&#1604;&#1576; &#1571;&#1608;
  &#1589;&#1583;&#1585; &#1578;&#1608;&#1580;&#1610;&#1607;
  &#1576;&#1584;&#1604;&#1603; &#1605;&#1606; &#1580;&#1575;&#1606;&#1576;
  &#1571;&#1610;&#1577; &#1580;&#1607;&#1577;
  &#1581;&#1603;&#1608;&#1605;&#1610;&#1577;
  &#1585;&#1587;&#1605;&#1610;&#1577;.
  &#1608;&#1587;&#1610;&#1602;&#1608;&#1605; &#1602;&#1610;&#1605;
  &#1576;&#1584;&#1604;&#1603; &#1581;&#1587;&#1576;
  &#1605;&#1591;&#1604;&#1602;
  &#1575;&#1582;&#1578;&#1610;&#1575;&#1585;&#1607; &#1571;&#1608;
  &#1581;&#1587;&#1576;&#1605;&#1575; &#1602;&#1583;
  &#1610;&#1578;&#1602;&#1585;&#1585; &#1605;&#1604;&#1575;&#1569;&#1605;&#1578;&#1607;
  &#1605;&#1606; &#1580;&#1575;&#1606;&#1576;&#1606;&#1575; &#1571;&#1608;
  &#1573;&#1606; &#1603;&#1575;&#1606; &#1584;&#1604;&#1603; &#1575;&#1604;&#1573;&#1601;&#1589;&#1575;&#1581;
  &#1575;&#1605;&#1585;&#1575; &#1610;&#1606;&#1589;&#1576; &#1601;&#1610;
  &#1605;&#1589;&#1604;&#1581;&#1578;&#1606;&#1575;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:90%'><span style=' 
;line-height:90%;
  font-family:"brandfont"'>Clever shall be entitled to add to, vary or
  amend any or all of these Terms and Conditions at any time without notice.
  The User shall be bound by any additions, variations, or amendments once
  incorporated into these Terms and Conditions on Clever's Website.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1610;&#1603;&#1608;&#1606;
  &#1605;&#1606; &#1581;&#1602; &#1602;&#1610;&#1605;
  &#1573;&#1580;&#1585;&#1575;&#1569; &#1571;&#1610;&#1577;
  &#1573;&#1590;&#1575;&#1601;&#1577; &#1573;&#1604;&#1609;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577; &#1571;&#1608;
  &#1575;&#1604;&#1578;&#1594;&#1610;&#1610;&#1585; &#1571;&#1608;
  &#1575;&#1604;&#1578;&#1593;&#1583;&#1610;&#1604;
  &#1601;&#1610;&#1607;&#1575; &#1601;&#1610; &#1571;&#1610;
  &#1581;&#1610;&#1606; &#1608;&#1583;&#1608;&#1606;&#1605;&#1575;
  &#1573;&#1582;&#1591;&#1575;&#1585; &#1576;&#1584;&#1604;&#1603;.
  &#1608;&#1610;&#1603;&#1608;&#1606; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
  &#1575;&#1604;&#1575;&#1604;&#1578;&#1586;&#1575;&#1605; &#1576;&#1571;&#1610;&#1577;
  &#1573;&#1590;&#1575;&#1601;&#1575;&#1578; &#1571;&#1608;
  &#1578;&#1594;&#1610;&#1610;&#1585;&#1575;&#1578; &#1571;&#1608;
  &#1578;&#1593;&#1583;&#1610;&#1604;&#1575;&#1578;&#1613;
  &#1581;&#1610;&#1606; &#1573;&#1583;&#1585;&#1575;&#1580;&#1607;&#1575;
  &#1601;&#1610; &#1605;&#1578;&#1606;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1605;&#1608;&#1602;&#1593;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;&#1577;
  &#1604;&#1602;&#1610;&#1605;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:90%'><span style=' 
;line-height:90%;
  font-family:"brandfont"'>Clever shall not be liable to you for
  indirect, incidental, special, exemplary, punitive, or consequential damages,
  lost data, or damages that may arise, even if Clever has been advised of the
  possibility of such damages. Clever 'shall not be liable for any damages, liability
  or losses incurred by you arising out of your use of or reliance on the
  Services or your inability to access or use the Services or any transaction
  or relationship between you and r the Third-Party Service Providers, even if Clever
  'has been advised of the possibility of such damages. , Clever shall not be
  liable for delay or failure in performance resulting from causes beyond Clever`s
  reasonable control, and We shall not be liable in the event that the Supplier
  or Third-Party Service Providers may not be professionally licensed or
  permitted.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:95%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1604;&#1575;
  &#1578;&#1578;&#1581;&#1605;&#1604; &#1602;&#1610;&#1605; '&#1571;&#1610;&#1577;
  &#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1578;&#1580;&#1575;&#1607;&#1603;&#1605; &#1593;&#1605;&#1575;
  &#1602;&#1583; &#1610;&#1606;&#1588;&#1571; &#1605;&#1606;
  &#1571;&#1590;&#1585;&#1575;&#1585;&#1613; &#1594;&#1610;&#1585;
  &#1605;&#1576;&#1575;&#1588;&#1585;&#1577; &#1571;&#1608;
  &#1593;&#1575;&#1585;&#1590;&#1577; &#1571;&#1608;
  &#1582;&#1575;&#1589;&#1577; &#1571;&#1608;
  &#1580;&#1586;&#1575;&#1574;&#1610;&#1577; &#1571;&#1608;
  &#1593;&#1602;&#1575;&#1576;&#1610;&#1577; &#1571;&#1608;
  &#1578;&#1576;&#1593;&#1610;&#1577;&#1548; &#1581;&#1578;&#1609;
  &#1604;&#1608; &#1608;&#1585;&#1583; &#1604;&#1602;&#1610;&#1605;
  &#1605;&#1575; &#1610;&#1601;&#1610;&#1583; &#1576;&#1575;&#1581;&#1578;&#1605;&#1575;&#1604;
  &#1608;&#1602;&#1608;&#1593; &#1578;&#1604;&#1603;
  &#1575;&#1604;&#1575;&#1590;&#1585;&#1575;&#1585;. &#1603;&#1605;&#1575;
  &#1604;&#1575; &#1578;&#1578;&#1581;&#1605;&#1604; &#1602;&#1610;&#1605; '&#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1593;&#1606; &#1571;&#1610;&#1577; &#1575;&#1590;&#1585;&#1575;&#1585;
  &#1571;&#1608;
  &#1575;&#1604;&#1578;&#1586;&#1575;&#1605;&#1575;&#1578;&#1613;
  &#1571;&#1608; &#1582;&#1587;&#1575;&#1574;&#1585;
  &#1578;&#1578;&#1581;&#1605;&#1604;&#1608;&#1606;&#1607;&#1575;&#1548;
  &#1608;&#1603;&#1575;&#1606;&#1578; &#1578;&#1606;&#1588;&#1571;
  &#1605;&#1606; &#1608;&#1575;&#1602;&#1593;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1603;&#1605;
  &#1571;&#1608; &#1575;&#1593;&#1578;&#1605;&#1575;&#1583;&#1603;&#1605;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1571;&#1608; &#1580;&#1585;&#1575;&#1569; &#1593;&#1583;&#1605;
  &#1571;&#1605;&#1603;&#1575;&#1606;&#1610;&#1577;
  &#1583;&#1582;&#1608;&#1604;&#1603;&#1605; &#1593;&#1604;&#1609;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1571;&#1608;
  &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1607;&#1575;
  &#1571;&#1608; &#1605;&#1606; &#1608;&#1575;&#1602;&#1593;
  &#1571;&#1610;&#1577; &#1605;&#1593;&#1575;&#1605;&#1604;&#1577;
  &#1571;&#1608; &#1593;&#1604;&#1575;&#1602;&#1577; &#1606;&#1588;&#1571;&#1578;
  &#1576;&#1610;&#1606;&#1603;&#1605; &#1608;&#1576;&#1610;&#1606;
  &#1571;&#1610; &#1605;&#1602;&#1583;&#1605;&#1610;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1605;&#1606;
  &#1575;&#1604;&#1594;&#1610;&#1585;&#1548; &#1576;&#1604; &#1608;&#1581;&#1578;&#1609;
  &#1604;&#1608; &#1608;&#1585;&#1583; &#1604;&#1602;&#1610;&#1605; '&#1605;&#1575;
  &#1610;&#1601;&#1610;&#1583;
  &#1576;&#1575;&#1581;&#1578;&#1605;&#1575;&#1604;
  &#1608;&#1602;&#1608;&#1593; &#1578;&#1604;&#1603;
  &#1575;&#1604;&#1575;&#1590;&#1585;&#1575;&#1585;. &#1608;&#1604;&#1575;
  &#1578;&#1578;&#1581;&#1605;&#1604; &#1602;&#1610;&#1605; '&#1571;&#1610;&#1590;&#1575;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1593;&#1606; &#1571;&#1610; &#1578;&#1571;&#1582;&#1610;&#1585;
  &#1571;&#1608; &#1573;&#1582;&#1601;&#1575;&#1602; &#1601;&#1610;
  &#1575;&#1604;&#1578;&#1606;&#1601;&#1610;&#1584;&#1548;
  &#1608;&#1603;&#1575;&#1606; &#1584;&#1604;&#1603;
  &#1610;&#1606;&#1588;&#1571; &#1603;&#1606;&#1578;&#1610;&#1580;&#1577;
  &#1604;&#1571;&#1587;&#1576;&#1575;&#1576;&#1613;
  &#1582;&#1575;&#1585;&#1580;&#1577; &#1593;&#1606; &#1606;&#1591;&#1575;&#1602;
  &#1578;&#1581;&#1603;&#1605; &#1602;&#1610;&#1605; '&#1571;&#1608;
  &#1581;&#1583;&#1608;&#1583;
  &#1587;&#1610;&#1591;&#1585;&#1578;&#1607;&#1575;
  &#1575;&#1604;&#1605;&#1593;&#1602;&#1608;&#1604;&#1577;&#1548;
  &#1603;&#1605;&#1575; &#1604;&#1575; &#1606;&#1578;&#1581;&#1605;&#1604;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1601;&#1610; &#1581;&#1575;&#1604;&#1577; &#1575;&#1581;&#1578;&#1605;&#1575;&#1604;
  &#1593;&#1583;&#1605; &#1581;&#1610;&#1575;&#1586;&#1577;
  &#1575;&#1604;&#1605;&#1608;&#1585;&#1583; &#1571;&#1608;
  &#1605;&#1602;&#1583;&#1605; &#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
  &#1605;&#1606; &#1575;&#1604;&#1594;&#1610;&#1585; &#1593;&#1604;&#1609;
  &#1578;&#1585;&#1582;&#1610;&#1589; &#1571;&#1608;
  &#1578;&#1589;&#1585;&#1610;&#1581; &#1605;&#1607;&#1606;&#1610; &#1571;&#1608;
  &#1578;&#1582;&#1589;&#1589;&#1610;
  &#1576;&#1575;&#1604;&#1593;&#1605;&#1604;.' '''</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>These
  limitations do not purport to limit liability that cannot be excluded under
  the applicable laws.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1608;&#1604;&#1575;
  &#1578;&#1593;&#1578;&#1576;&#1585; &#1581;&#1583;&#1608;&#1583;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1575;&#1604;&#1605;&#1584;&#1603;&#1608;&#1585;&#1577;
  &#1571;&#1605;&#1585;&#1575; &#1605;&#1606; &#1588;&#1571;&#1606;&#1607;
  &#1578;&#1602;&#1610;&#1610;&#1583;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1575;&#1604;&#1578;&#1610; &#1604;&#1575; &#1610;&#1580;&#1608;&#1586;
  &#1575;&#1587;&#1578;&#1579;&#1606;&#1575;&#1574;&#1607;&#1575;
  &#1608;&#1601;&#1602;
  &#1575;&#1604;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606;
  &#1608;&#1575;&#1580;&#1576;&#1577;
  &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;. </span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Indemnification</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1575;&#1604;&#1578;&#1593;&#1608;&#1610;&#1590;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:115%;page-break-after:avoid'><span style='
  line-height:115%;font-family:"brandfont"'>You agree to indemnify
  and hold Clever, its affiliates, sponsors, partners, directors, officers and
  employees harmless from and against, and to reimburse Clever 'with respect to,
  any and all losses, damages, liabilities, claims, judgments, settlements,
  fines, costs and expenses (including reasonable related expenses, legal fees,
  costs of investigation) arising out of or relating to your breach of this
  Terms &amp; Conditions, along with our Privacy Policy or use by you or any service
  provider of the same Services.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:95%'><span style=' 
;line-height:95%;
  font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span style='font-family:"brandfont"'><span
  dir=RTL></span>'</span><span lang=AR-SA style='font-family:
  "Times New Roman",serif;color:black'>&#1610;&#1603;&#1608;&#1606;
  &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
  &#1593;&#1604;&#1609; &#1578;&#1593;&#1608;&#1610;&#1590; </span><span
  lang=AR-EG style='font-family:"brandfont";
  color:black'>&#1602;&#1610;&#1605; </span><span lang=AR-SA style='font-size:
  12.0pt;font-family:"brandfont";color:black'>'&#1576;&#1604;
  &#1608;&#1578;&#1580;&#1606;&#1610;&#1576;&#1607;&#1575;
  &#1575;&#1604;&#1590;&#1585;&#1585; &#1607;&#1610;
  &#1608;&#1603;&#1610;&#1575;&#1606;&#1575;&#1578;&#1607;&#1575;
  &#1575;&#1604;&#1578;&#1575;&#1576;&#1593;&#1577; &#1608;&#1580;&#1607;&#1575;&#1578;
  &#1575;&#1604;&#1585;&#1593;&#1575;&#1610;&#1577;
  &#1604;&#1583;&#1610;&#1607;&#1575;
  &#1608;&#1588;&#1585;&#1603;&#1575;&#1574;&#1607;&#1575;
  &#1608;&#1605;&#1583;&#1585;&#1575;&#1569;&#1607;&#1575;
  &#1608;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1607;&#1575;
  &#1608;&#1593;&#1575;&#1605;&#1604;&#1610;&#1607;&#1575; &#1605;&#1606;
  &#1571;&#1610;&#1577; '&#1590;&#1585;&#1585;&#1576;&#1604;
  &#1608;&#1605;&#1606; &#1580;&#1605;&#1610;&#1593;
  &#1575;&#1604;&#1582;&#1587;&#1575;&#1574;&#1585;
  &#1608;&#1575;&#1604;&#1578;&#1593;&#1608;&#1610;&#1590;&#1575;&#1578;
  &#1608;&#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1575;&#1578;
  &#1608;&#1575;&#1604;&#1583;&#1593;&#1575;&#1608;&#1609;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1602;&#1590;&#1575;&#1574;&#1610;&#1577;
  &#1608;&#1575;&#1604;&#1578;&#1587;&#1608;&#1610;&#1575;&#1578; &#1608;&#1575;&#1604;&#1594;&#1585;&#1575;&#1605;&#1575;&#1578;
  &#1608;&#1575;&#1604;&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;
  &#1608;&#1575;&#1604;&#1605;&#1589;&#1585;&#1608;&#1601;&#1575;&#1578;
  (&#1608;&#1605;&#1606;&#1607;&#1575; &#1605;&#1575; &#1607;&#1608;
  &#1605;&#1593;&#1602;&#1608;&#1604; &#1605;&#1606;
  &#1605;&#1589;&#1585;&#1608;&#1601;&#1575;&#1578; &#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1577;
  &#1608;&#1575;&#1578;&#1593;&#1575;&#1576;
  &#1605;&#1581;&#1575;&#1605;&#1575;&#1577;
  &#1608;&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;
  &#1578;&#1581;&#1602;&#1610;&#1602;) &#1576;&#1604;
  &#1608;&#1589;&#1585;&#1601; &#1602;&#1610;&#1605;&#1578;&#1607;&#1575;
  &#1573;&#1604;&#1609; &#1602;&#1610;&#1605; &#1548;
  &#1608;&#1584;&#1604;&#1603; &#1573;&#1584;&#1575;
  &#1603;&#1575;&#1606;&#1578; &#1607;&#1584;&#1607;
  &#1575;&#1604;&#1605;&#1589;&#1585;&#1608;&#1601;&#1575;&#1578;
  &#1578;&#1606;&#1588;&#1571; &#1575;&#1608;
  &#1578;&#1585;&#1578;&#1576;&#1591; &#1576;&#1571;&#1610;
  &#1573;&#1582;&#1604;&#1575;&#1604; &#1610;&#1602;&#1593; &#1605;&#1606;
  &#1580;&#1575;&#1606;&#1576;&#1603;&#1605;
  &#1576;&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605; &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;
  &#1571;&#1608; &#1576;&#1587;&#1610;&#1575;&#1587;&#1577;
  &#1575;&#1604;&#1582;&#1589;&#1608;&#1589;&#1610;&#1577;
  &#1575;&#1604;&#1587;&#1575;&#1585;&#1610;&#1577;
  &#1604;&#1583;&#1610;&#1606;&#1575; &#1571;&#1608;
  &#1576;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1603;&#1605;
  &#1571;&#1608; &#1571;&#1610; &#1605;&#1602;&#1583;&#1605;
  &#1582;&#1583;&#1605;&#1575;&#1578;
  &#1604;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;.</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Force Majeure</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1575;&#1604;&#1602;&#1608;&#1577;
  &#1575;&#1604;&#1602;&#1575;&#1607;&#1585;&#1577;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:115%;page-break-after:avoid'><span style='
  line-height:115%;font-family:"brandfont"'>Clever shall not be liable
  for any delay, interruption or failure in the provisioning of Services if
  caused by acts of God, including but not limited to declared or undeclared
  war, fire, flood, storm, slide, earthquake, power failure, the inability to
  obtain equipment, supplies or other facilities that are not caused by a
  failure to pay, labor disputes, or other similar events beyond our control
  that may prevent or delay service provisioning.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='line-height:90%;font-family:"brandfont";
  color:black'>&#1604;&#1575; &#1578;&#1578;&#1581;&#1605;&#1604; &#1602;&#1610;&#1605;
  &#1575;&#1604;&#1605;&#1587;&#1574;&#1608;&#1604;&#1610;&#1577;
  &#1593;&#1606; &#1571;&#1610;&#1577; &#1581;&#1575;&#1604;&#1577;
  &#1578;&#1571;&#1582;&#1610;&#1585; &#1571;&#1608;
  &#1578;&#1593;&#1591;&#1604; &#1571;&#1608; &#1573;&#1582;&#1601;&#1575;&#1602;
  &#1601;&#1610; &#1578;&#1602;&#1583;&#1610;&#1605; &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
  &#1573;&#1584;&#1575; &#1603;&#1575;&#1606;
  &#1575;&#1604;&#1587;&#1576;&#1576; &#1601;&#1610; &#1584;&#1604;&#1603;
  &#1610;&#1585;&#1580;&#1593; &#1604;&#1571;&#1610;&#1577;
  &#1571;&#1581;&#1583;&#1575;&#1579; &#1602;&#1583;&#1585;&#1610;&#1577;&#1548;
  &#1608;&#1605;&#1606;&#1607;&#1575; &#1593;&#1604;&#1609;
  &#1608;&#1580;&#1607; &#1575;&#1604;&#1593;&#1605;&#1608;&#1605;
  &#1604;&#1575; &#1575;&#1604;&#1581;&#1589;&#1585;
  &#1581;&#1575;&#1604;&#1577; &#1575;&#1604;&#1581;&#1585;&#1576;
  (&#1587;&#1608;&#1575;&#1569; &#1603;&#1575;&#1606;&#1578;
  &#1605;&#1593;&#1604;&#1606;&#1577; &#1571;&#1608; &#1594;&#1610;&#1585;
  &#1605;&#1593;&#1604;&#1606;&#1577;)
  &#1608;&#1575;&#1604;&#1581;&#1585;&#1610;&#1602; &#1608;&#1575;&#1604;&#1601;&#1610;&#1590;&#1575;&#1606;
  &#1608;&#1575;&#1604;&#1593;&#1575;&#1589;&#1601;&#1577;
  &#1608;&#1575;&#1604;&#1575;&#1606;&#1586;&#1604;&#1575;&#1602;
  &#1608;&#1575;&#1604;&#1586;&#1604;&#1586;&#1575;&#1604;
  &#1608;&#1575;&#1606;&#1602;&#1591;&#1575;&#1593;
  &#1575;&#1604;&#1603;&#1607;&#1585;&#1576;&#1575;&#1569;
  &#1608;&#1593;&#1583;&#1605; &#1575;&#1604;&#1602;&#1583;&#1585;&#1577;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1581;&#1589;&#1608;&#1604;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1605;&#1593;&#1583;&#1575;&#1578;
  &#1571;&#1608;
  &#1575;&#1604;&#1578;&#1608;&#1585;&#1610;&#1583;&#1575;&#1578;
  &#1571;&#1608; &#1594;&#1610;&#1585; &#1584;&#1604;&#1603; &#1605;&#1606;
  &#1605;&#1585;&#1575;&#1601;&#1602; &#1571;&#1608; &#1578;&#1587;&#1607;&#1610;&#1604;&#1575;&#1578;&#1548;
  &#1608;&#1603;&#1575;&#1606; &#1584;&#1604;&#1603; &#1604;&#1575;
  &#1610;&#1585;&#1580;&#1593; &#1604;&#1571;&#1610;
  &#1573;&#1582;&#1601;&#1575;&#1602; &#1601;&#1610;
  &#1575;&#1604;&#1587;&#1583;&#1575;&#1583;&#1548;
  &#1608;&#1575;&#1604;&#1606;&#1586;&#1575;&#1593;&#1575;&#1578;
  &#1575;&#1604;&#1593;&#1605;&#1575;&#1604;&#1610;&#1577; &#1571;&#1608;
  &#1605;&#1575; &#1588;&#1575;&#1576;&#1577; &#1584;&#1604;&#1603;
  &#1605;&#1606; &#1571;&#1581;&#1583;&#1575;&#1579;
  &#1582;&#1575;&#1585;&#1580;&#1577; &#1593;&#1606;
  &#1606;&#1591;&#1575;&#1602; &#1575;&#1604;&#1578;&#1581;&#1603;&#1605;
  &#1605;&#1606; &#1580;&#1575;&#1606;&#1576;&#1606;&#1575;
  &#1608;&#1603;&#1575;&#1606; &#1605;&#1606; &#1588;&#1571;&#1606;&#1607;&#1575;
  &#1605;&#1606;&#1593; &#1578;&#1602;&#1583;&#1610;&#1605;
  &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578; &#1571;&#1608;
  &#1578;&#1571;&#1582;&#1610;&#1585;&#1607;&#1575;.</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Governing Laws</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1575;&#1604;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606;
  &#1608;&#1575;&#1580;&#1576;&#1577;
  &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style=' 
;font-family:"brandfont"'>The
  rights and obligations of the parties pursuant to this Terms &amp; Conditions
  are governed by and shall be construed in accordance with the laws of the
  Arab Republic of Egypt.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style=' 
;font-family:"brandfont"'>You
  hereby irrevocably submit to the exclusive jurisdiction of the Courts of the
  Arab Republic of Egypt for any dispute arising under or relating to this
  Terms &amp; Conditions and waive your right to institute legal proceedings in
  any other jurisdiction. We shall be entitled to institute legal proceedings
  in connection with any matter arising under these Terms &amp; Conditions in
  any jurisdiction where you reside, do business, or have assets. </span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:kashida;text-kashida:20%;line-height:90%;
  direction:rtl;unicode-bidi:embed'><span dir=RTL></span><span
  style='font-family:"brandfont"'><span dir=RTL></span>'</span><span
  lang=AR-SA style='line-height:90%;font-family:"brandfont";
  color:black'>&#1578;&#1582;&#1590;&#1593;
  &#1575;&#1604;&#1581;&#1602;&#1608;&#1602;
  &#1608;&#1575;&#1604;&#1575;&#1604;&#1578;&#1586;&#1575;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1582;&#1575;&#1589;&#1577;
  &#1576;&#1575;&#1604;&#1571;&#1591;&#1585;&#1575;&#1601; &#1608;&#1601;&#1602;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;&#1548; &#1576;&#1604;
  &#1608;&#1578;&#1601;&#1587;&#1585; &#1608;&#1578;&#1572;&#1608;&#1604;
  &#1576;&#1605;&#1575; &#1610;&#1578;&#1608;&#1575;&#1601;&#1602;
  &#1608;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606;
  &#1580;&#1605;&#1607;&#1608;&#1585;&#1610;&#1577; &#1605;&#1589;&#1585;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;.</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:kashida;text-kashida:20%;line-height:90%;
  direction:rtl;unicode-bidi:embed'><span lang=AR-EG style='font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:kashida;text-kashida:20%;line-height:90%;
  direction:rtl;unicode-bidi:embed'><span lang=AR-EG style='font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:kashida;text-kashida:20%;line-height:90%;
  direction:rtl;unicode-bidi:embed'><span lang=AR-EG style='font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:kashida;text-kashida:20%;line-height:90%;
  direction:rtl;unicode-bidi:embed'><span lang=AR-EG style='font-family:"brandfont"'>&nbsp;</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:kashida;text-kashida:20%;line-height:90%;
  direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='
  line-height:90%;font-family:"brandfont";color:black'>&#1603;&#1605;&#1575;
  &#1571;&#1606;&#1603;&#1605; &#1608;&#1576;&#1605;&#1608;&#1580;&#1576;
  &#1607;&#1584;&#1575; &#1575;&#1604;&#1605;&#1615;&#1581;&#1585;&#1585;
  &#1573;&#1584; &#1578;&#1582;&#1590;&#1593;&#1608;&#1606;
  &#1608;&#1583;&#1608;&#1606; &#1602;&#1610;&#1583; &#1571;&#1608;
  &#1588;&#1585;&#1591; &#1604;&#1605;&#1606;&#1575;&#1591; &#1575;&#1604;&#1575;&#1582;&#1578;&#1589;&#1575;&#1589;
  &#1575;&#1604;&#1581;&#1589;&#1585;&#1610;
  &#1604;&#1605;&#1581;&#1575;&#1603;&#1605;
  &#1580;&#1605;&#1607;&#1608;&#1585;&#1610;&#1577; &#1605;&#1589;&#1585;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;&#1548;
  &#1608;&#1584;&#1604;&#1603;
  &#1576;&#1575;&#1604;&#1606;&#1587;&#1576;&#1577;
  &#1604;&#1571;&#1610;&#1577;
  &#1605;&#1606;&#1575;&#1586;&#1593;&#1575;&#1578;
  &#1578;&#1606;&#1588;&#1571; &#1605;&#1606; &#1608;&#1575;&#1602;&#1593;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577; &#1571;&#1608;
  &#1601;&#1610;&#1605;&#1575; &#1610;&#1578;&#1593;&#1604;&#1602;
  &#1576;&#1607;&#1575;&#1548; &#1603;&#1605;&#1575;
  &#1610;&#1578;&#1593;&#1610;&#1606; &#1593;&#1604;&#1610;&#1603;&#1605;
  &#1575;&#1604;&#1578;&#1606;&#1575;&#1586;&#1604; &#1593;&#1606;
  &#1571;&#1610; &#1581;&#1602; &#1604;&#1603;&#1605; &#1601;&#1610;
  &#1575;&#1578;&#1582;&#1575;&#1584; &#1571;&#1610;&#1577;
  &#1573;&#1580;&#1585;&#1575;&#1569;&#1575;&#1578;
  &#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1577; &#1601;&#1610;
  &#1571;&#1610;&#1577; &#1605;&#1606;&#1591;&#1602;&#1577; &#1571;&#1608;
  &#1605;&#1606;&#1575;&#1591; &#1575;&#1582;&#1578;&#1589;&#1575;&#1589;
  &#1570;&#1582;&#1585;&#1548; &#1601;&#1610; &#1581;&#1610;&#1606;
  &#1610;&#1603;&#1608;&#1606; &#1605;&#1606; &#1581;&#1602;&#1606;&#1575;
  &#1575;&#1578;&#1582;&#1575;&#1584;
  &#1573;&#1580;&#1585;&#1575;&#1569;&#1575;&#1578;
  &#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1577; &#1601;&#1610;&#1605;&#1575;
  &#1610;&#1578;&#1593;&#1604;&#1602; &#1576;&#1571;&#1610;
  &#1605;&#1608;&#1590;&#1608;&#1593; &#1610;&#1606;&#1588;&#1571;
  &#1576;&#1589;&#1583;&#1583; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577; &#1601;&#1610;
  &#1571;&#1610;&#1577; &#1605;&#1606;&#1591;&#1602;&#1577; &#1578;&#1602;&#1610;&#1605;&#1608;&#1606;
  &#1601;&#1610;&#1607;&#1575; &#1571;&#1608;
  &#1578;&#1580;&#1585;&#1608;&#1606; &#1576;&#1607;&#1575;
  &#1571;&#1593;&#1605;&#1575;&#1604;&#1603;&#1605; &#1571;&#1608;
  &#1610;&#1603;&#1608;&#1606; &#1604;&#1603;&#1605; &#1576;&#1607;&#1575;
  &#1575;&#1610;&#1577; &#1571;&#1589;&#1608;&#1604;.</span></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:kashida;text-kashida:20%;line-height:90%;
  direction:rtl;unicode-bidi:embed'><span dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>Where
  there is any dispute or inconsistency arising from the text of these Terms
  and Conditions, the Arabic version shall prevail.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:kashida;text-kashida:20%;line-height:90%;
  direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-family:"brandfont"'>&#1601;&#1610;
  &#1581;&#1575;&#1604;&#1577; &#1608;&#1602;&#1608;&#1593; &#1571;&#1610;
  &#1606;&#1586;&#1575;&#1593; &#1571;&#1608; &#1579;&#1605;&#1577;
  &#1578;&#1593;&#1575;&#1585;&#1590; &#1610;&#1606;&#1588;&#1571;
  &#1605;&#1606; &#1608;&#1575;&#1602;&#1593; &#1606;&#1589;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577;&#1548;
  &#1601;&#1610;&#1587;&#1585;&#1610; &#1581;&#1610;&#1606;&#1574;&#1584;&#1613;
  &#1575;&#1604;&#1606;&#1589; &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;. </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Unenforceable Provisions</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1602;&#1585;&#1585; &#1593;&#1583;&#1605;
  &#1587;&#1585;&#1610;&#1575;&#1606;&#1607;&#1575;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>If
  any part of these Terms and Conditions is found to be invalid or
  unenforceable under applicable law, such part will be ineffective to the
  extent of such invalid or unenforceable part only, without affecting the
  remaining parts of the Terms and Conditions in any way.</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1573;&#1584;&#1575;
  &#1578;&#1602;&#1585;&#1585; &#1593;&#1583;&#1605;
  &#1589;&#1604;&#1575;&#1581;&#1610;&#1577; &#1571;&#1610;
  &#1580;&#1586;&#1569; &#1605;&#1606;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604;&#1577; &#1571;&#1608;
  &#1593;&#1583;&#1605; &#1587;&#1585;&#1610;&#1575;&#1606;&#1607;&#1575;
  &#1571;&#1608; &#1606;&#1601;&#1575;&#1584;&#1607;&#1575;
  &#1608;&#1601;&#1602; &#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;
  &#1608;&#1575;&#1580;&#1576;
  &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1548;
  &#1601;&#1610;&#1593;&#1578;&#1576;&#1585; &#1581;&#1610;&#1606;&#1574;&#1584;&#1613;
  &#1584;&#1604;&#1603; &#1575;&#1604;&#1580;&#1586;&#1569;
  &#1576;&#1605;&#1579;&#1575;&#1576;&#1577; &#1594;&#1610;&#1585;
  &#1587;&#1575;&#1585;&#1610; &#1571;&#1608; &#1606;&#1575;&#1601;&#1584;
  &#1601;&#1610; &#1581;&#1583;&#1608;&#1583; &#1605;&#1575; &#1607;&#1608;
  &#1605;&#1602;&#1585;&#1585; &#1593;&#1583;&#1605;
  &#1589;&#1604;&#1575;&#1581;&#1610;&#1578;&#1607; &#1571;&#1608;
  &#1587;&#1585;&#1610;&#1575;&#1606;&#1607; &#1571;&#1608;
  &#1606;&#1601;&#1575;&#1584;&#1607; &#1608;&#1581;&#1587;&#1576;&#1548;
  &#1608;&#1583;&#1608;&#1606; &#1571;&#1606; &#1610;&#1572;&#1579;&#1585;
  &#1584;&#1604;&#1603; &#1608;&#1576;&#1571;&#1610; &#1581;&#1575;&#1604;
  &#1605;&#1606; &#1575;&#1604;&#1571;&#1581;&#1608;&#1575;&#1604; &#1593;&#1604;&#1609;
  &#1576;&#1575;&#1602;&#1610; &#1571;&#1580;&#1586;&#1575;&#1569;
  &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
  &#1608;&#1575;&#1604;&#1575;&#1581;&#1603;&#1575;&#1605;.'' </span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><b><span style=' 
;font-family:
  "Times New Roman",serif;color:black'>Entire Agreement</span></b></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#DEEAF6;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"brandfont";color:black'>&#1605;&#1615;&#1580;&#1605;&#1604;
  &#1575;&#1604;&#1575;&#1578;&#1601;&#1575;&#1602;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>&nbsp;</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;background:#F2F2F2;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;  ;

;direction:rtl;unicode-bidi:embed'><span
  dir=LTR style='font-family:"brandfont"'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width="53%" valign=top style='width:53.06%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;  ;

'><span style=' 
;font-family:"brandfont"'>This
  Agreement along with Clever's Privacy Policy, as may be updated from time to
  time and posted at [xxxx], constitutes the complete agreement and
  understanding between us with respect to the Service and supersedes any other
  form of Agreement.</span></p>
  </td>
  <td width="46%" valign=top style='width:46.94%;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;line-height:90%;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"brandfont"'>&#1610;&#1605;&#1579;&#1604;
  &#1575;&#1604;&#1575;&#1578;&#1601;&#1575;&#1602;
  &#1575;&#1604;&#1605;&#1575;&#1579;&#1604; &#1608;&#1603;&#1584;&#1575;
  &#1587;&#1610;&#1575;&#1587;&#1577;
  &#1575;&#1604;&#1582;&#1589;&#1608;&#1589;&#1610;&#1577; &#1575;&#1604;&#1587;&#1575;&#1585;&#1610;&#1577;
  &#1604;&#1583;&#1609; &#1602;&#1610;&#1605;&#1548; &#1576;&#1604;
  &#1608;&#1605;&#1575; &#1610;&#1591;&#1585;&#1571;
  &#1593;&#1604;&#1610;&#1607;&#1605; &#1605;&#1606;
  &#1578;&#1581;&#1583;&#1610;&#1579; &#1605;&#1606; &#1581;&#1610;&#1606;
  &#1604;&#1570;&#1582;&#1585; &#1608;&#1605;&#1575; &#1610;&#1578;&#1605;
  &#1606;&#1588;&#1585;&#1607; &#1576;&#1588;&#1571;&#1606;&#1607;&#1605;
  &#1593;&#1604;&#1609; </span><span dir=LTR></span><span dir=LTR
  style='font-family:"brandfont"'><span dir=LTR></span>)</span><span
  dir=RTL></span><span lang=AR-EG style='font-family:"brandfont"'><span
  dir=RTL></span>....) &#1603;&#1604; &#1605;&#1575; &#1578;&#1605;
  &#1575;&#1604;&#1575;&#1578;&#1601;&#1575;&#1602;
  &#1608;&#1575;&#1604;&#1578;&#1608;&#1575;&#1601;&#1602;
  &#1608;&#1575;&#1604;&#1578;&#1601;&#1575;&#1607;&#1605;
  &#1576;&#1588;&#1571;&#1606;&#1607; &#1601;&#1610;&#1605;&#1575;
  &#1576;&#1610;&#1606;&#1606;&#1575;&#1548; &#1608;&#1584;&#1604;&#1603;
  &#1601;&#1610;&#1605;&#1575; &#1610;&#1578;&#1593;&#1604;&#1602;
  &#1576;&#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;&#1548;
  &#1603;&#1605;&#1575; &#1610;&#1593;&#1583;&#1608;
  &#1608;&#1610;&#1606;&#1587;&#1582; &#1571;&#1610; &#1605;&#1605;&#1575;
  &#1587;&#1604;&#1601;&#1607; &#1605;&#1606;
  &#1575;&#1578;&#1601;&#1575;&#1602;&#1575;&#1578;
  &#1571;&#1582;&#1585;&#1609; &#1605;&#1606; &#1571;&#1610; &#1606;&#1608;&#1593;.
  </span><span lang=AR-EG style='font-family:"brandfont"'>'</span></p>
  </td>
 </tr>
</table>

        </div>
    </div>

<p class=MsoNormal>&nbsp;</p>
        <a href="{{ route('home') }}" class="btn btn-primary">Back</a>
    </div>
    <!-- footer -->
    <section class="footer">
        <div class="container">
            <footer class="">
                <div class="d-flex justify-content-center">
                    <ul class="row">
                        <a href="#" style="padding-right: 10px;"><i class="fab fa-facebook fa-2x"></i></a>
                        <a href="#" style="padding-right: 10px;"><i class="fab fa-twitter fa-2x"></i></a>
                        <a href="#" style="padding-right: 45px;"><i class="fab fa-instagram fa-2x"></i></a>
                    </ul>
                </div><!-- Ende Sozial media -->
                <div class="d-flex justify-content-center">
                    <p> All rights reseved </p>
                </div><!-- Ende Copyright -->
            </footer>
        </div>
    </section>
</body>

<script>
    //ScrollReveal().reveal('.headline');
    //for nav bar
    $('.navbar-nav .nav-link small').click(function () {
        $('.navbar-nav .nav-link small').removeClass('active');
        $(this).addClass('active');
    });
</script>

</html>
