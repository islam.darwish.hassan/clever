
@extends('layouts.website')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('images/bg_6.jpg');">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9  text-center">
          <h1 class="mb-0 bread">Join Us</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Create New Acccount</a></span> </p>
        </div>
      </div>
    </div>
  </div>


      <section class=" ">
      <div class="container">
        <div class="col-md-12  ">
            @if (session('message'))
                <div class="alert alert-success mt-2  " role="alert">
                    <b>{{ session('message') }}</b>
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger mt-2" role="alert" >
                    <b>{{ session('error') }}</b>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger mt-2  p-5">
                    {{-- <p><b>Please fix these errors.</b></p> --}}
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <form class="form-group " method="POST" action={{route('web.client.store')}}>
           @csrf
            <div class=" form-group">
                <label class="form-label">E-mail</label>
                <input type="email" name="email" class="form-control" placeholder="E-mail" required
                value="{{request()->query('email')}}" >
            </div>
            <div class="  form-group">
                <label class="form-label">Password</label>
                <input type="password" name="password" class="form-control" placeholder="Password" required
                    value="{{request()->query('password')}}" becrypt>
            </div>
            <div class="  form-group">
                <label class="form-label">Password Confirmation</label>
                <input type="password" name="password_confirmation" class="form-control" placeholder="Password Confirmation" required
                    value="{{request()->query('password_confirmation')}}" becrypt>
            </div>

            <div class="row">
                <div class="form-group col">
                    <label class="form-label">Your name : </label>
                    <input type="name" name="name" class="form-control m-input"
                    id="name" aria-describedby="nameHelp" placeholder="Enter user name" value="{{ old('name') }}">
                </div>

                <div class="form-group col ">
                    <label class="form-label">النوع : </label>
                    <div>
                        <select class="form-control m-input " name="gender" ">
                                                <option value="">Select a gender </option>
                                                <option value="m" @if(request()->query('gender') == 'm') selected @endif>Male</option>
                            <option value="f" @if(request()->query('gender') == 'f') selected @endif>Female</option>
                        </select>

                    </div>
                </div>
                {{-- <div class="form-group col ">
                    <label class="form-label">السن : </label>
                    <input type="number" name="age" class="form-control m-input" min="5" max="130"
                    id="age" aria-describedby="nameHelp" placeholder="Enter user age"  value="{{ old('age') }}">
                </div> --}}

                <div class="form-group col">
                    <label class="form-label">تليفون : </label>
                    <input type="phone" name="phone" class="form-control m-input"
                     id="phone" aria-describedby="nameHelp" placeholder="Enter user phone" value="{{ old('phone') }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col ">
                    <label class="form-label">مدينة التوصيل : </label>
                    <div>
                        <select class="form-control m-input " name="city_id">
                       <option value="">Select a City </option>
                        @foreach(App\Models\City::all() as $city)
                        <option value="{{$city->id}}" @if(request()->query('city_id') == $city->id) selected @endif>{{$city->en_name}}</option>
                        @endforeach
                        </select>

                    </div>
                </div>
            <div class="form-group col-md-8">
                <label for="AddressTextArea">Delivery Address</label>
                <textarea class="form-control" id="AddressTextArea" name="address" rows="3"></textarea>
              </div>

            </div>
              <button type="submit" class="btn btn-primary mb-2 d-flex float-right">Join Now </button>

      </form>

      </div>

  </section>
@endsection
