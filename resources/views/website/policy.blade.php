<!DOCTYPE html>
<html lang="en">

<head>
    <title>Privacy Policy</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- MainStyle -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- CustomStyle -->
    <link rel="stylesheet" href="{{ asset('css/main2.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free-5.11.2-web/css/all.min.css') }}">
    <!-- BootstrapScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</head>
<body data-spy="scroll">
    <nav class="navbar navbar-expand-lg fixed-top navbar-light ">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
            aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand  " href="{{ route('home') }}"><img src="{{ asset('assets/imgs/logo.png') }}" width="auto" height="30px"></a>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
                <li class="nav-item ">
                    <a class="nav-link small" href="{{ route('terms') }}"><b>Terms & Conditions</b>  <span
                            class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item ">
                    <a class="nav-link small active" href="{{ route('policy') }}"><b>Privacy Policy</b> <span class="sr-only">(current)</span></a>
                </li>


            </ul>
        </div>

    </nav>
    <div class="policy">
        <h1 class="text-center" style="text-decoration: underline; padding-bottom: 10px;">Privacy Policy</h1>
        <div style="padding-bottom: 5px;">
            - When you use our services, you give us your trust.
            We understand this responsibility and work hard to protect your information. Our Privacy Policy is designed
            to
            help you understand the information we collect.
        </div>
        <div class=WordSection1>

<p class=MsoNormal><b><u>Introduction</u></b></p>

<p class=MsoNormal><b>Welcome to Clever's Privacy Policy.</b></p>

<p class=MsoNormal>Clever respects your privacy and is committed to protecting
your personal data. This Privacy Policy will inform you as</p>

<p class=MsoNormal>to how we look after your personal data when you visit our application/website
(regardless of where you visit it from)</p>

<p class=MsoNormal>and tell you about your privacy rights and how the law
protects you.</p>

<p class=MsoNormal><b>1. Important information and who we are</b></p>

<p class=MsoNormal><b>1.1 Purpose of this Privacy Policy</b></p>

<p class=MsoNormal>This Privacy Policy aims to give you information on how Clever
collects and processes your personal data through</p>

<p class=MsoNormal>your use of the website or the mobile application, including
any data you may provide through this application/website</p>

<p class=MsoNormal>when you when you [sign up to our newsletter, register
and/or purchase a product or service, as well as installing and</p>

<p class=MsoNormal>signing up to any Clever's powered application or our mobile
applications].</p>

<p class=MsoNormal>This application/website is not intended for children and we
do not knowingly collect data relating to children.</p>

<p class=MsoNormal>It is important that you read this Privacy Policy together
along with our Terms and Conditions [LINK] when we are</p>

<p class=MsoNormal>collecting or processing personal data about you so that you
are fully aware of how and why we are using your data.</p>

<p class=MsoNormal>This Privacy Policy supplements other notices and privacy
policies and is not intended to override them.</p>

<p class=MsoNormal><b>1.2 Controller</b></p>

<p class=MsoNormal>Clever is the controller and responsible for your personal data
(referred to as [&amp;Clever&amp;], &amp;();, &amp;() or &amp;() in this</p>

<p class=MsoNormal>Privacy Policy). Clever is the controller and responsible for
this application and website.</p>

<p class=MsoNormal>We have appointed a Data Protection Officer (DPO) who is
responsible for overseeing questions in relation to this</p>

<p class=MsoNormal>Privacy Policy. If you have any questions about this Privacy
Policy, including any requests to exercise, please contact</p>

<p class=MsoNormal>the DPO using the details set out below.</p>

<p class=MsoNormal><b>1.3 التواصل details</b></p>

<p class=MsoNormal>If you have any questions about this Privacy Policy or our
privacy practices, please contact our DPO in the following</p>

<p class=MsoNormal>ways:</p>

<p class=MsoNormal>البريد الإلكترونى address: [DETAILS]</p>

<p class=MsoNormal>Postal address: [DETAILS]</p>

<p class=MsoNormal>Telephone number: [DETAILS]</p>

<p class=MsoNormal>Address: [DETAILS]</p>

<p class=MsoNormal><b>1.4 Changes to the Privacy Policy and your duty to inform
us of changes</b></p>

<p class=MsoNormal>We keep our Privacy Policy under regular review.</p>

<p class=MsoNormal>It is important that the personal data we hold about you is
accurate and current. Please keep us informed if your</p>

<p class=MsoNormal>personal data changes during your relationship with us.</p>

<p class=MsoNormal><b>1.5 Third-party links</b></p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>2</b></p>

<p class=MsoNormal>This Platform (Website, Mobile Application, and المستخدم
Portal/Dashboard) may include links to third-party websites,</p>

<p class=MsoNormal>plug-ins and applications. Clicking on those links or enabling
those connections may allow third parties to collect or</p>

<p class=MsoNormal>share data about you. We do not control these third-party
applications/websites and are not responsible for them</p>

<p class=MsoNormal>privacy statements. When you leave our application/website,
we encourage you to read the Privacy Policy of every</p>

<p class=MsoNormal>application/website you visit.</p>

<p class=MsoNormal><b>2. Personal Data that we collect</b></p>

<p class=MsoNormal>Your personal data, or personal information, means any
information about an individual from which that person can</p>

<p class=MsoNormal>be identified. It does not include data where the identity
has been removed (anonymous data).</p>

<p class=MsoNormal>We may collect, use, store and transfer different kinds of
personal data about you which is including but not limited to</p>

<p class=MsoNormal>the <b><u>following:</u></b></p>

<p class=MsoNormal>a. Identity data includes [first name, maiden name, last
name, username or similar identifier, marital status, title,</p>

<p class=MsoNormal>date of birth and gender].</p>

<p class=MsoNormal>b. التواصل data includes [billing address, delivery address,
email address and telephone numbers].</p>

<p class=MsoNormal>c. Financial data includes [bank account and payment card
details].</p>

<p class=MsoNormal>d. Transaction data includes [details about payments to and
from you and other details of services you have</p>

<p class=MsoNormal>ordered from us].</p>

<p class=MsoNormal>e. Technical data includes [internet protocol (IP) address,
your login data, browser type and version, time zone</p>

<p class=MsoNormal>setting and location, browser plug-in types and versions,
operating system and platform, and other technology</p>

<p class=MsoNormal>on the devices you use to access this website].</p>

<p class=MsoNormal>f. Profile data includes [your username and password,
purchases or orders made by you, your interests,</p>

<p class=MsoNormal>preferences, and feedback and survey responses].</p>

<p class=MsoNormal>g. Usage data includes [information about how you use our
website, products and services].</p>

<p class=MsoNormal>We also collect, use and share aggregated data such as
statistical or demographic data for any purpose. Aggregated</p>

<p class=MsoNormal>data could be derived from your personal data but is not
considered personal data in law as this data will not directly</p>

<p class=MsoNormal>or indirectly reveal your identity. For example, we may
aggregate your usage data to calculate the percentage of users</p>

<p class=MsoNormal>accessing a specific website feature. However, if we combine
or connect aggregated data with your personal data so</p>

<p class=MsoNormal>that it can directly or indirectly identify you, we treat
the combined data as personal data which will be used in</p>

<p class=MsoNormal>accordance with this Privacy Policy.</p>

<p class=MsoNormal>We do not collect any Special Categories of Personal Data
about you (this includes details about your race or</p>

<p class=MsoNormal>ethnicity, religious or philosophical beliefs, sex life,
sexual orientation, political opinions, trade union membership,</p>

<p class=MsoNormal>information about your health, and genetic and biometric data).
Nor do we collect any information about criminal</p>

<p class=MsoNormal>convictions and offences.</p>

<p class=MsoNormal><b>3. If you fail to provide personal data</b></p>

<p class=MsoNormal>Where we need to collect personal data by law, or under the
terms of a contract we have with you, and you fail to</p>

<p class=MsoNormal>provide that data when requested, we may not be able to
perform the contract we have or are trying to enter into with</p>

<p class=MsoNormal>you (for example, to provide you with services). In this
case, we may have to cancel a service you have with us but we</p>

<p class=MsoNormal>will notify you if this is the case at the time.</p>

<p class=MsoNormal><b>4. How is your personal data collected?</b></p>

<p class=MsoNormal>We use different methods to collect data from and about you
including through:</p>

<p class=MsoNormal><b>4.1 Direct interactions. You may give us your Identity,
contact and financial data by filling in forms or by</b></p>

<p class=MsoNormal>corresponding with us by post, phone, and email or
otherwise. This includes personal data you provide when you</p>

<p class=MsoNormal>undertake the following:</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>3</b></p>

<p class=MsoNormal>a. purchases our products or services;</p>

<p class=MsoNormal>b. creates an account on our application or website;</p>

<p class=MsoNormal>c. subscribes to our service or publications;</p>

<p class=MsoNormal>d. request marketing to be sent to you;</p>

<p class=MsoNormal>e. enters a competition, promotion or survey; or</p>

<p class=MsoNormal>f. gives us feedback or contact us.</p>

<p class=MsoNormal><b>4.2 Automated technologies or interactions. As you
interact with our application/website, we will automatically collect</b></p>

<p class=MsoNormal>Technical Data about your equipment, browsing actions and
patterns. We collect this personal data by using cookies,</p>

<p class=MsoNormal>server logs and other similar technologies. We may also
receive Technical Data about you if you visit other mobile</p>

<p class=MsoNormal>applications/websites employing our cookies. Please see our
cookie policy [LINK] for further details.</p>

<p class=MsoNormal><b>5. How we use your personal data</b></p>

<p class=MsoNormal>We will only use your personal data when the law allows us
to. Most commonly, we will use your personal data in the</p>

<p class=MsoNormal>following circumstances:</p>

<p class=MsoNormal>a. Where we need to perform the contract, we are about to
enter into or have entered into with you.</p>

<p class=MsoNormal>b. Where it is necessary for our legitimate interests (or
those of a third party) and your interests and fundamental</p>

<p class=MsoNormal>rights do not override those interests.</p>

<p class=MsoNormal>c. Where we need to comply with a legal obligation.</p>

<p class=MsoNormal>d. Where we need to set up your account and administrate it.</p>

<p class=MsoNormal>e. Where we need to deliver marketing and events
communication.</p>

<p class=MsoNormal>f. Where we need to carry out surveys.</p>

<p class=MsoNormal>g. Where we need to personalize content, user experience or
business information.</p>

<p class=MsoNormal>h. Where we need to meet audit requirements internally.</p>

<p class=MsoNormal>i. Where you have given consent.</p>

<p class=MsoNormal>Generally, we do not rely on consent as a legal basis for
processing your personal data although we will get your</p>

<p class=MsoNormal>consent before sending third party direct marketing
communications to you via email or text message. You have the</p>

<p class=MsoNormal>right to withdraw consent to marketing at any time by
contacting us.</p>

<p class=MsoNormal><b>6. Purposes for which we will use your personal data</b></p>

<p class=MsoNormal><b>A) Performance of a contract with you</b></p>

<p class=MsoNormal>We process your personal data because it is necessary for
the performance of a contract to which you are a party or in</p>

<p class=MsoNormal>order to take steps at your request prior to entering into a
contract or agreement.</p>

<p class=MsoNormal>In this respect, we use your personal data for the <b><u>following:</u></b></p>

<p class=MsoNormal style='margin-left:.5in'>i. To prepare a proposal for you
regarding the services we offer;</p>

<p class=MsoNormal style='margin-left:.5in'>ii. To provide you with the
services as set as the scope of our services, or as otherwise agreed with you</p>

<p class=MsoNormal style='margin-left:.5in'>from time to time;</p>

<p class=MsoNormal style='margin-left:.5in'>iii. To deal with any complaints or
feedback you may have;</p>

<p class=MsoNormal style='margin-left:.5in'>iv. For any other purpose for which
you provide us with your personal data.</p>

<p class=MsoNormal>In this respect, we may share your personal data with or
transfer it to the following:</p>

<p class=MsoNormal>i. Subject to your consent, independent third parties whom
we engage to assist in delivering the services</p>

<p class=MsoNormal>to you, including third parties;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>4</b></p>

<p class=MsoNormal>ii. Our professional advisers where it is necessary for us
to obtain their advice or assistance, including</p>

<p class=MsoNormal>lawyers, accountants, IT or public relations advisers;</p>

<p class=MsoNormal>iii. Debt collection agencies where it is necessary to
recover money you owe us;</p>

<p class=MsoNormal>iv. Other third parties such as intermediaries who we
introduce to you. We will wherever possible tell</p>

<p class=MsoNormal>you who they are before we introduce you;</p>

<p class=MsoNormal>v. Our data storage providers.</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>The legal basis for the processing of the aforementioned
data categories is Art. 6 (1) (a) of the European General Data</p>

<p class=MsoNormal>Protection Regulation (GDPR). Due to the said purposes, in
particular to guarantee security and a smooth connection</p>

<p class=MsoNormal>setup, we have a legitimate interest to process this data.</p>

<p class=MsoNormal><b>B) Legitimate interests</b></p>

<p class=MsoNormal>We also process your personal data because it is necessary
for our legitimate interests, or sometimes where it is</p>

<p class=MsoNormal>necessary for the legitimate interests of another person.</p>

<p class=MsoNormal>In this respect, we use your personal data for:</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>i. For the administration and management of our business,
including recovering money you owe to us,</p>

<p class=MsoNormal>and archiving or statistical analysis;</p>

<p class=MsoNormal>ii. Seeking advice on our rights and obligations, such as
where we require our own legal advice. In this</p>

<p class=MsoNormal>respect we will share your personal data with our advisers
or agents where it is necessary for us to</p>

<p class=MsoNormal>obtain their advice or assistance and with third parties and
their advisers where those third parties are</p>

<p class=MsoNormal>acquiring, or considering acquiring, all or part of our business.</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>C) Legal obligations</b></p>

<p class=MsoNormal>We also process your personal data for our compliance with a
legal obligation which we are under. In this respect, we</p>

<p class=MsoNormal>will use your personal data for the following:</p>

<p class=MsoNormal>i. To meet our compliance and regulatory obligations, such
as compliance with anti-money laundering</p>

<p class=MsoNormal>laws;</p>

<p class=MsoNormal>ii. As required by tax authorities or any competent court or
legal authority. In this respect, we will share</p>

<p class=MsoNormal>your personal data with the following:</p>

<p class=MsoNormal>iii. Our advisers where it is necessary for us to obtain their
advice or assistance;</p>

<p class=MsoNormal>iv. Our auditors where it is necessary as part of their
auditing functions;</p>

<p class=MsoNormal>v. With third parties who assist us in conducting background
checks;</p>

<p class=MsoNormal>vi. With relevant regulators or law enforcement agencies
where we are required to do so.</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>D) Marketing</b></p>

<p class=MsoNormal>We will send you marketing about services we provide which
may be of interest to you, as well as other information</p>

<p class=MsoNormal>in the form of alerts, newsletters, discounts or functions
which we believe might be of interest to you or in order to</p>

<p class=MsoNormal>update you with information (such as legal or commercial
news) which we believe may be relevant to you. We will</p>

<p class=MsoNormal>communicate this to you in a number of ways including by
post, telephone, email or other digital channels.</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>5</b></p>

<p class=MsoNormal><b>E) Promotional offers from us</b></p>

<p class=MsoNormal>We may use your Identity, التواصل, Technical, Usage and
Profile Data to form a view on what we think you may want</p>

<p class=MsoNormal>or need, or what may be of interest to you. This is how we
decide which products, services and offers may be relevant</p>

<p class=MsoNormal>for you (Marketing).</p>

<p class=MsoNormal>You will receive marketing communications from us if you
have requested information from us or purchased services</p>

<p class=MsoNormal>from us [or if you provided us with your details when you
entered a competition or registered for a promotion] and, in</p>

<p class=MsoNormal>each case, you have not opted out of receiving that
marketing.</p>

<p class=MsoNormal><b>F) Third-party marketing</b></p>

<p class=MsoNormal>i. We will get your express opt-in consent before we share
your personal data with any company outside</p>

<p class=MsoNormal>Clever for marketing purposes.</p>

<p class=MsoNormal>ii. You can ask us or third parties to stop sending you
marketing messages at any time by logging into</p>

<p class=MsoNormal>the application/website and checking or unchecking relevant
boxes to adjust your marketing</p>

<p class=MsoNormal>preferences or by following the opt-out links on any
marketing message sent to you or by contacting</p>

<p class=MsoNormal>us at any time.</p>

<p class=MsoNormal><b>G) Cookies</b></p>

<p class=MsoNormal>You can set your browser to refuse all or some browser
cookies, or to alert you when application/websites set or</p>

<p class=MsoNormal>access cookies. If you disable or refuse cookies, please
note that some parts of this website may become inaccessible</p>

<p class=MsoNormal>or not function properly.</p>

<p class=MsoNormal><b>H) Change of purpose</b></p>

<p class=MsoNormal>We will only use your personal data for the purposes for
which we collected it, unless we reasonably consider that we</p>

<p class=MsoNormal>need to use it for another reason and that reason is
compatible with the original purpose. If you wish to get an</p>

<p class=MsoNormal>explanation as to how the processing for the new purpose is
compatible with the original purpose, please contact us. If</p>

<p class=MsoNormal>we need to use your personal data for an unrelated purpose,
we will notify you and we will explain the legal basis</p>

<p class=MsoNormal>which allows us to do so. Please note that we may process
your personal data without your knowledge or consent, in</p>

<p class=MsoNormal>compliance with the above rules, where this is required or
permitted by law.</p>

<p class=MsoNormal>I) Opting out</p>

<p class=MsoNormal>Where you opt out of receiving these marketing messages,
this will not apply to personal data provided to us as a</p>

<p class=MsoNormal>result of a product/service purchase, warranty registration,
product/service experience or other transactions.</p>

<p class=MsoNormal><b>7. Disclosures of your personal data</b></p>

<p class=MsoNormal>We may share your personal data with the parties set out in
article (6) purposes for which we will use your personal</p>

<p class=MsoNormal>data above.</p>

<p class=MsoNormal>Third parties to whom we may choose to sell, transfer or
merge parts of our business or our assets. Alternatively, we</p>

<p class=MsoNormal>may seek to acquire other businesses or merge with them. If
a change happens to our business, then the new owners</p>

<p class=MsoNormal>may use your personal data in the same way as set out in
this Privacy Policy.</p>

<p class=MsoNormal>We require all third parties to respect the security of your
personal data and to treat it in accordance with the law. We</p>

<p class=MsoNormal>do not allow our third-party service providers to use your
personal data for their own purposes and only permit them</p>

<p class=MsoNormal>to process your personal data for specified purposes and in
accordance with our instructions.</p>

<p class=MsoNormal><b>8. Data security</b></p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>6</b></p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>We have put in place appropriate security measures to
prevent your personal data from being accidentally lost, used or</p>

<p class=MsoNormal>accessed in an unauthorized way, altered or disclosed. In
addition, we limit access to your personal data to those</p>

<p class=MsoNormal>employees, agents, contractors, third party service providers
and other parties who have a business need to know.</p>

<p class=MsoNormal>They will only process your personal data on our
instructions and they are subject to a duty of confidentiality.</p>

<p class=MsoNormal>We have put in place procedures to deal with any suspected
personal data breach and will notify you and any</p>

<p class=MsoNormal>applicable regulator of a breach where we are legally
required to do so.</p>

<p class=MsoNormal><b>9. Data retention</b></p>

<p class=MsoNormal>How long will you use my personal data for?</p>

<p class=MsoNormal>We will only retain your personal data for as long as
reasonably necessary to fulfil the purposes we collected it for,</p>

<p class=MsoNormal>including for the purposes of satisfying any legal,
regulatory, tax, accounting or reporting requirements. We may</p>

<p class=MsoNormal>retain your personal data for a longer period in the event
of a complaint or if we reasonably believe there is a prospect</p>

<p class=MsoNormal>of litigation in respect to our relationship with you.</p>

<p class=MsoNormal>To determine the appropriate retention period for personal
data, we consider the amount, nature and sensitivity of the</p>

<p class=MsoNormal>personal data, the potential risk of harm from unauthorized
use or disclosure of your personal data, the purposes for</p>

<p class=MsoNormal>which we process your personal data and whether we can
achieve those purposes through other means, and the</p>

<p class=MsoNormal>applicable legal, regulatory, tax, accounting or other
requirements.</p>

<p class=MsoNormal>When it is no longer necessary to retain your personal data,
we will delete it.</p>

<p class=MsoNormal>[Details of retention periods for different aspects of your
personal data are [available in our retention policy which you</p>

<p class=MsoNormal>can request from us by contacting us.</p>

<p class=MsoNormal><b>10. What we may need from you</b></p>

<p class=MsoNormal>We may need to request specific information from you to help
us confirm your identity and ensure your right to access</p>

<p class=MsoNormal>your personal data (or to exercise any of your other
rights). This is a security measure to ensure that personal data is</p>

<p class=MsoNormal>not disclosed to any person who has no right to receive it.
We may also contact you to ask you for further information</p>

<p class=MsoNormal>in relation to your request to speed up our response.</p>

<p class=MsoNormal><b>11. Time limit to respond</b></p>

<p class=MsoNormal>We try to respond to all legitimate requests within one
month. Occasionally it could take us longer than a month if</p>

<p class=MsoNormal>your request is particularly complex or you have made a
number of requests. In this case, we will notify you and keep</p>

<p class=MsoNormal>you updated.</p>

<p class=MsoNormal><b>12. What we do if the information is incorrect?</b></p>

<p class=MsoNormal>We do perform checks on the information that we receive to
detect any defects or mistakes. However, we are reliant</p>

<p class=MsoNormal>upon suppliers, namely Companies House, providing accurate
information to us. You have the right to request that we:</p>

<p class=MsoNormal>Rectify any personal data relating to you that is
inaccurate; and</p>

<p class=MsoNormal>Complete any incomplete data, including by way of a
supplementing, corrective statement. This is known as the right</p>

<p class=MsoNormal>to rectification. If you do exercise your right to
rectification, we will take steps to check the information and correct it</p>

<p class=MsoNormal>where necessary.</p>

<p class=MsoNormal><b>13. How do we deal with the 'right to be forgotten'?</b></p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>7</b></p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>You have the right to request the erasure of personal data
that we hold about you in certain circumstances, for example</p>

<p class=MsoNormal>if it were not acquired for, or has ceased to be necessary
for, a lawful purpose. This is known as the right to be</p>

<p class=MsoNormal>forgotten. Where you request that we erase your data, we
will usually only do so where the data has ceased to be</p>

<p class=MsoNormal>publicly available, whether at Companies House or otherwise,
or where we no longer use it</p>

<p class=MsoNormal><b>14. Glossary</b></p>

<p class=MsoNormal>LAWFUL BASIS</p>

<p class=MsoNormal>Legitimate Interest means the interest of our business in
conducting and managing our business to enable us to give</p>

<p class=MsoNormal>you the best service and the best and most secure
experience. We make sure we consider and balance any potential</p>

<p class=MsoNormal>impact on you (both positive and negative) and your rights
before we process your personal data for our legitimate</p>

<p class=MsoNormal>interests. We do not use your personal data for activities
where our interests are overridden by the impact on you</p>

<p class=MsoNormal>(unless we have your consent or are otherwise required or
permitted to by law). You can obtain further information</p>

<p class=MsoNormal>about how we assess our legitimate interests against any
potential impact on you in respect of specific activities by</p>

<p class=MsoNormal>contacting us.</p>

<p class=MsoNormal>Performance of Contract means processing your data where it
is necessary for the performance of a contract to which</p>

<p class=MsoNormal>you are a party or to take steps at your request before
entering into such a contract.</p>

<p class=MsoNormal>Comply with a legal obligation means processing your
personal data where it is necessary for compliance with a legal</p>

<p class=MsoNormal>obligation that we are subject to.</p>

<p class=MsoNormal><b>THIRD PARTIES</b></p>

<p class=MsoNormal>Internal Third Parties</p>

<p class=MsoNormal>Other companies in Clever acting as joint controllers or
processors who are based in Egypt and provide IT and system</p>

<p class=MsoNormal>administration services and undertake leadership reporting.</p>

<p class=MsoNormal>External Third Parties</p>

<p class=MsoNormal>Service providers acting as processors-based Egypt who
provide IT and system administration services.</p>

<p class=MsoNormal>Professional advisers acting as processors or joint
controllers including lawyers, bankers, auditors and insurers who</p>

<p class=MsoNormal>provide consultancy, banking, legal, insurance and
accounting services.</p>

<p class=MsoNormal>Governmental bodies and other authorities acting as
processors or joint controllers based in the Arab Republic of</p>

<p class=MsoNormal>Egypt who require reporting of processing activities in
certain circumstances.</p>

<p class=MsoNormal><b>YOUR LEGAL RIGHTS</b></p>

<p class=MsoNormal>You have the right to:</p>

<p class=MsoNormal>مطلوب access to your personal data (commonly known as a ()
subject access()). This enables you to</p>

<p class=MsoNormal>receive a copy of the personal data we hold about you and to
check that we are lawfully processing it.</p>

<p class=MsoNormal>مطلوب correction of the personal data that we hold about
you. This enables you to have any incomplete or inaccurate</p>

<p class=MsoNormal>data we hold about you corrected, though we may need to
verify the accuracy of the new data you provide to us.</p>

<p class=MsoNormal>مطلوب erasure of your personal data. This enables you to
ask us to delete or remove personal data where there is no</p>

<p class=MsoNormal>good reason for us continuing to process it. You also have
the right to ask us to delete or remove your personal data</p>

<p class=MsoNormal>where you have successfully exercised your right to object
to processing (see below), where we may have processed</p>

<p class=MsoNormal>your information unlawfully or where we are required to
erase your personal data to comply with local law. Note,</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>8</b></p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>however, that we may not always be able to comply with your
request of erasure for specific legal reasons which will</p>

<p class=MsoNormal>be notified to you, if applicable, at the time of your
request.</p>

<p class=MsoNormal>Object to processing of your personal data where we are
relying on a legitimate interest (or those of a third party) and</p>

<p class=MsoNormal>there is something about your particular situation which
makes you want to object to processing on this ground as you</p>

<p class=MsoNormal>feel it impacts on your fundamental rights and freedoms. You
also have the right to object where we are processing</p>

<p class=MsoNormal>your personal data for direct marketing purposes. In some cases,
we may demonstrate that we have compelling</p>

<p class=MsoNormal>legitimate grounds to process your information which
override your rights and freedoms.</p>

<p class=MsoNormal>مطلوب restriction of processing of your personal data.
This enables you to ask us to suspend the processing of your</p>

<p class=MsoNormal>personal data in the following scenarios:</p>

<p class=MsoNormal>' If you want us to establish the data&amp;()accuracy.</p>

<p class=MsoNormal>' Where our use of the data is unlawful but you do not want
us to erase it.</p>

<p class=MsoNormal>' Where you need us to hold the data even if we no longer
require it as you need it to establish, exercise or</p>

<p class=MsoNormal>defend legal claims.</p>

<p class=MsoNormal>' You have objected to our use of your data but we need to
verify whether we have overriding legitimate</p>

<p class=MsoNormal>grounds to use it.</p>

<p class=MsoNormal>مطلوب the transfer of your personal data to you or to a
third party. We will provide to you, or a third party you have</p>

<p class=MsoNormal>chosen, your personal data in a structured, commonly used,
machine-readable format. Note that this right only applies</p>

<p class=MsoNormal>to automated information which you initially provided
consent for us to use or where we used the information to</p>

<p class=MsoNormal>perform a contract with you.</p>

<p class=MsoNormal>Withdraw consent at any time where we are relying on consent
to process your personal data. However, this will not</p>

<p class=MsoNormal>affect the lawfulness of any processing carried out before
you withdraw your consent. If you withdraw your consent,</p>

<p class=MsoNormal>we may not be able to provide certain products or services
to you. We will advise you if this is the case at the time you</p>

<p class=MsoNormal>withdraw your consent.</p>

<p class=MsoNormal>You will not have to pay a fee to access your personal data
(or to exercise any of the other rights). However, we may</p>

<p class=MsoNormal>charge a reasonable fee if your request is clearly
unfounded, repetitive or excessive. Alternatively, we could refuse to</p>

<p class=MsoNormal>comply with your request in these circumstances.</p>

<p class=MsoNormal>How to contact us? If you have any questions about how we
use your personal data, or you wish to exercise any of the</p>

<p class=MsoNormal>rights set out above, please contact our controller listed
under 1.3 of this Privacy Policy.</p>

</div>

                    <h3>التواصل us:</h3>
        <div style="padding-bottom: 5px;">
            All comments, inquiries, and requests associated with our use of your information are welcomed and should be
            directed to example@example.com or call the numbers provided.
        </div>
        <a href="{{ route('home') }}" class="btn btn-primary">Back</a>
    </div>
    <!-- footer -->
    <section class="footer">
        <div class="container">
            <footer class="">
                <div class="d-flex justify-content-center">
                    <ul class="row">
                        <a href="#" style="padding-right: 10px;"><i class="fab fa-facebook fa-2x"></i></a>
                        <a href="#" style="padding-right: 10px;"><i class="fab fa-twitter fa-2x"></i></a>
                        <a href="#" style="padding-right: 45px;"><i class="fab fa-instagram fa-2x"></i></a>
                    </ul>
                </div><!-- Ende Sozial media -->
                <div class="d-flex justify-content-center">
                    <p> All rights reseved </p>
                </div><!-- Ende Copyright -->
            </footer>
        </div>
    </section>
</body>

<script>
    //ScrollReveal().reveal('.headline');
    //for nav bar
    $('.navbar-nav .nav-link').click(function () {
        $('.navbar-nav .nav-link').removeClass('active');
        $(this).addClass('active');
    });
</script>

</html>
