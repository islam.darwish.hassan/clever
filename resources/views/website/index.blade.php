@extends('layouts.website')


@section('content')

<div class="container-fluid px-md-5 ">

  <div class="home-block mt-5" style="background-image:url({{'./images/Newin_man.jpg'}}">
    <h1 class="home-title">new in</h1>
    <a class="home-button" href="{{route('products',['filter'=>'new_in'])}}">shop</a>
  </div>
  <div class="home-block" style="background-image:url({{'./images/JoinLife_Man.jpg'}}">
    <h1 class="home-title">{{__('winter collection')}}</h1>
    <a class="home-button" href="{{route('products',['sub_category_name'=>'cloth'])}}">shop</a>
  </div>
  {{-- <div class="home-block" style="background-image:url({{'./images/wEditorial02X_1.jpg'}}">
    <h1 class="home-title">{{__('autumn collection')}}</h1>
    <a class="home-button" href="{{route('products',['sub_category_name'=>'autumn'])}}">shop</a>
  </div> --}}
  <div class="home-block" style="background-image:url({{'./images/wEditorial01EditaX_5.jpg'}}">
    <h1 class="home-title">{{__('classic collection')}}</h1>
    <a class="home-button" href="{{route('products',['sub_category_name'=>'cloth'])}}">shop</a>
  </div>

</div>

@endsection

