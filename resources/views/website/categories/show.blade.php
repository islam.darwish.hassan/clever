
@extends('layouts.website')

@section('content')
<div class="hero-wrap hero-bread" style="background: rgb(230,230,230);
background: linear-gradient(0deg, rgba(230,230,230,0.7483368347338936) 0%, rgba(191,237,238,0) 49%, rgba(34,193,195,0.2637429971988795) 100%);

">
{{-- <div class="hero-wrap hero-bread" style="background-image: url({{$category->image}});   position: absolute; z-index:-1;
    background-repeat: no-repeat;    background-size: 100% 100%;   filter: blur(100px);
    -webkit-filter: blur(100px);

    "> --}}
    <div class="container   " >
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9  text-center" >
          <h1 class="mb-0 bread ">{{$category->name}}</h1>
          <p class="breadcrumbs "><span class="mr-2"><a  class="" href="{{route('products.web.index',["category"=>$category->id])}}">Shop All </a></span> </p>
        </div>
      </div>
      @if (session('message'))
      <div class="alert alert-success mt-2 " role="alert">
          <b>{{ session('message') }}</b>
      </div>
    @endif
    @if (session('error'))
      <div class="alert alert-danger mt-2" role="alert" >
          <b>{{ session('error') }}</b>
      </div>
    @endif

    </div>
  </div>
 <section class=" pt-2 ">
    @foreach($subcategories as $subcategory )
    @if($subcategory->products->where('status',App\Models\المنتج::STATUS_ONLINE)->count()>0)
    <div class="container mt-5  " >
      <div class="row justify-content-center mb-3 pb-3  ">
                        <div class="col-md-12 heading-section text-center ">
                            <h1 class="big mt-5" style="z-index:-1">{{$subcategory->name}}</h1>
                            <h2 class=""style="z-index:-1">{{$subcategory->name}}</h2>
                            <p class="breadcrumbs mb-5 "><span class="mr-2"><a  class="" style="z-index: 999" href="{{route('products.web.index',["sub_category"=>$subcategory->id])}}">Shop All </a></span> </p>

                        </div>
        </div>
                {{-- <div class=" d-flex justify-content-end">
                <a  style="z-index:999"class="mb-4" > <strong style="font-family: 'brandfont_bold';" > See More </strong></a>
                </div> --}}
                <div class="incategory-slider owl-carousel ">
                    @foreach(App\Models\المنتج::where('sub_category_id',$subcategory->id)->where('status',App\Models\المنتج::STATUS_ONLINE)->limit(10)->get() as $product)
                    @include('layouts.includes.items.product_item',
                    ['field' => ['name' => $product->name, 'price' => $product->price ,'rate'=>$product->rate ,'id'=>$product->id,'image'=>$product->image,
                    'offer'=>isset($product->offer)?$product->offer->offer:null
                    ]])
                @endforeach
                 </div>
                </div>



                @endif

      @endforeach
    </div>

        </div>
  </section>
@endsection
