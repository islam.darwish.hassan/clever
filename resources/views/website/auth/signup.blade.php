@extends('layouts.auth')


@section('section_a')
<div class="section_a">
<div class="span-"></div>
</div>
@endsection

@section('section_b')
<div class="section_b">
<a href="{{ route('login') }}">login</a>
</div>
@endsection

@section('content')
<div class="auth-card">

  <form class="auth-form" method="POST" action="{{route('confirm_signup')}}">
    @csrf

    <h1 class="title">Create Account</h1>
    <p class="span">Join us now!</p>
    @include('website._includes.message')

    <div class="auth-without-text">
      <div data-validate="">
        <input class="input" type="tetx" name="name" placeholder="Name">
        @error('name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>


      <div data-validate="Valid email is required: ex@abc.xyz">
        <input class="input" type="email" name="email" placeholder="Email address">

        @error('email')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div data-validate="Password is required">
        <input class="input" id="password" type="password" name="password" placeholder="Password" required autocomplete="current-password">
        <i id="togglePassword" class="fa fa-fw fa-eye field-icon"></i>

        @error('password')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-check">
        <div class="female">
          <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
          <label class="form-check-label" for="flexRadioDefault1">
            female
          </label>
        </div>

        <div class="male">
          <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2">
          <label class="form-check-label" for="flexRadioDefault2">
            male
          </label>
        </div>
      </div>

      <div class="remember-me">
        <input class="remember-check" id="" type="checkbox" name="remember-me">
        <label class="remember-label" for="">I Agree Terms & conditions</label>
      </div>

      <div class="login-button">
        <button type="submit" class="">Continue</button>
      </div>
    </div>

    <p class="login-foo">I'm already a user <a class="already-user" href="{{ route('login') }}">Login</a></p>
  </form>

</div>

<script src="{{ asset('js/website2.js') }}"></script>
@endsection
