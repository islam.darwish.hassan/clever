@extends('layouts.auth')


@section('section_a')
<div class="section_a">
<div class="span-"></div>
</div>
@endsection

@section('section_b')
<div class="section_b">
<a href="{{ route('signup') }}">sign up</a>
</div>
@endsection

@section('content')
<div class="auth-card">

  <form class="auth-form" method="POST" action="{{route('confirm_login')}}">
    @csrf
    <h1 class="title">Begin Session</h1>
    <p class="span">Login to see status of order</p>

    <div class="auth-without-text">
      <div data-validate="Valid email is required: ex@abc.xyz">
        <input class="input" type="email" name="email" placeholder="Email Address">

        @error('email')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div data-validate="Password is required">
        <input class="input" id="password" type="password" name="password" placeholder="Password" required autocomplete="current-password">
        {{-- <i id="togglePassword" class="fa fa-fw fa-eye field-icon"></i> --}}

        @error('password')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <a href="#" class="forget-password">Forget Password !</a>

      <div class="remember-me">
        <input class="remember-check" id="" type="checkbox" name="remember-me">
        <label class="remember-label" for="">Remember Me</label>
      </div>

      <div class="login-button">
        <button type="submit" class="">Begin Session</button>
      </div>
    </div>

    <p class="login-foo">We kindly remind you that if you require assistance, our Customer Service team
      is at your disposal.</p>
  </form>

</div>
@endsection
