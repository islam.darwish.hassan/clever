<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>

<head>
    <title>Clever Login Page</title>
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{ asset('images/icons/favicon.ico"/') }}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate2.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/animsition/css/animsition.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/select2.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/daterangepicker/daterangepicker.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/util.css')}}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/login.css')}}">
	<!--===============================================================================================-->

</head>
<!--Coded with love by Mutiullah Samim-->
<body style="background-color: #666666;">

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="POST" action="{{ route('web.forget_password') }}">
					           @csrf


					<span class="login100-form-title p-b-43">
						Forget Password
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100 has-val" type="text" name="email">
						<span class="focus-input100"></span>
						<span class="label-input100">البريد الإلكترونى</span>
						@error('email')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror


					</div>

                    @if (session('message'))
                    <div class="alert alert-success mt-2 " role="alert">
                        <b>{{ session('message') }}</b>
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger mt-2" role="alert" >
                        <b>{{ session('error') }}</b>
                    </div>
                @endif
					<div class="flex-sb-m w-full p-t-3 p-b-32">

						<div>
							<a href="{{route('web.login')}}" class="txt1">
								back to login?
							</a>
                        </div>

					</div>


					<div class="container-login100-form-btn">
						<button  type="submit" class="login100-form-btn">
							{{ __('Reset Password') }}
						</button>
                    </div>
                    <p class="flex-fill text-center my-3">OR</p>
                    <div class="container-login100-form-btn">
                        <a href="{{route('web.client.create')}}" class="login100-form-btn-sec ">
                            Create New Account
                            </a>
                        </div>


                </form>


				<div class="login100-more" style="background-image: url({{ asset('images/bg_1.jpg') }});">
				</div>
			</div>
		</div>
	</div>





<!--===============================================================================================-->
	<script src="{{ asset('vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/daterangepicker/moment.min.js')}}"></script>
	<script src="{{ asset('vendor/daterangepicker/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('js/login.js') }}"></script>

</body>


</html>
