@extends('layouts.website')



@section('content')
  <div class="cart-content">
    <div class="content-1">
      <div class="tab">
        <button class="tablinks" onclick="openTag(event, 'Tab1')" id="defaultOpen">Shopping basket 1</button>
        <button class="tablinks" onclick="openTag(event, 'Tab2')">Shop later 0</button>
      </div>
      
      <div id="Tab1" class="tabcontent">
        <div class="tab-inside">
          <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid" style="height:400px;"alt="img1">
          <div class="tab-details">
            
            <div class="prod-title">
              <div class="prod-tag">
                <p>Polo sweater in 100% merino wool</p>
                <span>Ref. 0916/325/706</span>
              </div>
              <p>EGP 1,499.00</p>
            </div>
            <p class="size">Size M</p>
            <div class="color">
              <p>Colour </p>
              <span class="dot"></span>
              <p> Mink</p>
            </div>
            <p class="quantity">Quantity <span>1</span></p>
            <button>Delete</button>
            <button>Shop later</button>
          </div>
        </div>
      </div>
      
      <div id="Tab2" class="tabcontent">
        <div class="tab-inside2">
          <p>You haven’t saved an item yet!</p> 
        </div>
      </div>
    </div>

    <div class="content-2">
      <div class="order">
        <button class="orderlinks">Order summary</button>
      </div>
      <div class="order-details">
        <div class="order-detail">
          <p>Products</p>
          <p>EGP 1,499.00</p>
        </div>
        <div class="order-detail">
          <p>Estimated delivery costs</p>
          <p>EGP 69.00</p>
        </div>
        <div class="order-detail">
          <p>Discount of shipping expenses</p>
          <p>- EGP 69.00</p>
        </div>
      </div>
      <div class="total-price">
        <p>TOTAL <span>(VAT included)</span></p>
        <p>EGP 1,499.00</p>
      </div>
      <button type="submit" class="order-btn">process order</button>
    </div>
  </div>

  <script>
    function openTag(evt, tagName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(tagName).style.display = "block";
      evt.currentTarget.className += " active";
    }
    
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
  </script>

  <div class="recently-viewed bottom">
    <h5>recently viewed</h5>
    <div class="top-content">
      <div id="carousel-example2" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner row w-100 mx-auto cr-in-3" role="listbox">
          <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-3 active">
            <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img1">
            <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
            <label class="price mx-auto d-block" for="">400 EGP</label>
          </div>
          <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-3">
            <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img2">
            <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
            <label class="price mx-auto d-block" for="">400 EGP</label>
          </div>
          <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-3">
            <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img3">
            <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
            <label class="price mx-auto d-block" for="">400 EGP</label>
          </div>
          <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-3">
            <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img4">
            <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
            <label class="price mx-auto d-block" for="">400 EGP</label>
          </div>
          <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-3">
            <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img5">
            <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
            <label class="price mx-auto d-block" for="">400 EGP</label>
          </div>
          <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-3">
            <img src="{{ asset('./images/9710527611_2_1_8.jpg') }}" class="img-fluid mx-auto d-block" alt="img6">
            <label class="mx-auto d-block" for="">Brigitte Bardot photo T-shirt</label>
            <label class="price mx-auto d-block" for="">400 EGP</label>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carousel-example2" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example2" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
@endsection