@extends('layouts.website')



@section('filter')
    <div id="overlay" >
        <div id="myFilter" class="filter">
            <div class="clear">
                <p>Clear Filters</p>
                <a href="javascript:void(0)" class="closebtn" onclick="closeSearch()">&times;</a>
            </div>

            <div class="filter-by">
                <p>Filter By:</p>
            </div>

            <div class="filter-content">
                <div class="filter-item">
                    <p></p>
                </div>
                <div class="filter-item">
                    <p>Fabric / Care</p>
                    <i class="fas fa-plus"></i>
                </div>
                <div class="filter-item">
                    <p>Color</p>
                    <i class="fas fa-plus"></i>
                </div>
                <div class="filter-item">
                    <p>Size</p>
                    <a id="filterA" onclick="changeIcon()"><i id="filterIcon" class="fas fa-plus"></i></a>
                </div>
                <div class="filter-item">
                    <p>Material</p>
                    <i class="fas fa-plus"></i>
                </div>
            </div>

            <div class="order">
                <p>Order By:</p>
            </div>

            <div class="filter-content">
                <div class="filter-item">
                    <p>Order</p>
                    <i class="fas fa-plus"></i>
                </div>
            </div>

            <button class="filter-btn" onclick="closeFilter()">
                filter
            </button>
        </div>
    </div>

    <script>
        function openFilter() {
            document.getElementById("theFilter").onclick = function() {
                closeFilter();
            }
            document.getElementById("overlay").style.width = "100%";
            document.getElementById("myFilter").style.width = "20%";
        }

        function closeFilter() {
            document.getElementById("theFilter").onclick = function() {
                openFilter();
            }
            document.getElementById("overlay").style.width = "0";
            document.getElementById("myFilter").style.width = "0";
        }

        function changeIcon() {
            document.getElementById("filterA").onclick = function() {
                document.getElementById("filterIcon").classList.remove('fa-plus');
                document.getElementById("filterIcon").classList.add('fa-minus');
            }
        }

    </script>
@endsection

@section('content')
    <div class="content-b container">

        <div class="store-main">
            <div class="top">
                <a id="theFilter" onclick="openFilter()">Filters</a>
            </div>
            <div class=" store-block">
                @foreach ($data as $key => $item)
                    <x-product-card :item="$item" />
                @endforeach
            </div>

            <div class="pagination-container">
                <div class="d-flex justify-content-center">
                    {{ $data->links('vendor.pagination.simple-bootstrap-4') }}
                </div>

            </div>


        </div>

        <script>
            /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
            var dropdown = document.getElementsByClassName("dropdown-btn");
            var i;

            for (i = 0; i < dropdown.length; i++) {
                dropdown[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    var dropdownContent = this.nextElementSibling;
                    if (dropdownContent.style.display === "block") {
                        dropdownContent.style.display = "none";
                    } else {
                        dropdownContent.style.display = "block";
                    }
                });
            }

        </script>
    </div>

@endsection
