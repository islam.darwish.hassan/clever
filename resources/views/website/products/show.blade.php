@extends('layouts.website')




@section('content')
    <div class="product">
        <a href="{{ route('products') }}" class="back">Back</a>
        <div class="product-content row  ">
            <div class=" col-md-8">
                <div class="products-grid ">
                    @foreach ($product_images as $image )
                    <div class=" zooms ">
                        <img class="img-fluid " src="{{$image->image}}" data-magnify-src="{{$image->image }}">
                    </div>


                    @endforeach
                </div>
            </div>
            <div class="product-details col-md-4 p-sm-3 ">
                <div class="animate-top">
                    <h4 class="uppercase  ">{{ $product->name }}-{{$product->size}}-{{$product->color}}</h4>
                    @if($product->offer)
                    <p class="text-muted"><del>{{ $product->price }}EGP</del> </p>

                    <p class="price ">{{ $product->offer->offer }} EGP</p>
                    @else
                    <p class="price ">{{ $product->price }} EGP</p>
                    @endif
                </div>

                <div >
                    <h5 class="uppercase py-2 ">colors</h5>
                    <ul class="colors ">
                        @foreach ($varients as $item )
                        <li>
                            <a href={{route('product',$item[0]->product_id)}}>
                            <img class="img-thumbnail" src={{$item[0]->image}}>
                        </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <h5 class="uppercase py-2">sizes</h5>
                <ul class="sizes">
                    @foreach($sizes as $item)
                    <li>
                        <a href="{{route('product',$item->product_id)}}" >{{$item->size}}</a>
                    </li>
                    @endforeach
                </ul>
                <div class="details">
                    <div class="details-title">
                        <p class="accordion">Details</p>
                        <div class="details-content">
                            <p>- {{$product->description}}</p>
                            <p>- Branch : {{$product->branch}}</p>
                        </div>
                    </div>

                    <div class="details-extend">
                        <div class="details-title">
                            <p class="accordion">Fabric</p>
                            <div class="details-content">
                                <p>- {{$product->fabric}}</p>
                            </div>
                        </div>

                    </div>
                    <form method="POST" action="{{ route('cart.store', $product->id) }}">
                        @csrf
                        <button type="submit" class="details-btn">add to basket</button>
                    </form>

                    <button type="button" class="btn btn-sm  " role="button" data-toggle="modal"
                        data-target="#modal-wishlist-{{ $product->id }}"><i class="fas fa-heart"></i> Add To Wishlist
                    </button>

                </div>
                <script>
                    var acc = document.getElementsByClassName("accordion");
                    var i;

                    for (i = 0; i < acc.length; i++) {
                        acc[i].addEventListener("click", function() {
                            this.classList.toggle("active");
                            var panel = this.nextElementSibling;
                            if (panel.style.maxHeight) {
                                panel.style.maxHeight = null;
                                panel.style.marginBottom = null;
                            } else {
                                panel.style.maxHeight = panel.scrollHeight + "px";
                                panel.style.marginBottom = "15px";
                            }
                        });
                    }

                </script>
            </div>
        </div>
    </div>
    @if ($similar_products->count() > 0)
        <div class="interest">
            <h5>you may interested in</h5>
            <div class="top-content ">
                <div id="carousel-example2" class="carousel slide w-100" data-ride="carousel">
                    <div class="carousel-inner row w-100 mx-auto cr-in-3" role="listbox">
                        @foreach ($similar_products as $key => $item)
                            <div
                                class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-1 @if ($loop->first) active @endif">
                                <a href={{ route('product', $item->id) }}>
                                    <img loading="lazy" src="{{ $item->image }}" class="img-fluid  d-block"
                                        alt="img1">
                                </a>
                                <div class="textContainer">
                                    <p class="m-0 p-0">{{ \Illuminate\Support\Str::upper($item->name . '-' . $item->color . '-' . $item->size) }}
                                    </p>
                                    <a class="text-info"
                                        href={{ route('products', ['sub_category' => $item->sub_category_id]) }}>{{ $item->sub_category->name }}</a>
                                    @if ($item->offer)
                                    <div class="d-flex">

                                        <p class="text-muted"><del>{{ $item->price }} EGP</del></p>
                                        <p class="text-success px-2"> {{ $item->offer->offer }} EGP</p>
                                    </div>
                                    @else
                                        <p class="text-success">{{ $item->price }} EGP</p>

                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
            @endif



    {{-- wishlists --}}
    <form method="POST" action="{{ route('web.mywishlists.add') }}">
        @csrf

        <div class="modal fade" id="modal-wishlist-{{ $product->id }}" tabIndex="-1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="">
                        <h5 class="modal-title" id="exampleModalLabel">My Wishlists</h5>
                        <hr>

                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Wishlists:</label>
                                <div>
                                    @guest
                                        Login to add wishlists
                                    @endguest
                                    @auth
                                        @if (Auth::user()->wishlists->count() > 0)
                                            <input type="hidden" value="{{ $product->id }}" name="product" />
                                            <select class="form-control m-input " name="wishlist">
                                                @foreach (Auth::user()->wishlists as $wishlist)
                                                    <option value="{{ $wishlist->id }}">{{ $wishlist->name }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <a href={{ route('web.mywishlists.create') }}>Create New Wishlist</a>
                                        @endif
                                    @endauth
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add To Wishlist</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@section('extra_scripts')
    <script src={{ asset('js/jquery.zoom.min.js') }}></script>
    <script>
        // $(document).ready(function() {
        //   $('.zoom').magnify();
        // });
        $(document).ready(function() {
            $('.zooms').zoom({
                magnify: 1.5
            });
        });

    </script>

@endsection
@endsection
