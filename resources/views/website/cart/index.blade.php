@extends('layouts.website')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('images/bg_6.jpg');">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9  text-center">
                <h1 class="mb-0 bread">Cart [{{ Cart::getTotalQuantity() }}]</h1>
                <p class="breadcrumbs"><span class="mr-2"><a
                            href="{{ route('home') }}">Home</a></span> </p>
            </div>
        </div>
        <div class="col-md-12">
            @if(session('message'))
                <div class="alert alert-success mt-2 " role="alert">
                    <b>{{ session('message') }}</b>
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger mt-2" role="alert">
                    <b>{{ session('error') }}</b>
                </div>
            @endif
            @if($errors->any())
                <div class="alert  mt-2 px-2">
                    {{-- <p><b>Please fix these errors.</b></p> --}}
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

    </div>
</div>

<section>
    <div class="container">

        <div class="row">
            <div class="col-md-12 ">
                <div class="">
                    @if(Cart::getTotalQuantity()>0)
                        <table class="table">
                            <thead class="thead-primary">
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th style="width:5%">Quantity</th>
                                    <th>Total</th>
                                    <th>#</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach(Cart::getContent() as $item)
                                    <tr class="text-center">
                                        <td class="image-prod"> <a
                                                href="{{ route('product',$item->associatedModel->id) }}"
                                                class="img-prod "><img style="width:100px"
                                                    src="{{ $item->associatedModel->image }}"
                                                    alt="{{ $item->associatedModel->name }}"></a></td>

                                        <td class="product-name">
                                            <h4 class="text-center text-uppercase">{{ $item->associatedModel->name }}
                                               - {{ $item->associatedModel->size }}
                                               - {{ $item->associatedModel->color }}
                                            </h4>
                                            <p class="text-center">{{ $item->associatedModel->description }}</p>
                                        </td>

                                        <td class="price">
                                            @if(isset($item->associatedModel->offer))
                                                <p class="text-muted"><del>
                                                    {{ $item->associatedModel->price }} EGP
                                                </del></p>
                                                {{ $item->associatedModel->offer->offer }}
                                                EGP
                                            @else
                                                {{ $item->associatedModel->price }} EGP
                                        </td>
                                @endif
                                <td class="quantity">
                                    <p class="text-center">{{ $item->quantity }}</p>
                                </td>

                                <td class="total">{{ $item->getPriceSumWithConditions() }} EGP</td>
                                <td class="product-remove">
                                    <form method="POST"
                                        action="{{ route('cart.remove', $item->id) }}">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <button type="submit"
                                            onclick="return confirm('Are you sure you want to remove this item?')"
                                            data-toggle="modal" data-target="#exampleModal" class="btn  px-2  "><i class="fas fa-minus"></i></button>
                                    </form>

                                </td>
                                </tr><!-- END TR-->
                    @endforeach
                    </tbody>
                    </table>
                @else
                    <div class=" d-flex flex-fill justify-content-center">
                        <a href={{ route('home') }} class="btn btn-primary  ">
                            <span class="add-to-cart-btn ">Continue Shopping <span
                                    class="icon-shopping_cart"></span></span>
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">

        </div>
        <form method="POST" action="{{ route('web.checkout') }}">
            @csrf
            @if(Cart::getTotalQuantity()>0)
            <div class="col">
                <div class="form-group ">
                    <label>Choose Delivery Address <span style="color:red">*</span></label>
                    <select class="form-control m-input  " name="address">
                        <option selected value="">Select Destination Address</option>
                        @foreach($addresses as $address)
                            <option value="{{ $address->id }}"
                                {{ (old('address') == $address->id) ? 'selected' : '' }}>
                                {{ $address->address }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            @if(Cart::getTotalQuantity()>0)
            <div class="col">
                <div class="row justify-content-end">
                    <div class="col col-lg-4 col-md-6 mt-5 cart-wrap ">
                        <div class="cart-total mb-3  text-right">
                            <h3 class="border-bottom pb-2 ">Order summary</h3>

                            <p class="text-right ">
                                <b>Subtotal: </b>
                                <span>{{ Cart::getSubTotal() }} EGP</span>
                            </p>
                            {{-- <p class="text-right">
                    <b>Delivery: </b>
                    <span> 0.00 EGP</span>
                </p> --}}
                            <hr>
                            <p class="text-right">
                                <b>Total: </b>
                                <span>{{Cart::getTotal()
                    }} EGP</span>
                            </p>
                        </div>

                        <div class="text-right">
                            <h3 class=""><button type="submit" class=" btn btn-primary py-3 px-5 mt-2 ">
                                    <b>Processed to Checkout</b>

                                </button></h3>
                        </div>
                    </div>
                </div>
            </div>
            @endif
    </div>
    </form>
    </div>

    @if($similar_products->count()>0)
        <div class="interest mt-5">
            <h5>you may interested in</h5>
            <div class="top-content">
                <div id="carousel-example2" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner row w-100 mx-auto cr-in-3" role="listbox">
                        @foreach ($similar_products as $key => $item)
                            <div
                                class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-1 @if ($loop->first) active @endif">
                                <a href={{ route('product', $item->id) }}>
                                    <img loading="lazy" src="{{ $item->image }}" class="img-fluid  d-block"
                                        alt="img1">
                                </a>
                                <div class="textContainer">
                                    <p class="m-0 p-0">{{ \Illuminate\Support\Str::upper($item->name . '-' . $item->color . '-' . $item->size) }}
                                    </p>
                                    <a class="text-info"
                                        href={{ route('products', ['sub_category' => $item->sub_category_id]) }}>{{ $item->sub_category->name }}</a>
                                    @if ($item->offer)
                                    <div class="d-flex">

                                        <p class="text-muted"><del>{{ $item->price }} EGP</del></p>
                                        <p class="text-success px-2"> {{ $item->offer->offer }} EGP</p>
                                    </div>
                                    @else
                                        <p class="text-success">{{ $item->price }} EGP</p>

                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
    @endif


</section>
@endsection
