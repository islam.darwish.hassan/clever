@extends('layouts.website')
@section('content')
<div class="container contain">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 d-flex justify-content-center content1">
                <img src="{{ asset('assets/imgs/2.PNG') }}" alt="">
            </div>
            <div class="col-12 col-md-6   content2" >
            <form id="regForm" action="{{route("add_business.store")}}" method="POST" >

                @csrf
                
                {{-- Start of Tabs     --}}
                <div class="managetab">
                    <h5>Find and manage your business</h5>
                        <div class="input-group my-3">
                            <input type="text" class=" typeahead form-control"  maxlength="30" placeholder="Search by business name"
                                aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-danger" type="button"><i
                                        class="fas fa-search"></i></button>
                            </div>
                        </div>

                    <p class="text-muted small">Can't find your business?</p>
                </div>
                {{-- End of section 1 --}}

                <div class="managetab">
                        <h5>Business الاسم</h5>
                        <div class="input-group my-3">
                            <input type="text" class="form-control required_input" 
                            name="name"
                            oninput="this.className = 'form-control required_input'" placeholder="Enter business name here .." >
                        </div>
                        <h5>Business Category</h5>
                        <div class="input-group my-3   ">
                        <select class="selectpicker w-100 border rounded required_input" data-live-search="true" 
                        name="sub_category_id">
                            <option value="" @if(request()->query('sub_category') == "") selected @endif>اختر ...</option>
                            @foreach (App\Models\Category::all() as $category)
                            <optgroup label={{$category->name}}>
                                @foreach ($category->subcategories as $sub_category)
                              <option value={{$sub_category->id}}
                                @if(request()->query('sub_category') == $sub_category->id) selected @endif>{{$sub_category->name}} </option>
                                @endforeach
                            </optgroup>
                            @endforeach
                          </select>
                        </div>
               
                    <p class="text-muted small">By continuing you agree to the following
                    <a href="{{route('terms')}}">Terms of Service</a> and <a href="{{route('policy')}}"> Privacy Policy</a></p>
                </div>
                {{-- End of section 2 --}}
                <div class="managetab">
                        <h5>Business Address</h5>
                        <div class="input-group my-3">
                            <input type="text" class="form-control" name="address" placeholder="Enter business address here">
                        </div>
                        <h5>Business Governrate</h5>
                        <div class="input-group my-3">
                            <select class="custom-select dynamic"  name="governrate" id="governrate" data-dependent="city">
                                <option value="none"> Select City </option>
                                @foreach (App\Models\Governrate::where('country_id',66)->get() as $govrn)
                                <option value="{{ $govrn->id }}"
                                    @if(request()->query('govrn') == $govrn->id ||old('govrn')== $govrn->id) selected @endif
                                    >{{ $govrn->en_name }}</option>
                                @endforeach
                                </select>
                        </div>
                        <h5>Business City</h5>
                        <div class="input-group my-3">
                            <select class="custom-select dynamic" name="city" id="city">
                                @if(isset($city))
                                @foreach($cities as $city)
                                <option value="">Select City</option>
                                @endforeach
                                @else
                                <option value="">Select Governrate First</option>
                                @endif
                                </select>
                        </div>
                </div>
                <div class="managetab">
                        <h5>تليفون التواصل</h5>
                        <div class="input-group my-3" >
                            <input type="text" class="form-control" placeholder="01xxxxxxxxx" name="contact_phone">
                        </div>
                        <h5>البريد الالكترونى للتواصل</h5>
                        <div class="input-group my-3">
                            <input type="text" class="form-control" placeholder="example@qym.com" name="contact_email">
                        </div>
                        <h5>الموقع الالكترونى</h5>
                        <div class="input-group my-3">
                            <input type="text" class="form-control" placeholder="example" name="website">
                        </div>
                </div>
               {{-- End of section 4 --}}
            {{-- End of Tabs--}}
                    <div style="overflow:auto;">
                        <div style="float:left;">
                            <button type="button" id="prevBtn" class="btn my-2 my-sm-0"
                                onclick="nextPrev(-1)">Previous</button>
                            <button type="button" id="nextBtn" class="btn btn-danger  my-2 my-sm-0"
                                onclick="nextPrev(1)">Next</button>
                        </div>
                    </div>
                    <!-- Circles which indicates the steps of the form: -->
                    <div style="text-align:center;margin-top:40px;">
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                    </div>

            </form>
            </div>
        </div>


</div>

<script>
</script>
<script>
    var currentmanagetab = 0; // Current managetab is set to be the first managetab (0)
    showmanagetab(0);
    // for(v=0;  v<2 ;v++){
    //     showmanagetab(v); // Display the current managetab
    // }

    function showmanagetab(n) {
        // This function will display the specified managetab of the form ...
        var x = document.getElementsByClassName("managetab");
        x[n].style.display = "block";
        // ... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";

        } else {
            document.getElementById("prevBtn").style.display = "inline";

        }
        if (n == 0) {
            document.getElementById("nextBtn").innerHTML = "Add your business to Qym";

        }
        else if (n == (x.length)) {
            document.getElementById("nextBtn").type  = "submit";
            document.getElementById("nextBtn").innerHTML = "Submit";
        } else{
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        // ... and run a function that displays the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which managetab to display
        var x = document.getElementsByClassName("managetab");
        // Exit the function if any field in the current managetab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current managetab:
        x[currentmanagetab].style.display = "none";
        // Increase or decrease the current managetab by 1:
        currentmanagetab = currentmanagetab + n;
        console.log(currentmanagetab,x.length);
        // if you have reached the end of the form... :
        if (currentmanagetab >= x.length) {
            //...the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct managetab:
        showmanagetab(currentmanagetab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("managetab");
        y = x[currentmanagetab].getElementsByClassName("required_input");
        // A loop that checks every input field in the current managetab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";

                // and set the current valid status to false:
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentmanagetab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class to the current step:
        x[n].className += " active";
    }

</script>
<script type="text/javascript">
    var path = "{{ route('login.autocomplete') }}";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
            console.log(data);
                return process(data);
            });
        },  templates: {
    empty: [
      '<div class="empty-message">',
        'unable to find any Best Picture winners that match the current query',
      '</div>'
    ].join('\n'),
    suggestion: function (query, process) {
        return $.get(path, { query: query }, function (data) {
            console.log(data);
                return process(data);
            });
        }
  }

    });

</script>
<script>
    $(document).ready(function() {

        $('.dynamic').change(function() {
            if ($(this).val() != '') {
                var select = $(this).attr("id");
                var value = $(this).val();
                var dependent = $(this).data('dependent');
                var _token = $('input[name="_token"]').val();

                $.ajax({
                    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },

                    url: "{{ route('govrn.fetch') }}",
                    method: "POST",
                    data: {
                        select: select,
                        value: value,
                        _token: _token,
                        dependent: dependent
                    },
                    success: function(result) {
                        $('#' + dependent).html(result);

                    }

                })
            }
        });


        $('#governrate').change(function() {
            $('#city').val('');
        });


    });
</script>


</body>

</html>
@endsection
