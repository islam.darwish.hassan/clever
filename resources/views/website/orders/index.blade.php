
@extends('layouts.website')

@section('content')
<div class="hero-wrap hero-bread" style="background-color: #ededed">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9  text-center">
          <h1 class="mb-0 bread">My Profile</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="">Orders</a></span> </p>
        </div>
      </div>
      @if (session('message'))
      <div class="alert alert-success mt-2 " role="alert">
          <b>{{ session('message') }}</b>
      </div>
    @endif
    @if (session('error'))
      <div class="alert alert-danger mt-2" role="alert" >
          <b>{{ session('error') }}</b>
      </div>
    @endif

    </div>
  </div>
      <section class="pt-5 ">
      <div class="container">
            @foreach ($orders as $order)
                <div class="row my-2">
                <div class="card w-100 animate-top">
                <div class=" h-auto pt-3 ">
                    <div class="container d-flex flex-wrap justify-content-between">
                                        <p class=" mr-2"> Status:
                                            @if ($order->status == 1)
                                            <p class=" ">مطلوب</p>
                                            @elseif ($order->status == 2)
                                            <p class=" ">تحت الفحص</p>
                                            @elseif ($order->status == 3)
                                            <p class=" ">In فى الشحن</p>
                                            @elseif ($order->status == 4)
                                            <p class=" ">تم شحنها</p>

                                            @else
                                            <p class="badge bg-dark text-white text-black">غير محدد</p>
                                            @endif
                                        </p>


                                        <p>
                                            Order placed on : {{Carbon\Carbon::parse($order->created_at)->toDateString()}}
                                        </b>
                                        <p>
                                            Order ID : #{{$order->hash_code}}
                                        </b>
                                            <p>
                                                Recipient : {{$order->client->name}}
                                            </p>
                                            <p>
                                                Payment method: COD
                                            </p>
                                            <p>
                                                Total: {{$order->total_price}} EGP
                                            </p>
                                            <p >
                                                Delivery Address : {{$order->address->address}}
                                            </p>

                             </div>
                    </div>

                @foreach($order->shipments as $shipment)
                <div class="card-body">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                              <tr class="text-left">
                                <th>                 Shipment {{$loop->index +1}} of {{$order->shipments->count()}}
                                </th>
                                <th>المنتج</th>
                                <th>السعر</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>#</th>

                              </tr>
                            </thead>
                            <tbody>
                                @foreach($shipment->ordered_products as $product)

                              <tr class="text-left">

                                <td class="image-prod">  <a href="{{route('product',$product->original_product->id)}}" class="img-prod "><img style="width:100px" src="{{$product->original_product->image}}" alt="{{$product->original_product->name}}"></a></td>

                                <td class="product-name">
                                    <h3>{{Str::limit($product->original_product->name,20)}}</h3>
                                    <p>{{Str::limit($product->original_product->description,70)}}</p>
                                </td>

                                <td class="price">{{$product->product_price/$product->qty}} EGP </td>

                                <td class="quantity">
                                    {{$product->qty}}
                                </td>

                                <td class="total"> {{$product->product_price}} EGP</td>
                                <td class="product-remove">
                                </td>

                              </tr><!-- END TR-->
                              @endforeach
                            </tbody>
                          </table>
                      </div>
                     </div>
                @endforeach
            </div>

          </div>
          @endforeach

      </div>
      <div class="container ">
        {{ $orders->appends(\Request::except('page'))->render('vendor.pagination.bootstrap-4') }}
            @if($orders->total()<=0)
            <div class="row justify-content-center">
                <h5 class="empty-table-text"> There are no orders  😴  </h5>

            </div>
            @endif
        </div>

  </section>
@endsection
