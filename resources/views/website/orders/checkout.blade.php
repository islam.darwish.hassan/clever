
@extends('layouts.website')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('images/bg_6.jpg');">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9  text-center">
        <h1 class="mb-0 bread">Checkout [{{Cart::getTotalQuantity()}}]</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Home</a></span> </p>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    @if (session('message'))
        <div class="alert alert-success mt-2 " role="alert">
            <b>{{ session('message') }}</b>
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger mt-2" role="alert" >
            <b>{{ session('error') }}</b>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger mt-2">
            {{-- <p><b>Please fix these errors.</b></p> --}}
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>


  <section >
    <div class="container">
        <div class="row">
            <div class="d-flex justify-content-between flex-fill">
            {{-- <div class="form-group">
                <label>Choose Specific Address *</label>
                <select class="form-control m-input  " name="address">
                    <option selected value="">Select Destination Address</option>
                    @foreach($addresses as $address)
                    <option value="{{ $address->id }}" {{ (old('address') == $address->id) ? 'selected' : '' }}>
                        {{ $address->address }}</option>
                    @endforeach
                </select>
            </div> --}}
        </div>
        @foreach($order->shipments as $shipmment)
        <div class="col-md-12 ">
            <div class="">
                <table class="table">
                    Shipment {{$loop->index +1}} of {{$order->shipments->count()}}
                    <thead class="thead-primary">
                      <tr class="text-center">
                        <th>#</th>
                        <th>المنتج</th>
                        <th>السعر</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>#</th>

                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($shipmment->ordered_products as $item)


                      <tr class="text-center">

                        <td class="image-prod">  <a href="{{route('product',$item->product_id)}}" class="img-prod "><img style="width:100px"
                            src="{{$item->original_product->image}}" alt="{{$item->original_product->name}}"></a></td>

                        <td style="width:50%" class="product-name">
                            <h3>{{$item->product_name}}</h3>
                            <p>{{$item->original_product->description}}</p>
                        </td>

                        <td class="price">{{$item->product_price/$item->qty}} EGP </td>

                        <td class="quantity">
                            {{$item->qty}}  </td>


                        <td class="total">{{$item->product_price}} EGP</td>

                      </tr><!-- END TR-->
                      @endforeach
                    </tbody>
                  </table>
        @endforeach
              </div>
        </div>
    </div>

    @if(Cart::getTotalQuantity()>0)
    <div class="row justify-content-end">
        <div class="col col-lg-4 col-md-6 mt-5 cart-wrap ">
            <div class="cart-total mb-3  text-right">
                <h3 class="border-bottom pb-2 ">Cart Totals</h3>
                <p class="text-right ">
                    <b>Subtotal: </b>
                    <span>{{$order->total_price}} EGP</span>
                </p>
                <p class="text-right">
                    <b>Delivery: </b>
                    <span> {{$order->delivery_fees}} EGP</span>
                </p>
                <hr>
                <p class="text-right">
                    <b>Total: </b>
                    <span>{{$order->total_price +$order->delivery_fees
                    }} EGP</span>
                    <p> (COD) Cash On Delivery </p>

                </p>
            </div>

            <div class="text-right">

                    <h3 class="">
                        <form method="POST" action="{{route('web.orders.place_order',$order)}}">
                                       @csrf

                            <input type="hidden" value={{$order}}/>
                        <button type="submit" class=" btn btn-primary py-3 px-5 mt-2 " >
                        </form>

                      <b>Cash On Delivery</b>
                    </button></h3>
                    {{-- <p class="text-center mx-auto pt-2">OR</p>
                    <script id="kashier-iFrame"
                    src="https://test-iframe.kashier.io/js/kashier-checkout.js"
                    data-amount="{{$order->total_price}}"
                    data-description="paid from website"
                    data-hash={{$hash}}
                    data-currency="EGP"
                    data-orderId="{{$order->id}}"
                    data-merchantId="MID-6942-489"
                    data-merchantRedirect="{{route('web.myorders.index')}}"
                    data-store="Shanta"
                    data-type="external" data-display="en"> </script> --}}
                    <br>
            </div>


        </div>
        </div>

    @endif
    </div>
</div>

</section>
@endsection
