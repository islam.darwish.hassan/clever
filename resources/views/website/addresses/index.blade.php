
@extends('layouts.website')
@section('content')
<div class="pt-5" style="background-color: #ededed">
    <div class="container pt-5">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9  text-center">
          <h1 class="mb-0 bread">My Profile</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Addresses</a></span> </p>
        </div>
      </div>
      @if (session('message'))
      <div class="alert alert-success mt-2 " role="alert">
          <b>{{ session('message') }}</b>
      </div>
    @endif
    @if (session('error'))
      <div class="alert alert-danger mt-2" role="alert" >
          <b>{{ session('error') }}</b>
      </div>
    @endif

    </div>
  </div>
      <section class=" ">
      <div class="container">
          <div class="d-flex justify-content-center py-5 animate-opacity">
                <a href={{route('web.myaddresses.create')}} >
                    <span class="btn btn-primary">Add New Address <i class="fas fa-plus ml-1"></i></span>
                </a>
          </div>
            @foreach ($addresses as $address)
                <div class="row my-2">
                <div class="card w-100 animate-bottom">
                <div class="card-header d-flex justify-content-between">
                    <b>
                      {{$loop->index+1}} . Delivery Address
                    </b>
                    <div class="d-flex align-items-center">
                        <a href="{{route('web.myaddresses.edit',$address->id)}}" class="btn bg-green text-white mr-2"><i class="ion-md-reorder "></i> Edit Address</a>
                    <form method="POST" action="{{route('web.myaddresses.destroy',$address->id)}}">
                       @csrf
                        {{ method_field('DELETE') }}
                        <button type="submit"
                         onclick="return confirm('Are you sure you want to remove this item?')"
                         data-toggle="modal" data-target="#exampleModal"
                    class="btn"><i class="fas fa-minus"></i></button>
                    </form>
                    </div>

                </div>

                <div class="card-body">
                    <p>Address City: {{$address->city->en_name}}</p>
                    @if (isset($address->apartment))<p>Apartment: {{$address->apartment}} @endif
                   @if (isset($address->building))<p>Bulidng No: {{$address->building}} @endif
                   @if (isset($address->floor))<p>Floor: {{$address->floor}} @endif
                   @if (isset($address->street))<p>Street: {{$address->street}} @endif
                   @if (isset($address->address))<p>Address: {{$address->address}} @endif
                    @if (isset($address->landmark))<p>Landmarks: {{$address->landmark}} @endif

                    @if (isset($address->shipping_notes))<p>فى الشحن Notes: {{$address->shipping_notes}} @endif

                </div>
                </div>
          </div>
          @endforeach
      </div>
      </div>
      <div class="container"> {{ $addresses->appends(request()->query())->links() }}
      </div>

  </section>
@endsection
