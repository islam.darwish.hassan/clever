@extends('layouts.resource.index')
@section('create-btn')
<a href="{{ route('products' . '.create') }}" class=" clr-blue p-2 "><i class="fas fa-plus"></i></a>
@endsection
@section('title', 'ادارة المنتجات')


@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route('all_products' . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User' ]])
                <input type="text" class="form-control mb-2 mr-sm-2" name="product_code" placeholder="البحث بالكود ..." value="{{ request()->query('product_code') }}">

            </div>
            <div class="p-2 row flex-fill">
                <div class=" col form-group">
                    <select name="status" class="custom-select">
                        <option value="0"  @if(request()->query('status') == '0') selected @endif>الحالة...</option>
                        <option value="{{App\Models\Product::STATUS_ONLINE}}"  @if(request()->query('status') == App\Models\Product::STATUS_ONLINE) selected @endif>اونلاين</option>
                        <option value="2"  @if(request()->query('status') == '2') selected @endif>تحت الفحص</option>
                        <option value="3" @if(request()->query('status') == '3') selected @endif>مرفوض</option>
                        <option value="4" @if(request()->query('status') == '4') selected @endif>اوفلاين</option>
                        <option value="5" @if(request()->query('status') == '5') selected @endif>درافت</option>

                    </select>
                </div>

            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block btn-primary  mb-2">تصفية</button>
            </div>
            <a href="{{ route('all_products'. '.index') }}" class="small-header-bold"> X</a>
        </div>
    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-body')

@section('table-header')

        <th scope="col">@sortablelink('id', '#')</th>
        <th scope="col">الصورة</th>
        <th scope="col">@sortablelink('product_code','كود المنتج')</th>
        <th scope="col">@sortablelink('name','المنتج')</th>
        {{-- <th scope="col">@sortablelink('store_id','المحل')</th> --}}
        <th scope="col">@sortablelink('sub_catogrey_id','تصفية فرعية')</th>
        <th scope="col">@sortablelink('price','السعر')</th>
        {{-- <th scope="col">Visits</th> --}}
        <th scope="col">@sortablelink('ordered','طلبت')</th>
        <th scope="col">@sortablelink('stock','فى المخزون')</th>
        <th scope="col">@sortablelink('status','الحالة')</th>
        <th scope="col">@sortablelink('created_at','وقت الاضافة') </th>
        <th scope="col">العمليات </th>
@endsection
@section('table-body')
@foreach($products as $product)
<!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->


<td>{{ $product->id }}</td>
<td><img src={{$product->image}} class="pp-square"></td>
<td>{{ Str::limit($product->name ,70) }} </td>
<td>{{ $product->product_code }}  </td>

{{-- <td>{{ Str::limit($product->store->name ,70) }} </td> --}}
<td>{{ $product->sub_category->name }}  </td>
<td>{{ $product->price }} EGP </td>
{{-- <td>{{ $product->visited }} </td> --}}
<td>{{ $product->ordered }} </td>
<td>{{ $product->stock }} </td>

<td>
    @if ($product->status == 1)
    <span class="badge badge-success text-white">اونلاين</span>
    @elseif ($product->status == 2)
    <span class="badge badge-primary text-black">تحت الفحص</span>
    @elseif ($product->status == 3)
    <span class="badge badge-danger text-white">مرفوض</span>
    @elseif ($product->status == 4)
    <span class="badge bg-dark text-white  ">اوفلاين</span>
    @elseif ($product->status == 5)
    <span class="badge bg-dark text-white text-black">درافت</span>
    @else
    <span class="badge bg-dark text-white text-black">غير محدد</span>
    @endif
</td>

<td>
    @php
    Carbon\Carbon::setlocale("ar");
    echo Carbon\Carbon::parse($product->created_at)->diffForHumans()
    @endphp
</td>

<!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->

<td class="d-flex justify-content-center">
    <a href="{{ route('products.create',["product_code"=>$product->product_code]) }}"
        class="btn btn-primary  "><i class="fas fa-plus"></i></a>
    <a href="{{ route('products.show',[$product->id]) }}"
    class="btn clr-black  "><i class="fas fa-eye"></i></a>
   <a href="{{ route('products.edit', [$product->id]) }}"
     class="btn clr-black "><i class="fas fa-edit"></i></a>
       <form method="POST" action="{{ route('products.'.'destroy',[$product->id]) }}">
           {{ csrf_field() }}
           {{ method_field('DELETE') }}
           <button type="submit"
            onclick="return confirm('Are you sure you want to delete this product?')"
            data-toggle="modal" data-target="#exampleModal"
       class="btn  clr-blue  bg-transparent"><i class="fas fa-trash-alt "></i></button>
       </form>
   </td>
</tr>
@endforeach
@endsection
    {{ $data->appends(request()->query())->links() }}
@section('table-footer')
<p class="table-footer">النتائج :  {{$data->total()}}</p>

@endsection
@endsection
