@extends('layouts.resource.create')
@section('title', 'اضافة تصنيف جديد')


@section('form')

    <form class="p-2 flex-grow-1" method="post" action="{{ route($name . '.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="d-flex bd-highlight">
            <div class="form1 p-2 flex-fill bd-highlight">
                <div class="row">
                    <div class="col form-group">
                        <label>الاسم</label>
                        <input type="text" name="name" class="form-control" placeholder="الاسم" required
                            value="{{ old('name') }}">
                    </div>
                    <div class="col form-group">
                        <label>القسم </label>
                        <select class="form-control form-inline" name="department_id">
                            <option value="none" @if (request()->query('department') == 'none') selected @endif>القسم</option>
                            @foreach ($departments as $department)
                                <option value={{ $department->id }} @if (request()->query('sub_category') == $department->id) selected @endif>{{ $department->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn1 btn btn-block btn-success bg-brandgreen ">اضافة تصنيف جديد</button>
    </form>

@endsection
