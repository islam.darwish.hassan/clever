@extends('layouts.resource.edit')


@section('form')
<form method="POST" action="{{ route($name. '.update', $category->id) }}" enctype="multipart/form-data">

        @csrf

        @method('PUT')

        <div class="d-flex bd-highlight">
            <div class="p-2 flex-fill bd-highlight">
                <div class="row">
                    <div class="col form-group">
                        <label>الاسم</label>
                        <input type="text" name="name" class="form-control" placeholder="الاسم"
                            value="{{ $category->name }}">
                    </div>
                    <div class="col form-group">
                        <label>الصورة</label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="validatedCustomFile">اختر الملف ...</label>
                            <input type="file" name="image" class="custom-file-input" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        <button type="submit" class="btn btn-block btn-success bg-brandgreen ">تحديث</button>

    </form>
@endsection