@extends('layouts.resource.index')
@section('title', "ادارة التصنيفات ")
@section('create-btn')
<a href="{{ route($name . '.create') }}" class="  p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block btn-primary  mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-header')

    <th scope="col" style="width: 10%" >@sortablelink('id', '#')</th>
    <th scope="col">@sortablelink('name', 'أسم التصنيف')</th>
    <th scope="col">@sortablelink('department_id', 'القسم')</th>
    <th scope="col" style="width: 10%">@sortablelink('subcategories_count', 'التصنيفات الفرعية')</th>
    <th scope="col">الصورة</th>
    <th  scope="col" style="width: 10%">العمليات</th>

@endsection



@section('table-body')

    @foreach ($categories as $category)
        <tr>
            <td>{{ $category->id }}</td>
            <td>{{ $category->name }}</td>
            <td>{{ $category->department->name }}</td>
            <td>{{ $category->subcategories_count }}</td>
            <td><img src="{{ $category->image }}" alt="image" class="pp-square"></td>
            <td class="d-flex justify-content-center">
                <a href="{{ route('subs'.'.create', $category->id) }}" 
                    class="btn  "><i class=" fas fa-plus"></i></a>
                <a href="{{ route($name.'.show', $category->id) }}"
                class="btn   "><i class="fas fa-eye"></i></a>
               <a href="{{ route($name.'.edit', $category->id) }}" 
                 class="btn  "><i class="fas fa-edit"></i></a>
                   <form method="POST" action="{{ route($name.'.destroy', $category->id) }}">
                       {{ csrf_field() }}
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this category?')" 
                        data-toggle="modal" data-target="#exampleModal" 
                   class="btn    bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form>
               </td>
        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
    @endsection

@endsection