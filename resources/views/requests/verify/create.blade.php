@extends('layouts.app')
@section('title', 'Create ' . ucfirst(\Illuminate\Support\Str::singular($name)) . ' Info')



@section('content')


<form method="post" action="{{ route($name. '.store') }}" enctype="multipart/form-data">

    @csrf
    <div class="row">
        <div class="col form-group">
            <label>Files</label>
            <div class="custom-file">
                <label class="custom-file-label" for="validatedCustomFile">اختر الملف ...</label>
                <input type="file" name="file_data" id="custom-file-input-image" class="custom-file-input" >
            </div>
            <p>Please upload verification files in ".zip or .rar" formats ,<strong>max size:12mb</strong></p>
        </div>
        <div class="col form-group">
            <label>التواصل البريد الإلكترونى </label>
            <input type="text" name="contact_email" class="form-control" placeholder="التواصل البريد الإلكترونى"
                value="{{ old('contact_email') }}">
        </div>
    </div>
    <div class="row">
        <div class="col form-group">
            <label>Sender Comment</label>
            <textarea type="text" row="5" name="comment" class="form-control" placeholder="Enter comment here..."
             value="{{ old('comment')}}">{{ old('comment')}}</textarea>
        </div>
    </div>
    <br>
    <button type="submit" class="btn btn-block btn-success bg-brandgreen ">Create
        {{ ucfirst(\Illuminate\Support\Str::singular($name)) }}</button>

</form>
<script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>

@endsection
