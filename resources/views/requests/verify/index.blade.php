@extends('layouts.resource.index')
@section('title', 'Verification Requests')


@section('search-filter')
    <!-- Search and Filters START-->
    <x-table-filters name={{$name}}>
        <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
        </div>
    </x-table-filters>
    <!-- Search and Filters END-->

@endsection

@section('table-header')

    <th scope="col" style="width: 5%" >#</th>
    <th scope="col" style="width:10%">المحل</th>
    <th scope="col">Sender البريد الإلكترونى</th>
    <th scope="col">Attachments</th>
    <th  scope="col" style="width: 10%">العمليات</th>

@endsection



@section('table-body')

    @foreach ($data as $row)
        <tr>
            <td>{{ $row->id }}</td>
            <td><a href="{{route('stores.show',$row->user->store->id)}}">{{$row->user->store->name}}</td>
            <td>{{ $row->sender_email }}</td>
            <td>{{ $row->attachments->count()}}</td>
            <td class="d-flex justify-content-center">
                <a href
                    role="button" data-toggle="modal" data-target="#modal-edit-{{ $row->user->store->id }}"
                    class="  float-right pr-2"><i class="fas fa-edit fa-sm"></i></a>
               </td>
               <div class="modal fade" id="modal-edit-{{ $row->user->store->id }}" tabIndex="-1">

                <form method="POST" action="{{route($name.'.store',$row->user->store->id)}}">
                    @csrf
                    @method('PUT')
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{$row->user->store->name}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="col form-group">
                                <label class="form-label">Attachments</label><br>
                            @foreach($row->attachments as $attachment)
                              <a href="{{$attachment->content}}" target="blank">{{$attachment->content}}</a>
                            @endforeach
                            </div>

                            <div class="col form-group">
                                <label class="form-label">مطلوب Comment</label><br>
                                <p>{{$row->comment}}</p>
                            </div>
                            <div class="col form-group">
                                <label class="form-label">Sender email</label><br>
                                <p>{{$row->sender_email}}</p>
                            </div>

                            <div class="col form-group">
                                <label class="form-label">Decision</label><br>
                                <select name="sub_category" class="sub custom-select" id="sub_category">
                                    <option>اختر ...</option>
                                </select>
                            </div>
                              </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn bg-red">Save changes</button>
                          </div>
                        </div>
                      </div>
    
                </form>
    
            </div>
        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
    @endsection

@endsection
