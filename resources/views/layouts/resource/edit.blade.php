@extends('layouts.app')
@section('title', 'تعديل ' . ucfirst(\Illuminate\Support\Str::singular($name)))

@section('back')
    @if(\Illuminate\Support\Facades\Route::has($name . '.index'))
        <a href="{{ route($name . '.index') }}" class=" text-gold " >{{ ucfirst($name) }}</a> / 
    @endif
@endsection



@section('content')

@yield('content-header')
    @yield('form')

@endsection
