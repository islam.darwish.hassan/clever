@extends('layouts.app')
@section('title', 'Create ' . ucfirst(\Illuminate\Support\Str::singular($name)) . ' Info')


@section('back')
    @if(\Illuminate\Support\Facades\Route::has($name . '.index'))
        <a href="{{ route($name . '.index') }}" class="  ">{{ ucfirst($name) }}</a> / 
    @endif
@endsection



@section('content')

    @yield('form')

@endsection

@section('extra-scripts')
@yield('extra_scripts')
@endsection