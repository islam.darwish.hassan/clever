@extends('layouts.app')

@section('content')


    @yield('search-filter')



    <!-- Table START-->
    <table class="table  table-bordered  table-hover table-sm text-center  table-center px-3 border-white">
        <thead class="thead-light  ">
            <tr >
                @yield('table-header')
            </tr>
        </thead>
        <tbody>
                @yield('table-body')
        </tbody>
    </table>
    {{ $data->links('vendor.pagination.bootstrap-4')  }}
    @if($data->total()<=0)
    <div class="row justify-content-center">
        <h5 class="empty-table-text"> لا توجد نتائج  😴  </h5>
    </div>
    @endif
    @yield('table-footer')
    <!-- Table END-->

@endsection

