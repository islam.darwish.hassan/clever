<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clever</title>
    <link rel="icon" type="image/png" href="{{ asset('images/logo.png') }}">
    <link rel="stylesheet" href="{{ asset('css/website6.css') }}">

</head>

<body>

    <div class="clever-navbar ">
        <nav class="navbar">
            {{-- @yield('section_a') --}}
            <div class="section_a">
                {{-- <a id="theDrop2" onclick="openDrop2()" class="under">kids</a> --}}
                <a id="theDrop" onclick="openDrop()" class="under">men</a>
            </div>


            <div class="brand"><a href="{{ route('home') }}"><img loading="lazy"
                        src="{{ asset('images/logo.png') }}" /></a></div>
            {{-- @yield('section_b') --}}
            <div class="section_b ">
                @guest
                    <a href="{{ route('login') }}"><i class="fas fa-user "></i><span class="hide-md">
                            {{ __('Login') }}</span></a>
                @endguest
                <a id="theSearch" onclick="openSearch()"><i class="fas fa-search "></i><span class="hide-md">
                        {{ __('Search') }}</span></a>
                <a href="{{ route('cart.index') }}"><i class="fas fa-shopping-cart pr-2"></i> <span
                        class="hide-md">
                        {{ __('Cart') }}</span>
                    @if (Cart::getTotalQuantity() > 0)
                        [{{ Cart::getTotalQuantity() }}]
                    @endif
                </a>
                @auth

                    <button class="btn btn-primary dropdown-toggle " id="dropdown-menu" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        @if (Auth::user()->role == 2)
                            <span
                                id="text-btn">{{ App\Models\Client::where('user_id', Auth::user()->id)->first()->name }}</span>
                        @else
                            <span id="text-btn">{{ Auth::user()->email }}</span>
                        @endif
                    </button>
                    <div class="dropdown-menu bg-dark text-white dropdown-menu-right" aria-labelledby="dropdown-menu">
                        <li><a class="dropdown-item" href={{ route('web.myaddresses.index') }}> My Addresses</a>
                        <li>
                        <li><a class="dropdown-item" href={{ route('web.mywishlists.index') }}> My Wishlists</a>
                        <li>
                        <li><a class="dropdown-item" href={{ route('web.myorders.index') }}> My Orders</a>
                        <li>

                        <li>
                            <a class="dropdown-item" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </div>
                @endauth

            </div>

        </nav>
        @php
            $department = App\Models\Department::where('name', 'men')->first();
            $m_categories = App\Models\Category::where('department_id', $department->id)
                ->with('subcategories')
                ->limit(5)
                ->get();
        @endphp

        {{-- @yield('drop') --}}
        <div id="myDrop" class="drop">
            <div class="inner">
                <div class="column">
                    <h3> <a href="{{ route('products', ['filter' => 'new_in']) }}">new in</a></h3>
                </div>
                @foreach ($m_categories as $item)
                    <div class="column">
                        <h3>{{ $item->name }}</h3>
                        <ul>
                            @foreach ($item->subcategories as $sub_item)
                                <li>
                                    <a class="text-captalize"
                                        href="{{ route('products', ['sub_category' => $sub_item->id]) }}">{{ $sub_item->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
        @php
            $k_categories = App\Models\Category::where('department_id', '2')
                ->with('subcategories')
                ->limit(5)
                ->get();

        @endphp
        <div id="myDrop2" class="drop">
            <div class="inner">
                <div class="column">
                    <h3> <a href="{{ route('products', ['filter' => 'new_in']) }}">new in</a></h3>
                </div>
                @foreach ($k_categories as $item)
                    <div class="column">
                        <h3>{{ $item->name }}</h3>
                        @foreach ($item->subcategories as $sub_item)
                            <a class="text-captalize"
                                href="{{ route('products', ['sub_category' => $sub_item->id]) }}">{{ $sub_item->name }}</a>
                        @endforeach
                    </div>
                @endforeach
            </div>



        </div>

        <script>
            function openDrop() {
                closeDrop();
                closeDrop2();
                document.getElementById("myDrop").style.height = "100%";
                document.getElementById("myDrop").style.opacity = "1";
            }

            function closeDrop() {
                document.getElementById("myDrop").style.height = "0";
                document.getElementById("myDrop").style.opacity = "0";
            }

            function openDrop2() {
                closeDrop();
                closeDrop2();

                document.getElementById("myDrop2").style.height = "100%";
                document.getElementById("myDrop2").style.opacity = "1";
            }

            function closeDrop2() {
                document.getElementById("myDrop2").style.height = "0";
                document.getElementById("myDrop2").style.opacity = "0";
            }
        </script>

        {{-- @yield('search') --}}
        <div id="mySearch" class="search">
            <a href="javascript:void(0)" class="closebtn" onclick="closeSearch()">&times;</a>
            <div class="search-bar">
                <i class="fas fa-search"></i>
                <input id="search_area" placeholder="What you are searching for ...">
            </div>
            @php
                $top_content = App\Models\Product::
                with('offer')
                ->inRandomOrder()
                    ->limit(10)
                    ->get();
                $top_searches = App\Models\Product::with('offer')
                    ->inRandomOrder()
                    ->orderBy('ordered')
                    ->limit(10)
                    ->get();
            @endphp
            <!-- Top content -->
            <div class="top-content">
                <div id="carousel-example" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner row w-100 mx-auto cr-in-1" role="listbox">
                        @foreach ($top_content as $key => $item)
                            <div
                                class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-1 @if ($loop->first) active @endif">
                                <a href={{ route('product', $item->id) }}>
                                    <img loading="lazy" src="{{ $item->image }}" class="img-fluid mx-auto d-block"
                                        alt="img1">
                                </a>
                                <div class="textContainer">
                                    <p class="m-0 p-0">{{ \Illuminate\Support\Str::upper($item->name . '-' . $item->color . '-' . $item->size) }}
                                    </p>
                                    <a class="text-info"
                                        href={{ route('products', ['sub_category' => $item->sub_category_id]) }}>{{ $item->sub_category->name }}</a>
                                    @if ($item->offer)
                                    <div class="d-flex">

                                        <p class="text-muted"><del>{{ $item->price }} EGP</del></p>
                                        <p class="text-success px-2"> {{ $item->offer->offer }} EGP</p>
                                    </div>
                                    @else
                                        <p class="text-success">{{ $item->price }} EGP</p>

                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-next text-primary" href="#carousel-example" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

            <!-- Top searches -->
            <div class="top-content top-search">
                <h3>top searches</h3>
                <div id="carousel-example1" class="carousel slide" data-ride="carousel1">
                    <div class="carousel-inner row w-100 mx-auto cr-in-1" role="listbox1">
                        @foreach ($top_searches as $key => $item)
                            <div
                                class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 cr-it-1 @if ($loop->first) active @endif">
                                <a href={{ route('product', $item->id) }}>
                                    <img loading="lazy" src="{{ $item->image }}" class="img-fluid mx-auto d-block"
                                        alt="img1">
                                </a>
                                <div class="textContainer">
                                    <p class="m-0 p-0">{{ \Illuminate\Support\Str::upper($item->name . '-' . $item->color . '-' . $item->size) }}
                                    </p>
                                    <a class="text-info"
                                        href={{ route('products', ['sub_category' => $item->sub_category_id]) }}>{{ $item->sub_category->name }}</a>
                                    @if ($item->offer)
                                    <div class="d-flex">

                                        <p class="text-muted"><del>{{ $item->price }} EGP</del></p>
                                        <p class="text-success px-2"> {{ $item->offer->offer }} EGP</p>
                                    </div>
                                    @else
                                        <p class="text-success">{{ $item->price }} EGP</p>

                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div> <a class="carousel-control-prev" href="#carousel-example1" role="button1" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-example1" role="button1" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

        </div>

        <script>
            // Get the input field
            var input = document.getElementById("search_area");

            // Execute a function when the user releases a key on the keyboard
            input.addEventListener("keyup", function(event) {
                // Number 13 is the "Enter" key on the keyboard
                if (event.keyCode === 13) {
                    window.location.href = "{{ route('products') }}" + '?search=' + input.value
                }
            });

            function openSearch() {
                document.getElementById("theSearch").onclick = function() {
                    closeSearch();
                }
                document.getElementById("mySearch").style.height = "100%";
                document.getElementById("mySearch").style.opacity = "1";
            }

            function closeSearch() {
                document.getElementById("theSearch").onclick = function() {
                    openSearch();
                }
                document.getElementById("mySearch").style.height = "0";
                document.getElementById("mySearch").style.opacity = "0";
            }
        </script>
    </div>

    @yield('filter')
    <div class="contentContainer">
        @yield('content')

    </div>
    @include('layouts.includes.footer')
</body>
<link rel="stylesheet" href="https://use.typekit.net/oov2wcw.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnify/2.3.3/css/magnify.css"
    integrity="sha512-JxBFHHd+xyHl++SdVJYCCgxGPJKCTTaqndOl/n12qI73hgj7PuGuYDUcCgtdSHTeXSHCtW4us4Qmv+xwPqKVjQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
{{-- <script src="{{ asset('js/app.js') }}"></script> --}}
<script src="{{ asset('js/website2.js') }}"></script>
@yield('extra_scripts')

</html>
