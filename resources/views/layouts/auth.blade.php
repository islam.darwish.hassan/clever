<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clever</title>
    <link rel="icon" type="image/png" href="{{ asset('images/logo.png') }}">
    <link rel="stylesheet" href="{{ asset('css/website.css') }}">
    <link rel="stylesheet" href="https://use.typekit.net/oov2wcw.css">
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/website2.js') }}"></script>
</head>

<body>

    <div class="clever-navbar">
        <nav class="navbar">
            {{-- @yield('section_a') --}}
            <div class="section_a">
              </div>


            <div class="brand"><a href="{{ route('home') }}"><img
                        src="{{ asset('images/logo.png') }}" /></a></div>
            {{-- @yield('section_b') --}}
            <div class="section_b ">
                <a href="{{ route('signup') }}"></i>Sign UP</a>
              </div>

        </nav>
@yield('content')

</body>
<script src="{{ asset('js/website2.js') }}"></script>
@stack('extra')

</html>
