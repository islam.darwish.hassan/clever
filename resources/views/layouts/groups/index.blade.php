@extends('layouts.resource.index')
@section('title', 'Businessess Groups')

@section('create-btn')
<a href 
role="button" data-toggle="modal" data-target="#createModel"
class="  p-2 "><i class="fas fa-plus"></i>Add Group</a>

<!-- Modal -->
<div class="modal fade" id="createModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <form method="POST" action="{{route('groups.store')}}">
        @csrf
            <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="createModel">Add New Group</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="col form-group">
                        <label class="form-label">Title *</label>
                    <input type="text" name="title" class="form-control" placeholder="Enter title here ... * "
                    value="{{ old('title') }}"> 
                    </div>
                    <div class="col form-group">
                        <label class="form-label">Description</label>

                    <input type="text" name="description" class="form-control" placeholder="Enter description here ... "
                    value="{{ old('description') }}"> 
                </div>

                    <div class="col form-group">

                        <label class="form-label">Additional Info</label>
                    <input type="text" name="additional_info" class="form-control" placeholder="Enter additional_info here ... "
                    value="{{ old('additional_info') }}"> 
                </div>

                    </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn bg-red">Save changes</button>
                </div>
            </div>
            </div>
    </form>
  </div>
  
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->

@endsection

@section('table-header')

    <th scope="col" style="width: 5%" >#</th>
    <th scope="col">Title</th>
    <th scope="col">الصورة</th>
    <th scope="col">Group Owner</th>
    <th scope="col">Stores </th>
    <th scope="col">Type </th>
    <th scope="col">وقت الاضافة </th>
    <th  scope="col" style="width: 10%">العمليات</th>

@endsection



@section('table-body')

    @foreach ($data as $row)
        <tr>
            <td>{{ $row->id }}</td>
            <td><a href="{{route($name.".show",$row->id)}}">{{ $row->title }}</a></td>
            <td><img src="{{ isset($row->adminstore)?$row->adminstore->avatar:null}}" alt="avatar" class="pp-square"></td>
            <td><a href="{{isset($row->adminstore)?route('stores.show',$row->adminstore->id):null}}">{{isset($row->adminstore)?$row->adminstore->name:"No المشرف"}}</a></td>
            <td>{{ $row->stores_count}}</td>
            <td>{{isset($row->type)?($row->type==App\Models\Group::TYPE_BRANCHES?'Branches':"Custom"  ): 'Not Specific'}}</td>
            <td>{{ $row->created_at}}</td>

            <td class="d-flex justify-content-center">
                <a href class="btn"   data-toggle="modal" data-target="#modal-add-{{ $row->id }}"
                    > @if(request()->query('add_group')) Add Selected المحل @endif<i class="fas fa-plus"></i></a>
                <a href="{{ route($name.'.show', $row->id) }}"
                    class="btn  "><i class="fas fa-eye"></i></a>

                <div class="modal fade" id="modal-add-{{ $row->id }}"  tabIndex="-1">
                    <form method="POST" action="{{route('groups.add',$row->id)}}" >
                        @csrf
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{$row->title}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <div class="col form-group">
                                    <label class="form-label float-left">Business</label><br>
                                    <select name="store" class="custom-select" id="store">
                                        <option>اختر ...</option>
                                        @foreach ($stores as $store)
                                        <option value="{{ $store->id }}"  @if(request()->query('add_group') == $store->id) selected @endif >{{ $store->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                  </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn bg-red">Save changes</button>
                              </div>
                            </div>
                          </div>
                    </form>
                  {{-- 
                   <form method="POST" action="{{ route($name.'.destroy', $row->id) }}">
                      @csrf
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this row?')"
                        data-toggle="modal" data-target="#exampleModal"
                   class="btn    bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form> --}}
               </td>
        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
    @endsection
    <script>
        $(document).ready(function () {
            //to get params from url
            $.urlParam = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                return results[1] || 0;
            }
            var add_group = $.urlParam('add_group');
            if (add_group) {
                console.log(add_group);
                // $(".sel").val(type);
                // $(".form" + type).show();
            }
        });
    
    </script>
@endsection
