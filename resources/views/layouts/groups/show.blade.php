@extends('layouts.resource.index')
@section('title', $group->title)


@section('search-filter')
@if(isset($group->adminstore))
<strong>Group owner:
    <img src="{{ $group->adminstore->avatar}}" alt="avatar" class="pp-square img-thumbnail">
    <a href="{{route('stores.show',$group->adminstore->id)}}">{{$group->adminstore->name}}</a>
</strong>
@endif
<form class="form " action="{{ route($name . '.index') }}" method="get">
    <div class="d-flex justify-content-between">
        <div class="p-2 flex-fill ">
            @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
        </div>
        <div class="p-2 flex-fill ">
            <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
        </div>
        <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

    </div>

</form>
@endsection

@section('table-header')

    <th scope="col" style="width: 5%" >#store</th>
    <th scope="col">الصورة</th>
    <th scope="col">الاسم</th>
    <th scope="col">تليفون</th>
    <th scope="col">Address</th>
    <th scope="col">Location</th>
    <th scope="col">الحالة </th>
    <th scope="col">Added At </th>
    <th  scope="col" style="width: 10%">العمليات</th>

@endsection



@section('table-body')

    @foreach ($data as $row)
        <tr>
            <td><a href="{{route('stores.show',$row->id)}}">{{ $row->id }}</a></td>
            <td><img src="{{ $row->avatar}}" alt="avatar" class="pp-square img-thumbnail"></td>
            <td><a href="{{route('stores.show',$row->id)}}">{{$row->name}}</a></td>
            <td>{{ $row->phone?$row->phone:"Not available"}}</td>
            <td>{{ Str::limit($row->address,40)}}</td>
            <td><a href="https://www.google.com/maps/search/{{$row->latitude}},{{$row->longitude}}" target="blank">View Map</a></td>

            <td>
                @if ($row->status == 1)
                <span class="badge badge-success text-white">Verified</span>
                @elseif ($row->status == 2)
                <span class="badge badge-warning text-black">Not Verified</span>
                @elseif ($row->status == 3)
                <span class="badge badge-danger text-white">Suspended</span>
                @elseif ($row->status == 4)
                <span class="badge badge-primary text-white">Accepted</span>
                @else
                <span class="badge bg-dark text-white text-black">غير محدد</span>
                @endif
            </td>
            <td>{{ $row->created_at}}</td>

            <td class="d-flex justify-content-center">
                   <form method="POST" action="{{ route('comparelists'.'.store', $row->id) }}">
                      @csrf
                      <input hidden name="store_id" value={{$row->id}} />
                       <button type="submit"
                        onclick="return confirm('Do you want to add this item to compare list?')"
                        data-toggle="modal" data-target="#exampleModal"
                         class="btn btn-success "><i class="fas fa-stream "></i> Add To Comparelist</button>
                   </form>
                </td>
        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
    @endsection

@endsection
