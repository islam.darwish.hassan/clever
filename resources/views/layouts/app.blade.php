<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>كلفير - @yield('title')</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('head_charts')




</head>


<body >
    <div class="app-container app-theme-black body-tabs-shadow fixed-sidebar fixed-header">
        {{-- header --}}
        <div class="app-header header-shadow  header-text-light">
            {{-- logo --}}
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane mr-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--collapse-r"  data-class="closed-sidebar" >
                            <span class="hamburger-box" >
                                <span class="hamburger-inner" ></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--arrowalt mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="app-header__content">
                {{-- left --}}
                {{-- right --}}
                <div class="app-header-right">
                    <div class="header-btn-lg ">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">

                                <div class="widget-content-left  header-user-info ">
                                    <div class="widget-heading px-2">
                                        {{Auth::user()->email}}
                                    </div>
                                </div>
                                <div class="widget-content-left">
                                    <div class="  btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <i class="fa fa-angle-down  opacity-8"></i>
                                        </a>

                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-left">
                                            <li >
                                                <a href="{{route('home')}}" tabindex="0" class="dropdown-item">الذهاب الى الموقع</a>
                                                <div tabindex="-1" class="dropdown-divider"></div>
                                                <a   class="dropdown-item" href="{{ route('admin_logout') }}"
                                                onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                    تسجيل الخروج
                                                    </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- header-end --}}

        <div class="app-main">
            {{-- sidebar --}}
            <div class="app-sidebar  ">
                <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner ">
                        {{-- sidebar content --}}
                        @can('view-super',Auth::user())
                        <ul class="vertical-nav-menu">
                            <li class="app-sidebar__heading">لوحة التحكم</li>
                            <li>
                                <a href="{{ route('dashboard.index') }}" @if(request()->routeIs('dashboard.index'))class="mm-active"@endif >
                                    <i class="metismenu-icon pe-7s-rocket"></i>
                                    اللوحة الرئيسية
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('categories.index') }}" @if(request()->routeIs('categories.index'))class="mm-active"@endif >
                                    <i class="metismenu-icon  pe-7s-keypad"></i>
                                    التصنيفات والفرعيات
                                </a>
                            </li>
                             <li @if(request()->routeIs('clients.*'))class="mm-active"@endif
                                @if(request()->routeIs('users.*'))class="mm-active"@endif
                                >
                                <a href="#" >
                                    <i class="metismenu-icon pe-7s-users "></i>
                                    المستخدمين
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{route('clients.index')}}" @if(request()->routeIs('clients.index'))class="mm-active"@endif >
                                            ادارة مستخدمين الموقع
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('users.index')}}" @if(request()->routeIs('users.index'))class="mm-active"@endif>
                                            ادارة كل المستخدمين

                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li @if(request()->routeIs('all_products.*'))class="mm-active"@endif

                                >
                                <a href="#" >
                                    <i class="metismenu-icon pe-7s-shopbag "></i>
                                    المنتجات
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ route('all_products.index') }}" @if(request()->routeIs('all_products.*'))class="mm-active"@endif >
                                            ادارة المنتجات
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li @if(request()->routeIs('orders.*'))class="mm-active"@endif>
                                <a href="#" >
                                    <i class="metismenu-icon pe-7s-shopbag "></i>
                                    الطلبات
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ route('orders.index') }}" @if(request()->routeIs('orders.*'))class="mm-active"@endif >
                                            ادارة الطلبات
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                        @endcan

                    </div>
                </div>

            </div>
            {{-- content --}}
            <div class="app-main__outer">
                @if (session('message'))
                <div class="alert alert-success mt-2" role="alert">
                    <b>{{ session('message') }}</b>
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger mt-2" role="alert" dir="rtl">
                    <b>{{ session('error') }}</b>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger mt-2">
                    <p><b>Please fix these errors.</b></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-header bg-white mt-1  " >
                    <div>
                        <div>
                        @yield('back')
                        </div>
                        <div>
                        @yield('title')
                        @yield('create-btn')
                        </div>
                    </div>
                </div>
                <div class="app-main__inner">
                    @yield('content')
            </div>
            {{-- footer --}}
            <div class="app-wrapper-footer">
                <div class="app-footer">
                    <div class="app-footer__inner">
                        <div class="app-footer-left">
                        </div>
                        <div class="app-footer-right">
                            <ul class="nav">
                                </li>
                                <li class="nav-item">
                                    <a href="javascript:void(0);" class="nav-link">
                                        <div class="badge badge-success ">
                                            <small>الاصدار</small>
                                        </div>
                                        1.0.0
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class='cookies d-flex justify-content-center'>
                    @include('cookieConsent::index')
                </div>


            </div>
</body>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('extra-scripts')

</html>



