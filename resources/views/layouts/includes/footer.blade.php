<div class="footer container">
    <hr>
    <div class="about">
      <div class="about-1">
        <p class="tag">Help</p>
        <p><a href="#">Frequently asked questions</a></p>
        <p><a href="#">How to purchase</a></p>
        <p><a href="#">Transport and delivery</a></p>
        <p><a href="#">Exchanges and returns</a></p>
        <p><a href="#">Payment</a></p>
        <p><a href="#">Contacts</a></p>
      </div>
      <div class="about-2">
        <p class="tag">Company</p>
        <p><a href="#">History of the brand</a></p>
        <p><a href="#">Inditex</a></p>
        <p><a href="#">Policy</a></p>
        <p><a href="#">Work with us</a></p>
        <p><a href="#">Privacy policy</a></p>
        <p><a href="#">Cookie settings</a></p>
      </div>
      <div class="about-3">
        <p class="tag">Follow us</p>
        <p><a href="#">Facebook</a></p>
        <p><a href="#">Twitter</a></p>
        <p><a href="#">Youtube</a></p>
        <p><a href="#">Pinterest</a></p>
        <p><a href="#">Instegram</a></p>
        <p><a href="#">Newsletter</a></p>
      </div>
    </div>
    <div class="copyright">
      <p>copyrighted &copy 2021</p>
    </div>
  </div>
