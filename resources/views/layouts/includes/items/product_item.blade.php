<div class="item shadow ml-2 ">

    <div>
        <a href="{{route('products.index',$field['id'])}}" class="img-prod ">
            <img class="img-fluid" src="{{$field['image']}}" alt="{{$field['name']}}">

            @if(isset($field['offer']))
            <div class="badge-overlay ">
                <span class="top-right badge blue"> {{$field['offer']}}% Off</span>
            </div>
            @endif
        </a>

        <div class="text pt-3 px-2 ">
            <div style="height:100px">
                <p style="height:50px"><a class="clr-green text-bold"
                        href="{{route('product',$field['id'])}}">{{Str::limit($field['name'],50)}}</a></p>
                <div class="d-flex align-items-center">
                    <div class="pricing">
                            @if(isset($field['offer']))
                            <p class="price mb-0"><span>
                            {{$field['price']-(($field['price']*$field['offer'])/100)}} EGP {{""}}
                             </span></p>
                            <small class="text-muted" style="  text-decoration: line-through;
                            ">{{$field['price']}} EGP</small>
                            @else
                            {{$field['price']}} EGP

                            @endif
                    </div>
                    <div class="flex-fill">
                        <p class="text-right">
                            @for($j = 0; $j < $field['rate']; $j++) <span class="ion-ios-star"></span>
                                @endfor
                                @for($j = $field['rate']; $j <5; $j++) <span class="ion-ios-star-outline"></span>
                                    @endfor
                        </p>
                    </div>
                </div>
            </div>
            <hr>

            <div class="d-flex align-content-center">
                <form method="POST" action="{{ route('cart.store', $field['id']) }}">
                   @csrf
                    <button type="submit" class="btn btn-primary pb-3 ">
                        <span class="text_brand_bold_cap">Add to cart <i class="fas fa-plus ml-1"></i></span>
                    </button>
                </form>
                {{-- <a href="#" class="mt-2"><span><i class="ion-ios-heart-empty"></i></span></a> --}}
                <button type="button" class="btn pb-3 clr-green p-0 bg-transparent " role="button" data-toggle="modal"
                    data-target="#modal-wishlist-{{$field['id']}}">
                    <span><i class="ion-ios-heart-empty"></i></span></button>

            </div>

        </div>
    </div>

</div>
