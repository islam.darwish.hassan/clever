@extends('layouts.app')
@section('title', "احصائيات عن الموقع")
@section('head_charts')
<!-- Charting library -->
<script src="{!! mix('js/charts.js') !!}"></script>
@endsection
@section('content')
<!--Statistics-->
<div class="row">
    <div class="col"
        onclick="window.location='{{ route('orders' . '.index' ) }}'">
        <div class="card bg-brand mb-2  flex-fill">
            <div class="card-header  d-flex justify-content-center">
                <i class="fas fa-box-open text-white my-3 fa-2x"></i>
            </div>
            <div class="card-body bg-brand  text-center text-bold ">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">المنتجات :  </div>
                    <div class="text-vlarge text-white ">{{ $products_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">الاونلاين:</div>
                    <div class="text-vlarge text-white">{{ $product_online_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">الاوفلاين:</div>
                    <div class="text-vlarge text-white">{{ $product_offline_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">المرفوضة:</div>
                    <div class="text-vlarge text-white">{{ $product_rejected_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">تحت الفحص:</div>
                    <div class="text-vlarge text-white">{{ $product_inreview_count }}</div>
                </div>

            </div>
        </div>
    </div>
    <div class="col"
        onclick="window.location='{{ route('users' . '.index' ) }}'">
        <div class="card bg-brand mb-2  flex-fill">
            <div class="card-header  d-flex justify-content-center">
                <i class="fas fa-user-ninja text-white my-3 fa-2x"></i>
            </div>
            <div class="card-body  text-center text-bold ">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white text-bold "> المستخدمين: </div>
                    <div class="text-vlarge text-white ">{{ $clients_count +$admins_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">مستخدمين الموقع:</div>
                    <div class="text-vlarge text-white">{{ $clients_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">المشرفين:</div>
                    <div class="text-vlarge text-white">{{ $admins_count }}</div>
                </div>

            </div>
        </div>
    </div>

    <div class="col"
        onclick="window.location='{{ route('users' . '.index' ) }}'">
        <div class="card bg-brand mb-2  flex-fill">
            <div class="card-header  d-flex justify-content-center">
                <i class="fas fa-air-freshener text-white my-3 fa-2x"></i>
            </div>
            <div class="card-body  text-center text-bold ">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white "> التصنيفات : </div>
                    <div class="text-vlarge text-white ">{{ $categories_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">التصنيفات الفرعية:</div>
                    <div class="text-vlarge text-white">{{ $sub_categories_count }}</div>
                </div>

            </div>
        </div>
    </div>
    <div class="col"
        onclick="window.location='{{ route('users' . '.index' ) }}'">
        <div class="card bg-brand mb-2  flex-fill">
            <div class="card-header  d-flex justify-content-center">
                <i class="fas fa-shopping-cart text-white my-3 fa-2x"></i>
            </div>
            <div class="card-body  text-center text-bold ">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white "> الطلبات: </div>
                    <div class="text-vlarge text-white ">{{ $orders_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">مطلوب:</div>
                    <div class="text-vlarge text-white">{{ $orders_requests_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">تحت الفحص:</div>
                    <div class="text-vlarge text-white">{{ $orders_inprocess_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">فى الشحن:</div>
                    <div class="text-vlarge text-white">{{ $orders_inshipping_count }}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="text-uppercase text-white ">تمت شحنها:</div>
                    <div class="text-vlarge text-white">{{ $orders_shipped_count }}</div>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- Charts --}}
<div class="row">
    {{-- Chart --}}
    <div class=" col-lg-6 p-3 ">
        <div class="card-shadow-info  widget-chart   card  ">
            <div class="card-header-tab card-header bg-transparent">
                <div class="card-header-title">
                    <i class="header-icon lnr-bicycle icon-gradient bg-love-kiss"> </i>
                    
                    @foreach( $datasets as $dataset )
                        {{ $dataset['label'] }}
                    @endforeach
                </div>
                <ul class="nav">
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg-{{$config['id']}}-0" class="active nav-link">الرسم البيانى</a>
                    </li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg-{{$config['id']}}-1" class="nav-link">البيانات</a></li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-eg-{{$config['id']}}-0" role="tabpanel">
                        <div class="col-12">
                            <x-basic-chart :config="$config" :labels="$months" :datasets="$datasets">
                            </x-basic-chart>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-eg-{{$config['id']}}-1" role="tabpanel">
                        @foreach($datasets as $dataset)
                            <div style="row px-5 ">
                                <div class="mt-2 pr-3">
                                    <b>{{ ucfirst($dataset['label']) }}:</b>
                                    <hr>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        @foreach($months as $label )
                                        <div class="col-lg-4 col-md-6">
                                        <div class="card mb-3 widget-content" style="background-color:{{$dataset['labelBackgroundColor']}}">
                                                <div class="widget-content-wrapper text-white">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">{{ $label }}</div>
                                                        <div class="widget-subheading">الرسم البيانى </div>
                                                    </div>
                                                    <div class="widget-content-right">
                                                        <div class="widget-numbers text-white">
                                                            <span>{{ $dataset['data'][$loop->index] }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
    
                        @endforeach
    
    
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- End chart --}}
    <div class=" col-lg-6 p-3 ">
        <div class="card-shadow-info  widget-chart   card  ">
            <div class="card-header-tab card-header bg-transparent">
                <div class="card-header-title">
                    <i class="header-icon lnr-bicycle icon-gradient bg-love-kiss"> </i>
                    
                    @foreach( $datasets_2 as $dataset )
                        {{ $dataset['label'] }}
                    @endforeach
                </div>
                <ul class="nav">
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg-{{$config_2['id']}}-0" class="active nav-link">الرسم البيانى</a>
                    </li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg-{{$config_2['id']}}-1" class="nav-link">البيانات</a></li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-eg-{{$config_2['id']}}-0" role="tabpanel">
                        <div class="col-12">
                            <x-basic-chart :config="$config_2" :labels="$months" :datasets="$datasets_2">
                            </x-basic-chart>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-eg-{{$config_2['id']}}-1" role="tabpanel">
                        @foreach($datasets as $dataset)
                            <div style="row px-5 ">
                                <div class="mt-2 pr-3">
                                    <b>{{ ucfirst($dataset['label']) }}:</b>
                                    <hr>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        @foreach($months as $label )
                                        <div class="col-lg-4 col-md-6">
                                        <div class="card mb-3 widget-content" style="background-color:{{$dataset['labelBackgroundColor']}}">
                                                <div class="widget-content-wrapper text-white">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">{{ $label }}</div>
                                                        <div class="widget-subheading">الرسم البيانى </div>
                                                    </div>
                                                    <div class="widget-content-right">
                                                        <div class="widget-numbers text-white">
                                                            <span>{{ $dataset['data'][$loop->index] }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
    
                        @endforeach
    
    
                    </div>
                </div>
            </div>
        </div>
    </div>
{{-- End chart --}}
</div>

<!--end of Statistics-->
@endsection
