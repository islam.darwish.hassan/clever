@extends('layouts.resource.index')


@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form-inline" action="{{ route($name . '.index') }}" method="get">
        <label class="sr-only" for="inlineFormInputName2">الاسم</label>
        <input type="text" class="form-control mb-2 mr-sm-2" name="query" placeholder="Search for category name...">
        <button type="submit" class="btn btn-primary mb-2">Search</button>
    </form>
    <!-- Search and Filters END-->
@endsection




@section('table-header')
    
    <!------------------------------------------------------------- Columns START---------------------------------------------------------------------------->


    <th scope="col">#</th>    
    <th scope="col">Flag</th>
    <th scope="col">English الاسم</th>
    <th scope="col">Arabic الاسم</th>
    
    
    <!------------------------------------------------------------- Columns END---------------------------------------------------------------------------->
    
    <th scope="col">View</th>
    <th scope="col">تعديل</th>
    <th scope="col">Delete</th>

@endsection



@section('table-body')
    @foreach ($data as $row)
        <tr>
            
            <!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->
            

            <td>{{ $row->id }}</td>
            <td><img style="width: 60px; height: 60px;" src="{{ asset('files/general/countries/images') . '/' . $row->flag }}" alt="{{ $row->en_name }}"></td>
            <td>{{ $row->en_name }}</td>
            <td>{{ $row->ar_name }}</td>


            <!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->

            <td><a href="{{ route($name.'.show', $row->id) }}" class="btn btn-link">View</a></td>
            <td><a href="{{ route($name.'.edit', $row->id) }}" class="btn btn-primary">تعديل</a></td>
            <td>
                <form method="POST" action="{{ route($name.'.destroy', $row->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
    @endforeach
@endsection