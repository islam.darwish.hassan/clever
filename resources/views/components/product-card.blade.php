@props(['item' => null])
<div class="store-item  ">
    <a href="{{ route('product', $item->id) }}">
        <div class="image-alt">
            <img src="{{ $item->image }}"  alt="{{ $item->name }}" />
            @if(isset($item->images[1]))<img
            loading=lazy
            src="{{ $item->images[1]->small_image }}" class="img-top" alt="{{ $item->name }}" />
            @endif

        </div>
        <div class="productTextContainer">
            <div class="textContainer">
                <p>{{ \Illuminate\Support\Str::upper($item->name . '-' . $item->color . '-' . $item->size) }}
                </p>
                <a  class="text-info" href={{route('products',['sub_category'=>$item->sub_category_id])}} >{{ $item->sub_category->name }}</a>
                @if($item->offer)
                <p class="text-muted"><del>{{ $item->price }} EGP</del></p>
                    <p class="text-success">{{ $item->offer->offer }} EGP</p>

                @else
                <p class="text-success">{{ $item->price }} EGP</p>

                @endif
            </div>
            <button type="button" class="btn btn-sm    " role="button" data-toggle="modal"
            data-target="#modal-wishlist-{{ $item->id }}"><i class="fas fa-heart"></i></button>

        </div>
        <form method="POST" action="{{ route('web.mywishlists.add') }}">
            @csrf

            <div class="modal fade" id="modal-wishlist-{{ $item->id }}" tabIndex="-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">My Wishlists</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Wishlists:</label>
                                    <div>
                                        @guest
                                            Login to add wishlists
                                        @endguest
                                        @auth
                                            <input type="hidden" value="{{ $item->id }}" name="product" />
                                            <select class="form-control m-input " name="wishlist">
                                                <option value="">Select a Wishlist </option>
                                                @foreach (App\Models\User::where('id', Auth::user()->id)->first()->wishlists as $wishlist)
                                                    <option value="{{ $wishlist->id }}">{{ $wishlist->name }}</option>
                                                @endforeach
                                            </select>
                                        @endauth

                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add To Wishlist</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

</div>

