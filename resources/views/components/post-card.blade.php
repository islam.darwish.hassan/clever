<div class="col-6 col-sm-4 col-md-2">
    <!-- Card-->
    <div class="card rounded shadow-sm border-0 card_item">
        <div class="card-body p-4">

            <img src="{{$post->image}}" alt=""
                class="img-fluid d-block mx-auto mb-3" style="max-height:5rem">
                @if(isset($post->offer))
                <div class="badge-overlay ">
                    <span class="top-right badge brand">{{$offer->offer}}% Off</span>
                </div>
                @endif
        
        <h6><a href="{{route('web.products.show',$post->id)}}">{{Str::limit($post->name,38)}}</a></h6>
        
            <h6 class="new_price mr-3">{{$post->price}} EGP</h6>
            {{-- <p class="old_price text-muted">400.00 EGP</p> --}}
            <ul class="list-inline small pt-1">
                @for ( $i=0 ;  $i < $post->rate ; $i++)
                <li class="list-inline-item m-0"><i class="fa fa-star text-primary"></i></li>
                @endfor
                @for ( $i=0 ;  $i < 5-$post->rate ; $i++)
                <li class="list-inline-item m-0"><i class="fa fa-star text-muted"></i></li>
                @endfor            
        
            </ul>
            <div class="row px-3">
            <button type="button" class="btn btn-sm   btn-outline-primary " role="button" data-toggle="modal"
            data-target="#modal-wishlist-{{$post->id}}" ><i class="fas fa-heart"></i></button>
            <form method="POST" action="{{ route('cart.store', $post->id) }}">
                @csrf
                <button type="submit" class="btn btn-sm btn-outline-primary ml-2 "><i
                        class="fas fa-shopping-cart"></i></button>
           </form>
            </div>
        </div>

    </div>
</div>

<form method="POST" action="{{route('web.mywishlists.add')}}">
    @csrf

<div class="modal fade" id="modal-wishlist-{{$post->id}}" tabIndex="-1">
<div class="modal-dialog" role="document">
<div class="modal-content">
   <div class="modal-header">
       <h5 class="modal-title" id="exampleModalLabel">My Wishlists</h5>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
       </button>
   </div>
   <div class="modal-body">
       <form>
           <div class="form-group">
               <label for="recipient-name" class="col-form-label">Wishlists:</label>
               <div>
                 @guest
                 Login to add wishlists
                 @endguest
                 @auth
                 <input type="hidden" value="{{$post->id}}" name="product"/>
                 <select class="form-control m-input " name="wishlist">
                       <option value="">Select a Wishlist  </option>
                        @foreach(App\User::where('id',Auth::user()->id)->first()->wishlists as $wishlist)
                             <option  value="{{$wishlist->id}}" >{{$wishlist->name}}</option>
                         @endforeach
                 </select>
                 @endauth

               </div>
           </div>
       </form>
   </div>
   <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <button type="submit" class="btn btn-primary">Add To Wishlist</button>
   </div>
</div>
</div>
</div>
</form>
