<div>
    <!-- It is not the man who has too little, but the man who craves more, that is poor. - Seneca -->
    <canvas style="height: 200px" id="radar"></canvas>

</div>

<script>
    function alternatePointStyles(ctx) {
        var index = ctx.dataIndex;
        return index % 2 === 0 ? 'circle' : 'rect';
    }
    var config=  {!!json_encode($radarconfig) !!}
    var data = {
        labels: {!!json_encode($radarlabels) !!},
        datasets: [{
            "name":config.name,
            "data": {!!json_encode($radarvalues) !!}
        }],
    };
    var options = {
        legend: false,
        tooltips: {
            titleFontSize:10,
            titleFontStyle:"bold",
            titleFontColor:"#fff",
                callbacks: {
                            label: function(tooltipItem, data) {
                                var label = data.labels[tooltipItem.index]|| '';

                                if (label) {
                                    label += ': ';
                                }
                                label += Math.round(tooltipItem.yLabel * 100) / 100;
                               return label;
                            },
                            labelTextColor: function(tooltipItem, chart) {
                                return '#fff';
                            }
                            }
                },
        elements:{
                line: {
                    backgroundColor: 'rgba(255, 0, 0, 0.1)',
                    borderColor: '#D42323',
                },
                point: {
                    backgroundColor: '#D42323',
                    radius: 5,
                    pointStyle: alternatePointStyles,
                    hoverRadius: 10,
                    color: "#D42323",
                    pointBorderColor: '#D42323',
                }
        }
    };

    var chart = new Chart(config.id, {
        type: config.type,
        data: data,
        options: options
    });


    // // eslint-disable-next-line no-unused-vars
    // function addDataset() {
    //     chart.data.datasets.push({
    //         data: generateData()
    //     });
    //     chart.update();
    // }

    // // eslint-disable-next-line no-unused-vars
    // function randomize() {
    //     chart.data.datasets.forEach(function (dataset) {
    //         dataset.data = generateData();
    //     });
    //     chart.update();
    // }

    // // eslint-disable-next-line no-unused-vars
    // function removeDataset() {
    //     chart.data.datasets.shift();
    //     chart.update();
    // }
</script>