<div>
    <canvas style="height:300px" id={{$config['id']}}></canvas>

</div>
<script>
  var config=  {!!json_encode($config) !!}
  var datasets=  {!!json_encode($datasets) !!}
var chartconfig ={
    type:config.type,
    data:{labels:{!!json_encode($labels) !!}
        ,datasets:datasets
        },
    options: {
                legend:{
                    "position":'bottom'
                },
                
                scales: {
                    xAxes: [{
                        display:config.type=="doughnut"?false:true,
                        stacked: true
                    }],
                    yAxes: [{
                        display:config.type=="doughnut"?false:true,
                        stacked: true,

                    }]
                }
            }
}
  var chart = new Chart(config.id, chartconfig);
</script>
{{-- const basic_chart = new Chartisan({
    //     el: "#"+config.id,
    //     data: {
    //         chart: {"labels": {!!json_encode($labels) !!}
    //         },
    //         datasets:datasets,
    //     },
    
    //     // loader: {
    //     //     color: '#ff00ff',
    //     //     size: [30, 30],
    //     //     type: 'bar',
    //     //     textColor: '#ffff00',
    //     //     text: 'Loading some chart data...',
    //     // },
    //     hooks: new ChartisanHooks()
    //         .datasets([{
    //             type: config.type,
    //             borderColor: '#D42323',
    //             pointBackgroundColor: "#D42323",
    //             pointBorderWidth: 4,
    //             backgroundColor: 'rgba(0, 0, 0, 0)',
    //             options: {
    //                 scales: {
    //                     xAxes: [{
    //                         stacked: true
    //                     }],
    //                     yAxes: [{
    //                         stacked: true
    //                     }]
    //                 }
    //             }
    //         }, {
    //             type: config.type,
    //             borderColor: '#D42323',
    //             pointBackgroundColor: "#D42323",
    //             pointBorderWidth: 4,
    //             backgroundColor: 'rgba(0, 0, 0, 0)',
    //             options: {
    //                 scales: {
    //                     xAxes: [{
    //                         stacked: true
    //                     }],
    //                     yAxes: [{
    //                         stacked: true
    //                     }]
    //                 }
    //             }
    //         }])
    //         .beginAtZero(true)
    //         .legend({
    //             position: 'bottom'
    //         })
    //         .title('Users Chart')
    //         .responsive(true)
    //  --}}
    