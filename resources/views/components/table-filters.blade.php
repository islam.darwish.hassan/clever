<div>
    <form  action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-end align-items-center">
            {{$slot}}
            <div class="px-1  ">
                <button type="submit" class="btn btn-alternate  ">تصفية</button>
            </div>
            <div class="px-1  ">
            <a href="{{ route($name. '.index') }}" class=" mr-2 btn btn-light">Clear
            </a>
             </div>

        </div>

    </form>
</div>