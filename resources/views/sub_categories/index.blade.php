@extends('layouts.resource.index')
@section('title', "ادارة التصنيفات الفرعية")
@section('create-btn')
<a href="{{ route('subs' . '.create',$category->id) }}" class="  p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index',$category->id) }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index',$category->id) }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-header')
            <th scope="col"  >@sortablelink('id', '#')</th>
            <th scope="col">الصورة</th>
            <th scope="col"  >@sortablelink('name','الاسم')</th>
            <th scope="col"  >@sortablelink('products_count','المنتجات')</th>
            <th  scope="col" style="width: 10%">العمليات</th>
@endsection
@section('table-body')
            @foreach ($subs as $sub)
            <tr>
                <td>{{ $sub->id }}</td>
                <td><img src="{{ $sub->image }}" alt="avatar" class="pp-square"></td>
                <td>{{ $sub->name }}</td>
                <td>{{ $sub->products_count }}</td>
                <td class="d-flex justify-content-center">
                    <a href="{{ route('all_products'.'.index', 'sub_category='.$sub->id) }}"
                    class="btn   "><i class="fas fa-eye"></i></a>
                   <a href="{{ route($name.'.edit',  [$category->id, $sub->id]) }}" 
                     class="btn  "><i class="fas fa-edit"></i></a>
                       <form method="POST" action="{{ route($name.'.destroy',  [$category->id, $sub->id]) }}">
                           {{ csrf_field() }}
                           {{ method_field('DELETE') }}
                           <button type="submit"
                            onclick="return confirm('Are you sure you want to delete this subcategory?')" 
                            data-toggle="modal" data-target="#exampleModal" 
                       class="btn    bg-transparent"><i class="fas fa-trash-alt "></i></button>
                       </form>
                   </td>
    
            </tr>
        @endforeach
        @section('table-footer')
        <p class="table-footer">النتائج :  {{$data->total()}}</p>
        
    @endsection
    
    @endsection