@extends('layouts.app')
@section('title', 'تعديل ' . ucfirst(\Illuminate\Support\Str::singular($display_name)))

@section('back')
        <a href="{{ route($name. '.index', $category->id) }}" class=" text-gold ">{{ ucfirst($name) }}</a> / 
@endsection


@section('content')
 <form class="form" action="{{ route($name. '.update', [$category->id, $sub->id]) }}" method="post" enctype="multipart/form-data" >

    @csrf

    @method('put')

    <div class="row">
        <div class="col form-group">
            <label>الاسم</label>
            <input type="text" name="name" class="form-control" placeholder="الاسم"
                value="{{{ $sub->name }}}"> 
        </div> 
        <div class="col form-group">
            <label>الصورة</label>
            <div class="custom-file">
                <label class="custom-file-label" for="validatedCustomFile">اختر الملف ...</label>
                <input type="file" name="image" class="custom-file-input" >
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-block btn-success bg-brandgreen ">تحديث</button>

</form>

@endsection
