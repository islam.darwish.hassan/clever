@extends('layouts.resource.index')
@section('title', 'Banner ads request')
@section('create-btn')
<a href="{{ route($name . '.create') }}" class="  p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')

    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="role">
                    <option value="none" @if(request()->query('role') == 'none') selected @endif>- Choose region -</option>
                    <option value="1" @if(request()->query('role') == '1') selected @endif>1</option>
                    <option value="2" @if(request()->query('role') == '2') selected @endif>2</option>
                    <option value="3" @if(request()->query('role') == '3') selected @endif>3</option>
                    <option value="4" @if(request()->query('role') == '4') selected @endif>4</option>
                </select>
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>
    </form>

@endsection

@section('table-header')

    <th scope="col" style="width: 10%" >@sortablelink('id', '#')</th>
    <th scope="col"  >@sortablelink('Business image')</th>
    <th scope="col" >Business name</th>
    <th scope="col"  >@sortablelink('role','Ad image')</th>
    <th scope="col">@sortablelink('created_at','Title')</th>
    <th scope="col">@sortablelink('created_at','URL')</th>
    <th scope="col">@sortablelink('created_at','Region')</th>
    <th  scope="col" style="width: 10%">العمليات</th>
@endsection



@section('table-body')

    @foreach ($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->email }}</td>
            <td>
                @if($user->role!= '3' )
                    @if(null !==(App\Models\Client::where('user_id',$user->id)->pluck('first_name')->first()))
                    {{ App\Models\Client::where('user_id',$user->id)->pluck('first_name')->first() }}
                    @else
                    Not Available
                    @endif
                @elseif($user->role == '3')
                    @if(null !==(App\Models\المحل::where('user_id',$user->id)->pluck('name')->first()))
                    {{ App\Models\المحل::where('user_id',$user->id)->pluck('name')->first() }}
                    @else
                    Not Available
                    @endif
                @endif

            </td>

            <td>
                @if($user->role == '1')
                    المشرف
                @elseif($user->role == '2')
                    User
                @elseif($user->role == '3')
                    Businness
                @elseif($user->role == '4')
                    Cashier
                @endif
            </td>
            <td>{{Carbon\Carbon::parse($user->created_at)->diffForHumans()}}
            <td class="d-flex justify-content-center">
                <a href=""
                    class="btn  "><i class="fas fa-check-circle"></i></a>
                <a href=""
                    class="btn  "><i class="fas fa-times-circle"></i></a>
            </td>
        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
@endsection

@endsection
