@extends('layouts.resource.index')
@section('title', 'Banner ads')
@section('create-btn')
<a href="{{ route($name . '.create') }}" class="  p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    {{-- <x-table-filters name={{$name}}>
        <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
        </div>
    </x-table-filters> --}}
    <!-- Search and Filters END-->
    @if($banners->count()>0)
    <div style="width:32rem;">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">{{ucfirst($name)}} Preview</h5>
                <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($banners as $banner)
                        @if ($loop->first)
                        <div class="carousel-item active " style="width:30rem ; height:16.875rem">
                            <img class="d-block  "style="width:30rem ; height:16.875rem" src="{{$banner->content}}"  alt="{{$banner->title}}">
                        </div>
                        @else
                        <div class="carousel-item ">
                            <img class="d-block  " style="width:30rem ; height:16.875rem" src="{{$banner->content}}"  alt="{{$banner->title}}">
                        </div>
                        @endif
                     @endforeach
                  </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('table-header')

    <th scope="col" style="width: 5%" >#</th>
    <th scope="col" style="width:10%">الصورة</th>
    <th scope="col">Title</th>
    <th scope="col">Url</th>
    <th  scope="col" style="width: 10%">العمليات</th>

@endsection



@section('table-body')

    @foreach ($banners as $banner)
        <tr>
            <td>{{ $banner->id }}</td>
            <td><img src={{$banner->content}} class="img-thumbnail"/>
            <td>{{ $banner->title }}</td>
            <td>{{ $banner->url }}</td>
            <td class="d-flex justify-content-center">
               <a href="{{ route($name.'.edit', $banner->id) }}"
                 class="btn  "><i class="fas fa-edit"></i></a>
                   <form method="POST" action="{{ route($name.'.destroy', $banner->id) }}">
                      @csrf
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this banner?')"
                        data-toggle="modal" data-target="#exampleModal"
                   class="btn    bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form>
               </td>
        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer ">النتائج :  {{$data->total()}}</p>
    @endsection

@endsection
