@extends('layouts.resource.edit')

@section('title', 'Create ' . ucfirst(\Illuminate\Support\Str::singular($name)) )


@section('content')


<form class="p-2 flex-grow-1" method="post" action="{{ route($name. '.store') }}" enctype="multipart/form-data">
    @csrf

<div class="d-flex bd-highlight">
<div class="form1 p-2 flex-fill bd-highlight">
 <div class="row">
     <div class="col form-group">
         <label>Title <b style="color:red"> * </b></label>
         <input type="text" name="title" class="form-control" placeholder="Title" required
             value="{{ old('title') }}">
     </div>
     <div class="col form-group">
         <label class="form-label">Content الصورة<b style="color:red"> * </b></label>
         <div class="custom-file">
             <label class="custom-file-label" for="validatedCustomFile">اختر الملف ...</label>
             <input type="file" name="content" id="custom-file-input-image" class="custom-file-input">
         </div>
     </div>
 </div>
 <div class="row">
     <div class="col form-group">
         <label>Order <b style="color:red"> * </b></label>
         <input type="number" name="order" class="form-control" placeholder="Order to display"
             value="{{ old('order') }}">
     </div>
     <div class="col form-group">
         <label>Url </label>
         <input type="url" name="url" class="form-control" placeholder="Url"
             value="{{ old('url') }}">
     </div>

 </div>
</div>
</div>
<button type="submit" class="btn1 btn btn-block btn-success bg-brandgreen ">اضافة اعلان جديد</button>
</form>
<script>
$('#custom-file-input-image').on('change', function () {
//get the file name
var fileName = document.getElementById("custom-file-input-image").files[0].name;
//replace the "Choose a file" label

$('.custom-file-label').html(fileName);
console.log(fileName)

})
</script>

@endsection
