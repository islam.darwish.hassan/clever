@extends('layouts.app')
@section('title', 'Cards History of ' .$client->first_name)
@section('content')
<div class="container pb-5">
<label class="form-label text-vlarge">Cards History</label>

@foreach ($cards as $card)

<div style="row px-5 ">
  <div class="mt-2 pr-3">
    <b>{{$card->points}}</b> <p > According To :</p>
{{$card->action==0?
        'Redeeming Offer':
        $card->action==1?  'Register to Clever':
        $card->action==2?' Compeleting Your Profile':
        $card->action==3? 'Promocode consumption':
        $card->action==4? 'Promocode redemption':
        $card->action==5? 'Add compelete Review':
        $card->action==6?' Emotions on your review':
        $card->action==7? 'Add new business':
        $card->action==8? 'Add new collection':
        $card->action==9? 'Add new checkin':
        $card->action==11? 'Add new review':
        $card->action==12? 'Accept your added business'
        :null}}
        <small>({{Carbon\Carbon::parse($card->created_at)->diffForHumans()}})</small>

  </div>


<hr>

  @endforeach
</div>
</div>
{{ $cards->appends(request()->query())->links() }}



  @endsection
