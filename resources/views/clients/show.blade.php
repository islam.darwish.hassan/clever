@extends('layouts.app')
@section('title', $client->first_name." ".$client->last_name)

@section('content')
<!--Statistics-->
<div class="row">
  <div class="col" onclick="window.location='{{ route('cards_history.index' ,$client->id) }}'">
    <div class="card bg-red mb-2  flex-fill">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-medal text-white my-3 fa-2x"></i>
      </div>
      <div class="card-body row text-center" >
        <div class="col">
          <div class="text-vlarge text-white ">{{ App\Models\Point::where('client_id', $client->id)->sum('points')}}</div>
          <div class="text-uppercase text-white small">Cards</div>
        </div>
      </div>
    </div>
  </div>
  <div class="col ">
    <div class="card bg-red mb-2 flex-fill ">
      <div class="card-header  d-flex justify-content-center">
        <i class="fab fas fa-street-view text-white my-3 fa-2x"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">{{$client->checkins->count()}}</div>
          <div class="text-uppercase text-white small">Check-ins</div>
        </div>
      </div>
    </div>
  </div>
  <div class="col" >
    <div class="card bg-red mb-2 flex-fill ">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-pencil-alt text-white my-3 fa-2x"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">{{$client->reviews->count()}}</div>
          <div class="text-uppercase text-white small">المراجعات</div>
        </div>
      </div>
    </div>
  </div>
<div class="col" >
    <div class="card bg-red mb-2  flex-fill">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-clipboard-list text-white my-3 fa-2x"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">{{$client->collections->count()}}</div>
          <div class="text-uppercase text-white small">Collections</div>
        </div>
      </div>
    </div>
  </div>

  @if($client->status==1)
  <div class="col" >
    <div class="card bg-red mb-2  flex-fill">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-shield-alt text-white my-3 fa-2x"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">{{$trust_percentage}}%</div>
          <div class="text-uppercase text-white small">Trusted</div>
        </div>
      </div>
    </div>
    @endif
</div>
</div>

<!--end of Statistics-->
<!--extra Info-->
<div class="row justify-content-center d-flex">
  @if($client->reviews->count()>0)
  <div class="col">
    <div class="card border-white">
      <div class="card-header bg-black text-white"><b>المراجعات</b></div>
      <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
        <thead class="thead-light  ">
          <tr>
            {{-- <th scope="col">User</th> --}}
            <th scope="col">Business</th>
            <th scope="col">التقيم</th>
            <th scope="col">Good</th>
            <th scope="col">Bad</th>
            <th scope="col">التقيم</th>
            <th scope="col">وقت الاضافة</th>
            </tr>
        </thead>
        <tbody>
          @foreach($reviews as $review)
          <tr>
            <td><b><a href ={{route('stores.show',$review->store->id)}}>{{Str::limit($review->store->name,20)}} </a></b>
            </td>
            <td>{{$review->rate}}</td>
            <td>{{ Str::limit($review->good ,30) }}</td>
            <td>{{ Str::limit($review->bad ,30) }}</td>
            <td>{{ $review->rate}}</td>
            <td>{{Carbon\Carbon::parse($review->created_at)->diffForHumans()}}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <a href="{{route('clientsreviews.index',$client->id)}}" class=" text-bold float-right mb-2" >See more -></a>
  </div>
  @endif
</div>
<div class="row justify-content-center d-flex">
  @if($collections->count()>0)
  <div class="col">
    <div class="card border-white">
      <div class="card-header bg-black text-white"><b>Collections</b></div>
      <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
        <thead class="thead-light  ">
          <tr>
            {{-- <th scope="col">User</th> --}}
            <th scope="col">الاسم</th>
            <th scope="col">Bookmarked Counts</th>
            <th scope="col">Bookmarked Businesss</th>
            <th scope="col">وقت الاضافة</th>
            </tr>
        </thead>
        <tbody>
          @foreach($collections as $collection)
          <tr>
            <td>{{$collection->name}} </td>
            <td>{{$collection->bookmarked_stores->count()}} </td>
            <td>
            @if($collection->bookmarked_stores->count()<=0)
            Empty
            @else
            @foreach ($collection->bookmarked_stores as $store)
              @if($loop->last)
              <b><a href ={{route('stores.show',$store->id)}}>{{Str::limit($store->name,10)}} .</a></b>
              @else
              <b><a href ={{route('stores.show',$store->id)}}>{{Str::limit($store->name,10)}} ,</a></b>
              @endif
              @endforeach
            @endif
            </td>
            <td>{{Carbon\Carbon::parse($collection->created_at)->diffForHumans()}}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  <a href="{{route('collections.index',$client->id)}}" class=" text-bold float-right mb-2" >See more -></a>
  </div>
  @endif
</div>
<div class="row justify-content-center d-flex">
@if($checkins->count()>0)
<div class="col">
  <div class="card border-white">
    <div class="card-header bg-black text-white"><b>Check-ins</b></div>
    <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
      <thead class="thead-light  ">
        <tr>
          {{-- <th scope="col">User</th> --}}
          <th scope="col">Sub</th>
          <th scope="col">Business</th>
          <th scope="col">Business Bio</th>
          <th scope="col">Location</th>
          <th scope="col">وقت الاضافة </th>
          </tr>
      </thead>
      <tbody>
        @foreach($checkins as $checkin)
        <tr>
          <td><a href ={{route('stores.index','sub_category='.$checkin->store->sub_category->id)}}>
           <b> {{$checkin->store->sub_category->name}} </b></a> </td>
           <td><b><a href ={{route('stores.show',$checkin->store->id)}}>{{Str::limit($checkin->store->name,10)}} </a></b></td>
          <td>{{ Str::limit($checkin->store->bio ,30) }}</td>
          <td>{{$checkin->latitude}} , {{$checkin->longitude}} </td>
          <td>{{Carbon\Carbon::parse($checkin->created_at)->diffForHumans()}}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
<a href="{{route('checkins.index',$client->id)}}" class=" text-bold float-right mb-2" >See more -></a>
</div>
@endif
</div>

<!-- end of extra Info-->
@endsection
