@extends('layouts.resource.index')
@section('title', $client->first_name." ".$client->last_name."'s ".'collections')

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index',[$client->id]) }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                <input type="text" class="form-control mb-2 mr-sm-2" name="name" placeholder="Search in name ..." value="{{request()->query('name')}}">
            </div>          
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index',[$client->id]) }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-header')

<th scope="col">@sortablelink('name','الاسم')</th>
<th scope="col">@sortablelink('bookmarks_count','Bookmarked Counts')</th>
<th scope="col">Bookmarked Stores</th>
<th scope="col">@sortablelink('created_at','وقت الاضافة')</th>

@endsection

@section('table-body')
@foreach($collections as $collection)
<tr>
  <td>{{$collection->name}} </td>
  <td>{{$collection->bookmarked_stores->count()}} </td>
  <td>
  @if($collection->bookmarked_stores->count()<=0)
  Empty
  @else
  @foreach ($collection->bookmarked_stores as $store)
    @if($loop->last)
    <b><a href ={{route('stores.show',$store->id)}}>{{Str::limit($store->name,10)}} .</a></b>
    @else
    <b><a href ={{route('stores.show',$store->id)}}>{{Str::limit($store->name,10)}} ,</a></b>
    @endif
    @endforeach
  @endif
  </td>
  <td>{{Carbon\Carbon::parse($collection->created_at)->diffForHumans()}}
  </td>
</tr>
@endforeach
@section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
    @endsection

@endsection
