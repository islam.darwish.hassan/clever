@extends('layouts.resource.index')
@section('title', 'ادارة مستخدمين الموقع')
@section('create-btn')
<a href="{{ route('users' . '.create',["type"=>2]) }}" class="  p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="gender">
                    <option value="none" @if(request()->query('gender') == 'none') selected @endif>النوع</option>
                    <option value="m"  @if(request()->query('gender') == 'males') selected @endif>ذكور</option>
                    <option value="f"  @if(request()->query('gender') == 'اناث') selected @endif>اناث</option>

                </select>
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection




@section('table-header')

    <!------------------------------------------------------------- Columns START---------------------------------------------------------------------------->


    <th scope="col" scope="col" style="width: 10%" >@sortablelink('id', '#')</th>
    <th scope="col">الصورة</th>
    <th scope="col">@sortablelink('name', 'الاسم')</th>
    <th scope="col" >البريد الإلكترونى</th>
    <th scope="col">النوع</th>
    <th scope="col">@sortablelink('age', 'السن')</th>
    <th scope="col">@sortablelink('created_at','وقت الاضافة')</th>
    <th scope="col" scope="col" style="width: 10%">العمليات</th>


    <!------------------------------------------------------------- Columns END---------------------------------------------------------------------------->


@endsection



@section('table-body')
    @foreach ($clients as $client)
        <tr>

            <!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->


            <td>{{ $client->id }}</td>
            <td><img src={{$client->avatar}} class="pp-square"></td>
            <td>{{ $client->name }}</td>
            <td>{{ $client->user->email}}</td>
            <td>{{ ucfirst($client->gender)}}</td>

            <td>{{ $client->age }}</td>
            <td>
                @php
                Carbon\Carbon::setlocale("ar");
                echo Carbon\Carbon::parse($client->user->created_at)->diffForHumans()
                @endphp
            </td>

            <!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->

            <td class="d-flex justify-content-center">
               <a href="{{ route($name.'.edit', $client->id) }}"
                 class="btn  "><i class="fas fa-edit"></i></a>
                   <form method="POST" action="{{ route('users.'.'destroy', $client->user_id) }}">
                       {{ csrf_field() }}
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this user?')"
                        data-toggle="modal" data-target="#exampleModal"
                   class="btn    bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form>
               </td>
           </tr>
    @endforeach
@endsection
@section('table-footer')
<p class="table-footer">النتائج :  {{$data->total()}}</p>
@endsection
@section('extra_scripts')
@endsection
