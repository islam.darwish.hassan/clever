@extends('layouts.resource.index')
@section('title', $client->first_name." ".$client->last_name."'s ".'reviews')

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index',[$client->id]) }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                <input type="text" class="form-control mb-2 mr-sm-2" name="good" placeholder="Search in good ..." value="{{request()->query('good')}}">
            </div>
            <div class="p-2 flex-fill ">
            <input type="text" class="form-control mb-2 mr-sm-2" name="bad" placeholder="Search in bad ..." value="{{request()->query('bad')}}">
          </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index',[$client->id]) }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-header')

    <th scope="col">الصورة</th>
    <th scope="col">المحل</th>
    <th scope="col">Good</th>
    <th scope="col">Bad</th>
    <th scope="col">@sortablelink('rate','التقيم')</th>
    <th scope="col">@sortablelink('created_at','وقت الاضافة')</th>
    <th scope="col">Operation</th>

@endsection

@section('table-body')
    @foreach ($reviews as $row)
        <tr>
            <td><img src="{{ $row->client->avatar }}" alt="avatar" class="pp-square"></td>
            <td><b><a href ={{route('stores.show',$row->store->id)}}>{{Str::limit($row->store->name,20)}} </a></b>
            </td>
            <td>{{ Str::limit($row->good ,30) }}</td>
            <td>{{ Str::limit($row->bad ,30) }}</td>
            <td>{{ $row->rate}}<a href="{{ route($name.'.show',[ $client->id,$row->id]) }}"
                    class="   ">  <i class="fas fa-ellipsis-v fa-xs "></i></a> 
             </td>
             <td>
                @php
                Carbon\Carbon::setlocale("ar");   
                echo Carbon\Carbon::parse($user->created_at)->diffForHumans()
                @endphp
            </td>            
            <td class="d-flex justify-content-center"><a href="{{ route($name.'.show',[ $client->id,$row->id]) }}"
                class="btn   "><i class="fas fa-comment-dots"></i></a>
            </td>
        </tr>

    @endforeach
    @section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
    @endsection

@endsection
