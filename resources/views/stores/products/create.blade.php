@extends('layouts.app')
@section('title', 'اضافة منتج جديد')



@section('content')


    <form method="post" action="{{ route($name . '.store') }}" enctype="multipart/form-data">

        @csrf

        <div class="row">
            <div class="col form-group">
                <label class="form-label">كود المنتج <b style="color:red"> * </b></label class="form-label">
                <input type="number" name="product_code" class="form-control" placeholder="كود المنتج"
                    value="{{ request()->query('product_code') ? request()->query('product_code') : old('product_code') }}">
            </div>

            <div class="col form-group">
                <label class="form-label">المنتج الاسم <b style="color:red"> * </b></label class="form-label">
                <input type="text" name="name" class="form-control" placeholder="الاسم" value="{{ old('name') }}">
            </div>

            <div class="col form-group">
                <label class="form-label">الفرع <b style="color:red"> * </b></label class="form-label">
                <input type="text" name="branch" class="form-control" placeholder="الفرع"
                    value="{{ request()->query('branch') ? request()->query('branch') : old('branch') }}">
            </div>
        </div>
        <div class="row">
            <div class="col form-group">
                <label class="form-label">التصفية <b style="color:red"> * </b></label class="form-label">
                <select name="category" class="cat custom-select" id="category" data-dependent="sub_category">
                    <option>اختر ...</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}" @if (request()->query('category') == $category->id || old('category') == $category->id) selected @endif>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col form-group">
                <label class="form-label">التصفية الفرعية <b style="color:red"> * </b></label class="form-label"><br>
                <select name="sub_category" class="sub custom-select" id="sub_category">
                    <option>اختر ...</option>
                    @foreach ($subs as $sub)
                        <option value="{{ $sub->id }}" @if (request()->query('sub_category') == $sub->id || old('sub_category') == $sub->id) selected @endif>{{ $sub->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col form-group">
                <label class="form-label">الوصف <b style="color:red"> * </b></label class="form-label">
                <textarea type="text" row="8" name="description" class="form-control" placeholder="Body"
                    value="{{ old('description') }}">{{ old('description') }}</textarea>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5>المتغيرات</h5>
            </div>
            <div class="card-body">
                <input type="hidden" name="questions" id="questions" value="1">

                <div class="form-questions-container  pt-2">

                    <div class="form-question border card  " id="form-question-1">
                        <div class="card-header">
                            منتج
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-4 form-group">
                                    <label class="form-label">صورة المنتج </label class="form-label">
                                    <div>
                                        <input type="file" name="image-1" class="form-control"
                                            id="custom-file-input-image-1">
                                    </div>
                                </div>
                                <div class="col-4 form-group">
                                    <label class="form-label">فى المخزون <b style="color:red"> * </b></label
                                        class="form-label">
                                    <input type="number" name="in_stock-1" required class="form-control" placeholder="Stock"
                                        value="{{ old('in_stock-1') }}">
                                </div>
                                <div class="col-4 form-group">
                                    <label class="form-label">السعر <b style="color:red"> * </b></label class="form-label">
                                    <input type="number" name="price-1" required class="form-control" placeholder="Price"
                                        value="{{ old('price-1') }}">
                                </div>

                                <div class="col-4 form-group">
                                    <label class="form-label">نوع القماش <b style="color:red"> * </b></label
                                        class="form-label">
                                    <input type="text" name="fabric-1" required class="form-control" placeholder="Fabric"
                                        value="{{ old('fabric-1') }}">
                                </div>

                                <div class="col-4 form-group">
                                    <label class="form-label">الحجم <b style="color:red"> * </b></label class="form-label">
                                    <input type="text" name="size-1" required class="form-control" placeholder="Size"
                                        value="{{ old('size-1') }}">
                                </div>

                                <div class="col-4 form-group">
                                    <label class="form-label">اللون <b style="color:red"> * </b></label class="form-label">
                                    <input type="text" name="color-1" required class="form-control" placeholder="Color"
                                        value="{{ old('color') }}">
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
                <div class=" form-questions-add mt-3">
                    <input type="button" value="اضافة متغير جديد" class="btn btn-outline-primary btn-block    "
                        id="add-fields">
                </div>
            </div>
        </div>
        <br>

        <button type="submit" class="btn btn-block btn-success bg-brandgreen ">إضافة منتج جديد</button>

    </form>

@section('extra-scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script>
        var total = 1; // Our default
        console.log('test');
        $(document).on('click', '#add-fields', function() {

            var addBlockId = total = total + 1;
            $(`        <div class="form-question border card mt-2  " id="form-question-` + addBlockId + `">
    <div class="card-header">
        متغير
    </div>
    <div class="card-body">
    <div class="row">
        <div class="col-4 form-group">
                                <label class="form-label">صورة المنتج </label class="form-label">
                                <div>
                                    <input type="file" name="image-` + addBlockId + `" class="form-control" >
                                </div>
            </div>
            <div class="col-4 form-group">
                <label class="form-label">فى المخزون <b style="color:red"> * </b></label class="form-label">
                <input type="number" required name="in_stock-` + addBlockId + `" class="form-control" placeholder="Stock"
                    value="{{ old('in_stock-`+addBlockId+`') }}">
            </div>

            <div class="col-4 form-group">
                <label class="form-label">السعر <b style="color:red"> * </b></label class="form-label">
                <input type="number" required name="price-` + addBlockId + `" class="form-control" placeholder="Price"
                    value="{{ old('price-`+addBlockId+`') }}">
            </div>

            <div class="col-4 form-group">
                <label class="form-label">نوع القماش <b style="color:red"> * </b></label class="form-label">
                <input type="text" required name="fabric-` + addBlockId + `" class="form-control" placeholder="Fabric"
                    value="{{ old('fabric-`+addBlockId+`') }}">
            </div>

            <div class="col-4 form-group">
                <label class="form-label">الحجم <b style="color:red"> * </b></label class="form-label">
                <input type="text" required name="size-` + addBlockId + `" class="form-control" placeholder="Size"
                    value="{{ old('size-`+addBlockId+`') }}">
            </div>

            <div class="col-4 form-group">
                <label class="form-label">اللون <b style="color:red"> * </b></label class="form-label">
                <input type="text" required name="color-` + addBlockId + `" class="form-control" placeholder="Color"
                    value="{{ old('color-`+addBlockId+`') }}">
            </div>
        </div>
        </div>

`).appendTo($('.form-questions-container'));
            $('#questions').val(total);

        });
    </script>

    <script>
        $(document).ready(function() {
            //to get params from url
            $.urlParam = function(name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                return results[1] || 0;
            }
            var type = $.urlParam('type');
            if (type) {
                console.log(type);
                $(".sel").val(type);
                $(".form" + type).show();
            } else {
                console.log('no type')
            }
        });
    </script>

    <script>
        $('.sub').prop('disabled', false);
        $('.cat').change(function() {
            // $('.sub').prop('disabled', false);
            if ($(this).val() != '') {
                var select = $(this).attr("id");
                var value = $(this).val();
                var dependent = $(this).data('dependent');
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ route('category.fetch') }}",
                    method: "POST",
                    data: {
                        select: select,
                        value: value,
                        dependent: dependent,
                        _token: _token
                    },
                    success: function(result) {
                        $('#' + dependent).html(result);
                    }
                })
            }
        });

        $('#category').change(function() {
            $('#sub_category').val('');
        });
    </script>
@endsection

@endsection
