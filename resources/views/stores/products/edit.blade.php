@extends('layouts.app')
@section('title', ' تعديل المنتج ')))


@section('content')
 <form class="form" action="{{ route('products.update', [$product->id]) }}" method="post" enctype="multipart/form-data">

               @csrf


    @method('put')

    <div class="row">
        <div class="col form-group">
            <label>الاساسية الصورة </label>
            <div class="custom-file">
                <label class="custom-file-label" for="validatedCustomFile">اختر الملف ...</label>
                <input type="file" name="image" id="custom-file-input-image" class="custom-file-input" >
            </div>
        </div>
        <div class="col form-group">
            <label>المنتج الاسم </label>
            <input type="test" name="name" class="form-control" placeholder="الاسم"
                value="{{ old('name')?old('name'):$product->name}}">
        </div>

        <div class="col form-group">
            <label>فى المخزون </label>
            <input type="number" name="in_stock" class="form-control" placeholder="Stock"
            value="{{ old('in_stock')?old('in_stock'):$product->stock}}">
        </div>

        <div class="col form-group">
            <label>السعر </label>
            <input type="number" name="price" class="form-control" placeholder="Stock"
            value="{{ old('price')?old('price'):$product->price}}">
        </div>

    </div>
    <div class="row">
        <div class="col form-group">
            <label>اللون </label>
            <input type="text" name="color" class="form-control" placeholder="color"
            value="{{ old('color')?old('color'):$product->color}}">
        </div>        <div class="col form-group">
            <label>المقاس </label>
            <input type="text" name="size" class="form-control" placeholder="Size"
            value="{{ old('size')?old('size'):$product->size}}">
        </div>        <div class="col form-group">
            <label>الفرع </label>
            <input type="text" name="branch" class="form-control" placeholder="branch"
            value="{{ old('branch')?old('branch'):$product->branch}}">
        </div>
    </div>
    <div class="row">
        <div class="col form-group">
            <label class="form-label">Category </label>
            <select name="category" class="cat custom-select" id="category" data-dependent="sub_category">
                <option >اختر ...</option>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}" {{isset($product) ? $product->sub_category->category_id== $category->id ? 'selected' :"":""}}>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col form-group">
            <label class="form-label">Sub category </label><br>
            <select name="sub_category" class="sub custom-select" id="sub_category">
                <option>اختر ...</option>
                @foreach ($subs as $sub)
                <option value="{{ $sub->id }}"  {{isset($product) ? $product->sub_category_id == $sub->id? 'selected' :"":""}}>{{ $sub->name }}</option>
                @endforeach
            </select>
        </div>
</div>

    <div class="row">
        <div class="col form-group">
            <label>Description</label>
            <textarea type="text" row="8"  name="description" class="form-control  " placeholder="Body"
            value="{{old('description')?old('description'):$product->description}}">
            {{ old('description')?old('description'):$product->description}}
        </textarea>
        </div>
    </div>
    {{-- <div id="standalone-container">
        <div id="toolbar-container">
          <span class="ql-formats">
            <button class="ql-bold"></button>
            <button class="ql-italic"></button>
            <button class="ql-underline"></button>
          </span>
          <span class="ql-formats">
            <button class="ql-list" value="ordered"></button>
            <button class="ql-list" value="bullet"></button>
            <button class="ql-indent" value="-1"></button>
            <button class="ql-indent" value="+1"></button>
          </span>
          <span class="ql-formats">
            <button class="ql-direction" value="rtl"></button>
            <select class="ql-align"></select>
          </span>
          <span class="ql-formats">

        </div>
        <div id="editor-container">
            <textarea name="desc_rich" style="display:none" id="hiddenArea"></textarea>

        </div>
      </div>
 --}}


  <br>
    <button type="submit" class="btn btn-block btn-success bg-brandgreen ">تحديث المنتج </button>

</form>
<!-- Include the Quill library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.0.1/highlight.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/quill/2.0.0-dev.4/quill.min.js"></script>

<script>
    var quill = new Quill('#editor-container', {
      modules: {
        syntax: true,
        toolbar: '#toolbar-container',
      },
      placeholder: 'Write Description here...',
      theme: 'snow',
    });


  </script>
{{-- <script>
    $("form").submit(function(e){
        alert("Submitted");
        e.preventDefault();
        var myEditor = document.querySelector('#editor-container')
        var delta = quill.getContents();

        var html = myEditor.children[0].innerHTML;
        console.log(delta);
});

</script> --}}
@endsection
