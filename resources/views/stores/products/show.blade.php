@extends('layouts.app')
@section('title', ucfirst($product->name))


@section('content')
    <div class="container bg-white">

        @if ($product->status != 2 && $product->status != 1)
            <div class="d-flex align-items-between ">
                <a href="{{ route('products' . '.edit', [$product->id]) }}" class=" clr-blue p-2 "><b>تعديل المنتج </b><i
                        class="fas fa-cog">
                    </i></a>


                </i></a>
            </div>

        @elseif($product->status==1)
            <div class="row">
                <form method="POST" action="{{ route('products.' . 'to_offline', [$product->id]) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <button type="submit"
                        onclick="return confirm('Are you sure you want to offline this product there is no back for this process ?')"
                        data-toggle="modal" data-target="#Modal"
                        class="btn btn-dark text-black  float-right mb-2 mr-2 text-uppercase btn-sm">حول لحالة الاوفلاين
                    </button>
                </form>

            </div>
        @endif

        <div class="row shadow-sm mx-1 mb-3">
            <div class="col border-right">
                <img src={{ $product->image }} class="pp-product " alt={{ $product->name }}>
                @if ($product->status != 2 && $product->status != 1)
                    @if ($product->image)
                        <div class="col-12 mx-auto">
                            <div>
                                <label class="btn btn-default clr-blue d-flex justify-content-end   align-items-center">
                                    <b class=pr-1>تعديل صورة المنتج </b><i class=" fas fa-cog"></i>
                                    <form method="POST" action="{{ route('products.update_image', [$product->id]) }}"
                                        id="upload_image" enctype="multipart/form-data">
                                        @csrf

                                        @method('PUT')
                                        <input type="file" class="hide" name="image" id="image">
                                    </form>

                                </label>
                            </div>

                        </div>
                    @endif
                @endif
            </div>
            <div class="col pt-5">

                @if ($product->status == 1)
                    <span class="badge badge-success text-white">اونلاين</span>
                @elseif ($product->status == 2)
                    <span class="badge badge-primary text-black">تحت الفحص</span>
                @elseif ($product->status == 3)
                    <span class="badge badge-danger text-white">مرفوض</span>
                @elseif ($product->status == 4)
                    <span class="badge bg-dark text-white  ">اوفلاين</span>
                @elseif ($product->status == 5)
                    <span class="badge bg-info text-white">درافت</span>
                @else
                    <span class="badge bg-dark text-white text-black">غير محدد</span>
                @endif
                <h5 class="pt-1">{{ $product->name .' - '. $product->size. ' - ' .$product->color }}</h5>
                <p>{!! $product->description !!}</p>
                <p>{!! $product->price !!} EGP</p>

                <div class="row">
                    <div class="col">
                        <p class="text-bold"><u>فى المخزون: </u></p>
                        <p>{{ $product->stock }} عدد الموجود في المخزون </p>

                    </div>
                    <div class="col">
                        <p class="text-bold"><u>كود المنتج </u></p>
                        <p>{{ $product->product_code }} كودالتعريفي للمنتج </p>

                    </div>

                </div>
                @php
                $varients=[];
                if(null != ($product->group()->first()))
                    $varients = $product->group()->first()->products;
                @endphp
                <div >
                    <p class="text-bold"><u>المنتجات المرتبطه بالمنتج </u></p>
                    @foreach ($varients as $item)
                        <div class= "badge badge-light ">
                        <a  href={{route('products.show',$item->id)}}>
                        {{$item->name}} -{{$item->size}}-{{$item->color}}
                        </a>

                        </div>
                    @endforeach

                </div>
                <hr>
                @if ($product->status != 1 && $product->status != 2)
                    <form method="POST" action="{{ route('products.' . 'to_review', [$product->id]) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <button type="submit"
                            onclick="return confirm('Are you sure you want to confirm this product there is no back for this process ?')"
                            data-toggle="modal" data-target="#Modal"
                            class="btn bg-info text-white  float-right mb-2 mr-2 text-uppercase">حول الحاله
                            للمراجعة</button>
                    </form>
                @endif
                @if ($product->status == 2)

                    @can('view-super', Auth::user())
                        <form method="POST" action="{{ route('products.' . 'to_approve', [$product->id]) }}">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <button type="submit"
                                onclick="return confirm('Are you sure you want to confirm this product there is no back for this process ?')"
                                data-toggle="modal" data-target="#Modal"
                                class="btn bg-info text-white  float-right mb-2 mr-2 text-uppercase">وافق</button>
                        </form>
                        <form method="POST" action="{{ route('products.' . 'to_reject', [$product->id]) }}">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <button type="submit"
                                onclick="return confirm('Are you sure you want to confirm this product there is no back for this process ?')"
                                data-toggle="modal" data-target="#Modal"
                                class="btn bg-red text-white  float-right mb-2 mr-2 text-uppercase">ارفض</button>
                        </form>

                        <a class="btn  disabled  btn-block text-uppercase"></a>

                    @endcan
                @endif
            </div>
        </div>
        <div class="row d-flex justify-content-center mb-3">
            @if ($product->status != 2 && $product->status != 1)
                <div class="col-12 mx-auto  ">
                    <div>
                        <label class="btn btn-default clr-blue d-flex justify-content-end  align-items-center">
                            <b class=pr-1>اضافة صور المنتج </b><i class=" fas fa-cog"></i>
                            <form method="POST" action="{{ route('products.add_image', [$product->id]) }}" id="add_image"
                                enctype="multipart/form-data">
                                @csrf

                                @method('POST')
                                <input type="file" class="hide" name="added_image" id="added_image">
                            </form>
                        </label>
                    </div>
                </div>
            @endif
            @foreach ($product_images as $item)
                <div class="col-2">
                    <img src={{ $item->image }} alt={{ $item->image }} class="img-thumbnail"
                        style="max-width:200px; height:200px; object-fit: cover;" />
                    <form method="POST" action="{{ route('products.remove_image', [$product->id, $item->id]) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit"
                            onclick="return confirm('Are you sure you want to delete this product image?')"
                            data-toggle="modal" data-target="#exampleModal" class="btn  clr-blue  bg-transparent"><i
                                class="fas fa-trash-alt "></i>
                            حذف الصورة
                        </button>
                </div>
                </form>
            @endforeach

        </div>
        <script type="text/javascript">
            document.getElementById("upload_image").onchange = function() {
                // submitting the form
                document.getElementById("upload_image").submit();
            };

        </script>
        <script type="text/javascript">
            document.getElementById("add_image").onchange = function() {
                // submitting the form
                document.getElementById("add_image").submit();
            };

        </script>

        <script>
            $('.custom-file-input').on('change', function() {
                //get the file name
                var fileName = document.getElementById("custom-file-input-image").files[0].name;
                //replace the "Choose a file" label

                $('.custom-file-label').html(fileName);
                console.log(fileName)

            })

        </script>
    </div>

@endsection
