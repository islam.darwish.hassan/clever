@extends('layouts.app', ['activePage' => 'products', 'titlePage' => __('Products Offer Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('offer_update', [$product->id, $offer->id]) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('تعديل Products Offer') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-md-12 text-right">
                    <a href="{{ route('offer_show', [$product->id, $offer->id]) }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Offer') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('offer') ? ' has-danger' : '' }}">
                      <div class="row">
                        <div class="col">
                          <input class="form-control" type="number" id="start" name="offer" placeholder="25" value="{{ $offer->offer }}">
                        </div>
                        <div class="col">
                          <span class="font-weight-bold">%</span>
                        </div>
                      </div>
                      @if ($errors->has('offer'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('offer') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Started at') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('started_at') ? ' has-danger' : '' }}">
                      <div class="row">
                        <div class="col">
                          <input class="form-control" type="date" id="start" min="2020-01-01" name="started_at" placeholder="" value="{{ $offer->started_at }}">
                        </div>
                        <div class="col">
                          <span class="font-weight-bold"></span>
                        </div>
                      </div>
                      @if ($errors->has('offer'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('started_at') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Finished at') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('finished_at') ? ' has-danger' : '' }}">
                      <div class="row">
                        <div class="col">
                          <input class="form-control" type="date" id="start" min="2020-01-01" name="finished_at" placeholder="" value="{{ $offer->finished_at }}">
                        </div>
                        <div class="col">
                          <span class="font-weight-bold"></span>
                        </div>
                      </div>
                      @if ($errors->has('finished_at'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('finished_at') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection