@extends('layouts.app', ['activePage' => 'products', 'titlePage' => __('المنتج Offer Management')])

@section('content')
  <style>
    .rotate:hover .fa-stack{
      transition: 0.5s;
      transform: rotateZ(360deg);
    }
    .rotate{
      text-align: center;
    }
  </style>
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Optional Choices Info') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-md-12 text-right">
                    <a href="{{ route('product.show', $product) }}" class="btn btn-sm btn-primary">{{ __('Back to product') }}</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Offer') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <div class="row">
                        <div class="col">
                          <span name="name" for="input-name">{{ $offer->offer }}<span class="font-weight-bold"> %</span></span>
                        </div>
                        <form action="{{ route('offer_delete', [$product->id, $offer->id]) }}" method="post">
                          @csrf
                          @method('delete')
                        </form>
                        <form action="{{ route('offer_delete', [$product->id, $offer->id]) }}" method="post">
                          @csrf
                          @method('delete')
                      
                          <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('offer_edit', [$product->id, $offer->id]) }}" data-original-title="" title="">
                            <i class="material-icons">edit</i>
                            <div class="ripple-container"></div>
                          </a>
                          <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this product?") }}') ? this.parentElement.submit() : ''">
                            <i class="material-icons">close</i>
                            <div class="ripple-container"></div>
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Started at') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <div class="row">
                        <div class="col">
                          <span name="name" for="input-name">{{ $offer->started_at }}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Finished at') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <div class="row">
                        <div class="col">
                          <span name="name" for="input-name">{{ $offer->finished_at }}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

@endsection