@extends('layouts.app', ['activePage' => 'products', 'titlePage' => __('المنتج Offer Management')])
@section('title', 'Add Offer To المنتج')

@section('content')
<div class="row">
    <div class="col-md-12 text-right pb-3">
        <a href="{{ route('products.show', [$product->store->id,$product->id]) }}" class="btn btn-sm btn-primary">{{ __('Back to المنتج') }}</a>
    </div>
  </div>

  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('offers.store', [$product->store->id,$product->id]) }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add Offer') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Offer') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('offer') ? ' has-danger' : '' }}">
                      <div class="row">
                        <div class="col">
                          <input class="form-control" type="number" id="start" name="offer" placeholder="25" value="" min="1" max="99">
                        </div>
                        <div class="col">
                          <span class="font-weight-bold">%</span>
                        </div>
                      </div>
                      @if ($errors->has('offer'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('offer') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                {{-- <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Started at') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('started_at') ? ' has-danger' : '' }}">
                      <div class="row">
                        <div class="col">
                          <input class="form-control" type="date" id="start"  name="started_at" placeholder="" value="">
                        </div>
                        <div class="col">
                          <span class="font-weight-bold"></span>
                        </div>
                      </div>
                      @if ($errors->has('offer'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('started_at') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Finished at') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('finished_at') ? ' has-danger' : '' }}">
                      <div class="row">
                        <div class="col">
                          <input class="form-control" type="date" id="start" name="finished_at" placeholder="" value="">
                        </div>
                        <div class="col">
                          <span class="font-weight-bold"></span>
                        </div>
                      </div>
                      @if ($errors->has('finished_at'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('finished_at') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div> --}}
            </div>
              <div class="card-footer text-right ">
                <button type="submit" class="btn  btn-success bg-brandgreen ">{{ __('Add Offer') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
