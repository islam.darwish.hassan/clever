@extends('layouts.app')
@section('title', 'Manage المنتج Specs')
@section('create-btn')
<a href="{{ route('products' . '.show',[$store->id,$product->id]) }}" class=" clr-blue p-2 "><b>Back To المنتج</b></i></a>
@endsection

@section('content')
<div class=" card  ">
    <div class="card-header">
    <b >Add New Specification </b>
    </div>
    <div class="card-body ">
    <form method="POST"  action='{{route('specs.store',[$store->id,$product->id])}}' class="form-inline justify-content-center" >
                   @csrf

        <input type="text" name="spec_title" class="form-control" placeholder="Specification title"
            value="">
        <input type="text" name="spec_value" class="form-control ml-2" placeholder="Value"
            value="">
            <button type="submit" class="btn ml-2 bg-info text-white  ">Add New Spec</button>
        </form>
    </div>
</div>
@if($product->specs->count()>0)
<div class=" card mt-2 ">
    <div class="card-header">
    <b>Current Specifications</b>
    </div>
<div class="card-body row  justify-content-center">
    @foreach($product->specs as $spec)
    <div class="col-lg-5 form-group  ">
        <div class="form-inline ">
        <label class="form-label">{{$spec->title}} </label class="form-label">
                <form method="POST" action="{{ route('specs.'.'destroy',[$store->id, $product->id,$spec->id]) }}">
                   @csrf
                    {{ method_field('DELETE') }}
                    <button type="submit"
                    onclick="return confirm('Are you sure you want to delete this spec?')"
                    data-toggle="modal" data-target="#exampleModal"
                class="btn  clr-red  bg-transparent"><i class="fas fa-minus-square 	"></i> </button>
                </form>
            </div>
        <form method="POST" action='{{route('specs.update',[$store->id,$product->id,$spec->id])}}'  >
                   @csrf

        @method('PUT')
        <div class="d-flex justify-content-between">
        <input type="text" name="{{$spec->title}}" class="form-control" placeholder="{{$spec->title}}"
            value="{{$spec->value}}">
            <button type="submit" class="btn ml-2   ">تحديث</button>
        </form>

        </div>
    </div>
    @endforeach
</div>
@endif

@endsection
