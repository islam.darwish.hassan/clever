@extends('layouts.resource.index')
@section('title', "ادارة المحلات")

@section('create-btn')
<a href="{{ route('users' . '.create' ,["type"=>3]) }}" class="  p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="status">
                    <option value="none" @if(request()->query('status') == 'none') selected @endif>الحالة</option>
                    <option value="1"  @if(request()->query('status') == '1') selected @endif>Verfied</option>
                    <option value="2"  @if(request()->query('status') == '2') selected @endif>Not Verfied</option>
                    <option value="3"  @if(request()->query('status') == '3') selected @endif>Suspended</option>
                    <option value="4"  @if(request()->query('status') == '4') selected @endif>Accepted</option>

                </select>
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="subscribed">
                    <option value="none" @if(request()->query('subscribed') == 'none') selected @endif>Subscribtion</option>
                    <option value="1"  @if(request()->query('subscribed') == '1') selected @endif>Subscribed</option>
                    <option value="0"  @if(request()->query('subscribed') == '0') selected @endif>Not Subscribed</option>
                </select>
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="sub_category">
                    <option value="none" @if(request()->query('sub_category') == 'none') selected @endif>التصنيف الفرعى</option>
                    @foreach ($subCategories as $sub_category)
                    <option value={{$sub_category->id}}
                 @if(request()->query('sub_category') == $sub_category->id) selected @endif>{{$sub_category->name}} </option>
                    @endforeach
                </select>
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="pricing_class">
                    <option value="none" @if(request()->query('pricing_class') == 'none') selected @endif>Pricing</option>
                    <option value="1"  @if(request()->query('pricing_class') == '1') selected @endif>1</option>
                    <option value="2"  @if(request()->query('pricing_class') == '2') selected @endif>2</option>
                    <option value="3"  @if(request()->query('pricing_class') == '3') selected @endif>3</option>

                </select>
            </div>

            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-header')


    <th scope="col" style="width: 5%" >@sortablelink('id', '#')</th>
    <th scope="col">Img</th>
    <th scope="col">@sortablelink('name', 'Business الاسم')</th>
    <th scope="col">التصنيف الفرعى</th>
    <th scope="col">تليفون</th>
    <th scope="col">@sortablelink('subscribed', 'Subscribed')</th>
    <th scope="col">@sortablelink('rate', 'Rating')</th>
    <th scope="col">@sortablelink('reviews_count', 'المراجعات')</th>
    <th scope="col">@sortablelink('status', 'الحالة')</th>
    <th scope="col">@sortablelink('pricing_class', 'Pricing')</th>
    <th scope="col">Group</th>
    <th  scope="col" style="width: 10%">العمليات</th>

@endsection



@section('table-body')

    @foreach ($stores as $store)
        <tr>
            <td>{{ $store->id }}</td>
             <td><img src="{{ $store->avatar }}" alt="avatar" class="pp-square"></td>
            <td>{{ $store->name }}</td>
            <td > {{ $store->sub_category->name }}
                <a href="{{ route('stores'.'.edit', $store->id) }}"
                    role="button" data-toggle="modal" data-target="#modal-edit-{{ $store->id }}"
                    class="  float-right pr-2"><i class="fas fa-edit fa-sm"></i></a>

            </td>
            <td>{{ $store->phone }}</td>
            <td>
                @if($store->subscribed) Subscribed
                @else Not Subscribed
                @endif
            </td>
            <td>
                @for($j = 0; $j < $store->rate/2; $j++)
                    <span class="fa fa-star checked"></span>
                @endfor
                @if($store->rate/2 ==0)No Rating @endif
            </td>
            <td>{{ $store->reviews_count }}</td>

            <td>
                @if ($store->status == 1)
                <span class="badge badge-success text-white">Verified</span>
                @elseif ($store->status == 2)
                <span class="badge badge-warning text-black">Not Verified</span>
                @elseif ($store->status == 3)
                <span class="badge badge-danger text-white">Suspended</span>
                @elseif ($store->status == 4)
                <span class="badge badge-primary text-white">Accepted</span>
                @else
                <span class="badge bg-dark text-white text-black">غير محدد</span>
                @endif
            </td>
            <td>
                @for($i =0 ; $i< $store->pricing_class ; $i++ )
               <b> $ </b>
                @endfor
            </td>
            <td>
                @if($store->groups->count()>0)
                @foreach($store->groups as $group)
                <a  class="btn btn-success" href={{route('groups.show',$group->id)}}>{{$group->title}}</a>
                @endforeach
                @else
                 Not Assigned
                @endif
                <a  class="btn    bg-transparent" href={{route('groups.index',["add_group"=>$store->id])}}><i class="fas fa-plus"></i></a>

            </td>
            <td class="d-flex justify-content-center">
                @if(isset($store->id))
                @if(isset($store->google_maps_id))<a class="btn  " href="https://www.google.com/maps/place/?q=place_id:{{$store->google_maps_id}}">
                    <i class="fab fa-google"></i></a>
                @endif
                <a href="{{ route('stores'.'.show', $store->id) }}"
                class="btn   "><i class="fas fa-eye"></i></a>
               <a href="{{ route('stores'.'.edit', $store->id) }}"
                 class="btn  "><i class="fas fa-edit"></i></a>
                   <form method="POST" action="{{ route('users'.'.destroy', $store->user_id) }}">
                       {{ csrf_field() }}
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this store?')"
                        data-toggle="modal" data-target="#exampleModal"
                   class="btn    bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form>
                @endif
               </td>
           </tr>
           <div class="modal "  id="modal-edit-{{ $store->id }}" tabIndex="-1">

            <form method="POST" action="{{route('stores.store_single',$store->id)}}">
                @csrf
                @method('PUT')
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{$store->name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="col form-group">
                            <label class="form-label">Sub category</label><br>
                            <select name="sub_category" class="sub custom-select" id="sub_category">
                                <option>اختر ...</option>
                                @foreach ($subs as $sub)
                                <option value="{{ $sub->id }}"  {{isset($store) ? $store->sub_category_id == $sub->id? 'selected' :"":""}}>{{ $sub->name }}</option>
                                @endforeach
                            </select>
                        </div>
                          </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn bg-red">Save changes</button>
                      </div>
                    </div>
                  </div>

            </form>

        </div>
    @endforeach
    @section('table-footer')
        <p class="table-footer">النتائج :  {{$data->total()}}</p>
    @endsection

@endsection
