@extends('layouts.app')
@section('title', 'Create ' . ucfirst(\Illuminate\Support\Str::singular($name)) )


@section('content')


<form method="post" action="{{ route($name. '.store',$store->id) }}" >

    @csrf
    <div class="row ">
        <div class="col form-group">
            <input type="text" name="name" class="form-control" placeholder="الاسم "
                value="{{ old('name') }}"> 
        </div> 
        <div class="col form-group">
        <button type="submit" class="btn  btn-block btn-success bg-brandgreen ">Create
            {{ ucfirst(\Illuminate\Support\Str::singular($name)) }}</button>
        </div>

    
    </div>
    <br>

</form>

@endsection
