@extends('layouts.app')
@section('title', ucfirst($name))
@section('create-btn')
<a href="{{ route('tags' . '.create',$store->id) }}" class="  p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('content')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index',$store->id) }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index',$store->id) }}" class="small-header-bold"> X</a>
        </div>
    </form>
    <!-- Search and Filters END-->

            
    <div class="col-12 mb-2">
        <div class="card border-white">
          <div class="card-header bg-black text-white"><b>Tags</b></div>
          <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
            <thead class="thead-light  ">
              <tr>
                {{-- <th scope="col">User</th> --}}
                <th scope="col">#</th>
                <th scope="col">الاسم</th>
                <th scope="col">Added At</th>
                <th scope="col">Operation</th>
               </tr>
            </thead>
            <tbody>
            @foreach ($tags as $tag)
                <tr>
                    <td>{{ $tag->id}}</td>
                    <td>{{ $tag->name }}</td>
                    <td>{{Carbon\Carbon::parse($tag->created_at)->diffForHumans()}}
                    <td class="d-flex justify-content-center">
                           <form method="POST" action="{{ route($name.'.destroy',  [$store->id, $tag->id]) }}">
                               {{ csrf_field() }}
                               {{ method_field('DELETE') }}
                               <button type="submit"
                                onclick="return confirm('Are you sure you want to remove this tag?')" 
                                data-toggle="modal" data-target="#exampleModal" 
                           class="btn    bg-transparent"><i class="fas fa-trash-alt "></i></button>
                           </form>
                       </td>
                    </tr>
            @endforeach
        </tbody>
    </table>
  </div>

    {{-- {{ $data->appends(request()->query())->links() }} --}}
{{-- <p class="table-footer">النتائج :  {{$data->total()}}</p> --}}

@endsection
