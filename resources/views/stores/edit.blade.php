@extends('layouts.resource.edit')

@section ('content-header')
<div>
    <ul class="nav nav-tabs shadow-none ">
        <li class="nav-item ">
        <a class="nav-link " href="{{route('stores.branches.index',$store->id)}}">Branches</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="{{route('stores.edit',$store->id)}}">تحديث</a>
        </li>
    
      </ul>
</div>
@endsection
@section('form')
<div class="px-2 pt-5">
<form method="POST" action="{{ route($name. '.update', $store->id) }}" enctype="multipart/form-data">

    @csrf

    @method('PUT')

    <div class="form2">
        <div class=" mb-3">
        <h3 class="section-header ">Additional information: </h3>
        <p class="section-subheader">
            This is required additional information for creating new user
            <code>المحل</code>
        </p>
        </div>
        </div>
            <div class=" flex-fill ">
                <div class="row">
                    <div class="col form-group">
                        <label class="form-label">الاسم</label>
                        <input type="text" name="name" class="form-control" placeholder="الاسم"
                            value={{isset($store) ? $store->name:old('name')}}>
                    </div>
                    <div class="col form-group">
                        <label class="form-label">الصورة</label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="validatedCustomFile">Replace avatar...</label>
                            <input type="file" name="avatar" id="custom-file-input-image" class="custom-file-input">
                        </div>
                    </div>
                    <div class="col-1 form-group ">
                        <label class="form-label">Current </label>
                    <img class=" pp-square " src="{{ $store->avatar }}" alt="avatar" >
                    </div>
                </div>
                <div class="row">
                    <div class=" col form-group">
                        <label class="form-label">BIO</label>
                        <textarea type="text" row="5" name="bio" class="form-control" placeholder="BIO"
                       value= {{isset($store) ? $store->bio:old('bio')}}>{{isset($store) ? $store->bio:old('bio')}}</textarea>
                    </div>
                    <div class="col-4 form-group">
                        <input type="tel" name="phone" class="form-control my-2" placeholder="تليفون" id="phone"
                        value={{isset($store) ? $store->phone:old('phone')}}>
                            <input type="tel" name="website" class="form-control" placeholder="Website" id="website"
                            value={{isset($store) ? $store->website:old('website')}}>
                </div>

                </div>
                <div class="row">
                    <div class="col form-group">
                        <label class="form-label">Subscribtion</label>
                    <select class="form-control form-inline" name="subscribed">
                        <option value="none" {{isset($store) ? $store->subscribed == 'none' ? 'selected' :"":""}} >Subscribtion</option>
                        <option value="1"  {{isset($store) ? $store->subscribed == '1' ? 'selected' :"":""}}>Subscribed</option>
                        <option value="0"  {{isset($store) ? $store->subscribed == '0' ? 'selected' :"":""}}>Not Subscribed</option>
                    </select>
                     </div>

                    <div class="col form-group">
                        <label class="form-label">Category</label>
                        <select name="category" class="cat custom-select" id="category" data-dependent="sub_category">
                            <option >اختر ...</option>
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}" {{isset($store) ? $store->sub_category->category_id== $category->id ? 'selected' :"":""}}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col form-group">
                        <label class="form-label">Sub category</label><br>
                        <select name="sub_category" class="sub custom-select" id="sub_category">
                            <option>اختر ...</option>
                            @foreach ($subs as $sub)
                            <option value="{{ $sub->id }}"  {{isset($store) ? $store->sub_category_id == $sub->id? 'selected' :"":""}}>{{ $sub->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col form-group">
                        <label class="form-label">Pricing</label>
                        <select name="pricing_class" class="custom-select">
                        <option value="0"  @if(request()->query('pricing_class') == '0'||old('pricing_class')=='0' ||$store->pricing_class=="0") selected @endif>اختر ...</option>
                            <option value="1"  @if(request()->query('pricing_class') == '1'||old('pricing_class')=='1' ||$store->pricing_class=="1") selected @endif>1</option>
                            <option value="2"  @if(request()->query('pricing_class') == '2'||old('pricing_class')=='2' ||$store->pricing_class=="2") selected @endif>2</option>
                            <option value="3" @if(request()->query('pricing_class') == '3'||old('pricing_class')=='3' ||$store->pricing_class=="3") selected @endif>3</option>

                        </select>
                    </div>

                </div>

        </div>
        <div class="row">
            <div class="col form-group">
                <label class="form-label">الحالة</label>
                <select name="status" class="custom-select">
                    <option value="0" {{isset($store) ? $store->status == 'none' ? 'selected' :"":""}} >اختر ...</option>
                    <option value="1" {{isset($store) ? $store->status == '1' ? 'selected' :"":""}}>Verified</option>
                    <option value="2" {{isset($store) ? $store->status == '2' ? 'selected' :"":""}}>Not verified</option>
                    <option value="3" {{isset($store) ? $store->status == '3' ? 'selected' :"":""}}>Suspended</option>
                    <option value="4" {{isset($store) ? $store->status == '4' ? 'selected' :"":""}}>Accepted</option>

                </select>
            </div>
            <div class="col form-group">
                <label class="form-label">Opens at {{isset($store) ? $store->open_at: old('open_at')}} </label>
                <div class="input-append date form_datetime" id="exampleInputStartsAtDate">
                    <input class="form-control " size="30" type="text"
                    name="open_at"

                    placeholder="{{isset($store) ? $store->open_at: old('open_at')}}  "
                    value={{isset($store) ? $store->open_at: old('open_at')}}
                    >
                    <span class="add-on text-brand"><i class="icon-remove"></i></span>
                    <span class="add-on"><i class="icon-th"></i></span>
                </div>
            </div>
            <div class="col form-group">
                <label class="form-label">Closes at {{isset($store) ? $store->close_at: old('close_at')}} </label>
                <div class="input-append date form_datetime" id="exampleInputStartsAtDate">
                    <input class="form-control " size="30" type="text"
                    name="close_at"
                    placeholder="Change Closes at "
                     value={{isset($store) ? $store->close_at:old('close_at')}}
                      >
                    <span class="add-on text-brand"><i class="icon-remove"></i></span>
                    <span class="add-on"><i class="icon-th"></i></span>
                </div>
            </div>

        </div>
        <fieldset class="gllpLatlonPicker d-flex ">
            <div class="row">
                <div class="col-6 form-group ">
                    <label class="form-label">Location </label>
                    <div class="gllpMap" style="width:100% width: 500px; height: 250px; ">Google Maps</div>
                </div>
                <br />
                <div class="col-6 form-group">
                    <div class="row">
                        <div class=" col-md-5 form-group ">
                            <label class="form-label">Latitude</label>
                            <input type="text" class="gllpLatitude  form-control  " name="lat"
                                value={{isset($store) ? $store->latitude :""}} />
                        </div>
                        <div class=" col-md-5  form-group">
                            <label class="form-label">Longitude</label>
                            <input type="text" class="gllpLongitude form-control  " name="long"
                                value={{isset($store) ? $store->longitude :""}} />
                        </div>
                        <div class=" col-md-2  form-group">
                            <label class="form-label">Zoom</label>
                            <input type="text" class="gllpZoom form-control" value="20" />
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col form-group">
                            <label class="form-label">Address :{{isset($store) ? $store->address:""}}</label>
                            <input name="address" type="text" class="gllpLocationName form-control flex-fill"
                            value="{{ Str::limit($store->address,300)}}" placeholder="{{ Str::limit($store->address,300)}}" />
                        </div>
                    </div>
                    <input type="button" class="gllpUpdateButton p-2  btn btn-block  bg-black mb-2 " value="تحديث Map">

                </div>
            </div>
    </fieldset>
    <button type="submit" class="btn1 btn float-right bg-red mb-5 ">Submit</button>
    </div>

</form>
</div>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>

<script>
    $('.sub').prop('disabled', true);
    $('.cat').change(function () {
        $('.sub').prop('disabled', false);
        if ($(this).val() != '') {
            var select = $(this).attr("id");
            var value = $(this).val();
            var dependent = $(this).data('dependent');
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('category.fetch') }}",
                method: "POST",
                data: {
                    select: select,
                    value: value,
                    dependent: dependent,
                    _token: _token
                },
                success: function (result) {
                    $('#' + dependent).html(result);
                }
            })
        }
    });

    $('#category').change(function () {
        $('#sub_category').val('');
    });
</script>

@endsection
