@extends('layouts.resource.index')
@section('title', ucfirst($name))
@section('create-btn')
<a href="{{ route('offers' . '.create',$store->id) }}" class="  p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index',$store->id) }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index',$store->id) }}" class="small-header-bold"> X</a>
        </div>
    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-body')

@section('table-header')
            
            <th scope="col">الصورة</th>
            <th scope="col">Caption</th>
            <th scope="col">Body</th>
            <th scope="col">Redeem code</th>
            <th scope="col">@sortablelink('redeems_count',"Redemptions")</th>
            <th scope="col">@sortablelink('created_at','وقت الاضافة')</th>
            <th  scope="col" style="width: 10%">العمليات</th>
       
@endsection
@section('table-body')
            @foreach ($offers as $offer)
                <tr>
                    <td><img src="{{ $offer->image}}" class="pp-square"  ></td>
                    <td>{{ $offer->caption }}</td>
                    <td>{{ $offer->body }}</td>
                    <td>{{ $offer->redeem_code }}</td>
                    <td>{{ $offer->redeems_count }}</td>
                    <td>{{Carbon\Carbon::parse($offer->created_at)->diffForHumans()}}
                    <td class="d-flex justify-content-center"><a 
                        class="btn   "><i class="fas fa-eye"></i></a>
                       <a href="{{ route($name.'.edit',  [$store->id, $offer->id]) }}" 
                         class="btn  "><i class="fas fa-edit"></i></a>
                           <form method="POST" action="{{ route($name.'.destroy',  [$store->id, $offer->id]) }}">
                               {{ csrf_field() }}
                               {{ method_field('DELETE') }}
                               <button type="submit"
                                onclick="return confirm('Are you sure you want to delete this offer?')" 
                                data-toggle="modal" data-target="#exampleModal" 
                           class="btn    bg-transparent"><i class="fas fa-trash-alt "></i></button>
                           </form>
                       </td>
                    </tr>
            @endforeach
 @endsection
    {{ $data->appends(request()->query())->links() }}
@section('table-footer')
<p class="table-footer">النتائج :  {{$data->total()}}</p>

@endsection
@endsection
