@extends('layouts.app')
@section('title', 'Create ' . ucfirst(\Illuminate\Support\Str::singular($name)) . ' Info')


@section('back')
    @foreach ($storeID as $id)
        <a href="{{ route($name. '.index', $id->id) }}" class=" text-gold ">{{ ucfirst($name) }}</a> / 
    @endforeach
@endsection

@section('content')


<form method="post" action="{{ route($name. '.store', $store->id) }}" enctype="multipart/form-data">

    @csrf
    <div class="row">
        <div class="col form-group">
            <label>الصورة</label>
            <div class="custom-file">
                <label class="custom-file-label" for="validatedCustomFile">اختر الملف ...</label>
                <input type="file" name="image" id="custom-file-input-image" class="custom-file-input" >
            </div>
        </div>
        <div class="col form-group">
            <label>Caption</label>
            <input type="text" name="caption" class="form-control" placeholder="Caption"
                value="{{ old('caption') }}"> 
        </div> 
    </div>
    <div class="row">
        <div class="col form-group">
            <label>Body</label>
            <textarea type="text" row="5" name="body" class="form-control" placeholder="Body"
             value="{{ old('body')}}">{{ old('body')}}</textarea>
        </div>
    </div>
    <br>
    <button type="submit" class="btn btn-block btn-success bg-brandgreen ">Create
        {{ ucfirst(\Illuminate\Support\Str::singular($name)) }}</button>

</form>
<script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>

@endsection
