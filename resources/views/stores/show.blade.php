@extends('layouts.app') @section('head_charts')
<!-- Charting library -->
<script src="{!! mix('js/charts.js') !!}"></script>
@endsection
@section('content')
<!--Info-->
<div class="card mb-3  shadow-sm " style="margin-right: -1vh">
    <div class="row no-gutters">
        <div class="col-md-2 d-flex align-content-center justify-content-center border-right">
            <img src={{ $store->avatar }} class="pp-store " alt={{ $store->name }}>
        </div>
        <div class="col-md-8 border-right ">
            <div class="card-body ">
                <h5 class="card-title mb-0 text-bold">{{ $store->name }}</h5>
                <div class="justify-content-between align-content-center d-flex flex-fill">
                    <div class='row pl-3'>
                        @foreach($tags as $tag)
                            <div class='mr-2'>
                                <span class="badge badge-primary text-white">{{ $tag->name }}</span>
                            </div>
                        @endforeach

                    </div>
                    <div>
                        <div class="badge bg-red mb-2  flex-fill mr-2"
                            onclick="window.location='{{ route('tags'.'.index', $store->id) }}'">
                            <i class="fas fa-cog text-white  "></i>
                            <span class=" col-11 text-center "> Manage Tags </span>
                        </div>
                        <div class="badge bg-red mb-2  flex-fill mr-2"
                            onclick="window.location='{{ route('store_params' . '.index' ,$store->id) }}'">
                            <i class="fas fa-sliders-h text-white  "></i>
                            <span class=" col-11 text-center "> Rating Parameters </span>
                        </div>
                    </div>
                </div>

                <p class="card-text mb-0"><small class="text-muted bold">{{ $store->sub_category->name }}</small>
                </p>
                <p class="card-text mb-0">{{ $store->bio }}</p>
            </div>
        </div>
        <div class="col-md-2 border-right">
            <div class="card-header  bg-transparent py-0 d-flex justify-content-between pr-0 align-items-center">
                <b>التواصل Info</b>
                <a href="{{ route('stores'.'.edit', $store->id) }}"
                    class="btn   "><i class=" fas fa-cog"></i></a>
            </div>
            <div class="card-body">
                <p class="card-text mb-0"><small class="text-muted"><i class="fas fa-phone-square"></i>
                        {{ $store->phone }}</small></p>
                <p class="card-text mb-0"><small class="text-muted"><i class="fas fa-globe-europe"></i>
                        {{ $store->website }}</small></p>
                <p class="card-text mb-0"><small class="text-muted"> <i class="fas fa-map-marker-alt"></i>
                        {{ $store->address }}</small></p>
            </div>
        </div>

    </div>
</div>

<!--end of info -->

<!--Statistics-->
<div class="row">
    <div class="col">
        <div class="card bg-red mb-2  flex-fill ">
            <div class="card-header  d-flex justify-content-center">
                <i class="fas fa-star-half-alt text-white my-3 fa-2x"></i>
            </div>
            <div class="card-body row text-center">
                <div class="col">
                    <div class="text-vlarge text-white">{{ $store->rate }}</div>
                    <div class="text-uppercase text-white small">Stars</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col ">
        <div class="card bg-red mb-2 flex-fill ">
            <div class="card-header  d-flex justify-content-center">
                <i class="fab fas fa-street-view text-white my-3 fa-2x"></i>
            </div>
            <div class="card-body row text-center">
                <div class="col">
                    <div class="text-vlarge text-white">{{ $store->checkins->count() }}</div>
                    <div class="text-uppercase text-white small">Check-ins</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col"
        onclick="window.location='{{ route('reviews' . '.index' ,$store->id) }}'">
        <div class="card bg-red mb-2 flex-fill ">
            <div class="card-header  d-flex justify-content-center">
                <i class="fas fa-pencil-alt text-white my-3 fa-2x"></i>
            </div>
            <div class="card-body row text-center">
                <div class="col">
                    <div class="text-vlarge text-white">{{ $store->reviews->count() }}</div>
                    <div class="text-uppercase text-white small">المراجعات</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col"
        onclick="window.location='{{ route('offers' . '.index' ,$store->id) }}'">
        <div class="card bg-red mb-2  flex-fill">
            <div class="card-header  d-flex justify-content-center">
                <i class="fas fa-carrot text-white my-3 fa-2x"></i>
            </div>
            <div class="card-body row text-center">
                <div class="col">
                    <div class="text-vlarge text-white">{{ $store->offers->count() }}</div>
                    <div class="text-uppercase text-white small">Offers</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col"
        onclick="window.location='{{ route('casheers' . '.index' ,$store->id) }}'">
        <div class="card bg-red mb-2  flex-fill">
            <div class="card-header  d-flex justify-content-center">
                <i class="fas fa-cash-register text-white my-3 fa-2x"></i>
            </div>
            <div class="card-body row text-center">
                <div class="col">
                    <div class="text-vlarge text-white">{{ $store->casheers->count() }}</div>
                    <div class="text-uppercase text-white small">Casheers</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end of Statistics-->
{{-- Charts --}}
<div>
    <div class="row  ">
        <div class=" col-lg-6 p-3 ">
            <div class="card-shadow-info  widget-chart   card  ">
                <div class="card-header-tab card-header bg-transparent">
                    <div class="card-header-title">
                        <i class="header-icon lnr-bicycle icon-gradient bg-love-kiss"> </i>
                        Charts:
                        @foreach( $datasets as $dataset )
                            {{ $dataset['label'] }}
                        @endforeach
                    </div>
                    <ul class="nav">
                        <li class="nav-item"><a data-toggle="tab" href="#tab-eg6-0" class="active nav-link">Graph</a>
                        </li>
                        <li class="nav-item"><a data-toggle="tab" href="#tab-eg6-1" class="nav-link">Data</a></li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-eg6-0" role="tabpanel">
                            <div class="col-12">
                                <x-basic-chart :config="$config" :labels="$months" :datasets="$datasets">
                                </x-basic-chart>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-eg6-1" role="tabpanel">
                            @foreach($datasets as $dataset)
                                <div style="row px-5 ">
                                    <div class="mt-2 pr-3">
                                        <b>{{ ucfirst($dataset['label']) }}:</b>
                                        <hr>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            @foreach($months as $month )
                                            <div class="col-lg-4 col-md-6">
                                            <div class="card mb-3 widget-content" style="background-color:{{$dataset['pointBackgroundColor']}}">
                                                    <div class="widget-content-wrapper text-white">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">{{ $month }}</div>
                                                            <div class="widget-subheading">Graph </div>
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-white">
                                                                <span>{{ $dataset['data'][$loop->index] }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                            @endforeach


                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class=" col-lg-6 p-3 ">
            <div class="card-shadow-info  widget-chart   card  ">
                <div class="card-header-tab card-header bg-transparent">
                    <div class="card-header-title">
                        <i class="header-icon lnr-bicycle icon-gradient bg-love-kiss"> </i>
                        Parameters radar
                    </div>
                    <ul class="nav">
                        <li class="nav-item"><a data-toggle="tab" href="#tab-eg5-0" class="active nav-link">Graph</a>
                        </li>
                        <li class="nav-item"><a data-toggle="tab" href="#tab-eg5-1" class="nav-link">Data</a></li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-eg5-0" role="tabpanel">
                            <div class="col-12">
                                <x-radar-chart :radarconfig="$radarconfig" :radarvalues="$radarvalues"
                                    :radarlabels="$radarlabels">
                                </x-radar-chart>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-eg5-1" role="tabpanel">
                            @foreach($store->params as $param)
                                <div style="row px-5 ">
                                    <div class="mt-2 pr-3">
                                        <p>{{ $param->name }}</p>
                                        <div class="float-right">
                                            <b> المراجعات: {{ $param->review_rates->count() }}</b>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="progress mt-2">
                                                <div class="progress-bar progress-bar-striped progress-bar-animated
                                                {{ $param->review_rates->avg('rate')<=3 ? 'bg-black' : 'bg-black' }}
                                                {{ $param->review_rates->avg('rate')>=5 && $param->review_rates->avg('rate')<7 ? 'bg-green': null }}
                                                {{ $param->review_rates->avg('rate')>=7 ?'bg-green': null }}"
                                                    role="progressbar" aria-valuenow="75" aria-valuemin="0"
                                                    aria-valuemax="100"
                                                    style="width: {{ $param->review_rates->avg('rate')/10 *100 .'%' }}">
                                                    {{ ceil($param->review_rates->avg('rate')) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                            @endforeach


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
{{-- End of charts --}}
<!--extra Info-->
<div class="row justify-content-center d-flex">


    @if($store->reviews->count()>0)
        <div class="col-12 mb-2">
            <div class="card border-white">
                <div class="card-header bg-black text-white"><b>المراجعات</b></div>
                <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
                    <thead class="thead-light  ">
                        <tr>
                            {{--
                        <th scope="col">User</th> --}}
                            <th scope="col">الصورة</th>
                            <th scope="col">User</th>
                            <th scope="col">Good</th>
                            <th scope="col">Bad</th>
                            <th scope="col">التقيم</th>
                            <th scope="col">Recommended</th>
                            <th scope="col">وقت الاضافة</th>
                            <th scope="col">Reply</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reviews as $row)
                            <tr>
                                <td><img src="{{ $row->client->avatar }}" alt="avatar" class="pp-square"></td>
                                <td>{{ $row->client->first_name }}{{ $row->client->last_name }}</td>
                                <td>{{ Str::limit($row->good ,30) }}</td>
                                <td>{{ Str::limit($row->bad ,30) }}</td>
                                <td>{{ $row->rate }}
                                    <a href="{{ route('reviews'.'.show',[ $store->id,$row->id]) }}"
                                        class="   "> <i class="fas fa-ellipsis-v fa-xs "></i></a>
                                </td>
                                <td>@if ($row->is_recommended == 1)
                                    <span class="badge badge-success text-white">Recommended</span>
                                    @elseif($row->is_recommended == 0)
                                    <span class="badge badge-warning text-black">Not Recommended</span> @endif
                                </td>
                                <td>{{ Carbon\Carbon::parse($row->created_at)->diffForHumans() }}
                                </td>
                                <td class="d-flex justify-content-center"><a
                                        href="{{ route('reviews'.'.show',[ $store->id,$row->id]) }}"
                                        class="btn   "><i class="fas fa-comment-dots"></i></a>
                                    <form method="POST"
                                        action="{{ route('comments'. '.store',[$row->store_id,$row->id]) }}">
                                        @csrf
                                        @method('POST')
                                        <div>
                                            <input type="hidden" name="comment" class="form-control  "
                                                value="Thanks for your review ">
                                            <button class=" btn text-uppercase bg-red clr-black small"><i
                                                    class="fas fa-seedling"></i></button>
                                        </div>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <a href="{{ route('reviews.index', $store->id) }}"
                class=" text-bold float-right mb-2">See
                more -></a>
        </div>
        @endif
        @if($store->status == App\Models\المحل::STATUS_VERIFIED)
        @if($store->offers->count()>0)
        <div class="col-12 mb-2">
        <div class="card border-white">
            <div class="card-header bg-black text-white"><b>Offers</b>
                <a href="{{ route('offers' . '.create' ,$store->id) }}"
                    class="  p-2 "><i class="fas fa-plus"></i></a>
            </div>
            <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
                <thead class="thead-light  ">
                    <tr>
                        <th scope="col">الصورة</th>
                        <th scope="col">Caption</th>
                        <th scope="col">Body</th>
                        <th scope="col">Redeem code</th>
                        <th scope="col">Redemptions</th>
                        <th scope="col">وقت الاضافة</th>
                        <th scope="col" style="width: 10%">العمليات</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($offers as $offer)
                        <tr>
                            <td><img src="{{ $offer->image }}" class="pp-square"></td>
                            <td>{{ $offer->caption }}</td>
                            <td>{{ $offer->body }}</td>
                            <td>{{ $offer->redeem_code }}</td>
                            <td>{{ $offer->redeems_count }}</td>
                            <td>{{ Carbon\Carbon::parse($offer->created_at)->diffForHumans() }}
                            <td class="d-flex justify-content-center"><a class="btn clr-black  "><i
                                        class="fas fa-eye"></i></a>
                                <a href="{{ route($name.'.edit',  [$store->id, $offer->id]) }}"
                                    class="btn clr-black "><i class="fas fa-edit"></i></a>
                                <form method="POST"
                                    action="{{ route($name.'.destroy',  [$store->id, $offer->id]) }}">
                                    {{ csrf_field() }} {{ method_field('DELETE') }}
                                    <button type="submit"
                                        onclick="return confirm('Are you sure you want to delete this offer?')"
                                        data-toggle="modal" data-target="#exampleModal"
                                        class="btn  clr-red  bg-transparent"><i class="fas fa-trash-alt "></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <a href="{{ route('offers.index', $store->id) }}"
            class=" text-bold  float-right mb-2">See more -></a>
    </div>
@else

    <div class="col-12 mb-2">
        <div class="card  ">
            <div class="card-body row d-flex   ">
                <div class="border-right col-11 align-self-center">
                    <h4 class="text-muted text-bold text-uppercase">Add offers </h4>
                </div>
                <div class="col-1"> <a
                        href="{{ route('offers' . '.create' ,$store->id) }}"
                        class="  fa-4x "><i class="fas fa-plus"></i></a>
                </div>
            </div>
        </div>
    </div>


    @endif
    @if($store->casheers->count()>0)
    <div class="col-12 mb-2">
        <div class="card border-white">
            <div class="card-header bg-black text-white"><b>Casheers</b>
                <a href="{{ route('casheers' . '.create' ,$store->id) }}"
                    class="  p-2 "><i class="fas fa-plus"></i></a>
            </div>
            <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
                <thead class="thead-light  ">
                    <tr>
                        <th scope="col">الصورة</th>
                        <th scope="col">الاسم</th>
                        <th scope="col">Bio</th>
                        <th scope="col">تليفون</th>
                        <th scope="col">Address </th>
                        <th scope="col">وقت الاضافة</th>
                        <th scope="col" style="width: 10%">العمليات</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($casheers as $casheer)
                        <tr>
                            <td><img src="{{ $casheer->avatar }}" class="pp-square"></td>
                            <td>{{ $casheer->name }}</td>
                            <td>{{ $casheer->bio }}</td>
                            <td>{{ $casheer->phone }}</td>
                            <td>{{ $casheer->address }}</td>
                            <td>{{ Carbon\Carbon::parse($casheer->created_at)->diffForHumans() }}
                            <td class="d-flex justify-content-center"><a
                                    href="{{ route($name . '.show',[$store->id,$casheer->id]) }}"
                                    class="btn clr-black  "><i class="fas fa-eye"></i></a>
                                <a href="{{ route($name.'.edit',  [$store->id, $casheer->id]) }}"
                                    class="btn clr-black "><i class="fas fa-edit"></i></a>
                                <form method="POST"
                                    action="{{ route($name.'.destroy',  [$store->id, $casheer->id]) }}">
                                    {{ csrf_field() }} {{ method_field('DELETE') }}
                                    <button type="submit"
                                        onclick="return confirm('Are you sure you want to delete this casheer?')"
                                        data-toggle="modal" data-target="#exampleModal"
                                        class="btn  clr-red  bg-transparent"><i class="fas fa-trash-alt "></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        <a href="{{ route('casheers.index', $store->id) }}"
            class=" text-bold float-right mb-2">See more -></a>
    </div>
@else
    <div class="col-12 mb-2">
        <div class="card border-black ">
            <div class="card-body row d-flex  ">
                <div class="border-right col-11 align-self-center">
                    <h4 class="text-muted text-bold text-uppercase">Add Casheers </h4>
                </div>
                <div class="col-1"> <a
                        href="{{ route('casheers' . '.create' ,$store->id) }}"
                        class="  fa-4x "><i class="fas fa-plus"></i></a>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($store->cards->count()>0)
    <div class="col-12 mb-2">
        <div class="card  border-white">
            <div class="card-header bg-black text-white"><b>Cards</b></div>
            @foreach($cards as $card)
                <div class="row px-2 mt-2">
                    @if($card->type==1)
                        <div class="col-2">
                            <div class="card bg-green mb-2  flex-fill">
                                <div class="card-header bg-transparent d-flex justify-content-center">
                                    <i class="fas fa-puzzle-piece text-white my-3 fa-2x"></i>
                                </div>
                                <div class="card-body row text-center">
                                    <div class="col">
                                        <div class="text-uppercase text-white small">Recommendation</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @elseif($card->type==2)
                        <div class="col-2">
                            <div class="card bg-red mb-2  flex-fill">
                                <div class="card-header bg-transparent  d-flex justify-content-center">
                                    <i class="fas fa-exclamation-triangle text-white my-3 fa-2x"></i>
                                </div>
                                <div class="card-body row text-center">
                                    <div class="col">
                                        <div class="text-uppercase text-white small">Complain</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="col">
                        <div class="card bg-white mb-2  flex-fill">
                            <div class="card-header bg-transparent d-flex justify-content-start align-items-center">
                                <img src="{{ $card->client->avatar }}" class="pp-circle mr-2 ">
                                <b> {{ $card->client->first_name }} {{ $card->client->last_name }}</b>
                            </div>
                            <div class="card-body row text-center">
                                <div class="col-2 border-right">
                                    <div class="text-vlarge ">{{ $card->satisfaction_rate }}</div>
                                    <div class="text-uppercase  small">Satisfaction</div>
                                </div>
                                <div class="col-8">
                                    <b>
                                        @foreach($card->messages as $message)
                                            @if($loop->first)
                                                @if($message->type==1)
                                                    {{ Str::limit($message->message,250) }}
                                                @elseif($message->type==2)
                                                    <div class="row d-flex align-content-center">
                                                        <div class="col-1">
                                                            <img src={{ asset('files/cards/images/')."/".$message->message }}
                                                                class="pp-big">
                                                        </div>
                                                        <div class="col-10 align-self-center">
                                                            Photo has been sent
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                        @if($card->messages->count()<=0) There is no messages @endif </b> </div>
                                        <div class="col-2 border-left">
                                                <div class="text-vlarge ">
                                                    <a href="{{ route('cards'.'.show',[ $store->id,$card->id]) }}"
                                                        class="   "> <i class="fas fa-comment-dots fa-2x "></i></a>
                                                </div>
                                                <div class="text-uppercase  small">Replay</div>
                                            </div>
                                            <div class="col-6">
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>

            @endforeach
        </div>
        <a href="{{ route('cards.index', $store->id) }}"
            class=" text-bold float-right mb-2">See more -></a>
    </div>
    @endif
    @else
    <div class="alert alert-info d-flex flex-fill m-2 align-items-center justify-content-between" role="alert">
        <strong> Verify to Unlock المراجعات , Offers & More </strong>
        <a href={{ route( 'requests.verify.create',$store->id) }}><strong>Verify
                Now</strong></a>
    </div>
    @endif
</div>
<!-- end of extra Info-->




@endsection
