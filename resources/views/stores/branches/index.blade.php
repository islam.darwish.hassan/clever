@extends('layouts.resource.index')
@section('title', 'المحل Branches')
@section('create-btn')
<a href="{{ route($name . '.create',$store->id) }}" class="  p-2 "><i class="fas fa-plus"></i></a>
@endsection
@section('search-filter')
<div>
    <ul class="nav nav-tabs shadow-none ">
        <li class="nav-item ">
        <a class="nav-link active" href="{{route($name.'.index',$store->id)}}">Branches</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="{{route('stores.edit',$store->id)}}">تحديث</a>
        </li>

      </ul>
    </div>
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index',$store->id) }}" method="get">
        <div class="d-flex justify-content-between">
        <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
        </div>
        <div class="p-2 w-20 ">
            <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
        </div>
        <a href="{{ route($name. '.index',$store->id) }}" class="small-header-bold"> X</a>

        </div>
    </form>
     <!-- Search and Filters END-->


@endsection

@section('table-header')

    <th scope="col" style="width: 5%" >#</th>
    <th scope="col">الصورة</th>
    <th scope="col">الاسم</th>
    <th scope="col">تليفون</th>
    <th scope="col">الحالة </th>
    <th scope="col">Added At </th>
    <th  scope="col" style="width: 10%">العمليات</th>

@endsection



@section('table-body')

    @foreach ($data as $row)
        <tr>
            <td>{{ $row->id }}</td>
            <td><img src="{{ $row->avatar}}" alt="avatar" class="pp-square"></td>
            <td><a href="{{route('stores.show',$row->id)}}">{{$row->name}}</a></td>
            <td>{{ $row->phone}}</td>
            <td>
                @if ($row->status == 1)
                <span class="badge badge-success text-white">Verified</span>
                @elseif ($row->status == 2)
                <span class="badge badge-warning text-black">Not Verified</span>
                @elseif ($row->status == 3)
                <span class="badge badge-danger text-white">Suspended</span>
                @elseif ($row->status == 4)
                <span class="badge badge-primary text-white">Accepted</span>
                @else
                <span class="badge bg-dark text-white text-black">غير محدد</span>
                @endif
            </td>
            <td>{{ $row->created_at}}</td>

            <td class="d-flex justify-content-center">
               {{-- <a href="{{ route($name.'.edit', $row->id) }}"
                 class="btn  "><i class="fas fa-edit"></i></a>


                   <form method="POST" action="{{ route($name.'.destroy', $row->id) }}">
                      @csrf
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this row?')"
                        data-toggle="modal" data-target="#exampleModal"
                   class="btn    bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form> --}}
               </td>
        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
    @endsection

@endsection
