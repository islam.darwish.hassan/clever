@extends('layouts.app')
@section('title', 'Assign Branch')


@section('back')
        <a href="{{ route($name. '.index', $store->id) }}" class=" text-gold ">{{ ucfirst($name) }}</a> / 
@endsection

@section('content')

<ul class="nav nav-tabs shadow-none ">
    <li class="nav-item ">
    <a class="nav-link active" href="{{route($name.'.create',$store->id)}}">Assign Branch</a>
    </li>
    <li class="nav-item">
      <a class="nav-link " href="">Acquire Branch</a>
    </li>

  </ul>
</div>
<div class="px-2 pt-5">
<form method="post" action="{{ route($name. '.store', $store->id) }}" enctype="multipart/form-data">

    <div class=" flex-fill ">
        <div class="row">
            <div class="col form-group">
                <label class="form-label">الاسم *</label>
                <input type="text" name="name" class="form-control" placeholder="الاسم"
                    value="{{ old('name') }}">
            </div>
            <div class="col form-group">
                <label class="form-label">الصورة *</label>
                <div class="custom-file">
                    <label class="custom-file-label" for="validatedCustomFile">اختر الملف ...</label>
                    <input type="file" name="avatar_store" id="custom-file-input-image" class="custom-file-input">
                </div>
            </div>
        </div>
        <div class="row">
            <div class=" col form-group">
                <label class="form-label">BIO</label>
                <textarea type="text" row="5" name="bio_store" class="form-control" placeholder="BIO"
                value="{{null !==(old('bio_store'))? old('bio_store'):'' }}"
                >{{null !==(old('bio_store'))? old('bio_store'):'' }}</textarea>
            </div>
            <div class="col-4 form-group">
                <input type="tel" name="phone_store" class="form-control my-2" placeholder="تليفون" id="phone_store"
                value="{{null !==(old('phone_store'))? old('phone_store'):'' }}" />
                <input type="website" name="website" class="form-control" placeholder="Website" id="website"
                value="{{null !==(old('website'))? old('website'):'' }}" />
        </div>

        </div>
        <div class="row">
            <div class="col form-group">
                <label class="form-label">Subscribtion *</label>
            <select class="form-control form-inline" name="subscribed">
                <option value="none" @if(request()->query('subscribed') == 'none'||old('subscribed')=='none') selected @endif>اختر ...</option>
                <option value="1"  @if(request()->query('subscribed') == '1'||old('subscribed')=='1') selected @endif>Subscribed</option>
                <option value="0"  @if(request()->query('subscribed') == '0'||old('subscribed')=='0') selected @endif>Not Subscribed</option>
            </select>
             </div>

            <div class="col form-group">
                <label class="form-label">Pricing *</label>
                <select name="pricing_class" class="custom-select">
                    <option value="0"  @if(request()->query('pricing_class') == '0'||old('pricing_class')=='0') selected @endif>اختر ...</option>
                    <option value="1"  @if(request()->query('pricing_class') == '1'||old('pricing_class')=='1') selected @endif>1</option>
                    <option value="2"  @if(request()->query('pricing_class') == '2'||old('pricing_class')=='2') selected @endif>2</option>
                    <option value="3" @if(request()->query('pricing_class') == '3'||old('pricing_class')=='3') selected @endif>3</option>
                </select>
            </div>

        </div>
</div>
<div class="row">

    <div class="col form-group">
        <label class="form-label">Opens at *</label>
        <div class="input-append date form_datetime" id="exampleInputStartsAtDate">
            <input type="time" class="form-control " size="30"  name="open_at" value="{{ old('open_at', date('H:i:s')) }}">

            <span class="add-on text-brand"><i class="icon-remove"></i></span>
            <span class="add-on"><i class="icon-th"></i></span>
        </div>
    </div>
    <div class="col form-group">
        <label class="form-label">Closes at*</label>
        <div class="input-append date form_datetime" id="exampleInputStartsAtDate">
            <input class="form-control " size="30" type="time" value="{{ old('close_at', date('H:i:s')) }}" name="close_at"  placeholder="This Businness Closes at">
            <span class="add-on text-brand"><i class="icon-remove"></i></span>
            <span class="add-on"><i class="icon-th"></i></span>
        </div>
    </div>

</div>
<fieldset class="gllpLatlonPicker d-flex ">
    <div class="row">
        <div class="col-6 form-group ">
            <label class="form-label">Location * </label>
            <div class="gllpMap" style="width:100% width: 500px; height: 250px; ">Google Maps</div>
        </div>
        <br />
        <div class="col-6 form-group">
            <div class="row">
                <div class=" col-md-5 form-group ">
                    <label class="form-label">Latitude *</label>
                    <input type="text" class="gllpLatitude  form-control  " name="lat"
                        value="{{null !==(old('lat'))? old('lat'):30.1054859543 }}" />
                </div>
                <div class=" col-md-5  form-group">
                    <label class="form-label">Longitude *</label>
                    <input type="text" class="gllpLongitude form-control  " name="long"
                    value="{{null !==(old('long'))? old('long'):31.37631986912 }}" />
                </div>
                <div class=" col-md-2  form-group">
                    <label class="form-label">Zoom</label>
                    <input type="text" class="gllpZoom form-control" value="20" />
                </div>
            </div>
            <div class="row">
                <div class=" col form-group">
                    <label class="form-label">Address</label>
                    <input name="address_store" id="address_store" type="text" class="gllpLocationName form-control flex-fill"
                    value="{{null !==(old('address_store'))? old('address_store'):'23 El-Zohour, Sheraton Al Matar, El Nozha, Cairo Governorate, Egypt' }}" />
                </div>
            </div>
            <input type="button" class="gllpUpdateButton p-2  btn btn-block  bg-black mb-2 " value="تحديث Map">

        </div>
    </div>
</div>
</fieldset>
<button type="submit" class="btn1 btn float-right bg-red mb-5 ">Submit</button>
</div>

</form>
</div>

<script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>

@endsection
