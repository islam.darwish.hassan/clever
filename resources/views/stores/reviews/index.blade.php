@extends('layouts.resource.index')

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index',[$store->id]) }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                <input type="text" class="form-control mb-2 mr-sm-2" name="good" placeholder="Search in good ..." value="{{request()->query('good')}}">
            </div>
            <div class="p-2 flex-fill ">
            <input type="text" class="form-control mb-2 mr-sm-2" name="bad" placeholder="Search in bad ..." value="{{request()->query('bad')}}">
          </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="is_recommended">
                    <option value="none" @if(request()->query('is_recommended') == 'none') selected @endif>Recommended by Qym</option>
                    <option value="1"  @if(request()->query('is_recommended') == '1') selected @endif>Recommended</option>
                    <option value="0"  @if(request()->query('is_recommended') == '0') selected @endif>Not Recommended</option>
                </select>
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  btn-primary mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index',[$store->id]) }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-header')

    <th scope="col">الصورة</th>
    <th scope="col">User</th>
    <th scope="col">Good</th>
    <th scope="col">Bad</th>
    <th scope="col">@sortablelink('rate','التقيم')</th>
    <th scope="col">Recommended</th>
    <th scope="col">@sortablelink('created_at','وقت الاضافة')</th>
    <th scope="col">Reply</th>

@endsection

@section('table-body')
    @foreach ($reviews as $row)
        <tr>
            <td><img src="{{ $row->client->avatar }}" alt="avatar" class="pp-square"></td>
            <td>{{ $row->client->first_name }}{{ $row->client->last_name }}</td>
            <td>{{ Str::limit($row->good ,30) }}</td>
            <td>{{ Str::limit($row->bad ,30) }}</td>
            <td>{{ $row->rate}}<a href="{{ route('reviews'.'.show',[ $store->id,$row->id]) }}"
                    class="   ">  <i class="fas fa-ellipsis-v fa-xs "></i></a>
             </td>
            <td>@if ($row->is_recommended == 1)
            <span class="badge badge-success text-white">Recommended</span>
            @elseif ($row->is_recommended == 0)
            <span class="badge badge-warning text-black">Not Recommended</span>
            @endif
            </td>
            <td>{{Carbon\Carbon::parse($row->created_at)->diffForHumans()}}
            </td>
            <td class="d-flex justify-content-center"><a href="{{ route('reviews'.'.show',[ $store->id,$row->id]) }}"
                class="btn   "><i class="fas fa-comment-dots"></i></a>
                <form method="POST" action="{{ route('comments'. '.store',[$row->store_id,$row->id]) }}">
                    @csrf
                    @method('POST')
                    <div>
                      <input type="hidden" name="comment" class="form-control  " value="Thanks for your review ">
                      <button class=" btn text-uppercase bg-red  small"><i class="fas fa-seedling"></i></button>
                    </div>
                  </form>

            </td>
        </tr>

    @endforeach
    @section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
    @endsection

@endsection
