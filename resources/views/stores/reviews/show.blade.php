@extends('layouts.app')
@section('title', 'Review of ' .$review->client->first_name." ".$review->client->last_name)
@section('content')
<div class="row">
    <div class="col-sm-11">
      <div class="row">
        <div class="col-sm-2 ">
          <div class="card bg-red mb-2  flex-fill h-100 ">
            <div class="card-body row text-center">
              <div class="col">
                <div class="text-uppercase text-white small">Good</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-10  align-self-stretch">
          <div class="card bg-white mb-2  h-100">
            <div class="card-body row text-center">
              <div class="">
                <b class="mx-2">
                  {{$review->good}}
                </b>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row pt-2">
        <div class="col-sm-2 ">
          <div class="card bg-red mb-2  flex-fill h-100 ">
            <div class="card-body row text-center">
              <div class="col">
                <div class="text-uppercase text-white small">Bad</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-10  align-self-stretch">
          <div class="card bg-white mb-2  h-100">
            <div class="card-body row text-center">
              <div class="">
                <b class="mx-2">
                  {{$review->bad}}
                </b>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <div class="col-sm-1 ">
    <div>
     <div class="badge"><b>{{$helpfulls->count()}} Helpfull</b></div>
    <div class="badge"><b>{{$not_helpfulls->count()}} Not Helpfull</b></div>
    <div class="badge"><b>{{$funnys->count()}}</b> Funny</div>
    <div class="badge"><b>{{$reports->count()}}</b> Reports</div>
    </div>
  </div>
</div>
<!-- Start of parameters-->
<hr>
<div class=" popup-gallery">
@foreach ($review->images as $item)
    <a href="{{$item->image}}" title="Review الصورة">
        <img src={{$item->image}} alt={{$item->image}}  class="img-thumbnail" style="object-fit: cover;
        max-width:100px; height:100px;"></a>
@endforeach

<hr>

<div class="card-header bg-black   d-flex justify-content-start align-items-center">
  <b>Ratings</b>
</div>

@foreach ($params as $param)
<div style="row px-5 ">
  <div class="mt-2 px-3">
    <p>{{$param->params->name}}</p>
  </div>
  <div class="col ">
    <div class="progress mt-2">
      <div class="progress-bar progress-bar-striped progress-bar-animated
          {{$param->rate<=3 ? 'bg-black' : null}}
          {{$param->rate>=5 && $param->rate<7 ? 'bg-yellow': null}}
          {{$param->rate>=7 ?'bg-green': null}}" role="progressbar" aria-valuenow="75" aria-valuemin="0"
        aria-valuemax="100" style="width: {{$param->rate/10 *100 .'%'}}">{{$param->rate}}
      </div>
    </div>
  </div>
  @endforeach
</div>
<hr>

@if($comments->count()>0)
<div class="card-header bg-black   d-flex justify-content-between align-items-center">
  <b>Comments</b>
</div>
<div class=" mt-2  d-flex flex-fill justify-content-end small" style="margin-bottom:-10px">
  {{ $comments->appends(\Request::except('page'))->render() }}
</div>
@foreach ($comments as $comment)
<div style="row mx-5  ">
  <div class="card    my-2 ">
      @if($comment->user==1 )
      <div class="card-header   d-flex justify-content-between align-items-center">
      <b class="">{{$client->first_name}} {{$client->last_name}}</b>
      <b class="small">{{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</b>

      @elseif($comment->user==2)
      @if(isset($client))
      <div class="card-header   d-flex justify-content-between align-items-center">
      <b>{{$review->store->name}}</b>
      <b class="small">{{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</b>

      @else
      <div class="card-header   d-flex justify-content-between align-items-center">
      <b>You</b>
      <b class="small">{{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</b>

      @endif
      @endif
    </div>
    <div class="card-body row text-center px-5">
      <b>{{$comment->comment}}</b>
    </div>
  </div>
  @endforeach
</div>
<div class="table-footer ">
  <p class="table-footer ">النتائج : {{$comments->total()}}</p>
</div>
<div class="row mb-5">
</div>
@if($comments->total()<=0) <div class="row justify-content-center ">
  <h5 class="empty-table-text"> There are no results 😴 </h5>
  @endif
  @endif
  @if(!isset($client))
  <form method="POST" action="{{ route('comments'. '.store',[$review->store_id,$review->id]) }}">
    @csrf
    @method('POST')
    <div class="row p-3 ">
      <div class="row flex-fill p-1">
        <div class="col">
          <textarea type="text" row="5" name="comment" class="form-control mb-2 " placeholder="Add comment here ..."
            value=""></textarea>
          <button type="submit" class="btn1 btn float-right bg-red col-2  ">Submit</button>
        </div>
      </div>
  </form>
  <div class="col-md-2">
    <div class="card bg-red mb-2  ">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-seedling text-white my-3 fa-3x"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <form method="POST" action="{{ route('comments'. '.store',[$review->store_id,$review->id]) }}">
            @csrf
            @method('POST')
            <div>
              <input type="hidden" name="comment" class="form-control  " value="Thanks for your review ">
              <button class=" btn text-uppercase bg-red  small">Say Thanks !</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  </div>
@endif
  @endsection
