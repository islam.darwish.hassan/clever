@extends('layouts.app')
@section('title', 'ادارة الطلب')

@section('content')
<!--Info-->
<script src="{{asset('js/print.js')}}"></script>

@can('view-super',Auth::user())
  <div class="card mb-3  shadow-sm "  >
    <div class="card-header">
        Order Details : #{{$order->hash_code}}
    </div>
        <a href="{{ route('orders.print',$order) }}" class="btnprn btn" target="_blank">Print Preview</a>
        <script type="text/javascript">
        $(document).ready(function(){
        $('.btnprn').printPage();
        });
        </script>
    <div class="card-body text-muted">
        <div>
        <b>كود :</b> #{{$order->hash_code}}
        </div>
        <div>

            <b>اسم العميل :</b>{{$order->client->name}} |
            <b>عنوان الشحنة : </b> {{$order->address_log}}
            </div>
            <div>
                <b>ملحوظة على الشحنة: </b> {{null !==($order->address->shipping_note)?$order->address->shipping_note:'empty'}}
             </div>
            <div>
            <b>اختيار الدفع </b> {{null !==($order->delivery_option==1)?'الدفع عند الاستلام ':'empty'}}
            </div>
            <div>
                <b>مصاريف الشحن : </b> {{$order->delivery_fees}}
            </div>

            <div>
            <b>ملحوظة على الطلب</b> {{null !==($order->note)?$order->note:'empty'}}
            </div>
            <div>
             <b>عدد الشحنات :</b> {{$order->shipments->count()}}
            </div>
            <div>
                <b>ارقام الشحنات :</b>
                    @foreach($order->shipments as $shipment)
                    @if($loop->last)
                    {{$shipment->hash_code}}.
                    @else
                    {{$shipment->hash_code}},
                    @endif
                    @endforeach
             </div>
             <div>
                <b>السعر الاجمالى</b> {{$order->total_price}} جنيه مصرى
            </div>
            <div>
                <b>السعر الاجمالى والشحن </b> {{$order->delivery_fees+$order->total_price}} EGP
            </div>

               <b>الحالة</b>
               @if ($order->status == 1)
               <span class="badge bg-info text-white">الطلب</span>
               @elseif ($order->status == 2)
               <span class="badge badge-primary text-black">تحت المعالجة</span>
               @elseif ($order->status == 3)
               <span class="badge badge-warning text-black">فى الشحن</span>
               @elseif ($order->status == 4)
               <span class="badge badge-success text-white">شحنت</span>
               @elseif ($order->status == 5)
               <span class="badge bg-dark text-white text-black">درافت</span>
               @else
               <span class="badge bg-dark text-white text-black">غير محدد</span>
               @endif
        </div>
        @if($order->status==1)
        <form method="POST" action="{{ route('orders.'.'confirm',[$order->id]) }}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <button type="submit"
            onclick="return confirm('Are you sure you want to confirm this order there is no back for this process ,please be sure to confirm all shipment first with stores?')"
            data-toggle="modal" data-target="#Modal"
        class="btn bg-info text-white  float-right mb-2 mr-2 text-uppercase">تأكيد الارسال للمعالجة</button>
        </form>
    @elseif($order->status==2)
    <form method="POST" action="{{ route('orders.'.'in_shipping',[$order->id]) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <button type="submit"
        onclick="return confirm('Are you sure you want to change status to -in shipping- this order there is no back for this process ?')"
        data-toggle="modal" data-target="#Modal"
    class="btn btn-warning   float-right mb-2 mr-2 text-uppercase text-bold">من حالة المعالجة إلى فى الشحن</button>
    </form>
    @elseif($order->status==3)
    <form method="POST" action="{{ route('orders.'.'shipped',[$order->id]) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <button type="submit"
        onclick="return confirm('Are you sure you want to change status to -shipped- this order there is no back for this process ?')"
        data-toggle="modal" data-target="#Modal"
    class="btn btn-success   float-right mb-2 mr-2 text-uppercase text-bold ">من حالة فى الشحن لحالة شحنت </button>
    </form>

    @elseif($order->status==4)
    <a  class="btn  disabled  btn-block text-uppercase">شحنت إلى العميل</a>

    @endif

    </div>
    @endcan
    @foreach($order->shipments as $shipment)
    <div class="card mb-3  shadow-sm "  >
        <div class="card-header">
            الشحنة {{$loop->index +1}} of {{$order->shipments->count()}}  : {{isset($shipment->store)?$shipment->store->name:'(Deleted) '.$shipment->store_log}}
        </div>
        <div class="card-body">
            <div class="text-muted">
            <b>رقم الشحنة :</b> #{{$shipment->hash_code}} |
            </div>
        <div class="text-muted">
            <b>عنوان الدليفرى: </b> {{$order->address_log}}
        </div>
        <div class="text-muted">
            <b>عدد المنتجات: </b> {{$shipment->ordered_products->count()}}
        </div>
        <div class="text-muted">
            <b>السعر الاجمالى: </b> {{$shipment->total_price}} EGP
        </div>
        <div class="text-muted">
            <b>الحالة:</b>
            @if ($shipment->status == 1)
            <span class="badge bg-info text-white">تحت الطلب</span>
            @elseif ($shipment->status == 2)
            <span class="badge badge-primary text-black">تم التأكيد</span>
            @elseif ($shipment->status == 3)
            <span class="badge badge-warning text-black">فى الشحن</span>
            @elseif ($shipment->status == 4)
            <span class="badge badge-success text-white">شحنت</span>
            @elseif ($shipment->status == 5)
            <span class="badge bg-dark text-white text-black">درافت</span>
            @else
                <span class="badge bg-dark text-white text-black">غير محدد</span>
            @endif
        </div>

        @if($shipment->ordered_products->count()>0)
        <div class="text-muted pb-2">
            <b>منتجات الشحنة  : </b>
        </div>
            <table class="table border-bottom ">
                <thead class="">
                  <tr class="text-center">
                    <th>&nbsp;</th>
                    <th>المنتج</th>
                    <th>السعر</th>
                    <th>الكمية</th>
                    <th>الاجمالى</th>

                  </tr>
                </thead>
                <tbody >
                    @foreach ($shipment->ordered_products as $item)

                  <tr class="text-center">

                    <td class="image-prod">  <a href="{{route('all_products.index',$item->original_product->id)}}" class="img-prod "><img style="width:100px" src="{{$item->original_product->image}}" alt="{{$item->product_name}}"></a></td>

                    <td class="product-name">
                        <h3>{{$item->product_name}}</h3>
                        <p>{{$item->original_product->description}}</p>
                    </td>

                    <td class="price">{{$item->product_price/$item->qty}} EGP </td>

                    <td class="quantity">
                        {{$item->qty}}
                  </td>

                    <td class="total">{{$item->product_price}} EGP</td>
                  </tr><!-- END TR-->
                  @endforeach
                </tbody>
              </table>
    </div>
    @if($shipment->status==1)
    <form method="POST" action="{{ route('shipments.'.'confirm',[$shipment->id]) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <button type="submit"
         onclick="return confirm('Are you sure you want to confirm this shipment there is no back for this process?')"
         data-toggle="modal" data-target="#Modal"
    class="btn bg-info text-white btn-block text-uppercase">أكد هذه الشحنة </button>
    </form>
    @elseif($shipment->status==2)
    <a  class="btn  disabled  btn-block text-uppercase">تأكيد</a>
    @endif
    @else
    <p> No Products Available </p>
    @endif
    </div>
    @endforeach
    <!--end of info -->
    </div>
</div>

    <!-- end of extra Info-->
    @endsection

