<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Clever - @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!--Google-->
        {{-- <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> --}}
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9lwRaQPyQ4ZDOYN7cfhsl8HaCvyjpaL8&callback=myMap"></script>
        <!-- CSS and JS for our code -->
        {{-- <link   href={{asset("css/jquery-gmaps-latlon-picker.css")}} rel="stylesheet"/> --}}
        <script src={{asset('js/jquery-gmaps-latlon-picker.js')}}></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom2.css') }}" rel="stylesheet">
	<link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate2.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/animsition/css/animsition.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/select2.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/daterangepicker/daterangepicker.css')}}">
	<!--===============================================================================================-->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/quill/2.0.0-dev.4/quill.snow.min.css" />


</head>

<body>
<!--Info-->
<script src="{{asset('js/print.js')}}"></script>
<h3>Clever.co</h3>
@can('view-super',Auth::user())
  <div class="card mb-3  shadow-sm "  >
    <div class="card-header">
        Order Details : #{{$order->hash_code}}
    </div>
    <div class="card-body text-muted">
        <div>
        <b>كود :</b> #{{$order->hash_code}}
        </div>
        <div>

        <b>المستخدم الاسم :</b>{{$order->client->name}} |
        <b>Shipment Address: </b> {{$order->address_log}}
        </div>
        <div>
            <b>Shipment Address Note: </b> {{null !==($order->address->shipping_note)?$order->address->shipping_note:'empty'}}
         </div>
        <div>
        <b>Delivery Option: </b> {{null !==($order->delivery_option==1)?'(COD) Cash On Delivery ':'empty'}}
        </div>
        <div>
            <b>Delivery Fees: </b> {{$order->delivery_fees}}
        </div>

        <div>
        <b>Order Note: </b> {{null !==($order->note)?$order->note:'empty'}}
        </div>
        <div>
         <b>الشحنات Number: </b> {{$order->shipments->count()}}
        </div>
        <div>
            <b>الشحنات Codes: </b>
                @foreach($order->shipments as $shipment)
                @if($loop->last)
                {{$shipment->hash_code}}.
                @else
                {{$shipment->hash_code}},
                @endif
                @endforeach
         </div>
         <div>
            <b>Total السعر: </b> {{$order->total_price}} EGP
        </div>
        <div>
            <b>Total & Delivery Fees: </b> {{$order->delivery_fees+$order->total_price}} EGP
        </div>

    </div>

@endcan
@foreach($order->shipments as $shipment)
@can('view',$shipment->store)
<div class="card mb-3  shadow-sm "  >
    <div class="card-header">
        Shipment {{$loop->index +1}} of {{$order->shipments->count()}}  : {{isset($shipment->store)?$shipment->store->name:'(Deleted) '.$shipment->store_log}}
    </div>
    <div class="card-body">
        <div class="text-muted">
        <b>كود :</b> #{{$shipment->hash_code}} |
        </div>
    <div class="text-muted">
        <b>المحل الاسم :</b>{{isset($shipment->store)?$shipment->store->name:'(Deleted) '.$shipment->store_log}} |
        <b>Shipment Address: </b> {{$order->address_log}}
    </div>
    <div class="text-muted">
        <b>Items Number: </b> {{$shipment->ordered_products->count()}}
    </div>
    <div class="text-muted">
        <b>Total السعر: </b> {{$shipment->total_price}} EGP
    </div>

    @if($shipment->ordered_products->count()>0)
    <div class="text-muted pb-2">
    <b>Products :</b>
    </div>
        <table class="table border-bottom ">
            <thead class="">
              <tr class="text-center">
                <th>&nbsp;</th>
                <th>المنتج</th>
                <th>السعر</th>
                <th>Quantity</th>
                <th>Total</th>

              </tr>
            </thead>
            <tbody >
                @foreach ($shipment->ordered_products as $item)

              <tr class="text-center">

                <td class="image-prod">  <a href="{{route('all_products.index',$item->original_product->id)}}" class="img-prod "><img style="width:100px" src="{{$item->original_product->image}}" alt="{{$item->product_name}}"></a></td>

                <td class="product-name">
                    <h3>{{$item->product_name}}</h3>
                    <p>{{$item->original_product->description}}</p>
                </td>

                <td class="price">{{$item->product_price/$item->qty}} EGP </td>

                <td class="quantity">
                    {{$item->qty}}
              </td>

                <td class="total">{{$item->product_price}} EGP</td>
              </tr><!-- END TR-->
              @endforeach
            </tbody>
          </table>
</div>

@else
<p> No Products Available </p>
@endif
</div>
@endcan
@endforeach
<!--end of info -->
</div>
<!-- end of extra Info-->
</body>
</html>
