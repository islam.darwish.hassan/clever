@extends('layouts.resource.edit')


@section('form')
<form method="POST" action="{{ route($name. '.update', $banner->id) }}" enctype="multipart/form-data">

                   @csrf


        @method('PUT')

        <div class="d-flex bd-highlight">
            <div class="form1 p-2 flex-fill bd-highlight">
                <div class="row">
                    <div class="col form-group">
                        <label>Title <b style="color:red"> * </b></label>
                        <input type="text" name="title" class="form-control" placeholder="Title" required
                            value="{{$banner->title}}">
                    </div>
                    <div class="col form-group">
                        <label class="form-label">Upload New Content الصورة<b style="color:red"> * </b></label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="validatedCustomFile">اختر الملف ...</label>
                            <input type="file" name="content" id="custom-file-input-image" class="custom-file-input">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col form-group">
                        <label>Order <b style="color:red"> * </b></label>
                        <input type="number" name="order" class="form-control" placeholder="Order to display"
                            value="{{$banner->order}}">
                    </div>
                    <div class="col form-group">
                        <label>Url </label>
                        <input type="url" name="url" class="form-control" placeholder="Url"
                            value="{{$banner->url}}">
                    </div>

                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-block btn-success bg-brandgreen ">تحديث</button>

    </form>
@endsection
