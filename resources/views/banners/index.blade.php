@extends('layouts.resource.index')
@section('create-btn')
<a href="{{ route($name . '.create') }}" class=" clr-blue p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block  mb-2">تصفية</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-header')

    <th scope="col" style="width: 5%" >#</th>
    <th scope="col">Content</th>
    <th scope="col">Title</th>
    <th scope="col">Order</th>
    <th scope="col">Url</th>
    <th  scope="col" style="width: 10%">العمليات</th>

@endsection



@section('table-body')

    @foreach ($banners as $banner)
        <tr>
            <td>{{ $banner->id }}</td>
            <td><img src="{{ $banner->content }}" alt="image" class="pp-square-big"></td>
            <td>{{ $banner->title }}</td>
            <td>{{ $banner->order }}</td>
            <td>{{ $banner->url }}</td>
            <td class="d-flex justify-content-center">
                <a href="{{ route('home') }}"
                class="btn clr-black  "><i class="fas fa-eye"></i></a>
               <a href="{{ route($name.'.edit', $banner->id) }}"
                 class="btn clr-black "><i class="fas fa-edit"></i></a>
                   <form method="POST" action="{{ route($name.'.destroy', $banner->id) }}">
                      @csrf
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this banner?')"
                        data-toggle="modal" data-target="#exampleModal"
                   class="btn  clr-blue  bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form>
               </td>
        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer">النتائج :  {{$data->total()}}</p>
    @endsection

@endsection
