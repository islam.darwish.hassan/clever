<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hash_code')->unique();
            $table->unsignedBigInteger('client_id')->index()->nullable();
            $table->unsignedBigInteger('address_id')->index()->nullable();
            $table->boolean('delivery_option')->default(1);
            $table->text('note')->nullable();
            $table->integer('status')->default(0);
            $table->integer('total_price')->default(0);
            $table->text('address_log')->nullable();
            $table->text('client_log')->nullable();
            $table->integer('delivery_fees')->nullable();
            $table->integer('paid')->default(0);
            $table->string('transaction_kashier')->nullable();
            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table){
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('set null');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('set null') ;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
