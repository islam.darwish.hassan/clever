<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('ar_name')->nullable();
            $table->unsignedBigInteger('sub_category_id')->index()->nullable();
            $table->string('product_code')->nullable();
            $table->string('product_code_extend')->nullable();
            $table->string('size')->nullable();
            $table->string('color')->nullable();
            $table->string('branch')->nullable();
            $table->string('fabric')->nullable();
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->text('ar_description')->nullable();
            $table->integer('status')->default(0);
            $table->integer('price')->default(0);
            $table->integer('stock')->default(0);
            $table->integer('rate')->default(0);
            $table->integer('handling_days')->default(0);
            $table->integer('visited')->default(0);
            $table->integer('ordered')->default(0);
            $table->timestamps();
        });

        Schema::table('products', function (Blueprint $table){
            $table->foreign('sub_category_id')->references('id')->on('sub_categories')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
