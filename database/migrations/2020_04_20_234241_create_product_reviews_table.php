<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('rate')->default(0);
            $table->string('review')->nullable();
            $table->string('good')->nullable();
            $table->string('bad')->nullable();
            $table->boolean('to_recommend')->default(1);
            $table->unsignedBigInteger('product_id')->index();
            $table->unsignedBigInteger('client_id')->index();
            $table->timestamps();
        });

        Schema::table('product_reviews', function (Blueprint $table){
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_reviews');
    }
}
