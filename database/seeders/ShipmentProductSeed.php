<?php

namespace Database\Seeders;

use App\Models\ShipmentProduct;
use Illuminate\Database\Seeder;

class ShipmentProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ShipmentProduct::factory(2000)->create();

    }
}
