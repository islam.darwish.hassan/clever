<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\City;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user=User::inRandomOrder()->first();
        $city=City::inRandomOrder()->first();

        return [
            //
            'user_id' => $user->id,
            'city_id'=>$city->id,
            'address'=>$this->faker->address(),
            'street'=>$this->faker->streetName(),
            'building'=>rand(1,100),
            'floor'=>rand(1,10),
            'landline'=>$this->faker->e164PhoneNumber()
        ];
    }
}
