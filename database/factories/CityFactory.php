<?php

namespace Database\Factories;

use App\Models\City;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = City::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'en_name'=>$this->faker->firstName,
            'ar_name'=>'مدينة'.rand(1,100),
            'delivery_fees' =>rand(20,200)
        ];
    }
}
