<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Shipment;
use App\Models\ShipmentProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShipmentProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ShipmentProduct::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $shipment = Shipment::inRandomOrder()->first();
        $product=Product::inRandomOrder()->first();
        return [
            //
            'shipment_id'=>$shipment->id,
            'product_price'=>$product->price,
            'qty'=>rand(1,3),
            'product_id'=>$product->id,
            'product_name'=>$product->name,
            'product_log'=>$product->name .'-'.$product->price
        ];
    }
}
