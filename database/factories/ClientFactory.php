<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user=User::factory()->create();
        return [
            //
            'user_id'=> $user->id,
            'name'  =>  $this->faker->userName,
            'birth_date'=> $this->faker->dateTime(),
            'gender'=>  'm' ,
            'phone'=>$this->faker->phoneNumber

        ];
    }
}
