<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Shipment;
use App\Models\Store;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShipmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Shipment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $order=Order::inRandomOrder()->first();
        $store=Store::inRandomOrder()->first();

        return [
            //
            'hash_code'=>$this->faker->ean13,
            'order_id'=>$order->id,
            'store_id'=>$store->id,
            'total_price'=>rand(100,1000)
        ];
    }
}
