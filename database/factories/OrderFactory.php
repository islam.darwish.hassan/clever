<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Client;
use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $client=Client::inRandomOrder()->first();
        $address=Address::where('user_id',$client->user->id)->inRandomOrder()->first();
        return [
            //
            'hash_code'=>$this->faker->ean13,
            'client_id'=>$client->id,
            'address_id'=>$address->id,
            'delivery option'=>1,
            'note'=>$this->faker->sentence(),
            'status'=>rand(1,5),
            'total_price'=>rand(100,10000),
            'delivery_fees'=>rand(100,200)

        ];
    }
}
