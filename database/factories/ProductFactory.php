<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Store;
use App\Models\SubCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sub_category=SubCategory::inRandomOrder()->first();
        $store=Store::inRandomOrder()->first();

        return [
            //
            'name'=>$this->faker->firstName(),
            'ar_name'=>"بالعربى".rand(1,1000),
            'sub_category_id'=>$sub_category->id,
            'image'=>$this->faker->imageUrl(560 ,845),
            'description'=>$this->faker->sentence(),
            'ar_description'=>$this->faker->sentence(),
             'status'=>rand(1,5),
             'price'=>rand(50,2000),
             'stock'=>rand(0,1000),
             'rate'=>rand(0,10),
             'handling_days'=>rand(0,7),
             'store_id'=>$store->id
             
        ];
    }
}
