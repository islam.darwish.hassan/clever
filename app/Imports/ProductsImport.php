<?php

namespace App\Imports;

use App\Models\Category;
use App\Models\Department;
use App\Models\Group;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductOffer;
use App\Models\SubCategory;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Str;

class ProductsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        // dd($row);

        //check if department exists
        $department = Department::where('name', $row[0])->firstOrNew([
            "name" => $row[0]
        ]);
        $department->save();
        //check if category exists
        $category = Category::where('name', $row[1])->firstOrNew([
            "name" => $row[1],
            "department_id" => $department->id
        ]);
        $category->save();

        // //check if subcategory exists
        $sub_category = SubCategory::where('name', $row[2])->firstOrNew([
            "name" => $row[2],
            "category_id" => $category->id
        ]);
        $sub_category->save();
        $sizes = explode("/", $row[9]);

foreach ($sizes as $size) {
    $product_code = explode("-", $row[3]);

        $product = new Product([
            //
            'sub_category_id' => $sub_category->id,
            'product_code' =>$product_code[0] ,
            'name' => $row[4] ?? '',
            'description' => $row[5] ?? '',
            'product_code_extend'=>$product_code[1]??'',
            'stock' => 5,
            'image' => null,
            'price' => $row[8] ?? 0,
            'size' => $size,
            'color' => $row[10],
            'status' => Product::STATUS_ONLINE,
            'branch' => $row[13] ?? '',
            'fabric' => $row[11] ?? '',
        ]);
        $product->save();
        if($row[12]!="" && $row[12]!=$row[8]){
            $offer =new ProductOffer();
            $offer->offer =$row[12];
            $offer->product_id = $product->id;
            $offer->save();
        }
        if ($row[6] != "") {
            $images = explode(";", $row[6]);
            foreach ($images as $image) {
                if (strlen($image) > 0 && strlen(trim($image)) == 0){
            }elseif($image){
                $product_image = new ProductImage();
                $product_image->product_id = $product->id;
                $product_image->image = $image ? strtolower($image) . ".jpg" : null;
                $product_image->small_image = $image ? strtolower($image) . ".jpg" : null;

                $product_image->save();

            }
        }

        }



        $group = Group::where('product_code', $product->product_code)->first();

        if (!$group) {

            $group = new Group();
            $group->product_code = $product->product_code;
            $group->name = $product->name;
            $group->image = null;
            $group->sub_category_id = $product->sub_category->id;
        }
        $group->save();
        $group->products()->attach($product);

    }
    return $product;

}
    public function startRow(): int
    {
        return 2;
    }
}
