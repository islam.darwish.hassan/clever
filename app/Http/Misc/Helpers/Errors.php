<?php

namespace App\Http\Misc\Helpers;

class Errors
{
    // auth errors
    const COMPLETED_PROFILE_BEFORE = 'you have completed your profile before.';
    const COMPLETED_PROFILE_MUST = 'you must complete your profile.';
	const NOT_FOUND_USER  = 'There is no account associated with this email.';
    const WRONG_PASSWORD  = 'Invalid login, wrong email or password.';
    const NOT_VERIFIED_USER = 'Non Verified User.';
    const FOLLOW_REQUEST_NOT_FOUND = 'follow request not found';
    const ALREADY_FOLLOWED = 'already followed';
    const FOLLOW_REQUEST_FOUND_BEFORE = 'already sent a request.';
    const CANNOT_FOLLOW_YOURSELF = 'cannot follow/unfollow youself.';
    const NO_FOLLOW = 'you cannot unfollow a non followed user.';


	// general erros
	const TESTING  = 'Invalid Parameter.';
	const UNAUTHENTICATED  = 'Unauthenticated.';
	const UNAUTHORIZED = 'Unauthorized.';
	const GENERAL = "General Error.";
}
