<?php

namespace App\Http\Misc\Helpers;

class Success
{
	// user_success_messages
	const LOGGED_OUT = 'Logged out succesfully.';
	const FOLLOW_REQUEST_REFUSED = 'follow request refused successfully.';
    const FOLLOW_REQUEST_ACCEPTED = 'follow request accepted successfully.';
    const FOLLOW_REQUEST_SENT = 'follow request sent.';
    const UNFOLLOW = 'unfollowed successfully.';
    const OFFER_PINNED = 'offer pinned successfully.';
    const OFFER_UNPINNED = 'offer unpinned successfully.';
    const REVIEW_ADDED = 'review added successfully.';
    const STORE_RATED = 'store rated successfully.';
    const ORDER_CREATED = 'order created successfully.';
}
