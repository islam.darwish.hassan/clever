<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'first_name'     => 'max:50|min:3',
            'last_name'      => 'max:50|min:3',
            'phone'          => 'required |max:12|min:7',
            'address'        => 'max:190|min:7',
            'gender'         => 'in:m,f',
            'age'            => 'integer|between:1,150',
            'avatar'         => 'image',
            'status'         => 'between:1,3',

        ];
    }
}
