<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'     => 'min:30|max:500',
            'image'                   => 'image|between:1,200',
            'name'          => 'min:5|max:150',
            'price'          => 'integer',
            'in_stock'          => 'integer',

            //
        ];
    }
}
