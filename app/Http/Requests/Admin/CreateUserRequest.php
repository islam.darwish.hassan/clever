<?php

namespace App\Http\Requests\Admin;

use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'         => 'required|unique:users|max:190|email',
            'password'      => 'required|max:190|min:8|alpha_num',
            'role'          => 'required|integer|between:1,4',
           

        ];
    }
}
