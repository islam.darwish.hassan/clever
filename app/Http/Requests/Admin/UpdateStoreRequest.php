<?php

namespace App\Http\Requests\Admin;

use App\Models\SubCategory;
use Illuminate\Foundation\Http\FormRequest;

class UpdateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
                'name'                 => 'max:50|min:3',
                'bio_store'             => 'max:190|min:3',
                'phone_store'          => 'max:12|min:7',
                'avatar_store'         => 'image',
                'lat'                  => 'between:0.01,100.0',
                'long'                 => 'between:0.01,100.0',
                'status_store'         => 'nullable|between:1,3',
            ];
        }
    }
