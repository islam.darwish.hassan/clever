<?php

namespace App\Http\Requests\Website;

use App\Models\Product;
use App\Models\Wishlist;
use Illuminate\Foundation\Http\FormRequest;

class AddProductWishlistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $wishlists = Wishlist::all()->implode('id', ',');
        $products = Product::all()->implode('id', ',');

        return [
            //
            'wishlist'         => 'in:' . $wishlists,
            'product'         => 'in:' . $products,

        ];
    }
}
