<?php

namespace App\Http\Requests\Website;

use App\Models\City;
use Illuminate\Foundation\Http\FormRequest;

class AddNewAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $cities = City::all()->implode('id', ',');
        return [
            //
            'city_id'         => 'in:' . $cities,
            'building'         =>'nullable|min:1|max:100',
            'floor'             =>'nullable|min:1|max:100',
            'street'         =>'nullable|min:1|max:100',
            'landmark'         =>'nullable|min:1|max:250',
            'landline'         =>'nullable|min:4|max:16',
            'shipping_note'         =>'nullable|min:1|max:250',
            'apartment'         =>'nullable|min:1|max:100',
            'address'=>'required|min:5|max:250'

        ];
    }
}
