<?php

namespace App\Http\Requests\Website;

use App\Models\City;
use Illuminate\Foundation\Http\FormRequest;

class AddNewClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $cities = City::all()->implode('id', ',');

        return [
            //
            'city_id'         => 'in:' . $cities,
            'email'         => 'required|unique:users|max:190|email',
            'password'      => 'required|confirmed|max:190|min:8|alpha_num',
            'name'        => 'required|min:2|max:70',
            'address'           => 'min:2|max:250',
            'gender'            => 'in:m,f',
            'phone'             => 'required',

        ];
    }
}
