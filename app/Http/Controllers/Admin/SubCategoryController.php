<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Misc\Helpers\Filters;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Category $category)
    {

        $this->authorize('view-super', Auth::user());

        $subs = SubCategory::where('category_id',$category->id);
        $subs = Filters::searchBy($subs, ['name' => $request->query('name')]);
        $subs = $subs->withCount('products')->sortable([])->paginate(10);
        $name = 'subs';
        $display_name= 'subs of '.$category->name;
        $data = $subs;

        return view('sub_categories.index', compact('category', 'subs', 'name', 'data','display_name'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        $this->authorize('view-super', Auth::user());

        $name = 'subs';
        return view('sub_categories.create', compact('category', 'name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $this->authorize('view-super', Auth::user());

        $sub = new SubCategory();
        $sub->name  = $request->input('name');
        if(isset($request->image)){
            $img             = $request->file('image');
            Storage::disk('categories_images')->put($img->getClientOriginalName(), File::get($img));
            $sub->image = $img->getClientOriginalName();
        }else{
            $sub->image ='no-image';

        }
        $sub->category_id = $category->id;

        $sub->save();

        return redirect()->route('subs.index', $category->id)->with('message', 'Subcategory created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $this->authorize('view-super', Auth::user());

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category, SubCategory $sub)
    {
        $this->authorize('view-super', Auth::user());

        $name = 'subs';
        $display_name= 'subs of '.$category->name;
        return view('sub_categories.edit', compact('category','sub' ,'name','display_name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category, SubCategory $sub)
    {
        $this->authorize('view-super', Auth::user());

        if ($request->hasFile('image')) {
            //upload it
            Storage::disk('categories_images')->delete($sub->image);
            $image =FileHandler::store_img($request->image, 'categories_images');
            //delete old one
            $sub->image = $image;
        }
        //update attribites
        if (isset($request->name))
            $sub->name = $request->name;
        $sub->save();

        return redirect()->route('subs.index', $category->id)->with('message', 'Subcategory info updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category, SubCategory $sub)
    {
        $this->authorize('view-super', Auth::user());

        $sub->delete();
        return redirect()->back()->with('message', 'Subcategory deleted successfully');
    }
}
