<?php

namespace App\Http\Controllers\Admin;

use App\Offer;
use App\Models\Store;
use Illuminate\Http\Request;
use App\Http\Misc\Helpers\Filters;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateOfferRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class StoreOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Store $store)
    {
        $offers  = Offer::where('store_id', $store->id);
        $offers = Filters::searchBy($offers, ['caption' => $request->query('name')]);
        $offers = $offers->withCount('redeems')->sortable(['redeems_count'=>'desc'])->paginate(8);
        $name = 'offers';
        $data = $offers;

        return view('stores.offers.index', compact('store', 'offers', 'name', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $storeID = Store::where('id', $store->id)->get();
        $name    = 'offers';

        return view('stores.offers.create', compact('store', 'storeID', 'name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOfferRequest $request, Store $store)
    {


        $offer = new Offer();
        $offer->store_id = $store->id;
        $offer->image    = FileHandler::store_img($request->image, 'stores_images');;
        $offer->caption    = $request->input('caption');
        $offer->redeem_code     = str_random(8).rand(1,100);
        $offer->body     = $request->input('body');

        $offer->save();

        return redirect()->route('offers.index', $store->id)->with('message', 'Offer created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store, Offer $offer)
    {


        $storeID = Store::where('id', $store->id)->get();
        $name = 'offers';

        return view('stores.offers.edit', compact('offer', 'storeID', 'name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, Offer $offer)
    {


        if ($request->hasFile('image')) {
            //upload it
            Storage::disk('categories_images')->delete($offer->image);
            $image =FileHandler::store_img($request->image, 'stores_images');
            //delete old one
            $offer->image = $image;
        }
        //update attribites
        if (isset($request->caption))
            $offer->caption = $request->caption;

        if (isset($request->body))
            $offer->body = $request->body;

        $offer->save();

        return redirect()->route('offers.index', $store->id)->with('message', 'Offer info updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, Offer $offer)
    {


        $offer->delete();
        return redirect()->route('offers.index', $store->id)->with('message', 'Offer deleted successfully');
    }
}
