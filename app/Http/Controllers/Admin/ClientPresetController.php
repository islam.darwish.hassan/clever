<?php

namespace App\Http\Controllers\Admin;

use App\Models\ClientsPreset;
use App\Models\ClientsPresetParameter;
use App\Models\ClientsPresetsParameter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Filters;
use App\Http\Requests\Admin\CreatePresetRequest;
use App\Http\Requests\Admin\UpdatePresetRequest;
use App\Preset;
use App\PresetsParameter;
use Illuminate\Support\Facades\Auth;

class ClientPresetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,ClientsPreset $preset )
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $presets=  $preset;
        $presets = Filters::searchBy($presets, ['name' => $request->query('name')]);
        $presets=$presets->withCount('params')->sortable(['created_at'=>'desc'])->paginate(8);
        $data=$presets;
        $name="client_presets";
        return view('client_presets.index', compact('data','name','presets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $name="client_presets";
        return view('client_presets.create', compact('name'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePresetRequest $request)
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $preset = new ClientsPreset();
        $preset->name     = $request->input('name');
        $preset->desc     = $request->input('desc');

        $preset->save();

        return redirect()->route('client_presets.index')->with('message', 'Client Preset created successfully');
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id  )
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=ClientsPreset::where('id',$id)->first();
         $params=  ClientsPresetsParameter::where('client_preset_id',$preset->id);
        $params = Filters::searchBy($params, ['name' => $request->query('name')]);
        $params=$params->paginate(8);
        $data=$params;
        $name="clients_params";
        return view('client_presets.show', compact('preset','data','name','params'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $this->authorize('view-super', Auth::user()); 
        $preset=ClientsPreset::where('id',$id)->first();

        $name = 'client_presets';

        return view('client_presets.edit', compact('preset', 'name'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePresetRequest $request, $id )
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=ClientsPreset::where('id',$id)->first();

        if (isset($request->name))
            $preset->name = $request->name;

        if (isset($request->desc))
            $preset->desc = $request->desc;

        $preset->save();

        return redirect()->route('client_presets.index')->with('message', 'Client Preset info updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=ClientsPreset::where('id',$id)->first();
        $preset->delete();
        return redirect()->route('client_presets.index')->with('message', 'Client Preset deleted successfully');

    }
}
