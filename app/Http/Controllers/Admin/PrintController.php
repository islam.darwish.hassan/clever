<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class PrintController extends Controller
{
    //
    public function prnpriview(Order $order)
    {
        return view('orders.print',compact('order'));
    }

}
