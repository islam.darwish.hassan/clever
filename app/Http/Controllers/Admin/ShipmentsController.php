<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Filters;
use App\Models\Shipment;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShipmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request ,Shipment $shipment)
    {
        //
        $shipments = $shipment;
         $shipments=$shipment->where('store_id',Store::where('user_id',Auth::id())->first()->id);
        $shipments = Filters::searchBy($shipments, ['hash_code' => $request->query('code')]);


        if ($request->query('status') && $request->query('status') != 'none') {
            $shipments = $shipments->where('status', $request->query('status'));
        }

        $shipments = $shipments->withCount('ordered_products')->sortable()->paginate(8);
        $name = 'shipments';
        $data = $shipments;

        return view('shipments.index',compact('shipments','name','data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function confirm(Request $request, Shipment $shipment)
    {
        //
        $shipment->status = Shipment::STATUS_CONFIRMED;
        $shipment->save();
        return redirect()->back()->with('message', 'Shipment confirmed successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
