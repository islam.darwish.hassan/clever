<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Config;
use App\Http\Misc\Helpers\Filters;
use App\Models\Order;
use App\Models\Shipment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Order $order)
    {
        //


        $orders = $order;
        $orders = Filters::searchBy($orders, ['hash_code' => $request->query('code')]);

        if ($request->query('client_email') && $request->query('client_email') != 'none') {
            $users=User::where('role',User::CLIENT);
            $users=Filters::searchBy($users, ['email' =>  $request->query('client_email')])->pluck('id');
            $clients=Client::whereIn('user_id',$users)->pluck('id');
           $orders = $orders->whereIn('client_id', $clients);
        }
        if ($request->query('client_phone') && $request->query('client_phone') != 'none') {
            $clients=new Client();
            $clients=Filters::searchBy($clients, ['phone' => $request->query('client_phone')])->pluck('id');
           $orders = $orders->whereIn('client_id', $clients);

        }

        if ($request->query('status') && $request->query('status') != 'none') {
            $orders = $orders->where('status', $request->query('status'));
        }
        //exculde draft
        $orders=$orders->where('status','!=',Order::STATUS_DRAFT);
        $orders = $orders->withCount('shipments')->sortable()->paginate(Config::PAGINATION_LIMIT);
        $name = 'orders';
        $data = $orders;
        return view('orders.index',compact('orders','name','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function confirm(Request $request, Order $order)
    {
        //
        $this->authorize('view-super', Auth::user());
        foreach($order->shipments as $shipment)
        {
            if($shipment->status==Shipment::STATUS_REQUESTED){
                return redirect()->back()->with('error', 'Please confirm all shipments with sellers first ');
            }
        }
        $order->status = Order::STATUS_PROCESSING;
        $order->save();
        return redirect()->back()->with('message', 'Order confirmed successfully');
    }
    public function shipped(Request $request, Order $order)
    {
        //
        $this->authorize('view-super', Auth::user());
        foreach($order->shipments as $shipment)
        {
            $shipment->status=Shipment::STATUS_SHIPPED;
            $shipment->save();
        }
        $order->status = Order::STATUS_SHIPPED;
        $order->save();
        return redirect()->back()->with('message', 'Order status changed to  shipped successfully');
    }
    public function in_shipping(Request $request, Order $order)
    {
        //
        $this->authorize('view-super', Auth::user());
        foreach($order->shipments as $shipment)
        {
            $shipment->status=Shipment::STATUS_SHIPPING;
            $shipment->save();

        }
        $order->status = Order::STATUS_SHIPPING;
        $order->save();
        return redirect()->back()->with('message', 'Order status changed to  in shipping successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
        return view('orders.show',compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
