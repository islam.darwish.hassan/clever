<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateSpecRequest;
use App\Models\Product;
use App\Models\ProductSpec;
use App\Models\Store;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class SpecsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store , Product $product)
    {

                $categories=Category::whereHas('subcategories')->get();
                $subs = SubCategory::all();

                return view('stores.products.specs.index', compact('store', 'product'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSpecRequest $request, Store $store , Product $product)
    {
        //
        $specs=$product->specs->implode('title', ',');
        $request->validate([
            'spec_title'  => 'not_in:'.$specs,
        ]);
        $spec=new ProductSpec();
        $spec->product_id   =$product->id;
        $spec->title        =$request->spec_title;
        $spec->value        =$request->spec_value;
        $spec->save();
        return redirect()->back()->with('message', 'Spec added successfully');

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Store $store , Product $product,ProductSpec $spec)
    {
        //
         $title_to_get_content= $request->spec->title;
        $request->validate([
            $title_to_get_content  => 'required|min:1|max:120',

        ]);
        $spec->value=$request->$title_to_get_content;
        $spec->save();
        return redirect()->back()->with('message', 'Spec Updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Store $store , Product $product,ProductSpec $spec)
    {
        //
         $spec->delete();
         return redirect()->back()->with('message', 'Spec removed successfully');


    }
}
