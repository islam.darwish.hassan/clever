<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Misc\Helpers\Filters;
use App\Http\Requests\Admin\AddProductImageRequest;
use App\Http\Requests\Admin\CreateProductRequest;
use App\Http\Requests\Admin\UpdateProductRequest;
use App\Models\Group;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductSpec;
use App\Models\Store;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $products  = new Product();
        $products = Filters::searchBy($products, ['name' => $request->query('name')]);
        $products = Filters::searchBy($products, ['product_code' => $request->query('product_code')]);

        if ($request->query('status') && $request->query('status') != 'none') {
            $products = $products->where('status', $request->query('status'));
        }
        if ($request->query('sub_category') && $request->query('sub_category') != 'none') {
            $products = $products->where('sub_category_id', $request->query('sub_category'));
        }

        $products = $products->with('sub_category')->sortable()->paginate(8);
        $name = 'products';
        $data = $products;
        $subCategories = SubCategory::whereHas('products')->get();

        return view('stores.products.index', compact('products', 'name', 'data', 'subCategories'));
    }
    public function all_index(Request $request)
    {
        //\

        $products  = new Product();
        $products = Filters::searchBy($products, ['name' => $request->query('name')]);
        $products = Filters::searchBy($products, ['product_code' => $request->query('product_code')]);

        if ($request->query('status') && $request->query('status') != 'none') {
            $products = $products->where('status', $request->query('status'));
        }
        if ($request->query('sub_category') && $request->query('sub_category') != 'none') {
            $products = $products->where('sub_category_id', $request->query('sub_category'));
        }

        $products = $products->with(['sub_category'])->sortable()->paginate(10);
        $name = 'products';
        $data = $products;
        $subCategories = SubCategory::whereHas('products')->get();
        return view('products.index', compact('products', 'name', 'data', 'subCategories'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::whereHas('subcategories')->get();
        $subs = SubCategory::all();
        $name    = 'products';

        return view('stores.products.create', compact('name', 'categories', 'subs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        //
        for ($i = 0; $i < (int)$request->questions; $i++) {
            $product = new product();
            $product->name     = $request->name;
            $product->product_code = $request->product_code;
            $product->sub_category_id     = $request->sub_category;
            $product->description    = $request->input('description');

            $price = "price-" . strval($i + 1);
            $color = "color-" . strval($i + 1);
            $in_stock = "in_stock-" . strval($i + 1);
            $size = "size-" . strval($i + 1);
            $fabric = "fabric-" . strval($i + 1);
            $image = "image-" . strval($i + 1);
            $product->price     = $request->input($price);
            $product->stock     = $request->input($in_stock);
            $product->color     = $request->input($color);
            $product->size     = $request->input($size);
            $product->fabric     = $request->input($fabric);
            if($request->file($image) == null ) return redirect()->back()->with(['error'=>'ادخل كل صور المنتجات']);
            $product->image = FileHandler::store_img($request->file($image), 'products_images');
            $product->status    = Product::STATUS_ONLINE;
            $product->save();
            $group = Group::where('product_code', $product->product_code)->first();
            if (!$group) {

                $group = new Group();
                $group->product_code = $product->product_code;
                $group->name = $product->name;
                $group->image = $product->image;
                $group->sub_category_id = $product->sub_category->id;
            }
            $group->save();
            $group->products()->syncWithoutDetaching($product->id);
        }

        return redirect()->route('all_products.index')->with('message', 'Product created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //

        $product_images = ProductImage::where('product_id', $product->id)->limit(5)->get();
        return view('stores.products.show', compact('product', 'product_images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Product $product)
    {
        //


        $name    = 'products';
        $categories = Category::whereHas('subcategories')->get();
        $subs = SubCategory::all();

        return view('stores.products.edit', compact('product', 'name', 'categories', 'subs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Store $store, Product $product)
    {
        //


        if ($product->status != Product::STATUS_INREVIEW || $product->status != Product::STATUS_ONLINE) {

            if (isset($request->name))
                $product->name = $request->name;
            if (isset($request->description))
                $product->description = $request->input('description');
            if (isset($request->price))
                $product->price = $request->price;
            if (isset($request->color))
                $product->color = $request->color;
            if (isset($request->size))
                $product->size = $request->size;
            if (isset($request->branch))
                $product->branch = $request->branch;

            if (isset($request->stock))
                $product->stock = $request->in_stock;
            if ($request->hasFile('image')) {
                //upload it
                Storage::disk('products_images')->delete($product->image);
                $avatar = FileHandler::store_img($request->image, 'products_images');
                //delete old one
                if (isset($request->image))
                    $product->image = $avatar;
            }
            $product->status = Product::STATUS_DRAFT;
            $product->save();
        } else {
            return redirect()->route("products.show", [$product->id])->with('error', 'You can edit product while in review or online');
        }

        return redirect()->route("products.show", [$product->id])->with('message', 'Product Updated successfully');
    }
    public function to_review(Request $request, Store $store, Product $product)
    {


        $product->status = Product::STATUS_INREVIEW;
        $product->save();

        return redirect()->route("products.show", [$product->id])->with('message', 'Product Submited Wait For Admin Approval');
    }
    public function to_offline(Request $request, Store $store, Product $product)
    {


        $product->status = Product::STATUS_OFFLINE;
        $product->save();

        return redirect()->route("products.show", [$product->id])->with('message', 'Product becames Offline ');
    }
    public function to_approve(Request $request, Store $store, Product $product)
    {

        $this->authorize('view-super', Auth::user());
        $product->status = Product::STATUS_ONLINE;
        $product->save();

        return redirect()->route("products.show", [$product->id])->with('message', 'Product Approved');
    }
    public function to_reject(Request $request, Store $store, Product $product)
    {

        $this->authorize('view-super', Auth::user());
        $product->status = Product::STATUS_REJECTED;
        $product->save();

        return redirect()->route("products.show", [$product->id])->with('message', 'Product Rejected  ');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Store $store, Product $product)
    {
        //


        if ($product->status == Product::STATUS_DRAFT || $product->status==Product::STATUS_OFFLINE) {
            $product->delete();
            return redirect()->back()->with('message', 'Product deleted successfully');
        } else {
            $product->status = Product::STATUS_OFFLINE;
            $product->save();
            return redirect()->back()->with('message', 'Product deactivted successfully');
        }
    }

    public function update_image(Request $request, Store $store, Product $product)
    {
        //
        if ($request->hasFile('image')) {
            //upload it
            Storage::disk('products_images')->delete($product->image);
            $avatar = FileHandler::store_img($request->image, 'products_images');
            //delete old one
            if (isset($request->image))
                $product->image = $avatar;
            $product->save();
        }
        return redirect()->back()->with('message', 'Product info updated');
    }
    public function add_image(AddProductImageRequest $request, Store $store, Product $product)
    {
        //
        if ($request->hasFile('added_image')) {
            //upload it
            $avatar = FileHandler::store_img($request->added_image, 'products_images');
            $product_image = new ProductImage();
            $product_image->image = $avatar;
            //for future
            $product_image->small_image = $avatar;
            $product_image->product_id = $product->id;
            $product_image->save();
        }
        return redirect()->back()->with('message', 'Product info updated');
    }

    public function remove_image(Request $request, Store $store, Product $product, ProductImage $product_image)
    {
        //


        $product_image->delete();

        return redirect()->back()->with('message', 'Product Image deleted ');
    }
}
