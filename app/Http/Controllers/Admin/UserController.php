<?php

namespace App\Http\Controllers\Admin;

use App\Address;
use App\Models\User;
use App\Models\Store;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Misc\Helpers\Filters;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Config;
use App\Models\Client;
use App\Http\Requests\Admin\CreateUserRequest;
use Illuminate\Support\Facades\File;
use App\Http\Misc\Helpers\FileHandler;
use App\Nation;
use App\Preset;
use App\PresetsParameter;
use App\Models\StoreAddress;
use App\Models\StoreParameter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user, Request $request)
    {
        $this->authorize('view-super', Auth::user());

        $users = $user;
        $users = Filters::searchBy($users, ['email' => $request->query('name')]);
        if ($request->query('role') && $request->query('role') != 'none') {
            $users = $users->where('role', $request->query('role'));
        }
        $users = $users->sortable(['id' => 'asc'])->paginate(Config::PAGINATION_LIMIT);
        $name = 'users';
        $data = $users;

        return view('users.index', compact('users', 'name', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('view-super', Auth::user());
        $name = 'users';
        return view('users.create', compact( 'name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {

       // return $request;
        $this->authorize('view-super', Auth::user());

     if($request->role==2){
        $this->validate_client($request);
        }
    else if($request->role==3){
        $this->validate_store($request);
        }
        $user = new User();
        $user->email    = $request->input('email');
        $user->password = bcrypt($request->password);
        $user->role     = $request->input('role');
        $user->save();
        if ($request->role == 2) {
            $this->create_client($request, $user->id);
            return redirect()->route('clients.index')->with('success_message', 'Client Created Successfully!');
        } elseif ($request->role == 3) {
            $this->create_store($request, $user->id);
            return redirect()->route('stores.index')->with('success_message', 'Store Created Successfully!');
        } else {
            return redirect()->route('users.index')->with('success_message', 'Client Created Successfully!');
        }
        if ($user->role == 4) {
        } else {
            return redirect()->route('users.index')->with('message', 'User created successfully');
        }
    }
    public function validate_client($request){
        $nations = Nation::all()->implode('id', ',');
        $request->validate([
            'name'           => 'required|max:150|min:3',
            'status'         => 'between:1,3',
            'nation'         => 'nullable|integer|in:'.$nations,
            'gender'         => 'nullable|in:m,f',
            'phone'          => 'nullable|max:12|min:7',
        ]);
    }

    public function validate_store($request){
        $request->validate([
            'name_store'           => 'required|max:150|min:3',
            'bio_store'            => 'nullable|max:190|min:3',
            'phone_store'          => 'nullable|max:12|min:7',
            'address_store'        => 'nullable|max:190|min:7',
            'avatar_store'         => 'required|image',
            'lat'                  => 'required|between:0.01,100.0',
            'long'                 => 'required|between:0.01,100.0',
            'status_store'         => 'nullable|between:1,3',
        ]);
    }
    public function create_store($request, $id)
    {
        //store user data
        $store = new Store();
        $store->user_id         = $id;
        $store->name            = $request->name_store;
        $store->avatar          =  FileHandler::store_img($request->avatar_store, 'stores_images');
        $store->latitude        = $request->lat;
        $store->longitude       = $request->long;
        $store->rate            = 0;

        if (isset($request->bio_store)){
            $store->bio             = $request->bio_store;
        }

        if (isset($request->phone_store)){
        $store->phone           = $request->phone_store;
        }

        if (isset($request->status_store)){
        $store->status          =  $request->status_store;
        }else{
            $store->status= Store::STATUS_VERIFIED;
        }

        $store->save();
        if (isset($request->address_store)){
        $address = new StoreAddress();
        $address->store_id   =$store->id;
        $address->address   =$request->address_store;
        $address->save();
        }
        //set paramters

        return redirect()->route('stores.index')->with('message', 'Store created successfully');
    }
    public function create_client($request, $id)
    {
        $client= new Client();
        $client->user_id      = $id;
        $client->name   = $request->name;
        if (isset($request->gender)){
        $client->gender       = $request->gender;
        }
        if (isset($request->nation)){
            $client->nation_id   = $request->nation;

        }else{
        $client->nation_id   = 66;

        }
        if (isset($request->phone)){
        $client->phone        = $request->phone;
        }
        $client->save();


        return redirect()->route('clients.index')->with('message', 'Client created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $this->authorize('view-super', Auth::user());

        $name     = 'users';

        return view('users.show', compact('user', 'name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('view-super', Auth::user());

        $name = 'users';

        return view('users.edit', compact('user', 'name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('view-super', Auth::user());

        if (isset($request->role))
            $user->role = $request->role;

        $user->save();

        return redirect()->route('users.index')->with('message', 'User info updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('view-super', Auth::user());

        if ($user->role == 3) {
            $user->delete();
            return redirect()->back()->with('message', 'Store deleted successfully');
        } elseif ($user->role == 2) {
            $user->delete();
            return redirect()->back()->with('message', 'Client deleted successfully');
        }else{
            $user->delete();
            return redirect()->back()->with('message', 'User deleted successfully');

        }
    }

    public function fetch_category_data(Request $request)
    {
        //this function to dynamclly load data
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $temp = '';
        $table = "";
        if ($select == "category") {
            $temp = 'category_id';
            $table = "sub_categories";
        }

        if ($select == "category_data") {
            $temp = 'category_id';
            $table = "sub_categories";
        }

        $data = DB::table($table)
            ->where($temp, $value)
            ->get();
        $output = '<option value="">Choose...</option>';
        foreach ($data as $row) {
            $output .= '<option value="' . $row->id . '" ';

            if (session()->has(Str::singular($table)) && session()->get(Str::singular($table))->id ==  $row->id)
                $output .= 'selected';

            $output .= ' >' . $row->name . '</option>';
        }
        echo $output;
        echo $value;
        echo $select;
    }
}
