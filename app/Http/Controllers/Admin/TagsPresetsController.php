<?php

namespace App\Http\Controllers\Admin;

use App\TagsPreset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Filters;
use App\Http\Requests\Admin\CreatePresetRequest;
use App\Http\Requests\Admin\UpdatePresetRequest;
use App\TagsPresetsParameter;
use Illuminate\Support\Facades\Auth;

class TagsPresetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,TagsPreset $preset )
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $presets=  $preset;
        $presets = Filters::searchBy($presets, ['name' => $request->query('name')]);
        $presets=$presets->withCount('params')->sortable(['created_at'=>'desc'])->paginate(8);
        $data=$presets;
        $name="tags_presets";
        return view('tags_presets.index', compact('data','name','presets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $name="tags_presets";
        return view('tags_presets.create', compact('name'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePresetRequest $request)
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $preset = new TagsPreset();
        $preset->name     = $request->input('name');
        $preset->desc     = $request->input('desc');

        $preset->save();

        return redirect()->route('tags_presets.index')->with('message', 'Tag Preset created successfully');
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id  )
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=TagsPreset::where('id',$id)->first();
         $params=  TagsPresetsParameter::where('tags_preset_id',$preset->id);
        $params = Filters::searchBy($params, ['name' => $request->query('name')]);
        $params=$params->paginate(8);
        $data=$params;
        $name="tags_params";
        return view('tags_presets.show', compact('preset','data','name','params'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $this->authorize('view-super', Auth::user()); 
        $preset=TagsPreset::where('id',$id)->first();

        $name = 'tags_presets';

        return view('tags_presets.edit', compact('preset', 'name'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePresetRequest $request, $id )
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=TagsPreset::where('id',$id)->first();

        if (isset($request->name))
            $preset->name = $request->name;

        if (isset($request->desc))
            $preset->desc = $request->desc;

        $preset->save();

        return redirect()->route('tags_presets.index')->with('message', 'Tags Preset info updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=TagsPreset::where('id',$id)->first();
        $preset->delete();
        return redirect()->route('tags_presets.index')->with('message', 'Tags Preset deleted successfully');

    }
}
