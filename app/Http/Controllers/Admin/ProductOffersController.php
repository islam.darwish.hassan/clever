<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddNewOfferRequest;
use App\Models\Product;
use App\Models\ProductOffer;
use App\Models\Store;
use Illuminate\Http\Request;

class ProductOffersController extends Controller
{
    //.
    public function index($id)
    {
    }
    public function create(Store $store,Product $product)
    {

        return view('stores.products.offer.create', compact('product'));
    }

    /******* */
    /*******Store */
    public function store( AddNewOfferRequest $request, Store $store ,Product $product )
    {
        $offer = ProductOffer::where('product_id',$product->id)->firstOrNew(
            ["offer"        => $request->offer],
            ["product_id" => $product->id]
        );
        $offer->save();

        return redirect()->route('products.show', [$store->id,$product->id])->with(["message"=>'Offer successfully created.']);
    }

    /******* */
    /*******Show */
    public function show(Product $product, ProductOffer $offer)
    {
        return view('stores.products.offer.show', compact('product', 'offer'));
    }

    /******* */
    /*******Edit */
    public function edit(Product $product, ProductOffer $offer)
    {
        return view('stores.products.offer.edit', compact('product', 'offer'));
    }

    /******* */
    /*******Update */
    public function update(Request $request, Product $product, ProductOffer $offer)
    {
        if (isset($request->offer))
            $offer->offer = $request->offer;

        if (isset($request->started_at))
            $offer->started_at = $request->started_at;

        if (isset($request->finished_at))
            $offer->finished_at = $request->finished_at;

        $offer->save();

        return redirect()->route('offer_show', [$product->id, $offer->id])->withStatus(__('Required choice successfully updated.'));
    }

    /******* */
    /*******Delete */
    public function destroy( Store $store,Product $product, ProductOffer $offer)
    {
        $offer->delete();

        return redirect()->route('products.show', [$store->id ,$product->id])->with(["message"=>'Offer successfully deleted.']);
    }
}
