<?php

namespace App\Http\Controllers\Admin;
use App\Models\Category;
use App\Models\Store;
use Illuminate\Http\Request;
use App\Http\Misc\Helpers\Filters;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateUserRequest;
use App\Models\SubCategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\UpdateStoreRequest;
use App\Models\Product;
use App\Models\ProductReview;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Lava;
use Khill\Lavacharts\Lavacharts;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store, Request $request)
    {
        $this->authorize('view-super', Auth::user());

        $stores = $store;
        $stores = Filters::searchBy($stores, ['name' => $request->query('name')]);

        if ($request->query('sub_category') && $request->query('sub_category') != 'none') {
            $products=Product::where('sub_category_id',$request->query('sub_category'));
            $store_ids=$products->select('store_id')->distinct()->get();
           $stores = $stores->whereIn('id', $store_ids);
        }
        if ($request->query('status') && $request->query('status') != 'none') {
            $stores = $stores->where('status', $request->query('status'));
        }

        $stores = $stores->withCount('products')->sortable()->paginate(8);

        $name = 'stores';
        $data = $stores;
        $subCategories = SubCategory::wherehas('products')->get();
        return view('stores.index', compact('stores', 'name', 'data', 'subCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Store $store)
    {

        $products=Product::where('store_id', $store->id)->orderBy('created_at','desc')->limit(6)->get();
        $name     = 'stores';

        return view('stores.show', compact('store','name','products'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {


        $name = 'stores';
        $categories = Category::wherehas('subcategories')->get();
        $subs = SubCategory::all();

        return view('stores.edit', compact('store', 'subs', 'name', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( UpdateStoreRequest  $request, Store $store)
    {


        if ($request->hasFile('avatar')) {
            //upload it
            Storage::disk('stores_images')->delete($store->image);
            $avatar =FileHandler::store_img($request->avatar, 'stores_images');
            //delete old one
            if (isset($request->avatar))
            $store->avatar = $avatar;
        }
        //update attribites
        if (isset($request->name))
            $store->name = $request->name;
        if (isset($request->bio))
         $store->bio = $request->bio;
        // if (isset($request->address))
        //     $store->address = $request->address;
        // if (isset($request->phone))
        //     $store->phone = $request->phone;
        // if (isset($request->website))
        //     $store->website = $request->website;
        if (isset($request->lat))
            $store->latitude = $request->lat;
        if (isset($request->long))
            $store->longitude = $request->long;

        if (isset($request->status))
            $store->status = $request->status;
        $store->save();

        return redirect()->route('stores.show',$store->id)->with('message', 'Store info updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_cover(Request $request ,Store $store)
    {
        //
        if ($request->hasFile('headerImage')) {
            //upload it
            Storage::disk('stores_images')->delete($store->header_image);
            $avatar =FileHandler::store_img($request->headerImage, 'stores_images');
            //delete old one
            if (isset($request->headerImage))
            $store->header_image = $avatar;
            $store->save();
        }
        return redirect()->back()->with('message', 'Store info updated');

    }
}
