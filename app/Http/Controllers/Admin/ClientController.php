<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Review;
use App\BookmarkCollection;
use App\Checkin;
use App\Consumption;
use App\Http\Misc\Helpers\Config;
use App\Http\Misc\Helpers\FileHandler;
use App\ListCollection;
use App\Models\Order;
use App\Models\ProductReview;
use App\Tip;
use App\Models\Wishlist;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Client $client,Request $request)
    {
        $this->authorize('view-super', Auth::user());

        $clients = $client;
        if($request->name!=""){
            $names = explode(" ", $request->name);
            $clients = Client::where(function($query) use ($names) {
                $query->whereIn('name', $names);
            });
        }
        if ($request->query('age')) {
            $clients = $clients->where('age', $request->query('age'));
        }
        if ($request->query('gender')) {
            $clients = $clients->where('gender', $request->query('gender'));
        }
        $clients = $clients->sortable()->paginate(Config::PAGINATION_LIMIT);

        $name = 'clients';
        $data = $clients;

        return view('clients.index', compact('clients', 'name', 'data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('view-super', Auth::user());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->authorize('view-super', Auth::user());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
        $this->authorize('view-super', Auth::user());

        $reviews=ProductReview::where('client_id', $client->id)->orderBy('id','desc')->limit(3)->get();

        $wishlists=Wishlist::where('user_id', $client->user->id)->orderBy('id','desc')->limit(3)->get();
        $orders=Order::where('client_id', $client->id)->orderBy('id','desc')->limit(3)->get();
        $name     = 'clients';

        return view('clients.show', compact('client',  'name','reviews','wishlists','orders'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
        $this->authorize('view-super', Auth::user());
        $name = 'clients';
        return view('clients.edit', compact( 'client','name'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
        $this->authorize('view-super', Auth::user());

        if ($request->hasFile('avatar')) {
            //upload it
            Storage::disk('users_images')->delete($client->image);
            $avatar =FileHandler::store_img($request->avatar, 'users_images');
            //delete old one
            if (isset($request->avatar))
            $client->avatar = $avatar;
        }
        //update attribites
        if (isset($request->name))
            $client->name = $request->name;
        if (isset($request->age))
        $client->age = $request->age;
        if (isset($request->bio))
         $client->bio = $request->bio;
        if (isset($request->address))
            $client->address = $request->address;
        if (isset($request->phone))
            $client->phone = $request->phone;
        if (isset($request->status))
            $client->status = $request->status;
            if (isset($request->gender))
            $client->gender = $request->gender;

        $client->save();

        return redirect()->route('clients.index')->with('message', 'Client info updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->authorize('view-super', Auth::user());

    }
}
