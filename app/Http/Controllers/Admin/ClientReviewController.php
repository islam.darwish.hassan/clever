<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Http\Misc\Helpers\Filters;
use App\Review;
use App\ReviewComment;
use App\ReviewRate;
use App\ReviewReact;
use Illuminate\Support\Facades\Auth;

class ClientReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , Client $client , Review $review)
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $reviews=Review::where('client_id', $client->id);
        $reviews = Filters::searchBy($reviews, ['good' => $request->query('good')]);
        $reviews = Filters::orSearchBy($reviews, ['bad' => $request->query('bad')]);
        $reviews=$reviews->sortable(['id'=>'desc'])->paginate(8);
         $name     = 'clientsreviews';
        $data=$reviews;
        return view('clients.reviews.index', compact('client','data',  'name','reviews'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client , $id)
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $review=Review::where('id',$id)->first();
        $params=ReviewRate::where('review_id',$review->id)->get();
        $comments=ReviewComment::where('review_id',$review->id)->orderBy('created_at','desc')->paginate(5);
        $helpfulls=ReviewReact::where('review_id',$review->id)->where('type',1)->orderBy('created_at','desc')->get();
        $not_helpfulls=ReviewReact::where('review_id',$review->id)->where('type',2)->orderBy('created_at','desc')->get();
        $funnys=ReviewReact::where('review_id',$review->id)->where('type',3)->orderBy('created_at','desc')->get();
        $reports=ReviewReact::where('review_id',$review->id)->where('type',4)->orderBy('created_at','desc')->get();

        return view('stores.reviews.show', compact('client','review','params','comments','helpfulls','not_helpfulls','funnys','reports'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }
}
