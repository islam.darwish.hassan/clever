<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Country;
use App\Notification;
use LaravelFCM\Facades\FCM;
use Illuminate\Http\Request;
use LaravelFCM\Message\Topics;
use App\Events\NotificationSent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Http\Requests\Admin\Notifications\CreateRequest;


class NotificationController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->authorize('view-super', Auth::user());
        $countries  = Country::orderBy('ar_name')->get();
        $users      = User::orderBy('id')->get();
        $name = 'notifications';
        return view('notification', ['countries' => $countries, 'users' => $users], compact('name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->authorize('view-super', Auth::user());
        if ($request->type == 'by_country') {
            $users = User::where('country_id', $request->country_id)->get();
            foreach ($users as $user) {
                Notification::create(['body' => $request->notification, 'user_id' => $user->id]);

                $notificationBuilder = new PayloadNotificationBuilder('my title');
                $notificationBuilder->setBody('Hello world')->setSound('default');
                $notification = $notificationBuilder->build();
                $topic = new Topics();
                $topic->topic('news');
                $topicResponse = FCM::sendToTopic($topic, null, $notification, null);
                $topicResponse->isSuccess();
                $topicResponse->shouldRetry();
                $topicResponse->error();

                // broadcast(new NotificationSent($request->notification, $user));
            }
        } else {
            $user = User::find($request->user_id);
            Notification::create(['data' => $request->notification, 'notifiable_id' => $user->id]);

            $notificationBuilder = new PayloadNotificationBuilder('my title');
            $notificationBuilder->setBody('Hello world')->setSound('default');
            $notification = $notificationBuilder->build();
            $topic = new Topics();
            $topic->topic('news');
            $topicResponse = FCM::sendToTopic($topic, null, $notification, null);
            $topicResponse->isSuccess();
            $topicResponse->shouldRetry();
            $topicResponse->error();

            // broadcast(new NotificationSent($request->notification, $user));
            if ($request->from_page == 'win_leadership')
                return redirect()->route('win.board', $request->contest_id)->with('message', 'Notification Sent Successfully.');
        }

        return $this->redirectToCreate('notifications', 'created');
    }
}
