<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Misc\Helpers\Filters;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Config;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateCategoryRequest;
use App\Models\Department;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category , Request $request)
    {
        $this->authorize('view-super', Auth::user());
        $categories = $category;
        $categories = Filters::searchBy($categories, ['name' => $request->query('name')]);
         $categories = $categories->with('department')->withCount('subcategories')->sortable()->paginate(Config::PAGINATION_LIMIT);
        $name = 'categories';
        $data = $categories;

        return view('categories.index', compact('categories', 'name', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('view-super', Auth::user());
        $name = 'categories';
        $departments=Department::all();
        return view('categories.create', compact('name','departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $this->authorize('view-super', Auth::user());

        $category = new Category();
        $category->name  = $request->input('name');
        if(isset($request->image)){
            $img             = $request->file('image');
            Storage::disk('categories_images')->put($img->getClientOriginalName(), File::get($img));
            $category->image = $img->getClientOriginalName();
        }else{
            $category->image ='no-image';

        }
        $category->department_id = $request->department_id;
        $category->save();

        return redirect()->route('categories.index')->with('message', 'Category created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Category $category)
    {
        $this->authorize('view-super', Auth::user());

        return redirect()->route('subs.index', $category->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->authorize('view-super', Auth::user());

        $name = 'categories';

        return view('categories.edit', compact('category', 'name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->authorize('view-super', Auth::user());

        if (isset($request->name))
            $category->name = $request->name;

            if ($request->hasFile('image')) {
                //upload it
                Storage::disk('categories_images')->delete($category->image);
                $avatar =FileHandler::store_img($request->image, 'categories_images');
                //delete old one
                if (isset($request->image))
                $category->image = $avatar;
            }
            $category->save();

        return redirect()->route('categories.index')->with('message', 'Category info updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->authorize('view-super', Auth::user());

        $category->delete();

        return redirect()->back()->with('message', 'Category deleted successfully');
    }
}
