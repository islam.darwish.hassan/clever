<?php

namespace App\Http\Controllers\Admin;

use App\BookmarkCollection;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Filters;
use Illuminate\Support\Facades\Auth;

class ClientCollectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , Client $client)
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $collections=BookmarkCollection::where('client_id', $client->id);
        $collections = Filters::searchBy($collections, ['name' => $request->query('name')]);
        $collections=$collections->withCount('bookmarks')->sortable(['id'=>'desc'])->paginate(8);
        $name     = 'collections';
        $data=$collections;
        return view('clients.collections.index', compact('client','data',  'name','collections'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }
}
