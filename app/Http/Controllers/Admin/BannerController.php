<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateNewBannerRequest;
use App\Http\Requests\Admin\UpdateNewBannerRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->authorize('view-super', Auth::user());
        $banners=Banner::paginate(5);
        $data=$banners;
        $name='banners';
        return view('banners.index',compact('name','data','banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('view-super', Auth::user());
        $name = 'banners';
        return view('banners.create', compact('name'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewBannerRequest $request)
    {
        //
        $this->authorize('view-super', Auth::user());

        $banner = new Banner();
        $banner->title            = $request->title;
        $banner->content          =  FileHandler::store_img($request->content, 'general_images');
        $banner->order            = $request->order;
        if(isset($request->url))$banner->url              = $request->url;
        $banner->save();

        return redirect()->route('banners.index')->with('message', 'Banner created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        //
        $this->authorize('view-super', Auth::user());

        $name = 'banners';

        return view('banners.edit', compact('banner', 'name'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNewBannerRequest $request, Banner $banner)
    {
        //
        $this->authorize('view-super', Auth::user());

        if (isset($request->title))
            $banner->title = $request->title;
            if (isset($request->order))
            $banner->order = $request->order;
            if (isset($request->url))
            $banner->url = $request->url;

            if ($request->hasFile('content')) {
                //upload it
                Storage::disk('categories_images')->delete($banner->content);
                $avatar =FileHandler::store_img($request->image, 'general_images');
                //delete old one
                if (isset($request->content))
                $banner->content = $avatar;
            }
            $banner->save();

        return redirect()->route('banners.index')->with('message', 'Banner info updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        //
        $this->authorize('view-super', Auth::user());

        $banner->delete();

        return redirect()->back()->with('message', 'Banner deleted successfully');

    }
}
