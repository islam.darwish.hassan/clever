<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Order;
use App\Models\Product;
use App\Models\Store;
use App\Models\ProductReview;
use App\Models\SubCategory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Lava;
use Khill\Lavacharts\Lavacharts;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view-super', Auth::user());
        $admins_count  = User::where('role', 1)->count();
        $clients_count = Client::count();

        // $stores_count  = Store::count();
        // $stores_ver_count=Store::where('status',Store::STATUS_VERIFIED)->count();
        // $stores_not_ver_count=Store::where('status',Store::STATUS_NOT_VERIFIED)->count();
        // $stores_sus_count=Store::where('status',Store::STATUS_SUSPENDED)->count();

        $reviews_count = ProductReview::count();
        $categories_count    = Category::count();
        $sub_categories_count    = SubCategory::count();

        $products_count=Product::count();
        $product_online_count=Product::where('status',Product::STATUS_ONLINE)->count();
        $product_offline_count=Product::where('status',Product::STATUS_OFFLINE)->count();
        $product_rejected_count=Product::where('status',Product::STATUS_REJECTED)->count();
        $product_inreview_count=Product::where('status',Product::STATUS_INREVIEW)->count();

        $orders_count=Order::count();
        $orders_requests_count=Order::where('status',Order::STATUS_REQUESTED)->count();
        $orders_inprocess_count=Order::where('status',Order::STATUS_PROCESSING)->count();
        $orders_inshipping_count=Order::where('status',Order::STATUS_SHIPPING)->count();
        $orders_shipped_count=Order::where('status',Order::STATUS_SHIPPED)->count();

        $name = 'dashboard';
        //charts
        $months= array();
        $datasets= array();
        $datasets_2= array();


        $values= array();
        $values_2= array();

        for($i=6 ;$i>=0 ; $i--){
            array_push($months,Carbon::now()->subMonths($i)->format('F'));
            array_push($values,
            User::where('role',User::CLIENT)->whereMonth('created_at','=',Carbon::now()->subMonths($i))->get()->count() );
            array_push($values_2,
            Order::where('status','!=',Order::STATUS_DRAFT)->whereMonth('created_at','=',Carbon::now()->subMonths($i))->get()->count() );

        }
        $config=["name"=>"chart","type"=>"line","id"=>"chart"];
        $config_2=["name"=>"chart_2","type"=>"line","id"=>"chart_2"];

        $chart=["label"=>'علاقة المستخدمين بالوقت',"data"=>$values,
        'borderColor'=> '#7b41c5','pointBackgroundColor'=>'#7b41c5','labelBackgroundColor'=>'#7b41c5','backgroundColor'=>'rgba(0, 0, 0, 0)','pointBorderWidth'=>'4'];  //you can also add options here like border colr or type

        $chart_2=["label"=>'علاقة الطلبات بالوقت',"data"=>$values_2,
        'borderColor'=> '#7b41c5','pointBackgroundColor'=>'#7b41c5','labelBackgroundColor'=>'#7b41c5','backgroundColor'=>'rgba(0, 0, 0, 0)','pointBorderWidth'=>'4'];  //you can also add options here like border colr or type

        array_push($datasets,$chart);
        array_push($datasets_2,$chart_2);
        //end of charts

        return view('dashboard', compact('admins_count', 'clients_count',
        'reviews_count', 'categories_count','sub_categories_count','products_count',
        'product_online_count','product_offline_count','product_rejected_count','product_inreview_count','orders_count','orders_requests_count','orders_inprocess_count',
        'orders_inshipping_count','orders_shipped_count',
        'name','config_2','datasets_2','months','config','datasets'));
    }
}
