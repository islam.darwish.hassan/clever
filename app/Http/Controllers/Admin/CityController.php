<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddNewCityRequest;
use App\Http\Requests\Admin\UpdateCityRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->authorize('view-super', Auth::user());
        $cities=City::paginate(10);
        $data=$cities;
        $name='cities';
        return view('cities.index',compact('name','data','cities'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('view-super', Auth::user());
        $name = 'cities';
        return view('cities.create', compact('name'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddNewCityRequest $request)
    {
        //
        $this->authorize('view-super', Auth::user());

        $city = new City();
        $city->en_name =$request->en_name;
        $city->ar_name =$request->ar_name;
        $city->delivery_fees =$request->fees;
        $city->save();
        return redirect()->route('cities.index')->with('message', 'City created successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        //
        $this->authorize('view-super', Auth::user());

        $name = 'cities';

        return view('cities.edit', compact('city', 'name'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCityRequest $request, City $city)
    {
        //
        if($request->has('en_name')) $city->en_name = $request->en_name;
        if($request->has('ar_name')) $city->ar_name = $request->ar_name;
        if($request->has('fees'))    $city->delivery_fees = $request->fees;
        $city->save();
        return redirect()->route('cities.index')->with('message', 'City updated successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
        $this->authorize('view-super', Auth::user());

        $city->delete();

        return redirect()->back()->with('message', 'City deleted successfully');

    }
}
