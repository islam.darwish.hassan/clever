<?php

namespace App\Http\Controllers\Traits;

use App\Http\Misc\Helpers\Base64Handler;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

trait UserTrait
{


    public function saveUserData($request)
    {
        $this->email            = $request->email;
        $this->password         = Hash::make($request->password);
        $this->role             = User::CLIENT;
        $this->save();

    }


}
