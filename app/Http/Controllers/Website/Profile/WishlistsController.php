<?php

namespace App\Http\Controllers\Website\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Website\AddProductWishlistRequest;
use App\Http\Requests\Website\CreateWishlistRequest;
use App\Models\Client;
use App\Models\Wishlist;
use App\Models\WishlistProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $client=Client::where('user_id',Auth::user()->id)->first();
        $wishlists=Wishlist::where('user_id',Auth::user()->id)->paginate(10);
        return view('website.wishlists.index',compact('wishlists'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('website.wishlists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateWishlistRequest $request)
    {
        //

        $wishlist=new Wishlist();
        $wishlist->name=$request->name;
        $wishlist->user_id=Auth::user()->id;
        $wishlist->save();

        return redirect()->route('web.mywishlists.index')->with(['message'=>'Wishlist Added Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add(AddProductWishlistRequest $request)
    {
        //

        $productWishlist=WishlistProduct::where('product_id',$request->product)->where('wishlist_id',$request->wishlist)->firstOrNew(
            ['product_id'=>$request->product],
            ['wishlist_id'=>$request->wishlist]
        );
        $productWishlist->save();
        return redirect()->back()->with('modal_message', "Item added successfully!");;

    }
    public function update($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id)
    {
        //
        $product=WishlistProduct::where('id',$id)->first();
        $product->delete();
        return redirect()->route('web.mywishlists.index')->with('message', 'Product Removed Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Wishlist $wishlist)
    {
        //
        $wishlist->delete();
        return redirect()->route('web.mywishlists.index')->with('message', 'Wishlist Deleted Successfully');

    }
}
