<?php

namespace App\Http\Controllers\Website\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Website\AddNewAddressRequest;
use App\Http\Requests\Website\UpdateAddressRequest;
use App\Models\Address;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $client=Client::where('user_id',Auth::user()->id)->first();
         $addresses=Address::where('user_id',Auth::user()->id)->paginate(10);
         return view('website.addresses.index',compact('addresses'));
            //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('website.addresses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddNewAddressRequest $request)
    {
        //
        // dd($request->all());
        $address=new Address();
        $address->user_id   =Auth::user()->id;
        // $address->first_name   =$request->first_name;
        // $address->last_name   =$request->last_name;
        $address->city_id   =$request->city_id;
        if (isset($request->floor)){
            $address->floor             = $request->floor;
        }
        if (isset($request->building)){
            $address->building             = $request->building;
        }
        if (isset($request->landmark)){
            $address->landmark             = $request->landmark;
        }
        if (isset($request->landline)){
            $address->landline             = $request->landline;
        }
        if (isset($request->apartment)){
            $address->apartment             = $request->apartment;
        }
        if (isset($request->address)){
            $address->address             = $request->address;
        }
        if (isset($request->street)){
            $address->street             = $request->street;
        }
        if (isset($request->shipping_note)){
            $address->shipping_note             = $request->shipping_note;
        }
        $address->save();

        return redirect()->route('web.myaddresses.index')->with('message', 'Address Added Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        //
        return view('website.addresses.edit' ,compact('address'));
       }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAddressRequest $request, Address $address)
    {
        //
        if (isset($request->address)){
        $address->address   =$request->address;
        }
        if (isset($request->city_id)){

        $address->city_id   =$request->city_id;
        }
        if (isset($request->floor)){
            $address->floor             = $request->floor;
        }
        if (isset($request->building)){
            $address->building             = $request->building;
        }
        if (isset($request->landmark)){
            $address->landmark             = $request->landmark;
        }
        if (isset($request->landline)){
            $address->landline             = $request->landline;
        }
        if (isset($request->apartment)){
            $address->apartment             = $request->apartment;
        }


        if (isset($request->street)){
            $address->street             = $request->street;
        }
        if (isset($request->apartment)){
            $address->apartment             = $request->apartment;
        }
        if (isset($request->street)){
            $address->street             = $request->street;
        }

        if (isset($request->shipping_note)){
            $address->shipping_note             = $request->shipping_note;
        }
        $address->save();


        return redirect()->route('web.myaddresses.index')->with('message', 'Address Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Address $address)
    {
        //
        $address->delete();
        return redirect()->route('web.myaddresses.index')->with('message', 'Address Deleted Successfully');

    }
}
