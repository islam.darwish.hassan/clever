<?php

namespace App\Http\Controllers\Website\Cart;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::user()!=null){$addresses=Address::where('user_id',Auth::user()->id)->get();
        }else{
            $addresses=[];
        }
        $similar_products=Product::with('offer')->inRandomOrder()->limit(10)->get();
        return view('website.cart.index',compact('addresses','similar_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Product $product)
    {
        //
        if($product->stock <1){
            return redirect()->back()->with('modal_error', "Item Out Of Stock!");;
        }
        $rowId = $product->id;
        if(isset($product->offer)){
            $offer_condition = new \Darryldecode\Cart\CartCondition(array(
                'name' => 'SALE '.$product->offer->offer,
                'type' => 'sale',
                'value' => '+'.$product->offer->offer-$product->price,
            ));
            \Cart::add(array(
                'id' => $rowId,
                'name' => $product->name,
                'price' => $product->price,
                'quantity' => 1,
                'attributes' => array(),
                'associatedModel' => $product,
                'conditions'=>$offer_condition
            ));

        }else{
            \Cart::add(array(
                'id' => $rowId,
                'name' => $product->name,
                'price' => $product->price,
                'quantity' => 1,
                'attributes' => array(),
                'associatedModel' => $product,
            ));
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function empty($id)
    {
        //
        \Cart::remove();
    }
    public function remove($id)
    {
        //
        \Cart::remove($id);
        return redirect()->back()->with(['sucess_message'=>'item removed successfully']);
    }
}
