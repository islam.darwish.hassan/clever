<?php

namespace App\Http\Controllers\Website\Orders;

use App\Http\Controllers\Controller;
use App\Http\Requests\Website\CheckoutRequest;
use App\Models\Address;
use App\Models\Client;
use App\Models\Order;
use App\Models\Product;
use App\Models\Shipment;
use App\Models\ShipmentProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $client=Client::where('user_id',Auth::user()->id)->first();
        $orders=Order::where('client_id',$client->id)->with('shipments')->where('status','!=',Order::STATUS_DRAFT)->orderBy('created_at','desc')->paginate(1);
        return view('website.orders.index',compact('orders'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function place_order(Request $request,Order $order)
    {
        //

        $order->status          =Order::STATUS_REQUESTED;
        foreach($order->shipments as $shipment)
        {
            $shipment->status= Shipment::STATUS_REQUESTED;
            $shipment->save();
        }
        $order->save();

        \Cart::clear();

        return  redirect()->route('web.myorders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_checkout(CheckoutRequest $request)
    {
        //
        $client=Client::where('user_id',Auth::user()->id)->first();
        if(\Cart::getTotalQuantity()<0){
            return redirect('index');
        }
        $detailed_address=Address::where('id',$request->address)->first();

        $order =new Order();
        $order->hash_code   =  str_random(12).rand(1,10000);
        $order->client_id   =   $client->id;
        $order->total_price=\Cart::getTotal();
        $order->delivery_option= 1;
        $order->status      =  Order::STATUS_DRAFT;
        $order->address_id  =  $request->address;
        $order->address_log =  'City: '.$detailed_address->city->en_name.' / Address: '.$detailed_address->address ." / Street: ". $detailed_address->street ." / Building No: " . $detailed_address->building . " / Floor: ".$detailed_address->floor ." / Landline : ".$detailed_address->landline." / Landmark : ".$detailed_address->landmark ." / Apartment : " .$detailed_address->apartment ." / Shipping Notes : " .$detailed_address->shipping_note ." .";

        //get address fees
        $address=Address::where('id',$request->address)->first();
        $order->delivery_fees=$address->city->delivery_fees;
        //shelha b3den
        $order->save();

        foreach(\Cart::getContent() as $item){
            $shipment =Shipment::where('order_id',$order->id)->firstOrNew(
                ["hash_code"=> str_random(12).rand(1,10000),
                "order_id"=>$order->id,
                "status"=>Shipment::STATUS_DRAFT
                ]
            );
            $shipment->save();
            $product =new ShipmentProduct();
            $product->product_id = $item->associatedModel->id;
            $product->shipment_id = $shipment->id;
            $product->product_price =$item->getPriceSumWithConditions();
            $product->product_name=$item->associatedModel->name;
            $product->product_log=$item->associatedModel->description;
            $product->qty=$item->quantity ;
            if($product->original_product->stock <$item->quantity){
                \Cart::remove($item->id);
                return redirect()->route('home')->with("modal_error","Cart has item that out of stock ! ");
            }
             $original= Product::where('id',$product->original_product->id)->first();
            $original->stock=$original->stock-$item->quantity;
            $original->save();
            if($original->stock<=0){
                $original->status=Product::STATUS_OFFLINE;
                $original->save();

            }
            $product->save();
            //cal price of shipments
            $shipment->total_price=$shipment->total_price+$item->getPriceSumWithConditions();
            $shipment->save();

        }
        $hash = $this->generateKashierOrderHash($order);
         $hash;
        return view('website.orders.checkout',compact('order','hash'));
       }
    //Copy and paste this code in your Backend
    function generateKashierOrderHash($order)
    {
        $mid = "MID-6942-489"; //your merchant id
        $amount = $order->total_price; //eg: 100
        $currency = "EGP"; //eg: "EGP"
        $orderId = $order->id; //eg: 99, your system order ID
        $secret = "285739a0-da04-4378-b2cc-cf2261ad1fc0";

        $path = "/?payment=" . $mid . "." . $orderId . "." . $amount . "." . $currency;
        $hash = hash_hmac('sha256', $path, $secret, false);
        return $hash;
    }
    //The Result Hash for /?payment=mid-0-1.99.20.EGP with secret 11111 should result 606a8a1307d64caf4e2e9bb724738f115a8972c27eccb2a8acd9194c357e4bec

// https://clever-eg.net/home?paymentStatus=SUCCESS&cardDataToken=&maskedCard=511111******1118&merchantOrderId=4&orderId=437ebba8-04fa-4e8a-afe0-8347fcc26a25&cardBrand=Mastercard&orderReference=TEST-ORD-193294519&transactionId=TX-694248910&amount=100.00&currency=EGP&signature=9f74f4046d8c9fbb39c4c8b9907ca3b192f91293559985bf65843f0c40367265&mode=test

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
