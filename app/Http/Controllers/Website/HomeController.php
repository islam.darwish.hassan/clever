<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //

    public function index()
    {
        # home page code
        $best_products = Product::orderBy('ordered', 'desc')->inRandomOrder()->limit(4)->get();
        return view('website.index', compact('best_products'));
    }
}
