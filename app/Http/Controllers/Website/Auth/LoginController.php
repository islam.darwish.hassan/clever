<?php

namespace App\Http\Controllers\Website\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\Website\CreateNewClient;
use App\Http\Requests\Website\LoginRequest as WebsiteLoginRequest;
use App\Models\Client;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    //
    public function showLoginForm()
    {
        # code...
        return view('website.auth.login');
    }
    public function showRegisterForm()
    {
        # code...
        return view('website.auth.signup');
    }
    public function signup(CreateNewClient $request)
    {
        $user = new User();
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->role=User::CLIENT;
        $user->save();

        $client = new Client();
        $client->name=$request->name;
        $client->user_id=$user->id;
        $client->save();

        Auth::login($user);
        return redirect()->route('home');

    }
    public function login(WebsiteLoginRequest $request)
    {
        $user= User::where('email',strtolower($request->email))->first();
    
        if(!$user) return back()->with(['message'=>'Wrong Cred!']);
        else{
            if(!Hash::check($request->password, $user->password))
            return back()->with(['message'=>'Wrong Cred!']);
        }

        Auth::login($user);
        
        if($user->role ==User::ADMIN)
            return redirect()->route('dashboard.index');
        else if($user->role == User::CLIENT)
            return redirect()->route('home');

        
        return back();
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');

    }

}
