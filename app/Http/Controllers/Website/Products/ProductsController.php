<?php

namespace App\Http\Controllers\Website\Products;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Product;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data =new Product();
        if($request->query('collection_id')) $data=Collection::where('id',$request->query('collection_id'))->firstOrFail()->products();
        $collections=Collection::get();

        if($request->query('sub_category')){
            $data=$data->where('sub_category_id', $request->query('sub_category'));
        }
        if($request->query('sub_category_name')){
            $categories= Category::where('name','like', '%'.$request->query('sub_category_name').'%')->pluck('id');
            $subs=SubCategory::whereIn('category_id',$categories)->pluck('id');
            $data=$data->whereIn('sub_category_id',$subs);
        }

        if($request->query('search')) $data=$data->where('name','like', '%'.$request->query('search').'%');
        if($request->query('filter')=="new_in") $data=$data->orderBy('created_at','asc');
        if(!$data) $data=Product::orderBy('ordered','desc');
         $data =$data->with('offer')->inRandomOrder()->paginate(20);
        return view('website.products.index',compact('data','collections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request ,Product $product)
    {
        //
        $products = $product->group()->first()->products();
        $varients= $products->get(['products.id','image','color','product_id','size','fabric'])->groupBy('color');
        $current_products=$products->where('color',$product->color);
        $sizes= $current_products->get(['id','size']);
        $product_images= $product->images()->get();
        $similar_products=Product::with('offer')->where('sub_category_id',$product->sub_category->id)->inRandomOrder()->limit(5)->get();
        return view('website.products.show',compact('product','varients','product_images','sizes','similar_products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
