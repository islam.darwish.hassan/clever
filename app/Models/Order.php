<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Order extends Model
{
    //
    use Sortable;
    use HasFactory;

    const STATUS_REQUESTED=1;
    const STATUS_PROCESSING=2;
    const STATUS_SHIPPING=3;
    const STATUS_SHIPPED=4;
    const STATUS_DRAFT=5;
    public $sortable = ['id','hash_code','status','created_at'];
    public $sortableAs = ['shipments_count'];

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id');
    }
    public function address()
    {
        return $this->belongsTo('App\Models\Address', 'address_id');
    }
    public function shipments()
    {
        return $this->hasMany('App\Models\Shipment', 'order_id');
    }



}
