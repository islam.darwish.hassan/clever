<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'product_code', 'image','sub_category_id'];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'group_products', 'group_id', 'product_id');
    }
}
