<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model
{
    //
    use Sortable;
    use HasFactory;


    const STATUS_ONLINE = 1;
    const STATUS_INREVIEW = 2;
    const STATUS_REJECTED = 3;
    const STATUS_OFFLINE = 4;
    const STATUS_DRAFT = 5;
    protected $fillable = ['sub_category_id', 'name', 'price', 'stock', 'color', 'size', 'fabric', 'image', 'description', 'branch', 'product_code'];
    public $sortable = ['id', 'name', 'sub_category_id', 'status', 'rate', 'ordered', 'sub_category_id', 'size', 'color', 'fabric', 'stock', 'price', 'created_at'];
    public $sortableAs = ['reviews_count'];

    public function reviews()
    {
        return $this->hasMany('App\Models\ProductReview', 'product_id');
    }
    public function specs()
    {
        return $this->hasMany('App\Models\ProductSpec', 'product_id');
    }
    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('files/products/images/' . $value);
        } elseif($this->cover)
        {
            return $this->cover->small_image;

        }
        else{
            return asset('files/products/images/notfound.png');
        }
    }
    public function sub_category()
    {
        return $this->belongsTo('App\Models\SubCategory', 'sub_category_id');
    }
    public function offer()
    {
        return $this->hasOne('App\Models\ProductOffer', 'product_id');
    }
    public function group()
    {
        return $this->belongsToMany('App\Models\Group', 'group_products', 'product_id', 'group_id');
    }
    public function collections()
    {
        return $this->belongsToMany('App\Models\Collection');
    }
    public function cover(){
        return $this->hasOne('App\Models\ProductImage', 'product_id');

    }
    public function images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id');
    }
}
