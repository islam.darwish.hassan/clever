<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Malhal\Geographical\Geographical;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Store extends Model
{
    //
    use Geographical;
    use Sortable;
    use HasFactory;

    //
    const STATUS_VERIFIED = 1;
    const STATUS_NOT_VERIFIED = 2;
    const STATUS_SUSPENDED = 3;
    public $sortable = ['id','rate' ,'name','user_id','status','pricing_class'];
    public $sortableAs = ['products_count'];

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'store_id');
    }
    public function addresses()
    {
        return $this->hasMany('App\Models\StoreAddress', 'store_id');
    }
    public function getAvatarAttribute($value)
    {
        return asset('files/stores/images/'.$value);
    }

}
