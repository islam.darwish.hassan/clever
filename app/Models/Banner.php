<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    public function getContentAttribute($value)
    {
        return asset('files/general/images/'.$value);
    }

}
