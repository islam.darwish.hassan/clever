<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    //
    public $sortable = ['name','created_at' ];
    public $sortableAs = ['products_count'];

    public function products()
    {
        return $this->hasMany('App\Models\WishlistProduct','wishlist_id');
    }
    public function getImageAttribute($value)
    {
        return asset('files/wishlists/images/'.$value);
    }

}
