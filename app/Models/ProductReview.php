<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    //

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id');
    }


    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }


}
