<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class SubCategory extends Model
{
    //
    use Sortable;     use HasFactory;

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    public function products()
    {
        return $this->hasMany('App\Models\Product', 'sub_category_id');
    }
    protected $fillable = ['image','name','category_id'];
    public $sortable = ['id' ,'name'];
    public $sortableAs = ['products_count'];


    // public function getImageAttribute($value)
    // {
    //     return asset('files/categories/images/'.$value);
    // }

}
