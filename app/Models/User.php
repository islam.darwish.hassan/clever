<?php

namespace App\Models;

use App\Http\Controllers\Traits\UserTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    //
    use Notifiable, HasApiTokens;
    use UserTrait;
    use Sortable;
    use HasFactory;

    const ADMIN = 1;
    const CLIENT = 2;
    const STORE = 3;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['email', 'password','role'];
    protected $hidden = ['password', 'remember_token'];
    protected $casts = ['email_verified_at' => 'datetime'];
    public $sortable = ['id','email','role'];

    public function client()
    {
        return $this->hasOne('App\Models\Client', 'user_id');
    }


    public function store()
    {
        return $this->hasOne('App\Models\Store', 'user_id');
    }
    public function wishlists()
    {
        return $this->hasMany('App\Models\Wishlist', 'user_id');
    }
    public function addresses()
    {
        return $this->hasMany('App\Models\Address', 'user_id');
    }

}
