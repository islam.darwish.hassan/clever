<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
    public function getSmallImageAttribute($value)
    {
        return asset('files/products/mini/'.$value);
    }
    public function getImageAttribute($value)
    {
        return asset('files/products/images/'.$value);
    }


}
