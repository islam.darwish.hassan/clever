<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Category extends Model
{
    //
    use Sortable;    use HasFactory;

    public $sortable = ['id',
    'name','type','department_id'
    ];
    protected $fillable = ['image','name','department_id'];

    public $sortableAs = ['subcategories_count'];
    public function subcategories()
    {
        return $this->hasMany('App\Models\SubCategory', 'category_id');
    }
    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'department_id');
    }

    // public function getImageAttribute($value)
    // {
    //     return asset('files/categories/images/'.$value);
    // }


}
