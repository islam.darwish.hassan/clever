<?php

namespace App\Models;

use App\Http\Controllers\Traits\ClientTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Client extends Model
{
    //
    use ClientTrait;
    use Sortable;
    use HasFactory;

    const STATUS_VERIFIED = 1;
    const STATUS_NOT_VERIFIED = 2;
    const STATUS_SUSPENDED = 3;

    public $sortable = ['id','age','name' ];
    public $sortableAs = ['reviews_count'];
protected $fillable=['name','user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function nation()
    {
        return $this->belongsTo('App\Models\Nation', 'nation_id');
    }
    public function reviews()
    {
        return $this->hasMany('App\Models\ProductReview', 'client_id');
    }
    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'client_id');
    }

    // public function getAvatarAttribute($value)
    // {
    //     return asset('files/users/images/'.$value);
    // }

}
