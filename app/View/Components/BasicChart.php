<?php

namespace App\View\Components;

use Illuminate\View\Component;

class BasicChart extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $config;
    public $labels;
    public $datasets;

    public function __construct($config,$labels,$datasets)
    {
        //
        $this->config = $config;
        $this->labels = $labels;
        $this->datasets = $datasets;
        }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.basic-chart');
    }
}
