<?php

namespace App\View\Components;

use Illuminate\View\Component;

class RadarChart extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public  $radarconfig;
    public  $radarlabels;
    public  $radarvalues;

    public function __construct($radarconfig, $radarlabels, $radarvalues)
    {
        //
        $this->radarconfig = $radarconfig;
        $this->radarlabels = $radarlabels;
        $this->radarvalues = $radarvalues;

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.radar-chart');
    }
}
